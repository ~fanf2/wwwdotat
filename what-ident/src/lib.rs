extern crate proc_macro;
use proc_macro::*;

fn sccs_rcs(keyword: &str, content: &str) -> String {
    if content.is_empty() {
        String::new()
    } else {
        format!("@(#) ${keyword}: {content} $\n")
    }
}

fn env(name: &'static str) -> String {
    std::env::var(name).unwrap_or_default()
}

fn cargo_authors() -> String {
    let add = |all, one| all + sccs_rcs("Author", one).as_ref();
    let pointy = env("CARGO_PKG_AUTHORS").split(':').fold(String::new(), add);
    pointy.replace('<', "(").replace('>', ")") // `what` stops printing at '>'
}

fn cargo_urls() -> String {
    sccs_rcs("Home", &env("CARGO_PKG_HOMEPAGE"))
        + &sccs_rcs("Repo", &env("CARGO_PKG_REPOSITORY"))
}

type Error = String;

fn compile_error(message: &str) -> TokenStream {
    [
        TokenTree::Ident(Ident::new("compile_error", Span::call_site())),
        TokenTree::Punct(Punct::new('!', Spacing::Alone)),
        TokenTree::Group(Group::new(
            Delimiter::Parenthesis,
            TokenTree::Literal(Literal::string(message)).into(),
        )),
    ]
    .into_iter()
    .collect()
}

macro_rules! gitted {
    ( $cmd:expr, $fmt:literal $(, $args:expr)* ) => {
        Err(format!(concat!("git {} failed: ", $fmt), $cmd $(, $args)* ))
    };
}

fn git(args: &[&'static str]) -> Result<String, Error> {
    use std::process::*;
    match Command::new("git").args(args).output() {
        Err(err) => gitted!(args[0], "{}", err),
        Ok(Output { status, stdout, stderr }) => {
            if !stderr.is_empty() {
                match String::from_utf8(stderr) {
                    Err(err) => gitted!(args[0], "malformed error: {}", err),
                    Ok(message) => gitted!(args[0], "{}", message),
                }
            } else if !status.success() {
                match status.code() {
                    Some(err) => gitted!(args[0], "exit status {}", err),
                    None => gitted!(args[0], "stopped unexpectedly"),
                }
            } else {
                match String::from_utf8(stdout) {
                    Err(err) => gitted!(args[0], "malformed output: {}", err),
                    Ok(out) => Ok(out),
                }
            }
        }
    }
}

fn git_data() -> Result<(String, String, String), Error> {
    let show = git(&["show", "--no-patch", "--format=%H %ai"])?;
    let Some((hash, date)) = show.split_once(' ') else {
        return gitted!("show", "could not parse {}", show);
    };
    let mut describe = git(&["describe", "--long", "--dirty=.X"])?;
    let dirty = describe.trim().ends_with(".X");
    // remove dirty marker and commit hash
    let truncate = describe
        .trim()
        .trim_end_matches(".X")
        .trim_end_matches(|c: char| c.is_ascii_hexdigit())
        .trim_end_matches("-g")
        .len();
    describe.truncate(truncate);
    // change dash to dot before commit count
    if let Some(dash) = describe.rfind('-') {
        describe.replace_range(dash..=dash, ".");
    };
    // when there are no changes the version is the bare tag
    if dirty {
        describe += ".X";
    } else {
        describe.truncate(describe.trim_end_matches(".0").len());
    }
    Ok((
        sccs_rcs("Version", &describe),
        sccs_rcs("Date", date.trim()),
        sccs_rcs("Hash", hash.trim()),
    ))
}

fn try_what_ident(args: TokenStream) -> Result<TokenStream, Error> {
    if !args.is_empty() {
        return Err("this macro takes 0 arguments".to_string());
    }
    let (tag, date, hash) = git_data()?;
    let urls = cargo_urls();
    let authors = cargo_authors();
    let concatenated = tag + &authors + &date + &urls + &hash;
    Ok(TokenTree::Literal(Literal::string(&concatenated)).into())
}

#[proc_macro]
pub fn what_ident(args: TokenStream) -> TokenStream {
    match try_what_ident(args) {
        Ok(result) => result,
        Err(message) => compile_error(&message),
    }
}
