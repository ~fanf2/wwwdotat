let walk = {};
const grid = 64;
const maxiter = 32;
const infinity = 65536;
const alpha = 0.666;

function vadd(a, b) {
    return { x: a.x + b.x, y: a.y + b.y };
}

function vsub(a, b) {
    return { x: a.x - b.x, y: a.y - b.y };
}

function vscale(s, v) {
    return { x: s * v.x, y: s * v.y };
}

function mandelbrot(point) {
    const a = point.x;
    const b = point.y;
    let y = 0.0;
    let x = 0.0;
    let y2 = 0.0;
    let x2 = 0.0;
    for (let i = 0; i < maxiter; i++) {
	y = 2.0 * x * y + b;
	x = x2 - y2 + a;
	y2 = y * y;
	x2 = x * x;
	if (x2 + y2 > infinity) {
	    return false;
	}
    }
    return true;
}

function walk_start(step) {
    // to trace the boundary of the mandelbrot set, we walk a triangle
    // around keeping the rule that one point must be inside the set, one
    // must be outside, and the third point is probed for membership.
    walk = {
	outside: { x: 0, y: 0 },
	inside: { x: 0, y: 0 },
	place: { x: 0, y: 0 },
    };
    // search for the cusp, at roughly +0.25 +0i -- our level set boundary
    // will be slightly offset, so we need to search if the step is small.
    while(mandelbrot(walk.place)) {
	walk.place.x += step;
    }
    // set up a baseline that crosses the boundary at the cusp
    walk.outside.x = walk.place.x;
    walk.inside.x = walk.place.x - step;
    // the first probe point off the real axis sets the shape of
    // the triangular grid, in this case an equilateral grid
    walk.place.x = (walk.inside.x + walk.outside.x) / 2;
    walk.place.y = (3 ** 0.5) * step / 2;
}

function walk_step(swap) {
    // rename the points of the current triangle by swapping the
    // current place with the previous inside or outside point,
    // according to the result of the iteration. this gives us the
    // new baseline that crosses the boundary, and an old place
    // that's the third point of the current triangle.
    [ walk.place, walk[swap] ] = [ walk[swap], walk.place ];
    // reflect the old place across the baseline to find a new
    // place that's the third point of a new triangle
    walk.place = vsub(vadd(walk.inside, walk.outside), walk.place);
}

function walk_next() {
    // find a new place that's outside the mandelbrot set
    while(mandelbrot(walk.place)) {
	walk_step("inside");
    }
    walk_step("outside");
}

function draw() {
    const width = canvas.width = window.innerWidth;
    const height = canvas.height = window.innerHeight;
    const size = width < height ? width : height;
    const scale = width < height ? size/2.75 : size/2.0;

    let c = canvas.getContext("2d")
    c.clearRect(0, 0, width, height);
    c.save();
    c.translate(width*3/4, height*1/2);

    function make_path(points) {
	let first = true;
	c.beginPath();
	for (const p of points) {
	    if (first) {
		first = false;
		c.moveTo(p.x * scale, p.y * scale);
	    } else {
		c.lineTo(p.x * scale, p.y * scale);
	    }
	}
    }

    walk_start(1/grid);
    let outside = [];
    outside.push({...walk.outside});
    for(let i = 0; i < 1000; i++) {
	let a = walk.outside.y < 0;
	walk_next();
	let b = walk.outside.y < 0;
	if(a > b)
	    break;
	outside.push({...walk.outside});
    }
    if(walk.outside.y > -2/grid)
	outside.push({...walk.outside});

    c.fillStyle = "#000";
    c.strokeStyle = "#fff";
    make_path(outside)
    c.fill();
    c.stroke();

    c.restore();
}
