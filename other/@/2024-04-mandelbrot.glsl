#version 100

precision highp float;
varying highp vec4 position;

uniform mat4 transform;
uniform vec4 marker;

const int maxiter = 2000;
const float infinity = 65536.0;

void colour(int i, float r2) {
	float c = log2(float(i) - log2(log2(r2)));
	float r = (sin(c + radians(120.0)) + 1.0) / 2.0;
	float g = (sin(c + radians(240.0)) + 1.0) / 2.0;
	float b = (sin(c + radians(360.0)) + 1.0) / 2.0;
	gl_FragColor = vec4(r, g, b, 1.0);
}

void main() {
	vec4 coord = transform * position;
	float b = coord.y;
	float a = coord.x;

	float mb = marker.y - b;
	float ma = marker.x - a;
	float md = mb * mb + ma * ma;
	if (marker.p < md && md < marker.q) {
		gl_FragColor = vec4(0.1, 0.1, 0.1, 1.0);
		return;
	}

	float y = 0.0;
	float x = 0.0;
	float y2 = 0.0;
	float x2 = 0.0;
	for(int i = 0; i < maxiter; i++) {
		y = 2.0 * x * y + b;
		x = x2 - y2 + a;
		y2 = y * y;
		x2 = x * x;
		float r2 = x2 + y2;
		if(r2 > infinity) {
			colour(i, r2);
			return;
		}
	}
	gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
}
