#version 100

// this vertex shader provides a simple canvas on which a fragment
// shader can draw. it expects the input vertex coordinates to be a
// simple square, from -1.0 to +1.0 on both axes; these vertices do not
// need to be transformed to make a canvas that covers the viewport.
// the varying position is interpolated between -1.0 to +1.0 to tell
// the fragment shader where it is on the canvas relative to the input
// vertex coordinates, whereas gl_FragCoord is measured in pixels.

attribute vec4 vertex;
varying highp vec4 position;

void main() {
	gl_Position = vertex;
	position = vertex;
}
