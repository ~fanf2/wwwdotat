import * as THREE from 'three';

const abs = Math.abs;
const cos = Math.cos;
const sgn = Math.sign;
const sin = Math.sin;
const tau = Math.PI * 2;

// not part of three.js, must copy from an example
class ParametricGeometry extends THREE.BufferGeometry {

    constructor(
	func = (u, v, target) => target.set(u, v, cos(u) * sin(v)),
	slices = 8,
	stacks = 8
    ) {
	super();

	this.type = 'ParametricGeometry';

	this.parameters = {
	    func: func,
	    slices: slices,
	    stacks: stacks
	};

	const indices = [];
	const vertices = [];
	const normals = [];
	const uvs = [];

	const EPS = 0.00001;

	let p0 = new THREE.Vector3();
	let p1 = new THREE.Vector3();
	let pu = new THREE.Vector3();
	let pv = new THREE.Vector3();
	let normal = new THREE.Vector3();

	for (let i = 0; i <= stacks; i++) {
	    const v = i / stacks;
	    for (let j = 0; j <= slices; j++) {
		const u = j / slices;

		func(u, v, p0);
		vertices.push(p0.x, p0.y, p0.z);

		// approximate tangent vectors via finite differences
		if (u - EPS >= 0) {
		    func(u - EPS, v, p1);
		    pu.subVectors(p0, p1);
		} else {
		    func(u + EPS, v, p1);
		    pu.subVectors(p1, p0);
		}
		if (v - EPS >= 0) {
		    func(u, v - EPS, p1);
		    pv.subVectors(p0, p1);
		} else {
		    func(u, v + EPS, p1);
		    pv.subVectors(p1, p0);
		}

		// cross product of tangent vectors returns surface normal
		normal.crossVectors(pu, pv).normalize();
		normals.push(normal.x, normal.y, normal.z);

		uvs.push(u, v);
	    }
	}

	const slize = slices + 1;
	for (let i = 0; i < stacks; i++) {
	    const i0 = (i + 0) * slize;
	    const i1 = (i + 1) * slize;
	    for (let j = 0; j < slices; j++) {
		const a = i0 + j+0;
		const b = i0 + j+1;
		const c = i1 + j+1;
		const d = i1 + j+0;
		indices.push( a, b, d );
		indices.push( b, c, d );
	    }
	}

	let vertex_buf = new THREE.Float32BufferAttribute(vertices, 3);
	let normal_buf = new THREE.Float32BufferAttribute(normals, 3);
	let uv_buf = new THREE.Float32BufferAttribute(uvs, 2);

	this.setIndex(indices);
	this.setAttribute('position', vertex_buf);
	this.setAttribute('normal', normal_buf);
	this.setAttribute('uv',uv_buf);
    }

    copy(source) {
	super.copy(source);
	this.parameters = Object.assign({}, source.parameters);
	return this;
    }
}

function superpoint2(a, e) {
    let [c, s] = [cos(a * tau), sin(a * tau)];
    return [ sgn(c) * abs(c)**e,
             sgn(s) * abs(s)**e ];
}

function superpoint3(u, v, xye, ze) {
    let [x,y] = superpoint2(u, xye);
    let [r,z] = superpoint2(v, ze);
    return [x*r, y*r, z]
}

function superellipsoid(w, d, h, xye, ze) {
    return (u, v, target) => {
	let [x,y,z] = superpoint3(u, v, xye, ze);
	target.set(w * x, d * y, h * z);
    }
}

function draw() {
    const canvas = document.querySelector('#canvas');
    const renderer = new THREE.WebGLRenderer(
	{ antialias: true, canvas });

    const fov = 75;
    const aspect = canvas.width/canvas.height;
    const near = 0.1;
    const far = 1000;
    const camera = new THREE.PerspectiveCamera(
	fov, aspect, near, far);
    camera.position.z = 10;

    const scene = new THREE.Scene();

    const colour = 0x8844cc;
    const intensity = 4;
    const light = new THREE.DirectionalLight(colour, intensity);
    light.position.set(5, 5, 20);
    scene.add(light);

    function render(time) {
	let xye = 0.5 + 0.45 * sin(time * 0.0004);
	let ze = 0.5 + 0.45 * sin(time * 0.0003);
	let fun = superellipsoid(3, 2, 1, xye, ze);

	let blob = new THREE.Mesh(
	    new ParametricGeometry(fun, 64, 64),
	    new THREE.MeshPhongMaterial(),
	);
	blob.rotation.x = time * 0.0002;
	blob.rotation.y = time * 0.0001;

	scene.add(blob)
	renderer.render(scene, camera);
	scene.remove(blob);

	requestAnimationFrame(render);
    }
    requestAnimationFrame(render);
}

function reposition(ev) {
    let x = (ev.pageX - scale) / scale;
    let y = (ev.pageY - scale) / scale;
}

window.main = function () {
    console.log(canvas.width = window.innerWidth,
		canvas.height = window.innerHeight);
    draw();
}

window.moused = function (ev) {
    if (ev.buttons != 0) {
	reposition(ev);
    }
}

window.touched = function (ev) {
    if (ev.touches.length > 0) {
	reposition(ev.touches.item(0));
    }
}
