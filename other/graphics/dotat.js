function dotat(canvas) {
    let c = canvas.getContext("2d");

    let w = 30; // white
    let b = 20; // black
    let r = w*3 + b*3;

    let r1 = w*2 + b*0.5;
    let r2 = w*3 + b*1.5;
    let ay = w*2;
    let dy = w*2 + b*0.5;
    let c1x = r1;
    let c1y = dy;
    let ix = (r1 + r2) / 2 - b*0.5;
    let iy = dy;
    let c2x = r2;
    let c2y = dy;

    let pixels = canvas.width < canvas.height
               ? canvas.width : canvas.height;
    let scale = pixels / (r*2);
    c.translate(canvas.width/2, canvas.height/2);
    c.scale(scale, scale);

    let bg = new Path2D();
    bg.ellipse(0, 0, r, r, 0, 0, 2 * Math.PI);
    c.fillStyle = "white";
    c.fill(bg);

    let dot = new Path2D();
    dot.ellipse(0, 0, w, w, 0, 0, 2 * Math.PI);
    c.fillStyle = "black";
    c.fill(dot);

    let at = new Path2D()
    at.ellipse(0,0, r1,r1, 0, 0, 2 * Math.PI);
    at.moveTo(r1, -ay);
    at.quadraticCurveTo(c1x,c1y, ix,iy);
    at.quadraticCurveTo(c2x,c2y, r2,0);
    at.arc(0,0, r2, 0, -1.68 * Math.PI, true);

    c.lineCap = "round";
    c.lineWidth = b;
    c.stroke(at);
}

function go() {
    let body = document.getElementById("body");
    for (size of [ 64, 128, 256, 512 ]) {
	let div = document.createElement("div");
	div.innerHTML = `
            <h1>${size}</h1>
            <canvas id="canvas${size}"
                 width="${size}" height="${size}">
            </canvas>
            <img id="img${size}">`;
	body.append(div);
	let canvas = document.getElementById(`canvas${size}`);
	let img = document.getElementById(`img${size}`);
	dotat(canvas)
	img.setAttribute("src", canvas.toDataURL());
    }
}
