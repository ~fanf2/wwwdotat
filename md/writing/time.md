Date, Time, and Leap Seconds
============================

I have an idle fascination with matters of time and date, so I have
written a number of articles and bits of code — although it's just a
hobby with no ambitions to be practical or useful to anyone but me
(and mostly not even then).

<toc>


gregorian calendar
------------------

[**Counting the days**][toJDN] - tiny routines for converting
Gregorian dates into linear counts, like Julian day numbers or Unix
`time_t`.

[**The date of the count**][fromJDN] - a small routine for converting
linear day counts into Gregorian dates.

Most implementations I have seen of these functions use some kind
of iterative search to find the right result. My versions use
arithmetic like [Zeller's congruence][zeller] to go directly to
the result.

[toJDN]: https://dotat.at/@/2008-09-10-counting-the-days.html
[fromJDN]: https://dotat.at/@/2008-09-15-the-date-of-the-count.html
[zeller]: https://en.wikipedia.org/wiki/Zeller%27s_congruence


time zones
----------

[**iCalendar is wrong**][ical] - standard calendaring data models
have a confusing way to handle time zones, which could (in theory)
be improved.

[ical]: https://dotat.at/@/2009-12-09-icalendar-is-wrong.html

[**Timezone display by MUAs**][MUAtz] can be confusing (and
improved) in a similar way.

[MUAtz]: https://dotat.at/@/2010-08-24-timezone-display-by-muas.html

A related annoyance is that the Unicode CLDR has standard "friendly"
[localized descriptions for time zone names][CLDRtz], which are very
confusing and often wrong for more than half the year.

[CLDRtz]: https://unicode-org.github.io/cldr-staging/charts/latest/verify/zones/en.html

I wrote a little `datez` utility in Rust to help me deal with these problems.

  * [`datez` gitweb](https://dotat.at/cgi/git/datez.git)
  * [`datez` github](https://github.com/fanf2/datez)


chronophage clock
-----------------

There's a spectacular clock in the centre of Cambridge. I wrote
a few blog articles about it when it was new in 2008/9:

  * [a description of the clock][one]
  * [addenda and corrigenda][two]
  * [notes on a talk by John Taylor who invented the clock][three]

[one]: https://dotat.at/@/2008-09-22-the-corpus-christi-chronophage-clock.html
[two]: https://dotat.at/@/2008-09-30-more-on-the-corpus-christi-chronophage-clock.html
[three]: https://dotat.at/@/2009-03-15-john-taylor-talks-about-the-corpus-clock.html


where does time come from?
--------------------------

At [the RIPE86 meeting][RIPE86] I gave [a short presentation on where
time comes from][whence-ripe]. I have [a blog version of the
talk][whence-blog] and [some notes on the talk][whence-whence]. [Geoff
Huston wrote about the talk][whence-potaroo].

[RIPE86]: https://ripe86.ripe.net/
[whence-ripe]: https://ripe86.ripe.net/archives/video/1126/
[whence-blog]: https://dotat.at/@/2023-05-26-whence-time.html
[whence-whence]: https://dotat.at/@/2023-05-28-whence-whence-time.html
[whence-potaroo]: https://www.potaroo.net/ispcol/2023-06/ripe86-time.html


compact leap seconds list
-------------------------

<a href="https://twitter.com/fanf/status/1391090529874726914">
<img src="https://dotat.at/graphics/leapclock.png" width="50%" align="right" >
</a>

I publish cryptographically signed lists of leap seconds in the
DNS at `leapsecond.dotat.at` in various formats:

  * [`AAAA`][] - the date and time of the last second in months that end
    with a leap second, plus the last second of the known validity
    period if that is not a leap second, in binary-coded decimal.

  * [`A`][] - [Poul-Henning Kamp's encoding of leap seconds][phk]

  * [`TXT`][] - the intervals between leap seconds in months, separated by
    a `+` or `-` for positive or negative leap seconds, and
    terminated by a `?`

  * [`TYPE65432`][] - compressed binary encoding of the `TXT` record

  * [`URI`][] - a link to the specification

  * [`HINFO`][] - brief descriptions of the other record types

[phk]: http://phk.freebsd.dk/time/20151122/

[`AAAA`]: https://dotat.at/cgi/dig-leapsecond/AAAA
[`A`]: https://dotat.at/cgi/dig-leapsecond/A
[`TXT`]: https://dotat.at/cgi/dig-leapsecond/TXT
[`TYPE65432`]: https://dotat.at/cgi/dig-leapsecond/TYPE65432
[`URI`]: https://dotat.at/cgi/dig-leapsecond/URI
[`HINFO`]: https://dotat.at/cgi/dig-leapsecond/HINFO

I have a specification and reference implementation in Rust for my
compact leap second list formats (`TXT` and `TYPE65432` above):

  * [`leapsecs` gitweb](https://dotat.at/cgi/git/leapsecs.git)
  * [`leapsecs` github](https://github.com/fanf2/leapsecs)

You can find the [code for drawing the leap second clock logo
above][clock] in the `leapsecs` documentation.

[clock]: https://dotat.at/cgi/git/leapsecs.git/blob_plain/HEAD:/doc/logo.html


leap second hiatus
------------------

The last leap second was at the end of 2016, and the current gap is
going to be longer than the previous record of 7 years (1999-2005).

Unlike last time, the earth has sped up so much that since the end
of 2020 it now takes less than 24 * 60 * 60 seconds to rotate each
day (it always used to be longer). This brings us the prospect of a
**negative leap second** !!!!!

I wrote [a blog article in November 2020][hiatus] when this first came
to my attention.

[hiatus]: https://dotat.at/@/2020-11-13-leap-second-hiatus.html

This hiatus might become permanent if leap seconds are abolished. I
have written some notes on the [plans for leap seconds following the
2022 CGPM](https://dotat.at/@/2022-12-04-leap-seconds.html).
(This is, roughly speaking, round two of the effort to abolish leap
seconds, which has now been going on for over 20 years.)


past leap seconds
-----------------

There was an awkward gap from the start of TAI to the start of
UTC, when the difference between atomic time and civil time was
represented with varying "rubber seconds". I suggested
[**proleptic UTC**](https://dotat.at/@/2006-12-22-proleptic-utc.html)
as a way to make it easier to bridge the gap with fixed atomic seconds.


next leap second
----------------

I have a Rust program that makes a guesstimate of when the next leap
second will occur, and whether it will be positive or negative. It uses
the long-term projection formula from IERS Bulletin A.

  * [`bulletin-a` gitweb](https://dotat.at/cgi/git/bulletin-a.git)
  * [`bulletin-a` github](https://github.com/fanf2/bulletin-a)


twitter threads
---------------

My off-the-cuff wittering on this topic occured on Twitter and now
[the Mended Drum](https://mendeddrum.org/@fanf), where they are hard
to find and difficult to navigate, so I have collected them here:

  * [2022-11-19](https://twitter.com/fanf/status/1593975698611490816):
    the governance around leap seconds is amazingly complicated
  * [2022-11-18](https://mendeddrum.org/@fanf/109366130819733846):
    the CGPM approved the resolution to (probably) stop leap seconds
  * [2022-08-11](https://twitter.com/fanf/status/1557844904235130881):
    discussion of draft CGPM resolution on UTC
  * [2022-07-19](https://twitter.com/fanf/status/1549365264696893440):
    Bulletin D 142, and LoD = 24h - 320μs
  * [2022-07-05](https://twitter.com/fanf/status/1544281892484272128):
    Bulletin C 64, and LoD = 24h - 310μs
  * [2022-04-29](https://twitter.com/fanf/status/1519979880938708993):
    new LoD record, drops below 24h - 270μs
  * [2022-01-05](https://twitter.com/fanf/status/1478752466896400392):
    Bulletin C 63 cockup, LoD = 24h - 200μs apparently slowly growing?
  * [2021-06-29](https://twitter.com/fanf/status/1409853720561659906):
    a Bulletin D explainer, and what it means when DUT1 increases
  * [2021-04-27](https://twitter.com/fanf/status/1386840996449820672):
    reading Bulletin A and making predictive guesses
  * [2021-04-05](https://twitter.com/fanf/status/1378851706600054785):
    earlier notes on Bulletin A
  * [2021-01-26](https://twitter.com/fanf/status/1354211063911297028):
    a potted history of how leap seconds came to be
  * [2020-11-13](https://twitter.com/fanf/status/1327353787548393472):
    original version of my leap second hiatus blog post
  * [2020-07-07](https://twitter.com/fanf/status/1280621594616967171):
    Bulletin C 60, and the long gap between leap seconds


IERS bulletins
--------------

I pay attention to three of the [IERS bulletins][] (C and D by
subscribing to email notifications):

  * Bulletin A - weekly forecast of earth rotation parameters, which I
    use for guessing about the next Bulletins C and D

  * Bulletin C - leap second announcements, published every 6 months:
    in early January, regarding the possible leap second at the end of
    June; and in early July, regarding the possible leap second at the
    end of December.

  * Bulletin D - announcements of DUT1 in increments of 0.1 second
    (as required by ITU TF.460), published irregularly as necessary.

[IERS bulletins]: https://www.iers.org/IERS/EN/Publications/Bulletins/bulletins.html


other resources
---------------

  * [ITU TF.460](https://www.itu.int/rec/R-REC-TF.460/en) is a standard
    for radio time signals, and the specification for leap seconds

  * [leapsecond.com](http://leapsecond.com/) - Tom Van Baak's time nut pages

  * [Steve Allen's pages on leap seconds][sla-main] - I often refer to
    his [extrapolation of the difference between TAI and UT1][sla-diff]

[sla-main]: https://www.ucolick.org/~sla/leapsecs/
[sla-diff]: https://www.ucolick.org/~sla/leapsecs/dutc.html
