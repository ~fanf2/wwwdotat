Keep Mordor Tidy
================

Some time before [Peter Jackson](https://www.imdb.com/Name?Jackson,+Peter)'s
_Fellowship of the Ring_ was released,
[Simon Tatham](https://www.chiark.greenend.org.uk/~sgtatham/)
and
[Vicky Clarke](https://www.chiark.greenend.org.uk/~vclarke/)
came up with
[an evil visual pun](https://www.tartarus.org/~simon/things/kmt.png)
which absolutely cried out to be printed onto some t-shirts,
and so it duly was.

It has since become the envy of fannish people everywhere, to the
extent that I foolishly let myself be pursuaded that it would be a
good idea to organise another print run. However, as was the case with
Simon's earlier printing this turned out to require much more time and
effort than I expected, so neither of us plan to organize another one.

However, if you really want one,
you have Simon's and Vicky's permission to
[contact Talking T's](https://www.t-shirts.co.uk/)
(the shop that printed the shirts)
and organize it yourself.
It would also be nice if you dropped us a line
so we know who's doing this.
