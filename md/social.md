---
template: self
title: Tony Finch elsewhere on the web
...

{{> page-head }}

<main>


<h2>sociable sites</h2>

<p>{{svg "c-circle-fill"}}&ensp;
<a href="https://cohost.org/fanf">https://cohost.org/fanf</a></p>

<p><big>&#xAA5C;</big>&ensp;
<a href="https://fanf.dreamwidth.org/profile">https://fanf.dreamwidth.org/profile</a></p>

<p><big>&#x1F17B;</big>&ensp;
<a href="https://lobste.rs/u/fanf">https://lobste.rs/u/fanf</a></p>

<p>{{svg "mastodon"}}&ensp;
<a href="https://mendeddrum.org/@fanf">https://mendeddrum.org/@fanf</a></p>

<p><big>&#x1F148;</big>&ensp;
<a href="https://news.ycombinator.com/user?id=fanf2">https://news.ycombinator.com/user?id=fanf2</a></p>

<p>{{svg "twitter"}}&ensp;
<a href="https://twitter.com/fanf">https://twitter.com/fanf</a></p>


<h2>coding activity</h2>

<p>{{svg "git"}}&ensp;
<a href="https://gitlab.isc.org/fanf">https://gitlab.isc.org/fanf</a></p>

<p>{{svg "github"}}&ensp;
<a href="https://github.com/fanf2">https://github.com/fanf2</a></p>


<h2>old work sites</h2>

<p>{{svg "hdd-rack-fill"}}&ensp;
<a href="https://fanf2.user.srcf.net/">https://fanf2.user.srcf.net/</a></p>

<p>{{svg "globe"}}&ensp;
<a href="https://www.dns.cam.ac.uk">https://www.dns.cam.ac.uk</a></p>


</main>

{{> foot }}
