Tony Finch – pictures
=====================

  * [Me in 2021](https://dotat.at/graphics/selfie-shelfie.jpg)

  * [Me at our wedding in 2005](https://dotat.at/graphics/wedding-big.jpg)

  * [Me in 2002](https://www.personal.rdg.ac.uk/~sssogadr/afp/dwcon02/m042_keep-mordor-tidy.jpg)
    (classic pose: cheesy grin and beer in hand;
    [the t-shirt is explained on another page](https://dotat.at/writing/kmt.html))

  * [Me in 1996](https://www.lspace.org/ftp/images/afp-meetings/rogues-gallery/tony-finch.jpg)

  * [My home office](https://dotat.at/graphics/desk-home.jpg)

  * [My desk in Balfour 3 in 2010](https://dotat.at/graphics/desk-b3.jpg)
