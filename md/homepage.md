---
template: self
title: Tony Finch
...

{{> page-head }}

<main>

{{#md}}

A bit about me
--------------

I am a Unix system developer from Cambridge.
I work on [BIND9](https://www.isc.org/bind/) at
[the Internet Systems Consortium (`isc.org`)](https://www.isc.org/).

For many years I worked at the [University of
Cambridge](https://www.cam.ac.uk) at first helping to run the central
email services, and in later years running [the University's
DNS](https://www.dns.cam.ac.uk).

I am married to [Rachel][] and we have two children.

[Rachel]: https://www.chiark.greenend.org.uk/~rmc28/

I have some small pages about
[my name](name.html),
[my email address](email.html),
[my pgp keys](pgp.html),
[me on social media](social.html),
and
[what I look like](pictures.html).


Recent blog posts
-----------------

{{/md}}

<ul>
{{#each global.blog_teaser}}
  <li><a href="{{ url }}">{{ date }} &ndash; {{{ title }}}</a></li>
{{/each}}
</ul>

{{#md}}

_[more blog posts ...](https://dotat.at/@/)_


Some of my software
-------------------

[**unifdef**](https://dotat.at/prog/unifdef/) selectively
removes C preprocessor conditionals. My version of this program
now shipped by all the BSDs (including Mac OS X) and is used by
the Linux kernel build system.

[**nsdiff**](https://dotat.at/prog/nsdiff/) creates an `nsupdate`
script from DNS zone file differences. I used it at Cambridge as
part of the DNS update process.

[**nsnotifyd**](https://dotat.at/prog/nsnotifyd/) handles DNS NOTIFY
messages by running a command. It's a general-purpose tool for
responding quickly to DNS changes. It comes with several example
applications.

[**qp tries**](https://dotat.at/prog/qp/) are a data structure for
associating values with string keys. They are smaller and faster than
crit-bit tries. My main project at ISC is adapting a qp-trie for use
in BIND.

[**regpg**](https://dotat.at/prog/regpg/) safely stores server
secrets using gpg, so you can keep them in version control.

[**Date, Time, and Leap Seconds**](https://dotat.at/writing/time.html) -
a collection of my articles and programs on these topics.

[**Uplift from SCCS to git**](https://dotat.at/@/2014-11-27-uplift-from-sccs-to-git.html).
If you are ever in the unfortunate situation of having to work with an
old SCCS repository, I have suite of scripts to convert SCCS to git
which you might find helpful.
([I have done this twice now.](https://dotat.at/@/2016-07-19-uplift-from-sccs-to-git-again.html))
Contact me for more information.

I have [a directory containing various ancient unloved bits of code](https://dotat.at/prog/).

[I was one of the winners](https://www.ioccc.org/years.html#1998_fanf)
of the 1998/9
[International Obfuscated C Code Competition](https://www.ioccc.org).

[Simon Tatham](https://www.chiark.greenend.org.uk/~sgtatham/) has a
description of his really nice [mergesort algorithm for linked
lists](https://www.chiark.greenend.org.uk/~sgtatham/algorithms/listsort.html)
which sorts in place with O(1) overhead and only uses forward links.
In October 2005 I wrote [another description of the
algorithm](https://dotat.at/@/2005-10-20-mergesort-for-linked-lists.html)
which I think is easier to understand.

--------

Thanks to [Ian Jackson](https://www.chiark.greenend.org.uk/~ijackson/)
for hosting these pages on [chiark](https://www.chiark.greenend.org.uk).

{{/md}}

</main>

{{> foot }}
