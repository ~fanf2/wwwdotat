---
dw:
  anum: 231
  eventtime: "2016-01-02T20:48:00Z"
  itemid: 440
  logtime: "2016-01-02T20:48:23Z"
  props:
    commentalter: 1491292414
    import_source: livejournal.com/fanf/139925
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/112871.html"
format: html
lj:
  anum: 149
  can_comment: 1
  ditemid: 139925
  event_timestamp: 1451767680
  eventtime: "2016-01-02T20:48:00Z"
  itemid: 546
  logtime: "2016-01-02T20:48:23Z"
  props:
    give_features: 1
    personifi_tags: "nterms:no"
  reply_count: 3
  url: "https://fanf.livejournal.com/139925.html"
title: Hammerspoon hooks for better screen lock security on Mac OS X
...

<p>A couple of months ago we had a brief discussion on an ops list at
work about security of personal ssh private keys.</p>

<p>I've been using <code>ssh-agent</code> since I was working at Demon in the late
1990s. I prefer to lock my screen rather than logging out, to maintain
context from day to day - mainly my emacs buffers and window layout.
So to be secure I need to delete the keys from the <code>ssh-agent</code> when the
screen is locked.</p>

<p>Since 1999 I have been using <code>Xautolock</code> and <code>Xlock</code> with a little script
which automatically runs <code>ssh-add -D</code> to delete my keys from the
<code>ssh-agent</code> shortly after the screen is locked. This setup works well
and hasn't needed any changes since 2002.</p>

<p>But when I'm at home I'm using a Mac, and I have not had similar
automation. Having to type <code>ssh-add -D</code> manually is very tiresome and
because I am lazy I don't do it as often as I should.</p>

<p>At the beginning of December I found out about
<a href="http://www.hammerspoon.org/">Hammerspoon</a>, which is a Mac OS X
application that provides lots of hooks into the operating system
that you can script with Lua. The <a href="http://www.hammerspoon.org/go/">Hammerspoon getting started
guide</a> provides a good intro to the
kinds of things you can do with it. (Hammerspoon is a fork of an
earlier program called Mjolnir, ho ho.)</p>

<p>It wasn't until earlier this week that I had a brainwave when I
realised that I might be able to use Hammerspoon to automatically run
<code>ssh-add -D</code> at appropriate times for me. The key is the
<a href="http://www.hammerspoon.org/docs/hs.caffeinate.watcher.html"><code>hs.caffeinate.watcher</code></a>
module, with which you can get a Lua function to run when sleep or
wake events occur.</p>

<p>This is almost what I want: I configure my Macs to send the displays
to sleep on a short timer and lock them soon after, and I have a hot
corner to force them to sleep immediately. Very similar to my X11
screen lock setup if I use Hammerspoon to invoke an <code>ssh-add -D</code>
script.</p>

<p>But there are other events that should also cause ssh keys to be
deleted from the agent: switching users, switching to the login
screen, and (for wasteful people) screen-saver activation. So I had a
go at hacking Hammerspoon to add the functionality I wanted, and ended
up with <a href="https://github.com/Hammerspoon/hammerspoon/pull/710">a pull request that adds several extra event types to
<code>hs.caffeinate.watcher</code></a>.</p>

<p>At the end of this article is an example Hammerspoon script which uses
these new event types. It should work with unpatched Hammerspoon as
well, though only the sleep events will have any effect.</p>

<p>There are a few caveats.</p>

<p>I wanted to hook an event corresponding to when the screen is locked
after the screensaver starts or the screen sleeps, with the delay that
the user specified in the System Preferences. The
<code>NSDistributedNotificationCenter</code> event <code>"com.apple.screenIsLocked"</code>
sounds like it ought to be what I want, but it actually gets triggered
as soon as the screensaver starts or the screen sleeps, without any
delay. So a more polished hook script will have to re-implement that
feature in a similar way to my X11 screen lock script.</p>

<p>I wondered if locking the screen corresponds to locking the keychain
under the hood. I experimented with
<a href="https://developer.apple.com/library/mac/documentation/Security/Reference/keychainservices/index.html#//apple_ref/c/func/SecKeychainAddCallback">SecKeychainAddCallback</a>
but it was not triggered when the screen locks :-( So I think it might
make sense to invoke <code>security lock-keychain</code> as well as <code>ssh-add -D</code>.</p>

<p>And I don't yet have a sensible user interface for re-adding the keys
when the screen is unlocked. I can get Terminal.app to run <code>ssh -t
my-workstation ssh-add</code> but that is really quite ugly :-)</p>

<p>Anyway, here is a script which you can invoke from
<code>~/.hammerspoon/init.lua</code> using <code>dofile()</code>. You probably also want to
run <code>hs.logger.defaultLogLevel = 'info'</code>.</p>

<pre><code>    -- hammerspoon script
    -- run key security scripts when the screen is locked and unlocked

    -- NOTE this requires a patched Hammerspoon for the
    -- session, screen saver, and screen lock hooks.

    local pow = hs.caffeinate.watcher

    local log = hs.logger.new("ssh-lock")

    local function ok2str(ok)
        if ok then return "ok" else return "fail" end
    end

    local function on_pow(event)
        local name = "?"
        for key,val in pairs(pow) do
            if event == val then name = key end
        end
        log.f("caffeinate event %d =&gt; %s", event, name)
        if event == pow.screensDidWake
        or event == pow.sessionDidBecomeActive
        or event == pow.screensaverDidStop
        then
            log.i("awake!")
            local ok, st, n = os.execute("${HOME}/bin/unlock_keys")
            log.f("unlock_keys =&gt; %s %s %d", ok2str(ok), st, n)
            return
        end
        if event == pow.screensDidSleep
        or event == pow.systemWillSleep
        or event == pow.systemWillPowerOff
        or event == pow.sessionDidResignActive
        or event == pow.screensDidLock
        then
            log.i("sleeping...")
            local ok, st, n = os.execute("${HOME}/bin/lock_keys")
            log.f("lock_keys =&gt; %s %s %d", ok2str(ok), st, n)
            return
        end
    end

    pow.new(on_pow):start()

    log.i("started")
</code></pre>
