---
dw:
  anum: 160
  eventtime: "2009-02-21T22:54:00Z"
  itemid: 376
  logtime: "2009-02-21T23:46:32Z"
  props:
    commentalter: 1491292367
    import_source: livejournal.com/fanf/97845
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/96416.html"
format: html
lj:
  anum: 53
  can_comment: 1
  ditemid: 97845
  event_timestamp: 1235256840
  eventtime: "2009-02-21T22:54:00Z"
  itemid: 382
  logtime: "2009-02-21T23:46:32Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/97845.html"
title: Microblogging
...

<p>Since April 2002 I have been keeping a <a href="http://dotat.at/:/">link log</a>: a web page where I record links to other pages that are notable in some way, with a brief comment about each one. It's gatewayed to <a href="http://delicious.com/fanf">Delicious</a>, <a href="http://dotaturls.livejournal.com/">LiveJournal</a>, and <a href="http://twitter.com/fanf">Twitter</a>. The Twitter feed is new.</p>

<p>I've been on Twitter since March 2007, when it made a big splash at the SXSW festival. I joined to see what the fuss was about but soon stopped using it. None of my social circle were using it much, and there wasn't a sensible way of finding people in the more distant reaches of my social network. For 18 months my last tweet said "<a href="http://twitter.com/fanf/status/85418462"> Grumbling about the difficulty of finding out people's twitter usernames</a>".</p>

<p>Recently Twitter has taken off in the UK, since a fairly large contingent of journalists and celebrities have started using it and talking about it. (Reminds me in a small way of the rise of the web in 1993/4.) Coincidentally at about the same time I decided to get back into using Facebook and Twitter a bit more, to keep in better touch with my family's activities. (They're on Facebook but I linked my Twitter and Facebook statuses together for convenience.)</p>

<p>The celebrity thing helped me to get more out of Twitter in a significant way. I had thought of it as more like Facebook or LiveJournal: a way of keeping up with the activities of people I already know. But in fact it's more like the wider blogging culture where it's normal to follow someone's writing just because they are interesting. (Yes, LJ blurs into this kind of blogging too.) In fact the even the Twitter guys didn't realise this at first: until June 2007 Twitter sent me email saying "You are foo's newest friend!" but the following month it started sending me email saying "foo is now following you on Twitter!". I now feel free to follow well-known people just because they are interesting, and I found more interesting people by looking through the lists of people they follow.</p>

<p>Part of the Twitter culture is heavy use of short URL services when posting interesting links. I decided that my link log ought to be gatewayed to Twitter to join in the fun, so I embarked on a bit of hacking. Previously it had been just a static HTML page, with a command-line script that I used to insert a new link. There was another script to turn the HTML into an RSS feed for LJ, and some code for uploading the results to <a href="http://www.chiark.greenend.org.uk/">chiark</a>. I had a rudimentary click tracker so that I could get some idea of what links other people followed, but it did not shorten URLs. (That meant it was an open redirector until I found out what evil that made it vulnerable to.)</p>

<p>I replaced all the old static HTML and RSS gubbins with a CGI script which can format my link log in HTML or Atom, as well as providing short URLs for all the links. I decidided to host my own short URLs partly to continue tracking what people find interesting, and partly because I have a vanity domain that's just right for this job :-) The command line script for adding new links remained basically the same, apart from new code to create short tags and to post the link to Twitter.</p>

<p>After a couple of weeks of this new regime I decided I needed to be able to save links found when reading Twitter or LiveJournal on my iPod Touch. Its UI makes it easy to email links but not much else, so I set up a gateway from email to my link log. I send links to a specially tagged email address which is filtered to a special mailbox. A script polls this mailbox over IMAP every few minutes, looking for messages that match a number of paranoid checks. From these extracts a URL and a comment which it passes on to the old command-line script. (I still use the latter when I'm on a computer with a keyboard.)</p>

<p>The end result is pretty neat: I can easily save interesting links that I see in places where that was not previously practical. It's probably a good thing I don't have an iPhone because then I'd be able to spod <i>everywhere</i>.</p>

<p>PS. a brief hate: web sites that redirect deep links to the mobile version of their front page. Utter cWAP. They'll get no Google Juice from me. (In fact the iPhone browser is good enough that mobile sites are usually NOT an improvement.)</p>
