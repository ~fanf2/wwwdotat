---
dw:
  anum: 215
  eventtime: "2016-04-13T22:42:00Z"
  itemid: 450
  logtime: "2016-04-13T21:42:28Z"
  props:
    commentalter: 1491292415
    import_source: livejournal.com/fanf/142372
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/115415.html"
format: html
lj:
  anum: 36
  can_comment: 1
  ditemid: 142372
  event_timestamp: 1460587320
  eventtime: "2016-04-13T22:42:00Z"
  itemid: 556
  logtime: "2016-04-13T21:42:28Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 2
  url: "https://fanf.livejournal.com/142372.html"
title: Using a different X11 window manager with XQuartz
...

<h3>(In particular, <a href="https://i3wm.org">i3</a> running on a remote machine!)</h3>

<p>I have been rebuilding my workstation, and taking the opportunity to
do some major housecleaning (including moving some old home directory
stuff from CVS to git!)</p>

<p>Since 1999 I have used <a href="http://fvwm.org">fvwm</a> as my X11 window
manager. I have a weird configuration which makes it work a lot like a
tiling window manager - I have no title bars, very thin window
borders, keypresses to move windows to predefined locations. The main
annoyance is that this configuration is not at all
resolution-independent and a massive faff to redo for different screen
layouts - updating the predefined locations and the corresponding
keypresses.</p>

<p>I have heard good things about <a href="https://i3wm.org">i3</a> so I thought I
would try it. Out of the box it works a lot like my fvwm
configuration, so I decided to make the switch. It was probably about
the same faff to configure i3 the way I wanted (40 workspaces) but
future updates should be much easier!</p>

<h3>XQuartz and quartz-wm</h3>

<p>I did some of this configuration at home after work, using
<a href="http://www.xquartz.org">XQuartz</a> on my MacBook as the X11 server.
XQuartz comes with its own Mac-like window manager called <code>quartz-wm</code>.</p>

<p>You can't just switch window managers by starting another one - X only
allows one window manager at a time, and other window managers will
refuse to start if one is already running. So you have to configure
your X session if you want to use a different window manager.</p>

<h3>X session startup</h3>

<p>Traditionally, X stashes a lot of configuration stuff in
<code>/usr/X11R6/lib/X11</code>. I use <code>xdm</code> which has a subdirectory of ugly
session management scripts; there is also an <code>xinit</code> subdirectory for
simpler setups. Debian sensibly moves a lot of this gubbins into
<code>/etc/X11</code>; XQuartz puts them in <code>/opt/X11/lib/X11</code>.</p>

<p>As well as their locations, the contents of the session scripts vary
from one version of X11 to another. So if you want to configure your
session, be prepared to read some shell scripts. Debian has sensibly
unified them into a shared <code>Xsession</code> script which even has a man page!</p>

<h3>XQuartz startup</h3>

<p>XQuartz does not use a display manager; it uses <code>startx</code>, so the
relevant script is <code>/opt/X11/lib/X11/xinit/xinitrc</code>. This has a nice
<code>run-parts</code> style directory, inside which is the script we care about,
<code>/opt/X11/lib/X11/xinit/xinitrc.d/98-user.sh</code>. This in turn invokes
scripts in a per-user <code>run-parts</code> directory, <code>~/.xinitrc.d</code>.</p>

<h3>Choose your own window manager</h3>

<p>So, what you do is,</p>

<pre>
    <b>$</b> mkdir .xinitrc.d
    <b>$</b> cat >.xinitrc.d/99-wm.sh
    #!/bin/sh
    exec twm
    <b>^D</b>
    <b>$</b> chmod +x .xinitrc.d/99-wm.sh
    <b>$</b> open /Applications/Utilities/XQuartz.app
</pre>

<p>(The <code>.sh</code> and the <code>chmod</code> are necessary.)</p>

<p>This should cause an <code>xterm</code> to appear with <code>twm</code> decoration instead
of <code>quartz-wm</code> decoration.</p>

<h3>My supid trick</h3>

<p>Of course, X is a network protocol, so (like any other X application)
you don't have to run the window manager on the same machine as the X
server. My <code>99-wm.sh</code> was roughly,</p>

<pre>
    #!/bin/sh
    exec ssh -AY <i>workstation</i> i3
</pre>

<p>And with XQuartz configured to run fullscreen this was good enough to
have a serious hack at <code>.i3/config</code> :-)</p>
