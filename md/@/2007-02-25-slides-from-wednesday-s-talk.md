---
dw:
  anum: 0
  eventtime: "2007-02-25T18:36:00Z"
  itemid: 276
  logtime: "2007-02-25T18:41:44Z"
  props:
    import_source: livejournal.com/fanf/70966
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/70656.html"
format: casual
lj:
  anum: 54
  can_comment: 1
  ditemid: 70966
  event_timestamp: 1172428560
  eventtime: "2007-02-25T18:36:00Z"
  itemid: 277
  logtime: "2007-02-25T18:41:44Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/70966.html"
title: "Slides from Wednesday's talk"
...

On Wednesday afternoon I gave a talk to some of the University's "techlinks" (an IT support staff group) which was another of my annual overviews of our email systems. The slides are online at https://fanf2.user.srcf.net/hermes/doc/talks/2007-02-techlinks/
