---
format: html
lj:
  anum: 148
  can_comment: 1
  ditemid: 108436
  event_timestamp: 1282521360
  eventtime: "2010-08-22T23:56:00Z"
  itemid: 423
  logtime: "2010-08-22T22:56:04Z"
  props:
    personifi_tags: "15:4,2:2,6:2,45:1,8:41,10:1,23:4,42:2,1:37,3:8,4:5,19:9,9:37,14:2,nterms:yes"
    verticals_list: "computers_and_software,technology,life"
  reply_count: 14
  url: "https://fanf.livejournal.com/108436.html"
title: Simple shell scripting for Twitter
...

<p>I have a few scripts which manage <a href="http://dotat.at/:/">my URL log</a>, including posting a copy of the feed to my <a href="https://twitter.com/fanf">Twitter</a> and <a href="http://del.icio.us/fanf">del.icio.us</a> accounts. Until recently the scripts have just used <a href="http://www.gnu.org/software/wget/">wget</a>'s HTTP Basic Auth support to authenticate to my accounts. This has to change because <a href="http://dev.twitter.com/pages/basic_to_oauth">Twitter is switching to oauth</a>. This switch has <a href="http://blog.twitter.com/2010/06/switching-to-oauth.html">already been delayed</a> but is now <a href="http://www.countdowntooauth.com/">due to occur by the end of August</a>.</p>

<p>Most <a href="http://dev.twitter.com/pages/oauth_libraries">oauth implementations</a> are not designed for old school languages like the Unix shell, so I procrastinated because I didn't fancy dealing with the dependency hell of mainstream scripting languages. But I perked up when I noticed <a href="http://acme.com/jef/">Jef Poskanzer</a> mentioning his stand-alone oauth implementation on <a href="http://twitter.com/jef_poskanzer">his twitter feed</a>. I have liked Jef's approach to writing simple code since I worked on <a href="http://acme.com/software/thttpd/">thttpd</a> in support of <a href="http://www.demon.net/">Demon's</a> <a href="http://halplant.com:2001/server/thttpd_FAQ.html#WhoUses">homepages service</a>.</p>

<p>Posting to Twitter with basic auth didn't require anything beyond a Twitter account. You could just POST to <tt>https://twitter.com/statuses/update.json</tt> - which is the essence of the security problem that the Twitter crew want to solve.</p>

<p>To use oauth you must register an application. Go to <a href="http://dev.twitter.com/"><tt>https://dev.twitter.com/</tt></a>
-> <a href="https://dev.twitter.com/docs">Get started</a>
-> <a href="https://dev.twitter.com/apps">Your apps</a>
-> <a href="https://dev.twitter.com/apps/new">Register a new app</a>. Fill in the form. Tell it the app is a client app and give it read+write permission.</p>

<p>When you have done that you will be presented with your application's settings page. The interesting parts are the "consumer key" and the "consumer secret" which are the half of your app's oauth credentials which authenticate the application to Twitter. The other half of the credentials prove that your app may do something to a particular person's Twitter account. In order to obtain the second half oauth normally requires <a href="http://dev.twitter.com/pages/auth">a fairly complicated dance</a>. Happily, for simple cases where you want to script your own account, Twitter provides a shortcut.</p>

<p>On your application's settings page, click the "My Access Token" link. This gives you a page containing your "access token" and "access token secret" which together with your "consumer key" and "consumer secret" allow your app to post to your Twitter account.</p>

<p>Now you need some software. Fetch Jef Poskanzer's <a href="http://acme.com/software/oauth_sign/"><tt>oauth_sign</tt></a> and <a href="http://acme.com/software/http_post/"><tt>http_post</tt></a> packages. You can use <tt>oauth_sign</tt> with <tt>wget</tt> or <tt>curl</tt>, but <tt>http_post</tt> is more convenient since both it and <tt>oauth_sign</tt> encode the query parameters for you, whereas <tt>wget</tt> and <tt>curl</tt> do not. Typing <tt>make</tt> should be enough to build <tt>oauth_sign</tt>; for <tt>http_post</tt> you probably want <tt>make SSL_DEFS="-DUSE_SSL" SSL_LIBS="-lssl -lcrypto"</tt>.</p>

<p>Now you have everything you need to write a simple shell twitter client. Something like:</p>
<pre>
    #!/bin/sh

    consumer_key="COPY-FROM-APP-SETTINGS-PAGE"
    consumer_secret="COPY-FROM-APP-SETTINGS-PAGE"
    access_token="COPY-FROM-MY-ACCESS-TOKEN-PAGE"
    access_secret="COPY-FROM-MY-ACCESS-TOKEN-PAGE"
    url="https://api.twitter.com/1.1/statuses/update.json"

    http_post -h Authorization "$(oauth_sign \
	$consumer_key $consumer_secret \
	$access_token $access_secret \
	POST "$url" status="$*")" \
	     "$url" status="$*"
</pre>

<p>That should be enough to get you going.</p>

<p>For completeness (and since I worked out how to do it I'm going to inflict it on you) here's how to use Jef's tools to do the full three party oauth procedure.
<ul>
<li>First obtain a request oauth token and secret. For this transaction your oauth token and secret are empty. The HTTP request is a POST with an empty body.
<pre>
http_post -h Authorization "$(oauth_sign \
    $consumer_key $consumer_secret "" "" \
    POST https://api.twitter.com/oauth/request_token)" \
         https://api.twitter.com/oauth/request_token
</pre>
<li>The response will contain <tt>oauth_token</tt> and <tt>oauth_token_secret</tt> parameters which are your request token and secret. You must then get your victim to visit the URL <tt>https://api.twitter.com/oauth/authorize?oauth_token=$request_token</tt>. They will be asked to give your app permission to diddle with their account. (They may have to log in first.)
<li>When they have done this they will be redirected to your app's callback URL, with some extra parameters. The documentation says these will be a copy of the request <tt>oauth_token</tt> and an <tt>oauth_verifier</tt> but in my testing the verifier was missing.
<li>If your app is a client app (which has no callback URL or the URL is "oob") then the user will be presented with a PIN which is the request verifier.
<li>You can now obtain the access token and secret as follows.
<pre>
http_post -h Authorization "$(oauth_sign \
    $consumer_key $consumer_secret \
    $request_token $request_secret \
    POST https://api.twitter.com/oauth/access_token oauth_verifier="$verifier")" \
         https://api.twitter.com/oauth/access_token oauth_verifier="$verifier"
</pre>
<li>The response will contain <tt>oauth_token</tt> and <tt>oauth_token_secret</tt> parameters which are the access token and secret that you must use when your app wants to do something with your victim's account.
</ul>
<p>I hope this might be of use to someone else...</p>
