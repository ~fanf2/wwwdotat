---
dw:
  anum: 125
  eventtime: "2003-06-06T10:49:00Z"
  itemid: 26
  logtime: "2003-06-06T04:01:17Z"
  props:
    commentalter: 1491292304
    current_mood: antici
    import_source: livejournal.com/fanf/6748
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/6781.html"
format: casual
lj:
  anum: 92
  can_comment: 1
  ditemid: 6748
  event_timestamp: 1054896540
  eventtime: "2003-06-06T10:49:00Z"
  itemid: 26
  logtime: "2003-06-06T04:01:17Z"
  props:
    current_mood: antici
  reply_count: 2
  url: "https://fanf.livejournal.com/6748.html"
title: Travelling
...

This week I was supposed to be writing the documentation that's required before I can turn on virus filtering, and which should also reduce the number of support questions we get. So I have: made some improvements to MailScanner's spam details header; found and fixed a security hole in Hermes; found and fixed two bugs in sed; and not written any documentation.

This afternoon I'm flying to the Netherlands to meet up with friends and see a Dutch PhD defence. I've been looking forward to this more and more over the week -- I last went to NL in November for EuroBSDcon, and I always enjoy going there. Bounce, etc.
