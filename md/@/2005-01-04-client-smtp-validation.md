---
dw:
  anum: 97
  eventtime: "2005-01-04T23:19:00Z"
  itemid: 123
  logtime: "2005-01-04T23:24:08Z"
  props:
    commentalter: 1491292318
    import_source: livejournal.com/fanf/31810
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/31585.html"
format: casual
lj:
  anum: 66
  can_comment: 1
  ditemid: 31810
  event_timestamp: 1104880740
  eventtime: "2005-01-04T23:19:00Z"
  itemid: 124
  logtime: "2005-01-04T23:24:08Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/31810.html"
title: Client SMTP Validation
...

I've spent a lot of time recently discussing email server authorization with the <a href="http://mipassoc.org/clear/">CLEAR working group</a>.

The CLEAR WG is not an officially chartered IETF WG yet, but our general aim is to get a couple of fairly uncontroversial email security protocols pinned down. The first of these is CSV, which comprises two protocols: CSA and DNA. CSA retro-fits HELO authentication on to SMTP, and DNA specifies how to discover and use RHSBL<sup>1</sup>-like accreditation services. In the last several weeks we've mostly been working on CSA.

One of the features of SMTP is that the protocol starts off with the client saying HELO followed by its name. This is mostly redundant chatter, because SMTP servers are officially not supposed to reject messages if the HELO name is incorrect, and even if you would like to be strict about it you cannot because <a href="http://www.livejournal.com/users/fanf/30030.html">one third</a> of SMTP servers have misconfigured HELO names. (It's only <em>mostly</em> redundant chatter because SMTP clients say EHLO instead of HELO to indicate that they speak the current version of the protocol.) A side effect of this is that malware SMTP clients (spammers, viruses, etc.) lie like bastards in their HELO lines. You can detect a lot of this with some simple heuristics, but there's still a lot of slightly more sohpisticated lying which can only be detected with the co-operation of the site that is being joe-jobbed<sup>2</sup>.

CSA allows a site admin to state which of the machines are allowed to send email (and which are not) by putting SRV records in the DNS. SRV records have a few fields which CSA abuses to hold the authorization information, plus a target name. When you look up a SRV record the DNS server puts the addresses (A and AAAA records) of the target in the additional data section of the response. So an SMTP server can use a single DNS lookup to check that its client is authorized (from the SRV record) and that it isn't lying about who it is (by comparing the client's IP address with the additional data).

However there's a gaping hole in this scheme. Although you can fill your DNS zone with "not authorized" CSA records, this doesn't prevent attackers from making up a name that doesn't appear in the DNS and using it to bypass the CSA checks. This also undermines the planned accreditation and reputation systems based on the HELO name, because they'll have to deal with enormous numbers of made up names without any clear indication of whether this is the result of malice or misconfiguration. Made up HELO names may also sully the reputation of their purported parent domain.

So to plug this hole we needed a system for specifying that all authorized SMTP clients in a domain have a CSA record, and that any other name (whether present in the DNS or not) is implicitly unauthorized. This also saves DNS admins from filling their zones with CSA clutter. When an SMTP server is checking a HELO name, if the basic CSA lookup fails it can search for a default CSA record in parent domains, and thereby distinguish between implicitly unauthorized clients and clients in domains that don't know about CSA. The result is a solidly secure basis of authentication on which accreditation and reputation systems can stand.

One nice thing that CSA lets you do is implement a more lenient default-unauthorized policy than by blocking port 25 (assuming, of course, that SMTP servers check CSA). If a clueful user wants to bypass your default-unauthorized CSA policy, they just need to configure their SMTP client to use a HELO name that isn't in your domain. The policy that applies is then that of the user's domain and not yours, as is any reputation which accrues. This feature may be particularly attractive to broadband ISPs and their customers.

The process of deciding the details of this was unduly painful - too much miscommunication and arguing at cross-purposes, and it would have been two or three orders of magintude faster if we had been in the same room. However I think we've settled on a reasonably simple and efficient mechanism which minimises the number of lookups required to find a default record and maximises their cacheability. With any luck a new Internet Draft will be published soon.

It's worth noting that SPF is vulnerable to a variant of this attack, because most domains will only publish SPF records for their mail domains, along side the MX records. However any domain name with an A record is also valid as a mail domain, so an attacker can send mail "from" any existing host name belonging to their victim. SPF is not vulnerable to purely made up names, because many recipients perform a simple verification to ensure that the domain has an A or MX record in the DNS. HELO names don't have any equivalent to this, hence the need for CSA default records.

[1] RHSBLs are DNS blacklists that are keyed on domain names (in the case of DNA these are HELO names) instead of IP addresses.
RHS refers to the right-hand-side of an email address.

[2] You have been joe-jobbed if a spammer has sent out loads of email as if it were from you.
