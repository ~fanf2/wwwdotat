---
dw:
  anum: 172
  eventtime: "2018-11-14T22:08:00Z"
  itemid: 508
  logtime: "2018-11-14T22:41:39Z"
  props:
    commentalter: 1542376506
    hasscreened: 1
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/130220.html"
format: html
title: Help me choose colours
...

<p><a href="https://git.uis.cam.ac.uk/x/uis/web/light.git/blob_plain/HEAD:/index.html">Cambridge University's official web templates</a> come in a variety of colour schemes that are generally quite garish - see the links in the poll below for examples. I've nearly got my new web site ready to go (no link, because spoilers) and I have chosen two of the less popular colour schemes: one that I think is the most alarming (and I expect there would be little disagreement about that choice) and one that I think is the most queer.

<p>Do you think what I think?

<poll-20722>

<p>(This is a bit off-brand for my usual Dreamwidth posts - my fun stuff usually happens on Twitter. But Twitter's polls are too small!)
