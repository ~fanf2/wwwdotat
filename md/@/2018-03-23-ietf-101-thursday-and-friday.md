---
dw:
  anum: 161
  eventtime: "2018-03-23T15:34:00Z"
  itemid: 487
  logtime: "2018-03-23T15:45:08Z"
  props:
    commentalter: 1522095757
    interface: flat
    picture_keyword: ""
    revnum: 1
    revtime: 1521822128
  url: "https://fanf.dreamwidth.org/124833.html"
format: md
lj:
  anum: 130
  can_comment: 1
  ditemid: 151938
  event_timestamp: 1521819900
  eventtime: "2018-03-23T15:45:00Z"
  itemid: 593
  logtime: "2018-03-23T15:45:45Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/151938.html"
...

IETF 101 - Thursday and Friday
==============================

I've been quite lucky with the timetable, so
[like Wednesday](https://dotat.at/@/2018-03-22-ietf-101-wednesday.html)
I could have a relaxed morning on Thursday. Friday is a half day (so
that attendees can head home before the weekend) but I popped into
London for the last session and to say goodbye to anyone who was still
around.

Hallway track
-------------

Before lunch on Thursday I had a chat with various people including
[Tim Griffin](http://www.cl.cam.ac.uk/~tgg22/)
(who I have not previously met despite working in the building hext
door to him!) and
[Geoff Huston](http://www.potaroo.net).
Geoff thanked me for
[my suggestion](https://mailarchive.ietf.org/arch/msg/dnsop/mnTvtW97eKPHBDsbG1weQfZ1yfs)
about avoiding a potential interop gotcha in his kskroll-sentinel
draft that I covered
[on Tuesday](https://dotat.at/@/2018-03-21-ietf-101-tuesday.html),
and I thanked him for his measurement work on fragmentation.
He told me to not to forget Davey Song's clever "additional
truncated response" idea, so I posted
[a followup to yesterday's notes on fragmentation](https://mailarchive.ietf.org/arch/msg/dnsop/fRf3xqtih7VLY39ta39v9JyF02A)
to the dnsop list.

Root DNSSEC key rollover
------------------------

Over lunch there was
[a talk by David Conrad about replacing the root DNSSEC key](https://www.ietf.org/how/meetings/101/ietf-101-thursday-lunch-host-speaker-series/).
I have been paying attention to this process so there were no big
surprises. It's difficult to get good data on how DNSSEC is configured
or misconfigured, hence the kskroll-sentinel draft, and it's difficult
to get feedback from operators about their approaches to the rollover.
An awkward situation, but hopefully the rollover won't have to be
postponed again.

doh
---

After lunch was the
[DNS-over-HTTPS](https://datatracker.ietf.org/group/doh/about/)
working group meeting.

This started with some feedback from
[the hackathon](https://dotat.at/@/2018-03-18-ietf-101-hackathon-day-2.html),
and then a discussion of the current state of the draft spec. It is
close to being ready, so the authors hope to push it to last call
within a few weeks. (The DoH WG has been remarkably speedy - it helps
to have a simple protocol!)

After that, there was some discussion about what comes next. The WG
chairs plan to close the working group after the spec is published,
unless there is consensus to pusrsue some follow-up work. There was
also a presentation from
[dkg](https://www.aclu.org/bio/daniel-kahn-gillmor)
about using HTTP/2 push to send unsolicited DoH responses: in what
situations can browsers use these responses safely? are they useful
for avoiding DNS lookup latency?

I still don't know if DoH is a massive distraction from the bad idea
fairy. It feels to me like it might be one of those friction-reducing
technologies that changes the balance of trade-offs in ways that have
unexpected consequences.

ANAME
-----

In the next session I missed the
[jmap](https://datatracker.ietf.org/group/jmap/about/)
meeting and instead spent some time in the code lounge with
[Evan Hunt](https://twitter.com/nuthaven) (ISC BIND),
[Peter van Dijk](https://twitter.com/habbie) (PowerDNS),
and [Matthijs Mekking](https://twitter.com/pletterpet) (Dyn),
hammering out some details of ANAME (at least for authoritative
servers).

PowerDNS and Dyn have existing (non-standard, differing)
implementations of this functionality, so we were partly trying to
work out how a standardized version could cover existing use cases.
One thing that slightly surprised me was that PowerDNS does ALIAS
expansion during an outgoing zone transfer - I had not previously
considered that mechanism, but PowerDNS is designed around dynamic
zone contents, so I guess their zone transfer code has to quite a lot
more work than BIND.

We ended up with a few almost-orthogonal considerations: Is the server
a primary or a secondary for the zone? Is the zone signed or not? Does
the server have the private keys for the zone? Does the server
actively expand ANAME when answering queries, or passively serve
pre-expanded addresses from the zone? Does the server expand ANAME on
outgoing zone transfers, or transfer the zone verbatim?

There are a few combinations that don't make sense, and a few that end
up being equivalent, but it's quite a large and confusing space to
navigate.

I think we managed to resolve several questions (as it were) and had a
useful meeting of minds, so I'm looking forward to more progress with
this draft.

dnsop II
--------

The evening session was the second
[dnsop](https://datatracker.ietf.org/group/dnsop/about/)
meeting, which was for
[triage of new drafts](https://www.ietf.org/mail-archive/web/dnsop/current/msg22221.html).

[Shumon Huque](https://twitter.com/shuque)
has a nice operations draft explaining
[how to manage DNSSEC keys for zones served by multiple DNS providers](https://tools.ietf.org/html/draft-huque-dnsop-multi-provider-dnssec)
which
[I reviewed on the mailing list](https://www.ietf.org/mail-archive/web/dnsop/current/msg22192.html).

Ray Bellis presented
[catalog zones](https://tools.ietf.org/html/draft-muks-dnsop-dns-catalog-zones)
which I quite like, though it isn't quite the right shape for
simplifying the tricky parts of my configuration, though it does
[simplify our stealth secondary config](https://www.dns.cam.ac.uk/news/2017-09-06-catz.html)
a lot. But a lot of others in the room do not like
[abusing the DNS for server configuration](https://pbs.twimg.com/media/DY5rHo_WAAEISGh.jpg)

Matthijs Mekking presented his idea for less verbose zone transfers.
This is something we discussed in the mfld track at
[the previous London IETF](https://www.ietf.org/proceedings/89/)
and although it is quite a fun idea, Matthijs now thinks that if we
are going to revise the zone transfer protocol, it would probably be
better to move it out of band so that there's the flexibility to do
even more clever things without overloading Bert's camel.

We ran out of time before we got to Petr Špaček's
[camel-diet](https://tools.ietf.org/html/draft-spacek-edns-camel-diet)
draft. This is related to the agreement between the big 4 open source
DNS servers that
[next year they will stop working around broken EDNS implementations](https://blog.powerdns.com/2018/03/22/removing-edns-workarounds/)

lamps
-----

For the final session I went to the working group on
[limited additional mechanisms for PKIX and SMIME](https://datatracker.ietf.org/group/lamps/about/).
[Paul Hoffman](https://twitter.com/paulehoffman)
clued me in that there would be some discussion of CAA (X.509
certificate authority authorization) DNS records. There's a revision
of the spec in the works, which includes more operational advice that
I should review wrt
[the problems we had back in September preventing some certificates from being issued](http://news.uis.cam.ac.uk/articles/2017/09/20/unable-to-issue-tls-certs-for-names-in-private-cam-ac-uk).

mfld
----

On Thursday evening I went to a Thai restaurant with some
[friendly Dutch DNS folks](https://twitter.com/reseauxsansfil/status/976962954779389952).
I foolishly chose the
["most adventurous"](https://twitter.com/fanf/status/976956564308480000)
menu item, which was nice and stinky although a bit too spicy. I think
I still smell of fish sauce...

The end of my IETF was lunch with John Levine chatting about ISOC and
our shared tribulations of the small-scale DNS operator.

And now I am on my way home at the end of a long busy week, hopefully
in time to pick up my new specs from the optician.
