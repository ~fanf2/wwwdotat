---
format: html
lj:
  anum: 135
  can_comment: 1
  ditemid: 123783
  event_timestamp: 1348522380
  eventtime: "2012-09-24T21:33:00Z"
  itemid: 483
  logtime: "2012-09-24T20:33:46Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 7
  url: "https://fanf.livejournal.com/123783.html"
title: New TLDs and RFC 1535
...

<p>There has been a lengthy discussion on the DNS operations list about <a href="https://lists.dns-oarc.net/pipermail/dns-operations/2012-September/008936.html">new GDLDs with A, AAAA, or MX records at the zone apex</a>, which allows dotless fully-qualified hostnames and mail domains, such as <a href="http://dk/">dk</a> and <a href="mailto:pope@va">va</a>. This brings back the old spectre of <a href="http://tools.ietf.org/html/rfc1535">RFC 1535</a> and unpredictable name resolution.</p>

<p>One particular example that I care about is local unqualified mail domains. We use Exim's <a href="http://exim.org/exim-html-current/doc/html/spec_html/ch17.html#SECID118">widen_domains</a> feature, which (in our configuration) tries appending cam.ac.uk and ac.uk to the mail domain in turn if DNS resolution fails. This means that local mail domains such as @careers, @farm, @foundation, @law, @wiki and so forth are newly vulnerable to breakage by changes elsewhere in the DNS. This is not really a new problem (there were amusing problems with @uk.ac.cam.cl in the past) but the scale will become much larger.</p>

<p>So I'm wondering what to do. I could change widen_domains to work a bit more like qualify_single (except without depending on the behaviour of the resolver library), that is to try local names in preference to global ones when there are no dots in the mail domain. Or I could make a break with tradition and drop support for unqualified names - but I would have to wean a few dozen people (including myself) off the habit.</p>

<p>Bah.</p>
