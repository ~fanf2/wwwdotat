---
dw:
  anum: 237
  eventtime: "2009-02-21T00:28:00Z"
  itemid: 375
  logtime: "2009-02-21T00:29:11Z"
  props:
    commentalter: 1491292377
    import_source: livejournal.com/fanf/97572
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/96237.html"
format: html
lj:
  anum: 36
  can_comment: 1
  ditemid: 97572
  event_timestamp: 1235176080
  eventtime: "2009-02-21T00:28:00Z"
  itemid: 381
  logtime: "2009-02-21T00:29:11Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/97572.html"
title: The joy of lpeg
...

<p>I've recently started playing with <tt>lpeg</tt>, a parsing library for <a href="http://www.lua.org">Lua</a>. It is based on "<a href="http://pdos.csail.mit.edu/~baford/packrat/">Parsing Expression Grammars</a>", which were recently popularized by the prolific <a href="http://www.brynosaurus.com/">Bryan Ford</a>. PEGs have some nice properties: they're suitable for unified parsers that handle both the low-level lexical syntax as well as higher-level hierarchial syntax; they have much simpler operational semantics than traditional extended regexes or context-free grammars; and as well as familiar regex-like and CFG-like operators they have nice features for controlling lookahead and backtracking. PEGs were originally developed alongside <a href="http://www.brynosaurus.com/pub/lang/packrat-icfp02.pdf">a cute algorithm for linear-time parsing</a> which unfortunately also requires space linear in the input size with a fairly large multiplier. Lpeg instead uses a simple parsing machine, implemented somewhat like a bytecode interpreter. Its performance is quite competitive: <a href="http://www.inf.puc-rio.br/~roberto/docs/peg.pdf">the long paper</a>  says it has similar performance to <tt>pcre</tt> and <tt>glibc</tt>'s POSIX regex implementation, and <a href="http://www.inf.puc-rio.br/~roberto/docs/ry08-4.pdf">the short paper</a> says it has similar performance to <tt>lex</tt> and <tt>yacc</tt>.</p>

<p>Lpeg actually consists of two modules. <a href="http://www.inf.puc-rio.br/~roberto/lpeg/lpeg.html">The core lpeg module</a> is written in C and allows you to compose parser objects using operator overloading, building them up from primitives returned from tersely named constructor functions. The resulting syntax is rather eccentric. On top of that is <a href="http://www.inf.puc-rio.br/~roberto/lpeg/re.html">the <tt>re</tt> module</a> which provides a more normal PEG syntax for parsers, which despite the name of the module are rather different from regular expressions. This module is written in Lua, using an lpeg parser to parse PEGs and construct lpeg parsers from them. The PEG syntax is extended so that you can define "captures". Captures are the really nice thing about lpeg. You can use them like captures in Perl regexes to just extract substrings of the subject, but you can often do better. Lpeg captures are more like the semantic actions that you can attach to rules in parser generators like <tt>yacc</tt>. So, where in Perl you would do the match then fiddle around with <tt>$1</tt>, <tt>$2</tt>, etc, with lpeg the match can incorporate the fiddling in a nice tidy way. (In fact, probably the closest comparison is with Perl 6 rules, but they're not yet practically usable.)</p>

<p>The program I was writing with lpeg was to process some logs. I needed to convert the timestamps from ISO 8601 format into POSIX <tt>time_t</tt> which implied converting 8 fields from strings to numbers. Rather than having to convert each capture individually, or loop over the captures, I could write a single grammar rule to match a pair of digits and convert it to a number, then refer to that rule elsewhere in the grammar. (In fact Lua will coerce strings to numbers implicitly in most - but not all - circumstances. I happened to be tripped up trying to compare a number with a string, which doesn't coerce.) In the end it's nicest to let the parser drive all the program's activity through its semantic actions.</p>

<p>In the following, [[...]] delimits a "long string" containing the PEG grammar. {...} in the grammar denotes a capture, whereas (...) is for non-capturing groups. <tt>pat -> fun</tt> passes the captures of a pattern to a function. The second argument to <tt>compile</tt> is a table where the keys are the function names referred to in the parser. The main entry point to the rest of the program is <tt>process</tt>, whose definition I have left out.</p>
<pre>
    local parser = re.compile([[
        line &lt;- ( &lt;stamp> ' ' {%a*} ' ' &lt;count> !. ) -> process
        stamp &lt;- ( &lt;date> ' ' &lt;time> ' ' &lt;zone>   ) -> tostamp
        date  &lt;- ( &lt;num>&lt;num> '-' &lt;num> '-' &lt;num> ) -> todate
        time  &lt;- (      &lt;num> ':' &lt;num> ':' &lt;num> ) -> totime
        zone  &lt;- ( {[+-]} &lt;num> &lt;num>             ) -> tozone
        count &lt;- {'@'*} -> tocount
        num   &lt;- {%d^2} -> tonumber
    ]], {
        process = process,
        tonumber = tonumber,
        tocount = function (s) return #s end,
        todate = function (c,y,m,d)
            if m > 2 then m = m + 1;  y = c*100 + y
                     else m = m + 13; y = c*100 + y - 1 end
            return int(y*1461/4) - int(y/100) + int(y/400)
                 + int(m*153/5) + d - 719591
        end,
        totime = function (H,M,S)
            return H*3600 + M*60 + S
        end,
        tozone = function (zs,zh,zm)
            local z = zh*3600 - zm*60
            if zs == "-" then return -z
                         else return  z  end
        end,
        tostamp = function (date,time,zone)
            return date*86400 + time - zone
        end
    })

    for line in io.stdin:lines() do
        if not parser:match(line) then
            io.stdout:write("skipping "..line.."\n")
        end
    end
</pre>
