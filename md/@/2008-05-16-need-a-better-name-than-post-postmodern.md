---
dw:
  anum: 135
  eventtime: "2008-05-16T00:11:00Z"
  itemid: 342
  logtime: "2008-05-16T00:04:34Z"
  props:
    commentalter: 1491292354
    import_source: livejournal.com/fanf/88526
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/87687.html"
format: casual
lj:
  anum: 206
  can_comment: 1
  ditemid: 88526
  event_timestamp: 1210896660
  eventtime: "2008-05-16T00:11:00Z"
  itemid: 345
  logtime: "2008-05-16T00:04:34Z"
  props: {}
  reply_count: 10
  url: "https://fanf.livejournal.com/88526.html"
title: Need a better name than post-postmodern
...

A few days ago, <a href="http://badscience.net">Ben Goldacre</a> linked to <a href="http://www.boston.com/bostonglobe/ideas/articles/2008/05/11/measure_for_measure?mode=PF">an article by</a> and <a href="http://www.seedmagazine.com/news/2006/04/shakespeare_meets_the_selfish.php?page=all&amp;p=y">an interview with</a> a guy called Jonathan Gottschall who likes to apply the scientific method to literary theory. I was gobsmacked! I thought that no-one who takes literary theory seriously has any intellectual engagement with the real world.

I had a brief discussion about <a href="http://librarything.com">Librarything</a> with <a href="https://addedentry.livejournal.com/">👤addedentry</a> at his and <a href="https://j4.livejournal.com/">👤j4</a>'s birthday party on Saturday, in which he talked about its new approaches to cataloguing. Its use of tagging, as opposed to ontological classification, is its most obvious feature, but its concept of a "work" as an umbrella for the multiple editions of a book is also useful.

In the pub this evening we decided that both of these things are post-postmodern. Traditional cataloguing is very modernist in its approach: top-down, paternalistic, relying on the academic expert to benevolently provide for everyone's needs. Literary criticism is the ultimate expression of postmodernism: observing that experts are not always right and that new theories come from outside of the consensus, they deny the existence of objective truth and assert that all opinion is equally valid. Scientists and engineers instinctively reject postmodernism, but often fail to do so without relying on discredited modernist thinking.

How can we get beyond postmodernism? I think it has to be the acknowledgment that a fuzzy consensus is a valid approximation to the truth, and that we have experimental and statistical tools which can refine that approximation. But the crucial thing is to realize that these tools don't just work for physics or chemistry or biology or medicine, but they also work for cataloguing books, or establishing that the author does have a degree of control over the reader's thoughts, or showing that beauty does have a degree of commonality across cultures, or that we can automatically translate between natural languages.

<small><i>The preceding ill-informed rant was brought to you by <a href="http://www.hopback.co.uk/real-ale-online/index.php?cPath=3">Summer Lightning</a>.</i></small>
