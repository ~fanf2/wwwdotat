---
dw:
  anum: 79
  eventtime: "2006-01-06T22:44:00Z"
  itemid: 174
  logtime: "2006-01-06T23:09:39Z"
  props:
    commentalter: 1491292327
    import_source: livejournal.com/fanf/44881
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/44623.html"
format: casual
lj:
  anum: 81
  can_comment: 1
  ditemid: 44881
  event_timestamp: 1136587440
  eventtime: "2006-01-06T22:44:00Z"
  itemid: 175
  logtime: "2006-01-06T23:09:39Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/44881.html"
title: Irritations
...

Yesterday I upgraded my workstation with gigabit ethernet and a better graphics card, which is actually capable of driving my LCD monitor at 1600x1200 over DVI with a decent refresh rate. I had a cunning idea over the hols that I could keep my old graphics card to make a dual-head setup, but of course my PC has only one AGP slot. Bah.

However when wandering around the office I noticed a PCI graphics card lurking on a shelf. Aha! In it went, and I booted up. <tt>Xorg -configure</tt> spotted the two cards and auto-generated a dual-head configuration. Nice! However when I ran X with the new configuration file, the first screen started up OK but the second did not. It complained of being unable to find the card's video BIOS, and that the card had 0KB of video RAM, and that none of the standard video modes would fit in the available memory.

Much faffing ensued, including trying to run X on only the second head (no luck), trying to force the video RAM size (crash!), swapping between three monitors (no improvement) and eventually trying the PC without the AGP card (no image at all). Bah!

At this point I was feeling rather vexed by the computer, so I left it to look around the office some more to see if I could find another PCI graphics card. No luck, but I did eventually find a blue+white G3 Powermac. These machines have no AGP, so I hauled it out from under the desk to see if I could filch its graphics card. A missing blanking plate taunted me from the location where the video card had been.

I realized that the card I had been trying to make work had originally come from the blue+white Mac, and the reason it didn't work was that it had Open Firmware, not PC firmware. D'oh!

<hr width="25%" align="left">
This evening has also been irritating. I finally got the motivation to run a Jabber server on my workstation for my vanity domain dotat.at and for other testing purposes.

This helped me to find a couple of lurking bugs in my custom authentication module. The API has a number of functions, including one for checking the existence of a user and one for checking the validity of a password. Most of them are required to return 1 for an error and 0 for success, <em>except</em> for the user existence checker which has the opposite logic, and which (having noticed this anomaly) I therefore had to fix...

Once I got a client talking to the server OK, I tried to get it to exchange presence subscriptions between my dot@dotat.at and fanf@jabber.org accounts. No dice: the server-to-server dialback authentication timed out. Huh. I eventually remembered about our wonderful new port blocking setup which is intended to improve the security of MICROS~1 crapware. Of course, all firewalls do is break things, so I have asked our network admins to exempt my workstation from this pointless hindrance.
