---
dw:
  anum: 211
  eventtime: "2005-07-25T17:21:00Z"
  itemid: 147
  logtime: "2005-07-25T17:58:32Z"
  props:
    commentalter: 1491292391
    import_source: livejournal.com/fanf/38006
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/37843.html"
format: casual
lj:
  anum: 118
  can_comment: 1
  ditemid: 38006
  event_timestamp: 1122312060
  eventtime: "2005-07-25T17:21:00Z"
  itemid: 148
  logtime: "2005-07-25T17:58:32Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 8
  url: "https://fanf.livejournal.com/38006.html"
title: Clustered hints databases for Exim?
...

Shortly before my <a href="http://www.livejournal.com/users/rmc28/126863.html">wedding</a> there was a discussion on <a href="http://www.exim.org/mailman/listinfo/exim-users">the Exim-users mailing list</a> about Exim's handling of its hints databases, which cache information about the retry status of remote hosts and messages in the queue, callout results, <a href="http://www.livejournal.com/users/fanf/36373.html">ratelimit state</a>, etc. At the moment Exim just uses a standard Unix DB library for this, e.g. GDBM, with whole-file locking to protect against concurrent access from multiple Exim processes.

There are two disadvantages with this: firstly, the performance isn't great because Exim tends to serialize on the locks, and the DB lock/open/fsync/close/unlock cycle takes a while, limiting the throughput of the whole system; secondly, if you have a cluster of mail servers the information isn't shared between them, so each machine has to populate its own database which implies poor information (e.g. an incomplete view of clients' sending rates) and duplicated effort (e.g. repeated callouts, wasted retries).

The first problem is a bit silly, because the databases are just caches and can be safely deleted, so they don't need to be on persistent storage. In fact some admins mount a ram disk on Exim's hints db directory which avoids the fsync cost and thereby ups the maximum throughput. If you go a step further, you can take the view that Exim is to some extent using the DB as an IPC mechanism.

The traditional solution to the second problem is to slap a standard SQL DB on the back-end, but if you do this the SQL DB becomes a single point of failure. This is bad given that a system like ppswitch which is a cluster of identical machines that relay email does not currently have a SPOF. It also compounds the excessive persistence silliness.

It occurs to me that what I want is something like <a href="http://anoncvs.aldigital.co.uk/splash/">Splash</a>, which is a distributed masterless database, which uses <a href="http://www.spread.org/">the Spread toolkit</a> to reliably multicast messages around the cluster. Wonderful! The hard work has already been done for me, so all I need to do is overhaul Exim's hints DB layer for the first time in 10 years - oh, and get a load of other stuff off the top of my to-do list first.

If it's done properly it should greatly improve the ratelimit feature on clustered systems, and make it much easier to write a high-quality greylisting implementation. (A BALGE implementation is liable to cause too many operational problems to be acceptable to us.) It should also be good for Exim even in single-host setups, by avoiding the hints lock bottleneck. The overhaul of the hints DB layer will also allow Exim to make use of other more sophisticated databases as well as Splash, e.g. standard SQL databases or Unix-style libraries that support multiple-reader / single-writer locking.
