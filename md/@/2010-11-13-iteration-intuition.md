---
lj:
  anum: 106
  can_comment: 1
  ditemid: 110186
  event_timestamp: 1289608020
  eventtime: "2010-11-13T00:27:00Z"
  itemid: 430
  logtime: "2010-11-13T00:27:16Z"
  props:
    personifi_tags: "17:7,8:28,11:14,30:7,23:21,16:7,32:7,1:28,3:7,25:7,26:7,20:7,9:42,nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/110186.html"
title: Iteration Intuition
...

After writing
[my previous entry](https://dotat.at/@/2010-11-10-smooth-colouring-is-the-key-to-the-mandelbrot-set.html),
I noticed that the final equation is in fact just the negated log of
the Douady-Hubbard potential (assuming <i>n</i> is practically
infinite). (In the following I will use the binary logarithm
throughout, lg <i>x</i> = log<sub>2</sub> <i>x</i>.)

<math>
 <mtable>
  <mtr>
   <mtd>
    <mo>-</mo>
    <mo lspace="0em">lg</mo>
    <msub><mi>𝜙</mi><mi>c</mi></msub><mo>(</mo><mi>z</mi><mo>)</mo>
   </mtd>
   <mtd><mo>≃</mo></mtd>
   <mtd columnalign="left">
    <mo lspace="0em">-</mo>
    <mo lspace="0em">lg</mo>
    <mfrac>
     <mrow>
      <mo>lg</mo>
      <msub><mi>r</mi><mi>n</mi></msub>
     </mrow>
     <mrow>
      <mo>lg</mo>
      <msup><mn>2</mn><mi>n</mi></msup>
     </mrow>
    </mfrac>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <mo lspace="0em">lg</mo>
    <msup><mn>2</mn><mi>n</mi></msup>
    <mo>-</mo>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <msub><mi>r</mi><mi>n</mi></msub>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <mi>n</mi>
    <mo>-</mo>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <msub><mi>r</mi><mi>n</mi></msub>
   </mtd>
  </mtr>
 </mtable>
</math>

I didn't really understand this so I sought a more intuitive explanation.

Observe that when <i>z</i> is large,
<i>f</i><sub><i>c</i></sub>(<i>z</i>) is very close to
<i>f</i><sub>0</sub>(<i>z</i>). So in the following we can treat the
crinkly component <i>c</i> as insignificant.

<math>
 <mtable>
  <mtr>
   <mtd columnalign="right">
    <msub><mi>z</mi><mi>n</mi></msub>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <msubsup><mi>f</mi><mi>c</mi><mi>n</mi></msubsup>
    <mo>(</mo><mi>z</mi><mo>)</mo>
    <mo>≃</mo>
    <msup>
     <mi>z</mi>
     <msup><mn>2</mn><mi>n</mi></msup>
    </msup>
   </mtd>
  </mtr>
  <mtr>
   <mtd columnalign="right">
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <msub><mi>z</mi><mi>n</mi></msub>
   </mtd>
   <mtd><mo>≃</mo></mtd>
   <mtd columnalign="left">
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <msup>
     <mi>z</mi>
     <msup><mn>2</mn><mi>n</mi></msup>
    </msup>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <mo lspace="0em">lg</mo>
    <msup><mn>2</mn><mi>n</mi></msup>
    <mo lspace="0em"><!-- times --></mo>
    <mo lspace="0em">lg</mo>
    <mi>z</mi>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <mo lspace="0em">lg</mo>
    <msup><mn>2</mn><mi>n</mi></msup>
    <mo>+</mo>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <mi>z</mi>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <mi>n</mi>
    <mo>+</mo>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <mi>z</mi>
   </mtd>
  </mtr>
 </mtable>
</math>

So the double log gives us a sort of measure of distance in terms of
the number of iterations needed to get from <i>z</i> to
<i>z</i><sub><i>n</i></sub>.

<math>
 <mtable>
  <mtr>
   <mtd>
    <mi>n</mi>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <msub><mo lspace="0em" rspace="0em">log</mo><mn>2</mn></msub>
    <mo lspace="0em"><!-- of --></mo>
    <msup><mn>2</mn><mi>n</mi></msup>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <msub><mo lspace="0em" rspace="0em">log</mo><mn>2</mn></msub>
    <msub><mo rspace="0em">log</mo><mi>z</mi></msub>
    <mo lspace="0em"><!-- of --></mo>
    <msup>
     <mi>z</mi>
     <msup><mn>2</mn><mi>n</mi></msup>
    </msup>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <mo lspace="0em">lg</mo>
    <mfrac>
     <mrow>
      <mo>lg</mo>
      <msup>
       <mi>z</mi>
       <msup><mn>2</mn><mi>n</mi></msup>
      </msup>
     </mrow>
     <mrow>
      <mo>lg</mo>
      <mi>z</mi>
     </mrow>
    </mfrac>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <msup>
     <mi>z</mi>
     <msup><mn>2</mn><mi>n</mi></msup>
    </msup>
    <mo>-</mo>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <mi>z</mi>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd><mo>≃</mo></mtd>
   <mtd columnalign="left">
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <msub><mi>z</mi><mi>n</mi></msub>
    <mo>-</mo>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <mi>z</mi>
   </mtd>
  </mtr>
 </mtable>
</math>

Recall that

<math>
 <mtable>
  <mtr>
   <mtd>
    <mi>z</mi>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd>
    <mi>r</mi>
    <msup>
     <mi>e</mi>
     <mrow><mi>i</mi><mi>&theta;</mi></mrow>
    </msup>
   </mtd>
  </mtr>
  <mtr>
   <mtd>
    <msup><mi>z</mi><mi>p</mi></msup>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd>
    <msup><mi>r</mi><mi>p</mi></msup>
    <msup>
     <mi>e</mi>
     <mrow><mi>p</mi><mi>i</mi><mi>&theta;</mi></mrow>
    </msup>
   </mtd>
  </mtr>
 </mtable>
</math>

Hence the equations in <i>z</i> also apply to <i>r</i> = |<i>z</i>|,

<math>
 <mtable>
  <mtr>
   <mtd>
    <mo>-</mo>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <mi>z</mi>
   </mtd>
   <mtd><mo>≃</mo></mtd>
   <mtd>
    <mi>n</mi>
    <mo>-</mo>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <msub><mi>z</mi><mi>n</mi></msub>
   </mtd>
  </mtr>
  <mtr>
   <mtd>
    <mo>-</mo>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <mi>r</mi>
   </mtd>
   <mtd><mo>≃</mo></mtd>
   <mtd>
    <mi>n</mi>
    <mo>-</mo>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <msub><mi>r</mi><mi>n</mi></msub>
   </mtd>
  </mtr>
 </mtable>
</math>

When <i>z</i> is close to the set, <i>r</i> is slightly greater than
one (plus crinkles), so lg <i>r</i> is a small positive number, and
lg lg <i>r</i> is a large negative number. Hence the negation
to turn it into the equivalent of an iteration count.

The equation above also relates back to the definition of the
electrostatic potential and our simplifying approximation, that
is,

<math>
 <mtable>
  <mtr>
   <mtd>
    <msub><mi>𝜙</mi><mi>c</mi></msub><mo>(</mo><mi>z</mi><mo>)</mo>
   </mtd>
   <mtd><mo>≃</mo></mtd>
   <mtd>
    <mo>lg</mo>
    <mi>r</mi>
   </mtd>
  </mtr>
 </mtable>
</math>

Thinking about what happens when we finish iterating,

<math>
 <mtable>
  <mtr>
   <mtd></mtd>
   <mtd></mtd>
   <mtd>
    <msub>
     <mi>r</mi>
     <mi>n</mi>
    </msub>
   </mtd>
   <mtd><mo>&gt;</mo></mtd>
   <mtd>
    <mi>R</mi>
   </mtd>
   <mtd><mo>&gt;</mo></mtd>
   <mtd>
    <msub>
     <mi>r</mi>
     <mrow><mi>n</mi><mo>-</mo><mn>1</mn></mrow>
    </msub>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd></mtd>
   <mtd>
    <msub>
     <mi>r</mi>
     <mi>n</mi>
    </msub>
   </mtd>
   <mtd><mo>≃</mo></mtd>
   <mtd>
    <msubsup>
     <mi>r</mi>
     <mrow><mi>n</mi><mo>-</mo><mn>1</mn></mrow>
     <mn>2</mn>
    </msubsup>
   </mtd>
  </mtr>
  <mtr>
   <mtd>
    <msup><mi>R</mi><mn>2</mn></msup>
   </mtd>
   <mtd><mo>&gt;</mo></mtd>
   <mtd>
    <msub><mi>r</mi><mi>n</mi></msub>
   </mtd>
   <mtd><mo>&gt;</mo></mtd>
   <mtd>
    <mi>R</mi>
   </mtd>
   <mtd></mtd>
   <mtd></mtd>
  </mtr>
 </mtable>
</math>

So lg lg <i>r</i><sub><i>n</i></sub> lies between two numbers
with a difference of one,

<math>
 <mtable>
  <mtr>
   <mtd>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <msup><mi>R</mi><mn>2</mn></msup>
    <mo>=</mo>
    <mn>1</mn>
    <mo>+</mo>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <mi>R</mi>
   </mtd>
  </mtr>
 </mtable>
</math>

When lg lg <i>r</i><sub><i>n</i></sub> is subtracted from <i>n</i>, it
contributes a fractional part and a fixed offset determined by the
bail-out radius.

Finally, on a practical note I find that a better function for
colouring pixels is to take the log again:

<math>
 <mtable>
  <mtr>
   <mtd>
    <mi>m</mi>
    <mo>=</mo>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">(</mo>
    <mi>n</mi>
    <mo>-</mo>
    <mo lspace="0em">lg</mo>
    <mo lspace="0em">lg</mo>
    <msub><mi>r</mi><mi>n</mi></msub>
    <mo lspace="0em">)</mo>
   </mtd>
  </mtr>
 </mtable>
</math>


The extra log gives the value a more uniform gradient so it cycles
through the colours evenly, and it gets much closer to the set before
it flies off to infinity so less of the picture suffers from aliasing.
Even better, it retains these properties as you zoom in.
