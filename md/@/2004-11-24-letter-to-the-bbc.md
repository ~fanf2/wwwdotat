---
dw:
  anum: 93
  eventtime: "2004-11-24T08:30:00Z"
  itemid: 113
  logtime: "2004-11-24T00:31:38Z"
  props:
    commentalter: 1491292316
    import_source: livejournal.com/fanf/29177
    interface: flat
    opt_backdated: 1
    picture_keyword: silly
    picture_mapid: 6
  url: "https://fanf.dreamwidth.org/29021.html"
format: casual
lj:
  anum: 249
  can_comment: 1
  ditemid: 29177
  event_timestamp: 1101285000
  eventtime: "2004-11-24T08:30:00Z"
  itemid: 113
  logtime: "2004-11-24T00:31:38Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/29177.html"
title: Letter to the BBC
...

On the Today programme this morning (24 Nov), the Home Office minister Caroline Flint justified the recently announced crime bills on the grounds that terrorism is a new threat. This is offensively ignorant, and well illustrates the way the Labour government is exaggerating people's fears in order to gain political advantage.
