---
dw:
  anum: 47
  eventtime: "2015-02-24T17:12:00Z"
  itemid: 419
  logtime: "2015-02-24T17:12:26Z"
  props:
    commentalter: 1491292409
    import_source: livejournal.com/fanf/134571
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/107311.html"
format: html
lj:
  anum: 171
  can_comment: 1
  ditemid: 134571
  event_timestamp: 1424797920
  eventtime: "2015-02-24T17:12:00Z"
  itemid: 525
  logtime: "2015-02-24T17:12:26Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 13
  url: "https://fanf.livejournal.com/134571.html"
title: "DNSQPS: an alarming shell script"
...

<p>I haven't got round to setting up proper performance monitoring for our DNS servers yet, so I have been making fairly ad-hoc queries against the BIND statistics channel and pulling out numbers with <a href="https://stedolan.github.io/jq/">jq</a>.

<p>Last week I changed our new DNS setup for <a href="http://news.uis.cam.ac.uk/articles/2015/02/17/more-frequent-dns-updates">more frequent DNS updates</a>. As part of this change I reduced the TTL on all our records from one day to one hour. The obvious question was, how would this affect the query rate on our servers?

<p>So I wrote a simple monitoring script. The first version did,

<pre>
    while sleep 1
    do
        fetch-and-print-stats
    done
</pre>

<p>But the fetch-and-print-stats part took a significant fraction of a second, so the queries-per-second numbers were rather bogus.

<p>A better way to do this is to run `sleep` in the background, while you fetch-and-print-stats in the foreground. Then you can wait for the sleep to finish and loop back to the start. The loop should take almost exactly a second to run (provided fetch-and-print-stats takes less than a second). <strike>This is pretty similar to an alarm()/wait() sequence in C.</strike> <small>(Actually no, that's bollocks.)</small>

<p>My dnsqps script also abuses `eval` a lot to get a shonky Bourne shell version of associative arrays for the per-server counters. Yummy.

<p>So now I was able to get queries-per-second numbers from my servers, what was the effect of dropping the TTLs? Well, as far as I can tell from eyeballing, nothing. Zilch. No visible change in query rate. I expected at least some kind of clear increase, but no.

<p>The current version of my dnsqps script is:

<pre>
    #!/bin/sh
    
    while :
    do
        sleep 1 & # set an alarm
        
        for s in "$@"
        do
	    total=$(curl --silent http://$s:853/json/v1/server |
                    jq -r '.opcodes.QUERY')
            eval inc='$((' $total - tot$s '))'
            eval tot$s=$total
            printf ' %5d %s' $inc $s
        done
        printf '\n'
        
        wait # for the alarm
    done
</pre>
