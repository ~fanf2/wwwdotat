---
format: html
lj:
  anum: 46
  can_comment: 1
  ditemid: 107310
  event_timestamp: 1279298460
  eventtime: "2010-07-16T16:41:00Z"
  itemid: 419
  logtime: "2010-07-16T16:42:06Z"
  props:
    personifi_tags: "15:4,2:2,35:4,8:56,33:1,10:19,1:52,3:1,25:6,4:1,19:2,26:2,20:1,9:5,13:9,nterms:no"
    verticals_list: "computers_and_software,technology"
  reply_count: 12
  url: "https://fanf.livejournal.com/107310.html"
title: How to set up DNSSEC validation with BIND-9.7
...

<ul>
<li><a href="http://www.root-dnssec.org/">The root zone is now signed!</a> It's time to install the trust anchor on your recursive name servers. Getting it is more fiddly than it should be, since BIND does not recognize the format of the trust anchor as it is published by IANA.
<li>Get the root DNSKEY RR set which is roughly what BIND requires for trust anchors.
<br><tt>&nbsp;&nbsp;&nbsp;&nbsp;$ dig +multi +noall +answer DNSKEY . >root-dnskey</tt>
<br>The resulting file contains two keys, a short-lived zone-signing key (flags = 256) and the key-signing key (flags = 257) which is the one we care about.
<br><pre>
. 86400 IN DNSKEY 256 3 8 (
            AwEAAb1gcDhBlH/9MlgUxS0ik2dwY/JiBIpV+EhKZV7L
            ccxNc6Qlj467QjHQ3Fgm2i2LE9w6LqPFDSng5qVq1OYF
            yTBt3DQppqDnAPriTwW5qIQNDNFv34yo63sAdBeU4G9t
            v7dzT5sPyAgmVh5HDCe+6XM2+Iel1+kUKCel8Icy19hR
            ) ; key id = 41248
. 86400 IN DNSKEY 257 3 8 (
            AwEAAagAIKlVZrpC6Ia7gEzahOR+9W29euxhJhVVLOyQ
            bSEW0O8gcCjFFVQUTf6v58fLjwBd0YI0EzrAcQqBGCzh
            /RStIoO8g0NfnfL2MTJRkxoXbfDaUeVPQuYEhg37NZWA
            JQ9VnMVDxP/VHL496M/QZxkjf5/Efucp2gaDX6RS6CXp
            oY68LsvPVjR0ZSwzz1apAzvN9dlzEheX7ICJBBtuA6G3
            LQpzW5hOA2hzCTMjJPJ8LbqF6dsV6DoBQzgul0sGIcGO
            Yl7OyQdXfZ57relSQageu+ipAdTTJ25AsRTAoub8ONGc
            LmqrAmRLKBP1dfwhYB4N7knNnulqQxA+Uk1ihz0=
            ) ; key id = 19036
</pre>
<li>Turn the DNSKEY into a DS RR set. The dnssec-dsfromkey program ignores the ZSK and only emits DS RRs for the KSK.
<br><tt>&nbsp;&nbsp;&nbsp;&nbsp;$ dnssec-dsfromkey -f root-dnskey . >root-ds</tt>
<br> It emits two RRs, one using SHA-1 and one using SHA-256.
<br><pre>
. IN DS 19036 8 1 B256BD09DC8DD59F0E0F0D8541B8328DD986DF6E
. IN DS 19036 8 2 49AAC11D7B6F6446702E54A1607371607A1A41855200FD2CE1CDDE32F24E8FB5
</pre>
<li>Fetch <a href="https://data.iana.org/root-anchors/root-anchors.xml">https://data.iana.org/root-anchors/root-anchors.xml</a>
which contains a copy of the SHA-256 DS record in XML format.
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
  &lt;TrustAnchor id="AD42165F-3B1A-4778-8F42-D34A1D41FD93"
    source="http://data.iana.org/root-anchors/root-anchors.xml"&gt;
  &lt;Zone&gt;.&lt;/Zone&gt;
  &lt;KeyDigest id="Kjqmt7v" validFrom="2010-07-15T00:00:00+00:00"&gt;
    &lt;KeyTag&gt;19036&lt;/KeyTag&gt;
    &lt;Algorithm&gt;8&lt;/Algorithm&gt;
    &lt;DigestType&gt;2&lt;/DigestType&gt;
    &lt;Digest&gt;49AAC11D7B6F6446702E54A1607371607A1A41855200FD2CE1CDDE32F24E8FB5&lt;/Digest&gt;
  &lt;/KeyDigest&gt;
&lt;/TrustAnchor&gt;
</pre>
<li>You can also fetch <a href="https://data.iana.org/root-anchors/root-anchors.asc">https://data.iana.org/root-anchors/root-anchors.asc</a>
and use it to verify the XML trust anchor using PGP.
<li>Verify that the XML trust anchor matches the DS record you generated from the DNSKEY record.
<li>Reformat the DNSKEY record into a BIND managed-keys clause. This tells BIND to automatically update the trust anchor according to RFC 5011.
<pre>
managed-keys {
    "." initial-key 257 3 8 "
            AwEAAagAIKlVZrpC6Ia7gEzahOR+9W29euxhJhVVLOyQ
            bSEW0O8gcCjFFVQUTf6v58fLjwBd0YI0EzrAcQqBGCzh
            /RStIoO8g0NfnfL2MTJRkxoXbfDaUeVPQuYEhg37NZWA
            JQ9VnMVDxP/VHL496M/QZxkjf5/Efucp2gaDX6RS6CXp
            oY68LsvPVjR0ZSwzz1apAzvN9dlzEheX7ICJBBtuA6G3
            LQpzW5hOA2hzCTMjJPJ8LbqF6dsV6DoBQzgul0sGIcGO
            Yl7OyQdXfZ57relSQageu+ipAdTTJ25AsRTAoub8ONGc
            LmqrAmRLKBP1dfwhYB4N7knNnulqQxA+Uk1ihz0= ";
};
</pre>
<li>Add the managed-keys clause to your <tt>named.conf</tt>
<li>In the <tt>options</tt> section of <tt>named.conf</tt> you should have the directive
<br><tt>&nbsp;&nbsp;&nbsp;&nbsp;dnssec-lookaside auto;</tt>
<br>This enables DNSSEC lookaside validation, which is necessary to bridge gaps (such as ac.uk) in the chain of trust between the root and lower-level signed zones (such as cam.ac.uk). BIND comes with a DLV trust anchor built in, which it will also update according to RFC 5011.
<li><tt>$ rndc reconfig</tt>
<li>Check that DNSSEC validation is working. Verify that the "ad" (authenticated data) flag is present in the output of these commands:
<br><tt>&nbsp;&nbsp;&nbsp;&nbsp;$ dig +dnssec www.nic.cat.</tt>
<br><tt>&nbsp;&nbsp;&nbsp;&nbsp;$ dig +dnssec www.cam.ac.uk.</tt>
<br>The first of these is validated using a chain of trust from the root - DNSSEC as it is ideally intended to work. The second relies on the DLV stop-gap.
</ul>
<p><i>Edited to add:</i> <strike>IANA have a tool written in Python called <a href="https://itar.iana.org/instructions/">anchors2keys</a> which does most of this automatically (er, for the ITAR not the root anchor).</strike> Jakob Schlyter has a Perl program called <a href="http://www.kirei.se/xfiles/dnssec/ta-tool.pl">ta-tool</a> which does a similar job. So does Bjørn Mork, who called his <a href="http://www.mork.no/~bjorn/rootanchor2keys.pl">rootanchor2keys.pl</a>. Stephane Bortzmeyer has <a href="http://translate.google.com/translate?url=http://www.bortzmeyer.org/valider-racine.html">a Makefile and XSLT script</a> which also does the job.</p>
