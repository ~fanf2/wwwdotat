---
dw:
  anum: 158
  eventtime: "2008-07-24T13:30:00Z"
  itemid: 351
  logtime: "2008-07-24T14:30:10Z"
  props:
    commentalter: 1491292358
    import_source: livejournal.com/fanf/90667
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/90014.html"
format: casual
lj:
  anum: 43
  can_comment: 1
  ditemid: 90667
  event_timestamp: 1216906200
  eventtime: "2008-07-24T13:30:00Z"
  itemid: 354
  logtime: "2008-07-24T14:30:10Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/90667.html"
title: ClamAV aargh
...

We're being hammered by loads of vicious email trojans, which mutate fast. I've resorted to adding manual blocks in Exim because ClamAV isn't keeping up.

Just now I was very puzzled that freshclam wasn't downloading the latest version of the virus database. It turns out that although I have told it to poll 100 times a day (about every 15 minutes), freshclam uses the DNS to check what is the latest version, and the TTL on the relevant DNS record is 30 minutes.
