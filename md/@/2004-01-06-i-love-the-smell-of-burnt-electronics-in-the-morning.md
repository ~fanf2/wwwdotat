---
dw:
  anum: 102
  eventtime: "2004-01-06T13:08:00Z"
  itemid: 47
  logtime: "2004-01-06T13:08:40Z"
  props:
    commentalter: 1491292307
    import_source: livejournal.com/fanf/12230
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/12134.html"
format: casual
lj:
  anum: 198
  can_comment: 1
  ditemid: 12230
  event_timestamp: 1073394480
  eventtime: "2004-01-06T13:08:00Z"
  itemid: 47
  logtime: "2004-01-06T13:08:40Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/12230.html"
title: I love the smell of burnt electronics in the morning...
...

http://www.cus.cam.ac.uk/~fanf2/hermes/doc/misc/orange-fire/
