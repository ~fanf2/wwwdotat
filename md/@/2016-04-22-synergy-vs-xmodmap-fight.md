---
dw:
  anum: 6
  eventtime: "2016-04-22T03:41:00Z"
  itemid: 452
  logtime: "2016-04-22T02:41:32Z"
  props:
    commentalter: 1491292415
    import_source: livejournal.com/fanf/142908
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/115718.html"
format: html
lj:
  anum: 60
  can_comment: 1
  ditemid: 142908
  event_timestamp: 1461296460
  eventtime: "2016-04-22T03:41:00Z"
  itemid: 558
  logtime: "2016-04-22T02:41:32Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 2
  url: "https://fanf.livejournal.com/142908.html"
title: "Synergy vs xmodmap: FIGHT!"
...

<h2>Why you can't use xmodmap to change how Synergy handles modifier keys</h2>

<p>As <a href="http://fanf.livejournal.com/142372.html">previously mentioned</a> I
am doing a major overhaul of my workstation setup, the biggest since
1999. This has turned out to be a huge can of worms.</p>

<p>I will try to explain the tentacular complications caused by some
software called <a href="http://synergy-project.org">Synergy</a>...</p>

<h2>I use an Apple trackpad and keyboard, and <tt>1</tt>Password</h2>

<p>I made a less-big change several years ago - big in terms of the
physical devices I use, but not the software configuration: I fell in
love with Apple, especially the Magic Trackpad. On the downside,
getting an Apple trackpad to talk directly to a FreeBSD or Debian box
is (sadly) a laughable proposition, and because of that my setup at
work is a bit weird.</p>

<p>(Digression 1: I also like Apple keyboards. I previously used a
<a href="https://en.wikipedia.org/wiki/Happy_Hacking_Keyboard">Happy Hacking Keyboard (lite 2)</a>,
which I liked a lot, but I like the low profile and short travel of
the Apple keyboards more. Both my HHKB and Apple keyboards use rubber
membranes, so despite appearances I have not in fact given up on
proper keyswitches - I just never used them much.)</p>

<p>(Digression 2: More recently I realised that I could not continue
without a password manager, so I started using 1Password, which for my
purposes basically requires having a Mac. If you aren't using a
password manager, get one ASAP. It will massively reduce your password
security worries.)</p>

<p>At work I have an old-ish cast-off Mac, which is responsible for input
devices, 1Password, web browser shit, and disractions like unread mail
notifications, iCalendar, IRC, Twitter. The idea (futile and
unsuccessful though it frequently is) was that I could place the Mac
to one side and do Real Work on the (other) Unix workstation.</p>

<h2>Synergy is magic</h2>

<p>The key software that ties my workstations together is
<a href="http://synergy-project.org">Synergy</a>.</p>

<p>Synergy allows you to have one computer which owns your input devices
(in my case, my Mac) which can seamlessly control your other
computers. Scoot your mouse cursor from one screen to another and the
keyboard input focus follows seamlessly. It looks like you have
multiple monitors plugged into one computer, with VMs running
different operating systems displayed on different monitors. But they
aren't VMs, they are different bare metal computers, and Synergy is
forwarding the input events from one to the others.</p>

<p>Synergy is great in many ways. It is also a bit <em>too</em> magical.</p>

<h2>How I want my keyboard to work</h2>

<p>My main criterion is that the Meta key should be easy to press.</p>

<p>For reference look at this <a href="https://commons.wikimedia.org/wiki/File:Apple-wireless-keyboard-aluminum-2007.jpg">picture of an Apple keyboard on Wikimedia
Commons</a>.</p>

<p>(Digression 3: Actually, Control is more important than Meta, and
Control absolutely has to be the key to the left of A. I also
sometimes use the key labelled Control as part of special key chord
operations, for which I move my fingers down from the home row. In X11
terminology I need <code>ctrl:nocaps</code> not <code>ctrl:swapcaps</code>. If I need caps
it's easier for me to type with my little finger holding Shift than to
waste a key on Caps Lock confusion . Making Caps Lock into another
Control is so common that it's a simple configuration feature in Mac
OS.)</p>

<p>I use Emacs, which is why the Meta key is also important. Meta is a
somewhat abstract key modifier which might be produced by various
different keys (Alt, Windows, Option, Command, Escape, Ctrl+[, ...)
depending on the user's preferences and the vagaries of the software
they use.</p>

<p>I press most modifier keys (Ctrl, Shift, Fn) with my left little
finger; the exception is Meta, for which I use my thumb. For comfort I
do not want to have to curl my thumb under my palm very much. So Meta
has to come from the Command key.</p>

<p>For the same reasons, on a <a href="http://www.pcguide.com/ref/kb/layout/stdWin104-c.html">PC
keyboard</a> Meta
has to come from the Alt key. Note that on a Mac keyboard, the Option
key is also labelled Alt.</p>

<p>So if you have a keyboard mapping designed for a PC keyboard, you have
Meta &lt;- Alt &lt;- non-curly thumb, but when applied to a Mac keyboard you
get Meta &lt;- Alt &lt;- Option &lt;- too-curly thumb.</p>

<p>This is an awkward disagreement which I have to work around.</p>

<h2>X11 keyboard modifiers</h2>

<p>OK, sorry, we aren't quite done with the tedious background material yet.</p>

<p>The X11 keyboard model (at least the simpler pre-XKB model)
basically has four layers:</p>

<ul>
<li><p>keycodes: numbers that represent physical keys, e.g. 64</p></li>
<li><p>keysyms: symbols that represent key labels, e.g. Alt_L</p></li>
<li><p>modifiers: a few kesyms can be configured as one of 8
modifiers (shift, ctrl, etc.)</p></li>
<li><p>keymap: how keysyms plus modifiers translate to characters</p></li>
</ul>

<p>I can reset all these tables so my keyboard has a reasonably sane
layout with:</p>

<pre><code>    $ setxkbmap -layout us -option ctrl:nocaps
</code></pre>

<p>After I do that I get a fairly enormous variety of modifier-ish keysyms:</p>

<pre><code>    $ xmodmap -pke | egrep 'Shift|Control|Alt|Meta|Super|Hyper|switch'
    keycode  37 = Control_L NoSymbol Control_L
    keycode  50 = Shift_L NoSymbol Shift_L
    keycode  62 = Shift_R NoSymbol Shift_R
    keycode  64 = Alt_L Meta_L Alt_L Meta_L
    keycode  66 = Control_L Control_L Control_L Control_L
    keycode  92 = ISO_Level3_Shift NoSymbol ISO_Level3_Shift
    keycode 105 = Control_R NoSymbol Control_R
    keycode 108 = Alt_R Meta_R Alt_R Meta_R
    keycode 133 = Super_L NoSymbol Super_L
    keycode 134 = Super_R NoSymbol Super_R
    keycode 203 = Mode_switch NoSymbol Mode_switch
    keycode 204 = NoSymbol Alt_L NoSymbol Alt_L
    keycode 205 = NoSymbol Meta_L NoSymbol Meta_L
    keycode 206 = NoSymbol Super_L NoSymbol Super_L
    keycode 207 = NoSymbol Hyper_L NoSymbol Hyper_L
</code></pre>

<p>These map to modifiers as follows. (The higher modifers have
unhelpfully vague names.)</p>

<pre><code>    $ xmodmap -pm
    xmodmap:  up to 4 keys per modifier, (keycodes in parentheses):

    shift       Shift_L (0x32),  Shift_R (0x3e)
    lock      
    control     Control_L (0x25),  Control_L (0x42),  Control_R (0x69)
    mod1        Alt_L (0x40),  Alt_R (0x6c),  Meta_L (0xcd)
    mod2        Num_Lock (0x4d)
    mod3      
    mod4        Super_L (0x85),  Super_R (0x86),  Super_L (0xce),  Hyper_L (0xcf)
    mod5        ISO_Level3_Shift (0x5c),  Mode_switch (0xcb)
</code></pre>

<h2>How Mac OS -> Synergy -> X11 works by default</h2>

<p>If I don't change things, I get</p>

<pre><code>    Command -&gt; 133 -&gt; Super_L -&gt; Mod4 -&gt; good for controlling window manager

    Option -&gt; 64 -&gt; Alt_L -&gt; Mod1 -&gt; meta in emacs
</code></pre>

<p>which is not unexpected, given the differences between PC and Mac
layouts, but I want to swap the effects of Command and Option</p>

<p>Note that I get (roughly) the same effect when I plug the Apple
keyboard directly into the PC and when I use it via Synergy. I want
the swap to work in both cases.</p>

<h2>xmodmap to the rescue! not!</h2>

<p>Eliding some lengthy and frustrating debugging, the important insight
came when I found I could reset the keymap using <code>setxkbmap</code> (as I
mentioned above) and test the effect of <code>xmodmap</code> on top of that in a
reproducible way. (<code>xmodmap</code> doesn't have a reset-to-default option.)</p>

<p>What I want <em>should</em> be relatively simple:</p>

<pre><code>    Command -&gt; Alt -&gt; Mod1 -&gt; Meta in emacs

    Option -&gt; Super -&gt; Mod4 -&gt; good for controlling window manager
</code></pre>

<p>So in <code>xmodmap</code> I should be able to just swap the mappings of keycodes
64 and 133.</p>

<p>The following <code>xmodmap</code> script is a very thorough version of this
idea. First it completely strips the higher-order modifier keys, then
it rebuilds just the config I want.</p>

<pre><code>    clear Mod1
    clear Mod2
    clear Mod3
    clear Mod4
    clear Mod5

    keycode  64 = NoSymbol
    keycode  92 = NoSymbol
    keycode 108 = NoSymbol
    keycode 133 = NoSymbol
    keycode 134 = NoSymbol
    keycode 203 = NoSymbol
    keycode 204 = NoSymbol
    keycode 205 = NoSymbol
    keycode 206 = NoSymbol
    keycode 207 = NoSymbol

    ! command -&gt; alt
    keycode 133 = Alt_L
    keycode 134 = Alt_R
    add Mod1 = Alt_L Alt_R

    ! option -&gt; super
    keycode 64 = Super_L
    keycode 108 = Super_R
    add Mod4 = Super_L Super_R
</code></pre>

<h2>WAT?! That does not work</h2>

<p>The other ingredient of the debugging was to look carefully at the
output of <code>xev</code> and Synergy's debug logs.</p>

<p>When I fed that script into <code>xmodmap</code>, I saw,</p>

<pre><code>    Command -&gt; 64 -&gt; Super_L -&gt; Mod4 -&gt; good for controlling window manager

    Option -&gt; 133 -&gt; Alt_L -&gt; Mod1 -&gt; Meta in emacs
</code></pre>

<p>So Command was STILL being Super, and Option was STILL being Alt.</p>

<p>I had not swapped the effect of the keys! But I had swapped their key codes!</p>

<h2>An explanation for Synergy's modifier key handling</h2>

<p>Synergy's debug logs revealed that, given a keypress on the Mac,
Synergy thought it should have a particular effect; it looked at the
X11 key maps to work out what key codes it should generate to produce
that effect.</p>

<p>So, when I pressed Command, Synergy thought, OK, I need to make a Mod4
on X11, so I have to artificially press a keycode 113 (or 64) to have
this effect.</p>

<p>This also explains some other weird effects.</p>

<ul>
<li><p>Synergy produces one keycode for both left and right Command, and
one for both left and right Option. Its logic squashes a keypress to
a desired modifier, which it then has to map back to a keysym - and
it loses the left/right distinction in the process.</p></li>
<li><p>If I use my scorched-earth technique but tell <code>xmodmap</code> to map
keysyms to modifiers that Synergy isn't expecting, the Command or
Option keys will mysteriously have no effect at all. Synergy's log
complains that it can't find a mapping for them.</p></li>
<li><p>Synergy has its own modifier key mapping feature. If you tell it to
map a key to Meta, and the X11 target has a default keymap, it will
try to create Meta from a crazy combination of Shift+Alt. The reason
why is clear if you work backwards from these lines of <code>xmodmap</code> output:</p>

<pre><code>keycode  64 = Alt_L Meta_L Alt_L Meta_L
mod1        Alt_L (0x40),  Alt_R (0x6c),  Meta_L (0xcd)
</code></pre></li>
</ul>

<h2>My erroneous mental model</h2>

<p>This took a long time to debug because I thought Synergy was mapping
keypresses on the Mac to keycodes or keysyms on X11. If that had been
the case then <code>xmodmap</code> would have swapped the keys as I expected. I
would also have seen different keysyms in <code>xev</code> for left and right.
And I would not have seen the mysterious disappearing keypresses nor
the crazy multiple keypresses.</p>

<p>It took me a lot of fumbling to find some reproducible behaviour from
which I could work out a plausible explanation :-(</p>

<h2>The right way to swap modifier keys with Synergy</h2>

<p>The right way to swap modifier keys with Synergy is to tell the
Synergy server (which runs on the computer to which the keyboard is
attached) how they keyboard modifiers should map to modifiers on each
client computer. For example, I run <code>synergys</code> on a Mac called <code>white</code>
with a <code>synergyc</code> on a PC called <code>grey</code>:</p>

<pre><code>    section: screens
        grey:
            super = alt
            alt = super
        white:
            # no options
    end
</code></pre>

<p>You can do more complicated mapping, but a simple swap avoids craziness.</p>

<p>I also swap the keys with <code>xmodmap</code>. This has no effect for Synergy,
because it spots the swap and unswaps them, but it means that if I
plug a keyboard directly into the PC, it has the same layout as it
does when used via Synergy.</p>

<h2>Coda</h2>

<p>I'm feeling a bit more sad about Synergy than I was before. It isn't
just because of this crazy behaviour.</p>

<p>The developers have been turning Synergy into a business. Great! Good
for them! Their business model is paid support for open source
software, which is fine and good.</p>

<p>However, they have been hiding away their documentation, so I can't
find anything in the current packages or on the developer website
which explains this key mapping behaviour. Code without documentation
doesn't make me want to give them any money.</p>
