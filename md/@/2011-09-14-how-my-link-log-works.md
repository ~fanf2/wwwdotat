---
format: html
lj:
  anum: 183
  can_comment: 1
  ditemid: 116407
  event_timestamp: 1315996440
  eventtime: "2011-09-14T10:34:00Z"
  itemid: 454
  logtime: "2011-09-14T09:34:20Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/116407.html"
title: How my link log works
...

<p><a href="http://dotat.at/:">My link log</a> is a fairly horrible combination of three perl scripts. It's "symbiosisware", a quick hack specifically for my needs and not designed for general usage. (I think I got that term from Simon Tatham.) Still, people occasionally ask how it works, so here goes.</p>

<p>The visible part is <a href="http://dotat.at/prog/scripts/url">a 35,000 line CGI script</a> which consists almost entirely of a data structure containing my log of nearly 7000 links. (This is perhaps a bit wasteful - the CGI uses about a quarter of a second of CPU time.) This program produces the HTML web page, the atom feed, and does redirections for the short versions of the URLs. There's also a periodic log analysis job (some vile seddery) which counts how many times each short URL has been requested in the last couple of weeks.</p>

<p>New URLs are added to the CGI script using <a href="http://dotat.at/prog/scripts/blog">a command line utility</a> which also feeds the links to IRC, Twitter, and Delicious. (I posted previously about <a href="http://fanf.livejournal.com/108436.html">scripting Twitter using Jef Poskanzer's utilities</a>.) This script also chooses the random tag used in the short version of the URL.</p>

<p>Finally, I don't have convenient access to the command line when I am using my iPhone, so I use its build-in facility to mail a link to a particular address. These messages are delivered to a special mailbox (using Sieve) which is polled periodically to extract the links and feed them to the command-line tool. The <a href="http://dotat.at/prog/scripts/imap-to-link-log">cron script</a> is mostly a shonky IMAP implementation and RFC 822 message parser...</p>

<p>You can see I'm not particularly fond of digging through CPAN to find which of the million libraries is worth using...</p>
