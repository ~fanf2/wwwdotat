---
dw:
  anum: 18
  eventtime: "2006-01-04T21:23:00Z"
  itemid: 173
  logtime: "2006-01-04T22:38:51Z"
  props:
    commentalter: 1491292364
    import_source: livejournal.com/fanf/44733
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/44306.html"
format: casual
lj:
  anum: 189
  can_comment: 1
  ditemid: 44733
  event_timestamp: 1136409780
  eventtime: "2006-01-04T21:23:00Z"
  itemid: 174
  logtime: "2006-01-04T22:38:51Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 1
  url: "https://fanf.livejournal.com/44733.html"
title: from me on behalf of myself
...

Outlook has an odd idea about email headers, probably because it isn't an Internet email client (though it has had support for Internet Standards retro-fitted). For example, it uses [] to delimit addresses instead of &lt;&gt; and it uses semicolons as a list separators instead of commas, both of which are syntax errors. Furthermore, unlike all other MUAs, it insists on showing the user both the From: and Sender: headers if they are different - most MUAs only show you the Sender: if you ask to see the full headers. This means that email from most of our users appears in Outlook like "<tt>from Tony Finch [fanf2&#64;hermes.cam.ac.uk] on behalf of Tony Finch [fanf2&#64;cam.ac.uk]</tt>" which is rather irritating.

From Hermes's point of view, its native email domain is (obviously) hermes.cam.ac.uk, and most of our other domains are just aliases files (with a bit of magic, especially for cam.ac.uk itself). There's nothing preventing the determined person from directing their @cam email to someone else's Hermes account - and in fact Professor V. Important may quite legitimately want all @cam email to be handled by a secretary. In addition to that, Hermes users are allowed to send email "from" role addresses or vanity addresses, such as &lt;mail-support&#64;ucs.cam.ac.uk&gt; or &lt;dot@dotat.at&gt;. So in order to be clear about who sent a message, Hermes adds a Sender: header containing the authenticated user's @hermes address if the message's From: header is not @hermes - which is in line with decades of tradition in email software.

Hermes always did this for locally-submitted email, via Pine or Webmail, and since we introduced authenticated message submission it has done it for remote submissions too. We're trying to force everyone to configure securely authenticated IMAP and SMTP, so the slightly-redundant Sender: header is going to come to the notice of Outlook users more and more. I believe it was first complained about by Robin Walker just over a year ago, and it has caused another complaint this week.

I've recently made a few changes to ppswitch which improve the situation. When Exim is routing an email address belonging to a Hermes user (resolving aliases etc.) it eventually reaches that user's @hermes address. PPswitch now uses this to work out on the fly that &lt;bursar@botolph.cam.ac.uk&gt; is actually <tt>spqr1</tt>, and passes this result back up to the access control logic. This mapping of email addresses to users means we can do two things: we can make it a bit harder to spoof email, because ppswitch knows when a spotty undergrad is sending email "from" Professor V. Important; we can also say that spqr1@botolph and spqr1@cam are sufficiently similar to spqr1@hermes that email "from" those addresses doesn't need a disambiguating Sender: header.

The second part of the fix is to make this work for local submission as well as remote submissions. Nowadays, messages from Pine and Webmail go through the same submission process on ppswitch as messages from remote MUAs, except that the SMTP client is actually Exim on the machine hermes-1 or hermes-2. Behind the menu system these are fairly traditional multi-user Unix systems, so Exim still likes to add a Sender: header if a message's From: header doesn't match the local user's address - but this is now redundant and can be turned off. Webmail also likes to add a Sender: header in some circumstances, but this is less easy to turn off; fortunately it is less of a problem because Webmail has some knowledge about the mapping between @hermes and @cam - it doesn't know about other domains but Webmail users are less likely to reconfigure their From: line than others.

One notable thing that these changes don't handle is "friendly name" addresses, like &lt;tony.finch@ucs.cam.ac.uk&gt; which are particularly popular in the Judge Business School, but handling these is an AI-complete problem that I don't propose to fix :-)
