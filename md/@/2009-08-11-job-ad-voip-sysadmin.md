---
dw:
  anum: 169
  eventtime: "2009-08-11T14:49:00Z"
  itemid: 391
  logtime: "2009-08-11T14:57:14Z"
  props:
    import_source: livejournal.com/fanf/101867
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/100265.html"
format: html
lj:
  anum: 235
  can_comment: 1
  ditemid: 101867
  event_timestamp: 1250002140
  eventtime: "2009-08-11T14:49:00Z"
  itemid: 397
  logtime: "2009-08-11T14:57:14Z"
  props:
    personifi_tags: "18:21,8:14,23:7,43:7,1:7,32:14,3:28,5:7,19:28,20:14,9:42"
  reply_count: 0
  url: "https://fanf.livejournal.com/101867.html"
title: Job Ad - VOIP sysadmin
...

<p>The University of Cambridge Computing Service is currently looking for a second VOIP sysadmin. We've just finished replacing our old analogue phone switch with a Cisco-based setup serving over 16,000 lines. There are more details (including salary level and waffle) in <a href="http://www.cam.ac.uk/cs/jobs/">the job ad</a>.</p>
