---
dw:
  anum: 240
  eventtime: "2006-04-24T08:52:00Z"
  itemid: 223
  logtime: "2006-04-24T09:03:12Z"
  props:
    commentalter: 1491292339
    import_source: livejournal.com/fanf/57467
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/57328.html"
format: casual
lj:
  anum: 123
  can_comment: 1
  ditemid: 57467
  event_timestamp: 1145868720
  eventtime: "2006-04-24T08:52:00Z"
  itemid: 224
  logtime: "2006-04-24T09:03:12Z"
  props: {}
  reply_count: 14
  url: "https://fanf.livejournal.com/57467.html"
title: International jet set
...

Next week I am going to Nairobi for <a href="http://www.afnog.org/">AfNOG</a>, the African Network Operators Group 2006 event. A significant part of this is a workshop teaching the technical skills needed to run an ISP. I'll be going in place of Philip Hazel to teach people about Exim.

One thing I noticed this morning is that the credit cards that I have been issued this year do not have magnetic stripes: they are chip-and-pin only! This is rather inconvenient for international travel. Fortunately I still have an older credit card with a stripe - though it is one we want to get rid of - and my bank is sending me a replacement card which might arrive before I leave.

Anyone else who is planning to travel on new credit cards should check this. Of course banks like you to warn them beforehand anyway, so that their fraud detection systems are prepared.
