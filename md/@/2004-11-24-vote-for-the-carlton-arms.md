---
dw:
  anum: 238
  eventtime: "2004-11-24T14:02:00Z"
  itemid: 114
  logtime: "2004-11-24T06:03:42Z"
  props:
    commentalter: 1491292317
    import_source: livejournal.com/fanf/29439
    interface: flat
    opt_backdated: 1
    picture_keyword: photo
    picture_mapid: 3
  url: "https://fanf.dreamwidth.org/29422.html"
format: casual
lj:
  anum: 255
  can_comment: 1
  ditemid: 29439
  event_timestamp: 1101304920
  eventtime: "2004-11-24T14:02:00Z"
  itemid: 114
  logtime: "2004-11-24T06:03:42Z"
  props: {}
  reply_count: 7
  url: "https://fanf.livejournal.com/29439.html"
title: Vote for the Carlton Arms!
...

The Abbot Ale / Daily Telegraph perfect pub award:

http://www.abbotale.co.uk/vote_online.php
