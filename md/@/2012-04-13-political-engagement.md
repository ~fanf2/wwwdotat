---
format: html
lj:
  anum: 35
  can_comment: 1
  ditemid: 119587
  event_timestamp: 1334286840
  eventtime: "2012-04-13T03:14:00Z"
  itemid: 467
  logtime: "2012-04-13T02:14:41Z"
  props:
    give_features: 1
    personifi_tags: "nterms:no"
  reply_count: 1
  url: "https://fanf.livejournal.com/119587.html"
title: Political engagement
...

<p>I have been perplexed recently by the way my political engagement responds to external stimuli.

<p>Several months ago Rachel persuaded me to go to the Lib Dem spring conference, on the basis of her enjoyment of the autumn conference. I said OK, expecting I could treat it like a fan convention and spend the weekend shooting shit with like-minded people over a few beers. Unfortunately it coincided with the peak of the argument over the NHS bill, probably this government's most important*controversial bit of legislation so far.

<p>I ended up spending the conference utterly disengaged from any meaningful discussion, and not really understanding why. I care deeply about the NHS! I have strong opinions about it! But I felt unable to get involved.

<p>So I have been surprised by my reaction to the CCDP government snooping proposal: much more engaged and active in the arguments against it. Why?

<p>The most obvious thing is that I know more about the technicalities. I can join in wonkish arguments in a way I couldn't over the NHS.

<p>The second thing is that I am surrounded by like-minded people who also want to <a href="http://cambridgelibdems.org.uk/en/petition/the-big-brother-state-won-t-happen-on-the-liberal-democrat-watch">defend civil liberties and kill CCDP</a>. <a href="http://www.julianhuppert.org.uk/">My MP</a> <a href="http://www.julianhuppert.org.uk/content/civil-liberties-crime">understands and cares about these kinds of things</a> and has influence over them, being on the <a href="http://www.parliament.uk/business/committees/committees-a-z/commons-select/home-affairs-committee/membership/">home affairs select committee</a>.

<p>So yes, it's much <i>easier</i> for me to get involved and feel like I'm helping to improve the situation. But it doesn't explain the feeling of futility I had about the NHS.

<p>I have been struck by the force of the moral arguments around government snooping. This is quite well illustrated by <a href="http://falkvinge.net/2012/04/02/sweden-paradise-lost-part-1-general-wiretapping/">the Swedish wiretapping law</a>. Their security services made the same argument as ours: "we have permission to invade some people's privacy within this limited technical context, so we should have the ability to invade everyone's privacy without restriction". Discussing the technicalities of what snooping is feasible or sensible is <i>wrong</i> because it implicitly acknowledges that it is morally acceptable to violate privacy and legal process in this way, though we'll only do it a little bit, honest.

<p>The advantage of arguing from the basis of firm political principles came across to me most effectively from one of the older local activists, I think because she is the least like a cipherpunk of anyone I have encountered in this context.

<p>This is all very jolly when you have a shared political framework, and everyone agrees it is reasonable to argue that an individual's rights to privacy and freedom from arbitrary forced search outweigh the MI5 security blanket fear of fear. But what if you don't share such a framework?

<p>Looking back, it seems to me that the arguments over the NHS bill were wonking about the technicalities of how, or how much, or where to outsource health provision. A lot of discussion about improving outcomes, but there was no chance of <i>credibly</i> standing up to say, we should take a public-sector approach to fixing the weaknesses in the NHS.

<p>So I think useful political engagement comes when you agree with the general consensus, and know about the details, and want to help fine-tune; or when you feel the consensus is about to head off-course but can be pushed back into line. That is the realm of mainstream politics.

<p>But if you think the consensus is wrong (drug prohibition! market speculation! crisis austerity!) you have to campaign from the side-lines until the consensus shifts. Fine if some group exists to pursue such a campaign. However, political disengagement, or disaffection, or even extremism come when people disagree with the consensus <i>at a fundamental level</i> and there is no party up there arguing their point of view and representing their opinion.
