---
dw:
  anum: 234
  eventtime: "2016-09-26T11:08:00Z"
  itemid: 468
  logtime: "2016-09-26T10:08:11Z"
  props:
    import_source: livejournal.com/fanf/146996
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/120042.html"
format: html
lj:
  anum: 52
  can_comment: 1
  ditemid: 146996
  event_timestamp: 1474888080
  eventtime: "2016-09-26T11:08:00Z"
  itemid: 574
  logtime: "2016-09-26T10:08:11Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/146996.html"
title: DNS ADDITIONAL data is disappointingly poorly used
...

<p>Recently there was a thread on <tt>bind-users</tt> about "<a href="https://lists.isc.org/pipermail/bind-users/2016-September/thread.html#97679">minimal responses and speeding up queries</a>. The discussion was not very well informed about the difference between the theory and practice of addidional data in DNS responses, so I wrote the following.

<p>Background: DNS replies can contain "additional" data which (according to <a href="http://tppls.ietf.org/html/rfc1034.html">RFC 1034</a> "may be helpful in using the RRs in the other sections." Typically this means addresses of servers identified in NS, MX, or SRV records. In BIND you can turn off most additional data by setting the <tt>minimal-responses</tt> option.

<p><i>Reindl Harald &lt;h.reindl at thelounge.net&gt; wrote:
<blockquote>
additional responses are part of the inital question and
may save asking for that information - in case the additional 
info is not needed by the client it saves traffic
</blockquote>
</i>

<p><i>Matus UHLAR - fantomas &lt;uhlar at fantomas.sk&gt;
<blockquote>
If you turn mimimal-responses on, the required data may not be in the
answer. That will result into another query send, which means number of
queries increases.
</blockquote>
</i>

<p>There are a few situations in which additional data is useful in theory,
but it's surprisingly poorly used in practice.

<p>End-user clients are generally looking up address records, and the
additional and authority records aren't of any use to them.

<p>For MX and SRV records, additional data can reduce the need for extra A
and AAAA records - but only if both A and AAAA are present in the
response. If either RRset is missing the client still has to make another
query to find out if it doesn't exist or wouldn't fit. Some code I am
familiar with (Exim) ignores additional sections in MX responses and always does
separate A and AAAA lookups, because it's simpler.

<p>The other important case is for queries from recursive servers to
authoritative servers, where you might hope that the recursive server
would cache the additional data to avoid queries to the authoritative
servers.

<p>However, in practice BIND is not very good at this. For example,
let's query for an MX record, then the address of one of the MX target
hosts. We expect to get the address in the response to the first query, so
the second query doesn't need another round trip to the authority.

<p>Here's some log, heavily pruned for relevance.

<pre>
2016-09-23.10:55:13.316 queries: info:
        view rec: query: isc.org IN MX +E(0)K (::1)
2016-09-23.10:55:13.318 resolver: debug 11:
        sending packet to 2001:500:60::30#53
;; QUESTION SECTION:
;isc.org.                       IN      MX
2016-09-23.10:55:13.330 resolver: debug 10:
        received packet from 2001:500:60::30#53
;; ANSWER SECTION:
;isc.org.               7200    IN      MX      10 mx.pao1.isc.org.
;isc.org.               7200    IN      MX      20 mx.ams1.isc.org.
;; ADDITIONAL SECTION:
;mx.pao1.isc.org.       3600    IN      A       149.20.64.53
;mx.pao1.isc.org.       3600    IN      AAAA    2001:4f8:0:2::2b
2016-09-23.10:56:13.150 queries: info:
        view rec: query: mx.pao1.isc.org IN A +E(0)K (::1)
2016-09-23.10:56:13.151 resolver: debug 11:
        sending packet to 2001:500:60::30#53
;; QUESTION SECTION:
;mx.pao1.isc.org.               IN      A
</pre>

<p>Hmf, well that's disappointing.

<p>Now, there's a rule in RFC 2181 about ranking the trustworthiness of data:


<p><i>5.4.1. Ranking data

<blockquote>
   [ snip ]
</blockquote>
<blockquote>
   Unauthenticated RRs received and cached from the least trustworthy of
   those groupings, that is data from the additional data section, and
   data from the authority section of a non-authoritative answer, should
   not be cached in such a way that they would ever be returned as
   answers to a received query.  They may be returned as additional
   information where appropriate.  Ignoring this would allow the
   trustworthiness of relatively untrustworthy data to be increased
   without cause or excuse.
</blockquote>
</i>

<p>Since my recursive server is validating, and isc.org is signed, it should
be able to authenticate the MX target address from the MX response, and
promote its trustworthiness, instead of making another query. But BIND
doesn't do that.

<p>There are other situations where BIND fails to make good use of all the
records in a response, e.g. when you get a referral for a signed zone, the
response includes the DS records as well as the NS records. But BIND
doesn't cache the DS records properly, so when it comes to validate the
answer, it re-fetches them.

<p>In a follow-up message, Mark Andrews says these problems are on his to-do list,
so I'm looking forward to DNSSEC helping to make DNS faster :-)
