IETF 113, Vienna, second half
=============================

[IETF 113 in Vienna][ietf113] is now nearly finished. In between
meetings I found some time to look around the [Uhrenmuseum][] and
[catch up with old friends][mpk].

[ietf113]: https://www.ietf.org/how/meetings/113/
[Uhrenmuseum]: https://www.wienmuseum.at/en/locations/uhrenmuseum
[mpk]: https://twitter.com/mpk/status/1507107346924949518

As I mentioned in [my previous post][prev] I have not been keeping up
with IETF activity in the last year or two, so a lot of what I have
been doing this week is getting an overview of current activity in the
working groups that are most relevant to me. Here I am going to note a
few things that caught my attention.

[prev]: https://dotat.at/@/2022-03-21-ietf113-012.html

maprg
-----

Wednesday morning was the [measurement and analysis for protocols
research group meeting][maprg], with several DNS-related presentations
which provoked some interesting discussions.

I liked the performance measurements of DNS over different transport
protocols, mainly because it was unsurprising: it is reassuring when
measurements confirm predictions. They only measured the latency for
the first query on a connection, so it will be interesting to see some
follow-up work on multi-query connections.

[maprg]: https://datatracker.ietf.org/meeting/113/materials/agenda-113-maprg-03

dprive
------

The [DNS privacy working group][dprive] meeting was moved from Friday
to a joint session with `add` on Thursday.

The current topic is (still) the difficult question of how to encrypt
DNS traffic between recursive resolvers and authoritative servers. I
think I am less grumpy about the working group's problems with
authenticated encryption than I used to be, but it is still
frustrating.

[dprive]: https://datatracker.ietf.org/group/dprive/about/

[dprive-add]: https://datatracker.ietf.org/meeting/113/materials/agenda-113-add-05

add
---

The [adaptive DNS discovery working group][add] aims to give devices
more information about a network's DNS resolvers, such as support for
encrypted transports.

There was a presentation about "split horizon DNS configuration" which
seemed problematic to me in a number of ways. I am not sure I
understood the mechanism properly - it's to do with putting
information about private domains in the public DNS - so I need to
read the draft and make some thoughtful questions and comments.

[add]: https://datatracker.ietf.org/group/add/about/

dnsop
-----

On the mailing list, the brokenness of SHA-1 raised its ugly head
again. I posted a message to [remind everyone of what the SHA-1 break
means for DNSSEC][shambles] and that they should not be complacent
about it.

[shambles]: https://mailarchive.ietf.org/arch/msg/dnsop/8kAGl_OpVScvZsR-aYMNcs6S46A/

tcpm
----

I had a chat with my friend [Reese][] about the TCP maintenance and
minor extensions meeting. Reese works on TCP performance for Netflix,
so the presentations about congestion control tweaks were particularly
relevant to them. But the most entertaining talk was on [tcpls][],
which is basically "what if QUIC, but TCP?" to which most people here
answer, "why?!?!"

[Reese]: https://tenghardt.net/
[tcpls]: https://datatracker.ietf.org/meeting/113/materials/slides-113-tcpm-draft-piraux-tcpls-00-00

qp-trie
-------

My discussion about data structures with folks from [NLnet Labs][]
continued over email. Jeroen Koekkoek gave me an idea for reducing
locking and copy-on-write problems by getting rid of prev/next
pointers in DNS record objects.

[NLnet Labs]: https://www.nlnetlabs.nl/people/

end
---

Now it is Friday afternoon, the meeting is over, and soon it will be
time to leave for the journey home. It has been really nice to see
people in person again, and get to know some of my new colleagues
better.

And it isn't long until the next trip, which will be [UKNOF][] in
Manchester next month.

[UKNOF]: https://www.uknof.org.uk/
