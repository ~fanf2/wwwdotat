---
comments:
  Dreamwidth: https://fanf.dreamwidth.org/150998.html
  Fediverse: https://mendeddrum.org/@fanf/113602062467944511
...
nsnotifyd-2.3 released
======================

D'oh, I lost track of a bug report that should have been fixed in
`nsnotifyd-2.2`. Thus, hot on the heels of [the previous release][prev],
here's [`nsnotifyd-2.3`][nsnotifyd]. Sorry for causing extra work to
my uncountably many users!

The `nsnotifyd` daemon monitors a set of DNS zones and runs a command
when any of them change. It listens for DNS NOTIFY messages so it can
respond to changes promptly. It also uses each zone's SOA refresh and
retry parameters to poll for updates if `nsnotifyd` does not receive
NOTIFY messages more frequently. It comes with a client program
`nsnotify` for sending notify messages.

[This `nsnotifyd-2.3` release][nsnotifyd] includes some bug fixes:

  * When `nsnotifyd` receives a SIGINT or SIGTERM while running the
    command, it failed to handle it correctly. Now it exits promptly.

    Many thanks to Athanasius for reporting the bug!

  * Miscellaneous minor code cleanup and compiler warning suppression.

Thanks also to Dan Langille who sent me a lovely appreciation:

> Now that I think of it, nsnotifyd is in my favorite group of
> software. That group is software I forget I'm running, because they
> just run and do the work. For years. I haven't touched, modified, or
> configured nsnotifyd and it just keeps doing the job.

[nsnotifyd]: https://dotat.at/prog/nsnotifyd/
[prev]: https://dotat.at/@/2024-11-28-nsnotifyd-2-2-released.html
