---
format: html
lj:
  anum: 102
  can_comment: 1
  ditemid: 111462
  event_timestamp: 1294194480
  eventtime: "2011-01-05T02:28:00Z"
  itemid: 435
  logtime: "2011-01-05T02:28:37Z"
  props:
    personifi_tags: "17:5,18:5,6:2,8:28,23:5,16:5,1:28,3:15,19:5,25:7,5:10,20:5,9:15,22:5,14:2,nterms:no"
  reply_count: 1
  url: "https://fanf.livejournal.com/111462.html"
title: Do programming languages have terroir?
...

<p>James Noble gave a talk at ECOOP 2009 called "the myths of object-orientation". When I read the paper version the following quote caught my eye:</p>

<blockquote>When I was learning Smalltalk, [Brian Boutel] used to complain about “Californian” programming — no types, dynamic dispatch, a relaxed interactive programming environment — much warmer, and much less bracing, than Oregon or Glasgow that gave birth to his beloved Haskell. So I wonder if, like wine, do programming languages have terroir? What influence does the environment that nurtures a programming language, or a programming principle, or a myth, have on the result? Smalltalk is Californian, Dick Gabriel has described how Unix (and C) comes from the Bell Labs engineering culture, but what of the rest?</blockquote>

<p>The talk as a whole is very much tongue-in-cheek, but the idea is intriguing even if it isn't to be taken too seriously. There's more discussion at <a href="http://lambda-the-ultimate.org/node/4112">Lambda the Ultimate</a>.</p>
