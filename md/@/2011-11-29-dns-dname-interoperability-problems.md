---
format: html
lj:
  anum: 8
  can_comment: 1
  ditemid: 116744
  event_timestamp: 1322582940
  eventtime: "2011-11-29T16:09:00Z"
  itemid: 456
  logtime: "2011-11-29T16:33:27Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 2
  url: "https://fanf.livejournal.com/116744.html"
title: DNS DNAME interoperability problems
...

<p>David Blacka recently posted
<a href="http://blacka.com/david/2006/12/04/dns-dname-is-almost-useless/">
a complaint about the limited usefulness of DNAME</a>
sub-domain aliases in the DNS.
Everything he says is right (except perhaps his linkbait title!)
but I have a few points to add.</p>

<p>It's worth noting that the IETF has been working on
<a href="http://tools.ietf.org/html/draft-ietf-dnsext-rfc2672bis-dname">
updates and clarifications to RFC 2672</a>
which should soon be published as an RFC.</p>

<p>David points out the awkwardness of DNAME only aliasing sub-domains
and not the name itself. This was one of the main points of discussion
last year when the IETF dnsext working groups was talking about better
support for spelling variations. There were a few proposals to address
this problem. One option was to relax the restriction that a CNAME may
not coexist with any other RRs, so that you can have
<a href="http://tools.ietf.org/html/draft-sury-dnsext-cname-dname">
both CNAME+DNAME at a name</a>. Alternatively there is the proposed
<a href="http://tools.ietf.org/html/draft-yao-dnsext-bname">
BNAME RR type</a>
which acts as both a CNAME and a DNAME.
These are all options for the long term, and the whole discussion has
been on hold for several months while clearer requirements are
gathered from the IDN experts for who this feature is intended.</p>

<p>There is not very much deployment of DNAME out there.
<a href="http://blog.eiloart.com/">Ian Eiloart</a>
<a href="https://twitter.com/IanEiloart/status/141488212685230080">asked</a>
if any UK Universities use DNAME to do
<a href="http://en.wikipedia.org/wiki/JANET_NRS">NRS-style long form / short form</a>
aliasing. I did a quick survey and found five DNAME RRs at the apices of zones under <tt>ac.uk.</tt>:</p>
<pre>
cant.ac.uk.             300     IN      DNAME   canterbury.ac.uk.
king.ac.uk.             28800   IN      DNAME   kingston.ac.uk.
sund.ac.uk.             3600    IN      DNAME   sunderland.ac.uk.
oxford-brookes.ac.uk.   28800   IN      DNAME   brookes.ac.uk.
oxfordbrookes.ac.uk.    28800   IN      DNAME   brookes.ac.uk.
</pre>
<p>Cambridge's chief hostmaster Chris Thompson pointed out to me that there is currently one top-level domain with an apex DNAME record, using it for variant spellings of internationalized domain names as David Blacka described:</p>
<pre>
xn--kprw13d.		86293	IN	DNAME	xn--kpry57d.
</pre>
<p>De-punycoded, this aliases everything under 台湾 to the corresponding name under 台灣, which are respectively simplified and traditional Chinese for Taiwan.</p>

<p>At Cambridge we are using DNAME to consolidate 128 reverse DNS
domains, <tt><i>{128-255}</i>.232.128.in-addr.arpa</tt>, into a single
reverse zone <tt>in-addr.arpa.cam.ac.uk</tt>. The class B IP address
block 128.232.0.0/16 is delegated to the Computer Laboratory which has
in turn delegated the top half 128.232.128.0/17 to the Computing
Service for use by the rest of the University. The DNAME trick
slightly simplifies the Computer Lab's reverse zone, and massively
reduces the number of zones that the Computing Service has to run. It
is essentially classless reverse DNS for large CIDR blocks.</p>

<p>This is almost exactly what David Blacka calls the "canonical use"
for DNAME. However all is not sweetness and light. We have found that
DNAME in the reverse DNS causes occasional interoperability problems.
There are two cases I know of, both of which are due to software that
strictly checks DNS packet syntax and is upset by unexpected DNAME
RRs.</p>

<ul>
<li>

<p>The University Press's mail exchangers have IP addresses in
128.232.233.0/24, in our DNAME range. They were having problems
getting mail through to Comcast's mail servers, which dropped
connections from the Press with a 421 temporary error because of a
"Reverse DNS failure".</p>

<p>Because of this, that reverse DNS block contains 256 CNAME records
instead of one DNAME record.</p>

<li>

<p>The
<a href="http://sourceware.org/git/?p=glibc.git;a=blob;f=resolv/gethnamaddr.c">
glibc resolver code</a>
bleats into syslog whenever it encounters unexpected RR types, including DNAME.
The message it logs comes from the <tt>AskedForGot()</tt> macro on line 98.
In fact the glibc code is disgracefully out of date and poorly
maintained: for instance, it has some ancient support for skipping
DNSSEC records, but it doesn't know about the DNSSEC-bis RR types
introduced in 2004 with RFC 3755.</p>

<p>This is mostly benign, apart from putting a lot of unnecessary
noise in the system logs.</p>

</ul>

<p>I expect that any serious attempt to use DNAME in the forward DNS
will encounter many more interop problems, especially with MTAs (which
often have custom resolver code to deal with MX records) and crappy
DNS proxies in consumer routers and captive portals. A quick Google
fails to find anything on the topic published by the four universities
I listed above. Has anyone else published their experiences?</p>

<p><i>ETA</i>: Doug Barton reminded me of the other proposals that had
been suggested to support IDN variants. They avoid DNAME's interop problems
and somewhat reinforce David Blacka's argument that DNAME is useless.
The most straightforward suggestion was that no protocol support is needed,
if you add zone clone support to master servers. However this doesn't make
it easier to provision cloned zones on slave servers. Doug made a more
sophisticated proposal for a
<a href="http://tools.ietf.org/html/draft-barton-clone-dns-labels-fun-profit">
CLONES RR</a> which allows authoritative servers to auto-provision
alias zones, and allows clued-up resolvers to avoid duplicate cache entries
for a zone and its clones.</p>
