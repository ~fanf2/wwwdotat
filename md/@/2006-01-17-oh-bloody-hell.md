---
dw:
  anum: 251
  eventtime: "2006-01-17T16:18:00Z"
  itemid: 180
  logtime: "2006-01-17T16:35:51Z"
  props:
    commentalter: 1491292329
    import_source: livejournal.com/fanf/46433
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/46331.html"
format: casual
lj:
  anum: 97
  can_comment: 1
  ditemid: 46433
  event_timestamp: 1137514680
  eventtime: "2006-01-17T16:18:00Z"
  itemid: 181
  logtime: "2006-01-17T16:35:51Z"
  props: {}
  reply_count: 6
  url: "https://fanf.livejournal.com/46433.html"
title: Oh bloody hell
...

I just got a phone call as a follow-up to today's IT Syndicate meeting. This was the meeting at which my paper on the Chat service was presented. I have been asked to give a talk to the IT Syndicate Technical Committee in two weeks to "enlighten them about Jabber", whatever that means. I've asked them to give me some specific questions they would like answered or to indicate which parts of my briefing paper that they would like me to expand on - I don't know if they want a speaking-to-managers or a speaking-to-techies talk.

But in any case, Bah! and Faugh! How long does this have to take? This started as a skunk works project in October, and I've now been waiting nearly three months to get permission to put <tt>_xmpp-{client,server}._tcp.cam.ac.uk</tt> SRV records in the DNS.

<b>Update:</b> Looks like it'll be a speaking-to-techies talk, probably including a protocol overview and stuff like that.
