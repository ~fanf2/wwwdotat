---
dw:
  anum: 26
  eventtime: "2009-08-11T21:32:00Z"
  itemid: 392
  logtime: "2009-08-11T21:05:41Z"
  props:
    commentalter: 1491292380
    import_source: livejournal.com/fanf/101898
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/100378.html"
format: html
lj:
  anum: 10
  can_comment: 1
  ditemid: 101898
  event_timestamp: 1250026320
  eventtime: "2009-08-11T21:32:00Z"
  itemid: 398
  logtime: "2009-08-11T21:05:41Z"
  props:
    personifi_tags: "2:8,17:12,6:4,8:2,27:6,33:8,42:6,1:2,32:25,3:21,19:14,4:14,20:34,9:23,14:4,nterms:yes"
  reply_count: 11
  url: "https://fanf.livejournal.com/101898.html"
title: Duplex printers
...

<p>I'm annoyed by how slow our office printer is at printing on both sides of the paper. It occurs to me that it ought to be possible to make it much faster (a similar speed to single-sided printing) without too much extra complexity.</p>

<p>At the moment it handles one sheet at a time, in the sequence feed / print / reverse / print / eject. The feed and eject phases can be overlapped but there's otherwise little opportunity for pipelining.</p>

<p>If the duplex mechanism includes a paper path that goes from the print mechanism's exit to its entrance while turning over the sheet, then the path through the printer can be one-way (except inside the duplexer) and the duplexer can operate at the same time as the printer. The whole thing can then handle two sheets at once. The sequence goes like this:</p>

<ul>
<li>feed sheet 1
<li>print page 2 on sheet 1
<li>sheet 1 into duplexer / feed sheet 2
<li>sheet 1 through duplexer / print page 4 on sheet 2
<li>sheet 1 into printer / sheet 2 into duplexer
<li>print page 1 on sheet 1 / sheet 2 through duplexer
<li>sheet 1 exits / sheet 2 into printer
<li>print page 3 on sheet 2
<li>sheet 2 exits / feed sheet 3
<li>...
</ul>

<p>I have assumed a C-shaped or S-shaped paper path, where the printer prints on the top of the sheet which turns over before dropping into the hopper, so that you end up with the stack of paper face down in the correct order. Our printer's duplex mode presumably prints pages in even/odd order (i.e. page 2 then page 1 on sheet 1, page 4 then page 3 on  sheet 2, etc.) so the output stack still ends up in the right order. My duplexer's 2/4/1/3 page ordering puts greater demands on the RIPper's memory but that shouldn't be a big deal these days. (On the other hand, why does our printer appear to wait while RIPping? Shouldn't that be instant nowadays?)</p>

<p>So I wonder if anyone makes a printer with a duplexer that works this way. It should be fairly obvious from the duplex / duplex / exit / exit rhythm.</p>
