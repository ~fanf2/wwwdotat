---
dw:
  anum: 111
  eventtime: "2006-01-19T11:52:00Z"
  itemid: 184
  logtime: "2006-01-19T14:38:59Z"
  props:
    commentalter: 1491292328
    import_source: livejournal.com/fanf/47499
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/47215.html"
format: casual
lj:
  anum: 139
  can_comment: 1
  ditemid: 47499
  event_timestamp: 1137671520
  eventtime: "2006-01-19T11:52:00Z"
  itemid: 185
  logtime: "2006-01-19T14:38:59Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/47499.html"
title: Smoothed leap seconds
...

http://www.ietf.org/internet-drafts/draft-kuhn-leapsecond-00.txt

<a href="http://www.cl.cam.ac.uk/~mgk25/">&lt;cam user=mgk25&gt;</a> has written up his <a href="http://www.cl.cam.ac.uk/~mgk25/time/utc-sls/">smoothed leap seconds</a> proposal as an Internet-Draft. This idea, which he originally described in October 2000, is a suggestion for reconciling POSIX time and UTC with a minimum of nasty side-effects.

The essential problem is that the two are incompatible: POSIX says that each day has exactly 86400 seconds, but UTC says a day may have 86399, 86400, or 86401 seconds. This isn't just a POSIX problem: it isn't possible to accurately model UTC with only a count of seconds no matter how you fudge it. Consider the difference between UTC and TAI, i.e. atomic time with and without leap seconds. They are both based on the same seconds, so they will both be represented the same way if you simply count those seconds. The count omits any information about which seconds are leap seconds, so it needs some additional information to be able to model UTC. POSIX leaves no space for this additional information in its basic time APIs.

Markus's fudge is fairly well thought through, and since practicalities force us to have some kind of fudge, his is probably the best. However I can't stop being irritated that this is a fudge on top of a fudge, and if you remove the underlying fudge they both become unnecessary.

The underlying fudge is UTC itself, which is also trying to reconcile two incompatible standards: atomic time and astronomical time. The result is a rather complicated timescale which makes it hard to do common human-scale time-related computations, because it exposes some of the complexity of time synchronization to all the higher levels instead of encapsulating it. Ugh.

I'm a big fan of the effort to abolish leap seconds and instead use pure atomic time for civil time. Sub-second synchronization with the rotation of the earth isn't necessary for civil time-keeping tasks, as you can tell from the width of the time zones and their twice-annual adjustments. If atomic time gets too far from synchronization with astronomical time - maybe it'll be an hour out in a few thousand years - we can easily adjust the time zones, which is a common and simple operation (we do it twice a year). But I also appreciate Markus's pessimism about the likelihood of this abolition succeeding, and his defensive engineering in anticipation of the failure.

As a quick example of the problems caused by leap seconds, here's nice bit of code for converting a Gregorian date into a Modified Julian Day number:
<pre>
	int mjd(int y, int m, int d) {
		y -= m < 3;
		m += m < 3 ? 10 : -2;
		return y/400 - y/100 + y/4 + y*365
		    + m*367/12 + d - 678912;
	}
</pre>
You can easily adapt this to produce POSIX time values, by adjusting the epoch and multiplying by 86400. If you want to handle leap seconds, however, you have to add code to read a table of leap seconds, find the date in the table, and adjust the result accordingly. It all becomes at least ten times longer and a hundred times less portable.
