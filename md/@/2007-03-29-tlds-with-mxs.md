---
dw:
  anum: 30
  eventtime: "2007-03-29T14:32:00Z"
  itemid: 282
  logtime: "2007-03-29T14:59:52Z"
  props:
    commentalter: 1491292418
    import_source: livejournal.com/fanf/72486
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/72222.html"
format: html
lj:
  anum: 38
  can_comment: 1
  ditemid: 72486
  event_timestamp: 1175178720
  eventtime: "2007-03-29T14:32:00Z"
  itemid: 283
  logtime: "2007-03-29T14:59:52Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 6
  url: "https://fanf.livejournal.com/72486.html"
title: TLDs with MXs
...

<p>There's currently <a href="http://www.imc.org/ietf-smtp/mail-archive/msg02703.html">a discussion on the IETF SMTP mailing list</a> about the exact syntax of the domain part of an email address, since the cleaned-up ABNF in RFC 2821 unintentionally forbade domains with a single label. The argument is how to fix this error, whether the discrepancy between DNS syntax (optional trailing dot for FQDNs) and SMTP syntax (no trailing dot ever) matters, and whether email to a bare top-level domain makes any sense.</p>

<p><a href="http://www.oryx.com/arnt/">Arnt Gulbrandsen</a> noticed that a surprisingly large number of TLDs have MX records, so I thought it might be interesting to list all 26 of them...</p>

<pre>
$ dig axfr . @f.root-servers.net. |
  sed 's/^V^I/ /g;/\..* NS .*/!d;s///' |
  uniq | tr A-Z a-z |
  while read d; do
    dig mx $d. |
    sed 's/^V^I/ /g;/\..* MX /!d;s//. MX /' |
    grep . && whois -h whois.iana.org $d |
    sed '/Country:/!d;s///' | uniq;
  done |
  manual-fixup

ac. A                              ; not listening
     Ascension Islands
ai. MX 10 mail.offshore.ai.        ; seems to work <vince@ai> (sendmail/ubuntu "No UCE/UBE")
     Anguilla
as. MX 10 dca.relay.gdns.net.      ; accepts anything (sendmail)
as. MX 20 jfk.relay.gdns.net.      ; accepts anything (sendmail)
as. MX 30 lhr.relay.gdns.net.      ; accepts anything (sendmail)
     American Samoa
bi. A                              ; not listening
     Burundi
bj. MX 10 bow.intnet.bj.           ; accepts anything (postfix)
bj. MX 20 bow.rain.fr.             ; not listening
     Benin
cf. MX 10 bow.intnet.cf.           ; relaying denied (sendmail)
     Central African Republic
cm. A                              ; not listening
     Cameroon
cx. MX 5 mail.nic.cx.              ; relaying denied (exim/@mail)
     Christmas Island
dj. MX 5 smtp.intnet.dj.           ; relaying denied (trend anti-virus)
dj. MX 10 bow.intnet.dj.           ; not listening
dj. MX 20 bow.rain.fr.             ; not listening
     Djibouti
dk. A                              ; rejects everything (postfix)
     Denmark
dm. MX 10 mail.nic.dm.             ; relaying denied (imail)
     Dominica
gp. MX 5 ns1.nic.gp.               ; relaying denied (sendmail "I enjoy loveletters")
gp. MX 10 ns34259.ovh.net.         ; relaying denied (qmail)
gp. MX 20 manta.outremer.com.      ; not listening
     Guadeloupe
gt. MX 10 mail.gt.                 ; not listening
     Guatemala
hr. MX 10 alpha.carnet.hr.         ; accepts anything (exim/debian)
     Croatia/Hrvatska
im. MX 10 mail.advsys.co.uk.       ; relaying denied (msexchange)
im. A                              ; ignored
     Isle of Man
io. MX 10 mailer2.io.              ; seems to work <paul@io> (sendmail)
io. A                              ; ignored
     British Indian Ocean Territory
kh. MX 10 ns1.dns.net.kh.          ; not listening
     Cambodia
km. MX 100 mail1.comorestelecom.km.; relaying denied (sendmail)
km. MX 110 bow.snpt.km.            ; no such host
     Comoros                       ; SOA contains <root@km>
mh. MX 10 imap.pwke.twtelecom.net. ; accepts anything (?)
mh. MX 20 mx1.mail.twtelecom.net.  ; relay denied (postfix)
mh. MX 30 mx2.mail.twtelecom.net.  ; relay denied (postfix)
     Marshall Islands
mq. MX 10 mx.mediaserv.net.        ; domain not fully-qualified (postfix/debian)
     Martinique
ne. MX 10 bow.intnet.ne.           ; relaying denied ("*****")
ne. MX 20 bow.rain.fr.             ; not listening
     Niger
ni. MX 0 ns2.ni.                   ; relaying denied (sendmail)
ni. MX 10 ns.ni.                   ; not listening
     Nicaragua
pa. MX 5 ns.pa.                    ; relaying denied
     Panama
ph. A                              ; not listening
     Philippines
pn. A                              ; rejects everything (exim)
     Pitcairn
pw. A                              ; relaying denied (sendmail)
     Palau
sh. A                              ; not listening
     St Helena
td. MX 10 mail.intnet.td.          ; relaying denied (sendmail)
td. MX 20 bow.rain.fr.             ; not listening
     Chad
tk. MX 100 ATAFU.TALOHA.tk.        ; seems to work <joost@tk> (postfix)
     Tokelau
tl. MX 5 mail.nic.tl.              ; relaying denied (exim/@mail)
     Timor-Leste
tm. A                              ; not listening
     Turkmenistan
tt. MX 0 66-27-54-138.san.rr.com.  ; seems to work <admin@tt> (sendmail)
tt. MX 10 66-27-54-142.san.rr.com. ; relaying denied (postfix/ubuntu)
     Trinidad and Tobago
ua. MX 5 km.nic.net.ua.            ; seems to work <hostmaster@ua> (sendmail)
ua. MX 20 mx1.he.kolo.net.         ; seems to work (sendmail)
     Ukraine
uz. A                              ; not listening
     Uzbekistan
va. MX 10 lists.vatican.va.        ; accepts anything (postfix)
va. MX 20 paul.vatican.va.         ; not listening
va. MX 50 proxy2.urbe.it.          ; relaying denied (qmail)
va. MX 90 john.vatican.va.         ; not listening
     Vatican
ws. MX 10 mail.worldsite.ws.       ; relaying denied ("*****")
ws. A                              ; ignored
     Samoa
</pre>
