---
format: html
lj:
  anum: 82
  can_comment: 1
  ditemid: 125010
  event_timestamp: 1359496260
  eventtime: "2013-01-29T21:51:00Z"
  itemid: 488
  logtime: "2013-01-29T21:51:25Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 6
  url: "https://fanf.livejournal.com/125010.html"
title: Cambridge college domain names
...

<p>My friend Janet recently posted an article about <a href="http://theshapeofthings.wordpress.com/2013/01/29/a-name-thats-particular/">the peculiarities of Oxford college domain names</a>. Cambridge has a similar amount of semi-random hysterical raisin difference between its college domain names, so I thought I would enumerate them in a similar way to Janet.

<p>Like Oxford, we have two kinds of colleges, but our kinds are different. We have colleges of the University and colleges in the Cambridge theological federation. The latter are not formally part of the University in the way that the other colleges are, but they are closely affiliated with the University Faculty of Divinity, and the University Computing Service is their ISP.  There are also a number of non-collegiate institutions in the theological federation. I've included MBIT in the stats because it's interesting even though it's very borderline - a semi-college, let's say.</p>

<p>The Computing Service's governing syndicate allows colleges (and University departments) to have long domain names if their abbreviated version is considered by them to be too ugly. It's surprising how few of them have taken this up.</p>

<p>The following summary numbers count a few colleges more than once, if they have more than one domain name, or if they are a hall, etc. University colleges: 31; Theological colleges: 4.5; Straightforward (by Janet's criteria): 15.5; Initials: 0.5; First three letters: 7; First four letters: 5; Middle three letters: 2; Some other abbreviation: 5; Long + short: 3: Halls with hall: 3; Halls without hall: 2.</p>

<p>It is perhaps amusing to observe where the Oxbridge colleges with similar names differ in their domain labels in either place - see <a href="http://fanf.livejournal.com/125317.html">my next article</a>. I have previously written about <a href="http://fanf.livejournal.com/89717.html">colleges with more-or-less similar names in Oxford and Cambridge</a>, in which I list many of the less obvious formal expansions of the colleges' common names.</p>

<p>So, the Colleges of the University of Cambridge (and their domain names):</p>

<dl>
<dt>Christ's College
<dd>http://www.christs.cam.ac.uk/
<dt>Churchill College
<dd>http://www.chu.cam.ac.uk/
<dt>Clare College
<dd>http://www.clare.cam.ac.uk/
<dt>Clare Hall
<dd>http://www.clarehall.cam.ac.uk/
<dt>Corpus Christi College
<dd>http://www.corpus.cam.ac.uk/
<dt>Darwin College
<dd>http://www.darwin.cam.ac.uk/
<dd>http://www.dar.cam.ac.uk/
<dt>Downing College
<dd>http://www.dow.cam.ac.uk/
<dt>Emmanuel College
<dd>http://www.emma.cam.ac.uk/
<dt>Fitzwilliam College
<dd>http://www.fitz.cam.ac.uk/
<dt>Girton College
<dd>http://www.girton.cam.ac.uk/
<dt>Gonville & Caius College
<dd>http://www.cai.cam.ac.uk/
<dt>Homerton College
<dd>http://www.homerton.cam.ac.uk/
<dt>Hughes Hall
<dd>http://www.hughes.cam.ac.uk/
<dt>Jesus College
<dd>http://www.jesus.cam.ac.uk/
<dt>King's College
<dd>http://www.kings.cam.ac.uk/
<dt>Lucy Cavendish College
<dd>http://www.lucy-cav.cam.ac.uk/
<dt>Magdalene College
<dd>http://www.magd.cam.ac.uk/
<dt>Murray Edwards College (formerly known as New Hall)
<dd>http://www.murrayedwards.cam.ac.uk/
<dd>http://www.newhall.cam.ac.uk/
<dt>Newnham College
<dd>http://www.newn.cam.ac.uk/
<dt>Pembroke College
<dd>http://www.pem.cam.ac.uk/
<dt>Peterhouse
<dd>http://www.pet.cam.ac.uk/
<dt>Queens' College
<dd>http://www.queens.cam.ac.uk/
<dd>http://www.quns.cam.ac.uk/
<dt>Robinson College
<dd>http://www.robinson.cam.ac.uk/
<dt>Selwyn College
<dd>http://www.sel.cam.ac.uk/
<dt>Sidney Sussex College
<dd>http://www.sid.cam.ac.uk/
<dt>St Catharine's College
<dd>http://www.caths.cam.ac.uk/
<dt>St Edmund's College
<dd>http://www.st-edmunds.cam.ac.uk/
<dt>St John's College
<dd>http://www.joh.cam.ac.uk/
<dt>Trinity College
<dd>http://www.trin.cam.ac.uk/
<dt>Trinity Hall
<dd>http://www.trinhall.cam.ac.uk/
<dt>Wolfson College
<dd>http://www.wolfson.cam.ac.uk/
</dl>

<p>And the theological colleges:</p>

<dl>
<dt>Margaret Beaufort Institute for Theology
<dd>http://www.margaretbeaufort.cam.ac.uk/
<dd>http://www.mbit.cam.ac.uk/
<dt>Ridley Hall
<dd>http://www.ridley.cam.ac.uk/
<dt>Wesley House
<dd>http://www.wesley.cam.ac.uk/
<dt>Westcott House
<dd>http://www.Westcott.cam.ac.uk/
<dt>Westminster College
<dd>http://www.westminster.cam.ac.uk/
</dl>
