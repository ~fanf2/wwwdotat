---
dw:
  anum: 210
  eventtime: "2005-01-16T20:58:00Z"
  itemid: 125
  logtime: "2005-01-16T21:13:56Z"
  props:
    commentalter: 1491292319
    import_source: livejournal.com/fanf/32444
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/32210.html"
format: casual
lj:
  anum: 188
  can_comment: 1
  ditemid: 32444
  event_timestamp: 1105909080
  eventtime: "2005-01-16T20:58:00Z"
  itemid: 126
  logtime: "2005-01-16T21:13:56Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/32444.html"
title: UNIX or not?
...

http://www.google.com/search?q=apple+%22open+group%22+unix+trademark

Unsurprisingly, given Apple's <a href="http://www.apple.com/macosx/features/unix/">marketing of OSX</a> and its lack of <a href="http://www.opengroup.org/certification/registers.html">UNIX certification</a>, the Open Group (keepers of the UNIX trademark) sued Apple for trademark infringement at the beginning of 2002. The most recent news that I can find about this argument was April 2004, at which point the parties were still in dispute with two months left before the court's deadline.

However TOG were making conciliatory noises, and given that Apple is now the largest UNIX vendor it would have been foolish of them to go to court. If Apple won, it would have been bad for TOG and UNIX certification, and worrying for the status of UNIX standardization. However it would have been good for UNIX and Linux marketing because it would allow market analysts to group them together where they belong, <a href="http://www.oreillynet.com/manila/tim/stories/storyReader$56">ahead of Windows</a>.

So what happened? Anyone know?
