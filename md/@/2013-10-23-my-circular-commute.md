---
dw:
  anum: 166
  eventtime: "2013-10-23T14:43:00Z"
  itemid: 396
  logtime: "2013-10-23T13:43:36Z"
  props:
    commentalter: 1491292405
    import_source: livejournal.com/fanf/128605
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/101542.html"
format: html
lj:
  anum: 93
  can_comment: 1
  ditemid: 128605
  event_timestamp: 1382539380
  eventtime: "2013-10-23T14:43:00Z"
  itemid: 502
  logtime: "2013-10-23T13:43:36Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 7
  url: "https://fanf.livejournal.com/128605.html"
title: My circular commute
...

<p>We moved to new offices a month ago and I have settled in to the new route to work. Nico's nursery was a bit out of the way when I was working in the city centre, but now it is about a mile in the wrong direction.

<p>But this is not so bad, since I have decided on a -Ofun route [1] which is almost entirely on cycle paths and very quiet roads, and the main on-road section has good cycle lanes (at least by UK standards). There is lots of park land, a bit of open country, and it goes through the world heritage site in the city centre :-) And it's fairly easy to stop off on the way if I need to get supplies.

<p><span style="font-size: 0.5em">[1] optimize for fun!</span>

<p><a href="http://www.gmap-pedometer.com/?r=6119944">My route to work on gmap-pedometer</a>

<p>I don't have to wrangle children on the way home, so I take the direct route past the Institute of Astronomy and Greenwich House (where Rachel previously worked and where the Royal Greenwich Observatory was wound down).

<p><a href="http://www.gmap-pedometer.com/?r=6119948">My route home on gmap-pedometer</a>

<p>So far it has been pleasantly free of the adrenaline spikes I get from seeing murderous and/or suicidally stupid behaviour. Much better than going along Chesterton Lane and Madingley Road!
