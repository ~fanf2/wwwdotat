---
dw:
  anum: 38
  eventtime: "2005-09-16T14:31:00Z"
  itemid: 149
  logtime: "2005-09-16T15:48:25Z"
  props:
    import_source: livejournal.com/fanf/38546
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/38182.html"
format: casual
lj:
  anum: 146
  can_comment: 1
  ditemid: 38546
  event_timestamp: 1126881060
  eventtime: "2005-09-16T14:31:00Z"
  itemid: 150
  logtime: "2005-09-16T15:48:25Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/38546.html"
title: Subtle hackery
...

One of the few remnants of the old JANET grey book email system that we still support is abbreviated email domains such as in <code>&lt;fanf2@cam&gt;</code>. Exim has two configuration options which we use to implement this: <code>widen_domains = cam.ac.uk : ac.uk</code> and <code>rewrite_headers</code>.

When someone sends email to an abbreviated address, Exim does not recognize the domain as being one of the ones it is explicitly configured to handle, so looks it up in the DNS. This DNS lookup fails, so Exim tries appending each of the <code>widen_domains</code> in turn and re-doing the DNS lookup. Thus <code>@hermes</code> becomes <code>@hermes.cam.ac.uk</code>, which works, and <code>@cam</code> becomes <code>@cam.cam.ac.uk</code>, which doesn't, then <code>@cam.ac.uk</code>, which does.

Abbreviated domains are not valid on the public Internet, so they must be replaced by their un-abbreviated equivalents before the message leaves ppswitch. Email messages have two sets of metadata: the envelope, which determines who receives the message and who gets any delivery failure reports (i.e. the sender); and the header, which is what the user sees. [1] Since Exim is concerned with getting messages from A to B, it generally only looks at the envelope, and leaves the header alone. However when the envelope contains an abbreviated address there is probably a copy of the address in the header, so Exim must fix up both of them. This is specified by the <code>rewrite_headers</code> option.

<small>[1] The two do not necessarily agree: for example, a message sent via a mailing list has the list's address in its To: header, but the list members' addresses in the envelope. However, when a message is submitted the initial envelope is created from the addresses that the message's author specified in the header, so the two sets of addresses start off the same.</small>

That is all well and good, but there are some subtleties. Widening an abbreviated domain is a side-effect of the address being run through Exim's routers. Routing happens in two situations: when Exim is verifying addresses in order to decide if the message is acceptable; and when Exim is working out where to deliver the message after it has been accepted. The <code>rewrite_headers</code> option only has an effect in the latter case, because in the former case Exim has not received the message header yet. (Even if the header had been received, Exim doesn't record these header fix-ups persistently but instead works them out again whenever necessary.)

This is not a problem for recipient addresses, because these are all run through the routers at delivery time. However the sender address is not, because it is not necessary to work out the sender's destination in order to deliver the message to the recipients. The sender address is only run through the routers in order to verify it before the message is accepted. Therefore any fix-ups that may result from expanding the sender address do not happen!

Fortunately no-one configures their software to use abbreviated sender addresses, so we can safely prevent people from making this configuration error and thereby emitting broken messages, without causing disruption. This required a somewhat subtle change to Exim's source code. The idea is to ignore the <code>widen_domains</code> option when verifying a sender address, so that an abbreviated domain remains unrecognized and the address fails to verify, causing the message to be rejected. However if the postmaster has turned off <code>rewrite_headers</code> (for example, because they are working in a walled garden where it is OK for abbreviated domains to propagate) then the original problem doesn't obtain, so <code>widen_domains</code> should not be ignored.

Even more subtly, abbreviated addresses can appear in aliases files, for example <code>&lt;fanf2@ucs.cam.ac.uk&gt;</code> -> <code>&lt;fanf2@cam&gt;</code> which means that if I send a message using the alias, Exim will generate the abbreviated address as a result of sender verification. In this situation, the abbreviated address does not appear in the message header, so it does not need to be fixed up, so it should be permitted. Thus, we ignore <code>widen_domains</code> if we are verifying a sender address, but not if <code>rewrite_headers = false</code>, and only if this is the original sender address (not the result of alias expansion).

I didn't work out the latter "even more subtle" part of this fix until after I had rolled out my new Exim configuration yesterday and after I had received a bug report from a computer officer whose webserver's email was being bounced. Oops! I'll have to try the roll-out again on Monday...
