---
dw:
  anum: 54
  eventtime: "2020-08-27T22:30:00Z"
  itemid: 516
  logtime: "2020-08-27T22:18:09Z"
  props:
    commentalter: 1598642347
    interface: flat
    picture_keyword: ""
    revnum: 1
    revtime: 1598567013
  url: "https://fanf.dreamwidth.org/132150.html"
format: md
...

21st century lighting: LED tubes
================================

_AKA 2020 vision sorry not sorry_

The kitchen is the last room in our house to get modern lighting. We probably still have some compact fluorescents around the place, but we are mostly on LED bulbs - except the kitchen which was firmly stuck in the previous century.

The ceiling had a 1.5m fluorescent tube, and under the cupboards were incandescent strip lights. Incandescent! Ugh! Last time I bought replacements was some years ago, about when LEDs were becoming standard for bulbs, and although I looked around, there did not seem to be modern lamps for these linear fittings.

But now lighting suppliers are LED everywhere! However it is not quite trivially plug and play, at least for big fluorescent tube fittings. (Choosing the small tubes was not much more difficult than getting incandescents used to be.)

Colour temperature
------------------

Instead of choosing the wattage of our incandescents we now choose the colour temperature of our LEDs. I had three kinds of fitting to populate:

* The big fluorescent tube: I wasn’t really aware there was much choice before; this time I went for the default 4000K, often described as “soft white”, which looks basically the same as it used to.

* The under-cupboard lights: somewhat experimentally I thought I would go for 6000K “daylight” tubes, because I saw a suggestion that it’s a good choice for a work area. They are Quite Blue. If they ever die I think next time I will choose 4000K and (if possible) more watts. 

* Another small strip light above the mirror in the en suite bathroom: I got 2800K “warm white” because it gets used at night. But it also gets used for shaving so I got a brighter tube. So far this has worked out to be less self-contradictory than it might sound :-)

Lumens
------

The old fluorescent tube claimed 4600 lumens from 58W, whereas new LED tubes that are supposed to be substitutes generally claim about 2100 lumens from about 22W. After some searchengineering, I found out where the discrepancy in lumens comes from: fluorescent tubes emit light in all directions, but LED tubes typically emit through 150 degrees. So, the intensity of the direct light is about the same or a bit more, and there is a bit less indirect light reflected off the ceiling.

The new tube is definitely bright enough :-)

The Energizer tube I bought has an opaque diffuser round most of the tube and a solid white backside on the quarter of the tube that points at the fitting. I was tempted to get a tube with a transparent cover so you can see the LEDs (to make it blatantly obvious that this is not a gas-discharge lamp) but they were not clear enough about :—

Compatibility
-------------

It took me a while to get to grips with the compatibility issues, because a lot of the explanatory material out there seems to be aimed at office building technicians who are way more likely to be opening up their fluorescent light fittings than me, and the descriptions for the LED tubes themselves are often bad at explaining the compatibility requirements, and/or they don’t include the installation instructions.

There seem to be basically three kinds of LED tubes:

* Retrofit tubes: these want unadulterated mains, so you need to bypass the support circuitry that fluorescent tubes need (the ballast and starter).

* Compatible with electronic ballast: these tubes work in many office light fittings.

* Plug-and-play compatible with all fittings.

It was remarkably difficult to find a clear way to know if we have an electronic or magnetic ballast in our kitchen light. Some sites helpfully suggested taking a picture of the lit tube with a smartphone, to see if it looks stripy or not, which is no bloody use if the tube has died!

The answer I wanted was straightforward: if (like ours) the fitting has a little cylindrical starter, it has a magnetic ballast. (Electronic ballasts can start the tube by themselves without a separate replaceable component.)

My next question was whether I needed to worry about the starter. (It gives the tube a high voltage jolt when it turns on, so you can imagine an LED driver might not like that.) The answer is that usually you are expected to replace the starter with a “fuse”, a do-nothing bit of wire in a starter housing. LED tubes are sometimes supplied with the thing you need.

I saw hints that some tubes only need you to remove the starter and leave the hole empty, but in the end I chose an Energizer tube which is supplied with a suitable fuse, since that seemed to be least error-prone.

Sorted!
-------

Eventually the new tubes arrived. The small strip lights are glass like the old ones, but unlike an incandescent filament the LEDs are in a few long segments. They are better than 10x more efficient than the old incandescent ones, 30W to 2.5W.

The big tube is plastic, so it feels a bit lighter and less fragile than glass. It was covered in bubble wrap and cardboard for delivery, so it came in a cylinder that was at least 10x the diameter of the tube inside :-)

The last dead fluorescent tube has joined two others in the little mercury-contaminated toxic waste dump in our junk room. (Don't worry, the mercury is still inside the tubes!) We have collected them over the last 10+ years because they are way too much faff to take to the Milton recycling centre...
