---
dw:
  anum: 249
  eventtime: "2003-08-29T10:20:00Z"
  itemid: 32
  logtime: "2003-08-29T10:23:30Z"
  props:
    commentalter: 1491292305
    current_moodid: 15
    import_source: livejournal.com/fanf/8234
    interface: flat
    opt_backdated: 1
    picture_keyword: photo
    picture_mapid: 3
  url: "https://fanf.dreamwidth.org/8441.html"
format: casual
lj:
  anum: 42
  can_comment: 1
  ditemid: 8234
  event_timestamp: 1062152400
  eventtime: "2003-08-29T10:20:00Z"
  itemid: 32
  logtime: "2003-08-29T10:23:30Z"
  props:
    current_moodid: 15
  reply_count: 6
  url: "https://fanf.livejournal.com/8234.html"
title: "&lt;beer&gt;happiness&lt;/beer&gt;"
...

I just bought nine gallons of beer for my party tomorrow. It's a firkin of <a href="http://www.miltonbrewery.co.uk/electra.html">Milton Electra</a>. I've never bought that much beer in one go before...

Thanks very much to Peter Benie for ferrying me to and from the brewery to collect the beer.
