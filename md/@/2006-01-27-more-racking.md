---
dw:
  anum: 118
  eventtime: "2006-01-27T17:53:00Z"
  itemid: 190
  logtime: "2006-01-27T18:00:38Z"
  props:
    commentalter: 1491292328
    import_source: livejournal.com/fanf/48947
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/48758.html"
format: casual
lj:
  anum: 51
  can_comment: 1
  ditemid: 48947
  event_timestamp: 1138384380
  eventtime: "2006-01-27T17:53:00Z"
  itemid: 191
  logtime: "2006-01-27T18:00:38Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/48947.html"
title: More racking
...

Hate.

The 1U dual Opteron servers for the Chat service (which will be called wogan and parky) have arrived and we tried to rack them this afternoon. Their chassis is designed so that the lid is mounted in the rack, and the base (containing the guts of the machine) slides in and out. The tops have screw threads near the back to which a couple of L brackets attach which in turn tie the rear of the machine to the rack's rear verticals. These screw threads stick out enough to make the whole thing wider than the rack's verticals. Furthermore, the top has no slack within its nominal 1U which makes it near impossible to mount machines next to eah other.

TOTAL PAIN IN THE ARSE

We shall be having another go at them on Monday...
