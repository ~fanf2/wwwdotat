---
dw:
  anum: 76
  eventtime: "2006-01-25T17:06:00Z"
  itemid: 188
  logtime: "2006-01-25T17:09:45Z"
  props:
    commentalter: 1491292330
    import_source: livejournal.com/fanf/48457
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/48204.html"
format: casual
lj:
  anum: 73
  can_comment: 1
  ditemid: 48457
  event_timestamp: 1138208760
  eventtime: "2006-01-25T17:06:00Z"
  itemid: 189
  logtime: "2006-01-25T17:09:45Z"
  props: {}
  reply_count: 5
  url: "https://fanf.livejournal.com/48457.html"
title: Techlinks talk
...

http://www.cus.cam.ac.uk/~fanf2/hermes/doc/talks/2006-01-techlinks/

I did a talk this afternoon mostly about withdrawing insecure access to Hermes. Surprisingly few questions...

I did the slides using <a href="http://meyerweb.com/eric/tools/s5/">Eric Meyer's S5</a> which seems quite fun, though it suffers from the browser compatibility farce that plagues the web. (<a href="http://ptc24.livejournal.com/199828.html">see also...</a>)
