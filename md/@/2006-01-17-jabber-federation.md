---
dw:
  anum: 122
  eventtime: "2006-01-17T16:54:00Z"
  itemid: 181
  logtime: "2006-01-17T16:56:52Z"
  props:
    import_source: livejournal.com/fanf/46715
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/46458.html"
format: casual
lj:
  anum: 123
  can_comment: 1
  ditemid: 46715
  event_timestamp: 1137516840
  eventtime: "2006-01-17T16:54:00Z"
  itemid: 182
  logtime: "2006-01-17T16:56:52Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/46715.html"
title: Jabber federation
...

http://ralphm.net/blog/2006/01/17/gtalk_s2s

And in fact fanf@jabber.org and tony.finch@gmail.com can talk to each other quite happily.

However, when my dotat.at jabberd2 talks to Google, it completes dialback, then it responds to my first message or presence stanza with <tt>&lt;stream:error&gt; &lt;unsupported-stanza-type
xmlns="urn:ietf:params:xml:ns:xmpp-streams"/&gt; &lt;/stream:error&gt;</tt>
