---
dw:
  anum: 226
  eventtime: "2006-04-03T15:22:00Z"
  itemid: 215
  logtime: "2006-04-03T15:26:19Z"
  props:
    import_source: livejournal.com/fanf/55331
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/55266.html"
format: casual
lj:
  anum: 35
  can_comment: 1
  ditemid: 55331
  event_timestamp: 1144077720
  eventtime: "2006-04-03T15:22:00Z"
  itemid: 216
  logtime: "2006-04-03T15:26:19Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/55331.html"
title: More strategic visions
...

Forgot to mention these <a href="http://fanf.livejournal.com/55126.html">last time</a>:
<ul><li>Uniform handling of queries across the service: help-desk, postmaster,                                                                                                                                       webmaster, *-support. Access to support queues by all service staff.
<li>Webify managed mail domains interface. Perhaps integrate with MZS?
<li>Once MMDs and lists are fully webified, deprecate the Hermes terminal                                                                                                                                        interface and menu system in favour of PWF Linux. Benefits would include a                                                                                                                                   non-crippled Pine installation. We should move Pine configurations from                                                                                                                                      the local filesystem to the IMAP server first.
<li>More effective use of RSS/Atom. Could be a side-effect of a blogging/forum                                                                                                                                   service, or a veneer on our existing information feeds as in my quick hack http://canvas.csi.cam.ac.uk/atom/
</ul>
