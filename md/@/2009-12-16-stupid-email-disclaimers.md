---
format: html
lj:
  anum: 6
  can_comment: 1
  ditemid: 104966
  event_timestamp: 1260981900
  eventtime: "2009-12-16T16:45:00Z"
  itemid: 410
  logtime: "2009-12-16T16:45:39Z"
  props:
    personifi_tags: "15:15,18:3,8:42,11:3,57:3,23:7,42:3,1:26,3:7,25:3,19:23,26:3,9:42,nterms:no"
  reply_count: 3
  url: "https://fanf.livejournal.com/104966.html"
title: stupid email disclaimers
...

<p>Dear morons, I'm glad to know that this important marketing message is confidential, and that I shouldn't tell anyone about your branding change even though it's obvious because you are changing the signs on your buildings.</p>

<p>PS. if you are going to use an email service provider to send this shit, at least you could hire one that is able to delete your misleading disclaimers first.</p>

<pre>
Received: from mail.highford.com ([213.210.16.63]:53904)
    by ppsw-6.csi.cam.ac.uk (mx.cam.ac.uk [131.111.8.146]:25)
Message-Id: <CuRu/yJjeZx3Xvv8Q6i5Ew@hearfrom.com>
From: Bradford & Bingley <news@savings.bradford-bingley.co.uk>
To: Mr Finch <DOT@DOTAT.AT>
Subject: Update Bradford & Bingley Savings changing to Santander

Dear Mr Finch,

As of 11 January 2010 Abbey National plc which includes the
Bradford & Bingley savings business will change its name to
Santander UK plc and operate under the brand name of Santander.

[...]

This email and any files transmitted with it are confidential
and intended solely for the use of the individual or entity to
whom they are addressed. If you have received this email in
error please notify the system manager.

[...]
</pre>
