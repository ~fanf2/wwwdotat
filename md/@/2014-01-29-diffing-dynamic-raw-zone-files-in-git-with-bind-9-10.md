---
dw:
  anum: 171
  eventtime: "2014-01-29T22:15:00Z"
  itemid: 402
  logtime: "2014-01-29T22:15:49Z"
  props:
    commentalter: 1491292407
    import_source: livejournal.com/fanf/130165
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/103083.html"
format: html
lj:
  anum: 117
  can_comment: 1
  ditemid: 130165
  event_timestamp: 1391033700
  eventtime: "2014-01-29T22:15:00Z"
  itemid: 508
  logtime: "2014-01-29T22:15:49Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/130165.html"
title: Diffing dynamic raw zone files in git with BIND 9.10
...

<p>On my toy nameserver my master zones are configured with a
directory for each zone. In this directory is a "conf" file which is
included by the nameserver's main configuration file; a "master" file
containing the zone data in the raw binary format; a "journal" file
recording changes to the zone from dynamic UPDATEs and re-signing; and
DNSSEC and TSIG keys. The "conf" file looks something like this:

<pre>
    zone dotat.at {
        type master;
        file "/zd/dotat.at/master";
        journal "/zd/dotat.at/journal";
        key-directory "/zd/dotat.at";
        masterfile-format raw;
        auto-dnssec maintain;
        update-policy local;
    };
</pre>

<p>I must have been having a fit of excessive tidyness when I was
setting this up, because although it looks quite neat, the unusual
file names cause some irritation - particularly for the journal. Some
of the other BIND tools assume that journal filenames are the same as
the master file with a <tt>.jnl</tt> extension.

<p>I keep the name server configuration in git. This is a bit awkward
because the configuration contains precious secrets (DNSSEC private
keys), and the zone files are constantly-changing binary data. But it
is useful for recording manual changes, since the zone files don't
have comments explaining their contents. I don't make any effort to
record the re-signing churn, though I commit it when making other
changes.

<p>To reduce the awkwardness I configured git to convert zone files to
plain text when diffing them, so I had a more useful view of the
repository. There are three parts to setting this up.

<ul>
 <li>Tell git that the zone files require a special diff driver, which
  I gave the name "bind-raw".

  <p>All the zone files in the repository are called "master", so in
   the <tt>.gitattributes</tt> file at the top of the repository I
   have the line
   <pre>
    master diff=bind-raw
   </pre>

 <li>The diff driver is part of the repository configuration. (Because
  the implementation of the driver is a command it isn't safe to set
  it up automatically in a repository clone, as is the case
  for <tt>.gitattributes</tt> settings, so it has to be configured
  separately.) So add the following lines to <tt>.git/config</tt>

   <pre>
    [diff "bind-raw"]
	textconv = etc/raw-to-text
   </pre>

 <li>The final part is the <tt>raw-to-text</tt> script, which lives in
  the repository.
</ul>

<p>This is where journal file names get irritating. You can convert a
raw master file into the standard text format with
<tt>named-compilezone</tt>, which has a <tt>-j</tt> option to read the
zone's journal, but this assumes that the journal has the default file
name with the <tt>.jnl</tt> extension. So it doesn't quite work in my setup.

<p>(It also doesn't quite work on the University's name servers which
have a directory for master files and a directory for journal files.)

<p>So in September 2012 I patched BIND to add a <tt>-J</tt> option to
<tt>named-compilezone</tt> for specifying the journal file name. I
have a number of other small patches to my installation of BIND, and
this one was very simple, so in it went. (It would have been much more
sensible to change my nameserver configuration to go with the flow...)

<p>The patch allowed me to write the <tt>raw-to-text</tt> script as
follows. The script runs <tt>named-compilezone</tt> twice: the first
time with a bogus zone name, which causes <tt>named-compilezone</tt>
to choke with an error message. This helpfully contains the real zone
name, which the script extracts then uses to
invoke <tt>named-compilezone</tt> correctly.

  <pre>
    #!/bin/sh
    file="$*"
    command="named-compilezone -f raw -F text -J journal -o /dev/stdout"
    zone="$($command . "$file" 2>&1)"
    zone="${zone#*: }"
    zone="${zone%%:*}"
    $command "$zone" "$file" 2>/dev/null
  </pre>

<p>I submitted the <tt>-J</tt> patch to the ISC and got a favourable
response from Evan Hunt. At that time (16 months ago) BIND was at
version 9.9.2; since this option was a new feature (and unimportant)
it was added to the 9.10 branch. Wind forward 14 months to November
2013 and the first alpha release of 9.10 came out, with
the <tt>-J</tt> option, so I was able to retire my patch. Woo! There
will be a beta release of 9.10 in a few weeks.

<p>In truth, you don't need BIND 9.10 to use this git trick, if you
use the default journal file names. The key thing to make it simple is
to give all your master files the same name, so that you don't have to
list them all in your <tt>.gitattributes</tt>.
