---
dw:
  anum: 123
  eventtime: "2008-04-15T00:34:00Z"
  itemid: 334
  logtime: "2008-04-14T23:42:11Z"
  props:
    commentalter: 1491292352
    import_source: livejournal.com/fanf/86338
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/85627.html"
format: html
lj:
  anum: 66
  can_comment: 1
  ditemid: 86338
  event_timestamp: 1208219640
  eventtime: "2008-04-15T00:34:00Z"
  itemid: 337
  logtime: "2008-04-14T23:42:11Z"
  props: {}
  reply_count: 5
  url: "https://fanf.livejournal.com/86338.html"
title: Annoying things
...

<ul>
<li>An active PS/2 to USB keyboard adaptor that doesn't work with my old :CueCat, so I can't use the :cc with my Mac.</li>
<li>An old laptop PC with a flaky CDROM drive that refuses to read most OS install media, so I'm stuck with ancient FreeBSD and no wireless support until I can upgrade.</li>
<li>The :cc's stupid habit of typing Alt+F10 before the encoded barcode data, which occasionally gives Mozilla the heebie-jeebies or even accidentally kills it.</li>
<li>LibraryThing in overload mode refusing to let me log in again.</li>
</ul>
<p>On the other hand, one of my old bug reports reminded me <a href="http://www.freebsd.org/cgi/query-pr.cgi?pr=kern/26920">how to get a SHARP PC-AR10 working with FreeBSD-4.X</a> without much trouble, and when LT deigns to let me log in its import features are really easy to use.</p>
