---
dw:
  anum: 215
  eventtime: "2006-11-09T13:11:00Z"
  itemid: 262
  logtime: "2006-11-09T13:11:15Z"
  props:
    commentalter: 1491292387
    hasscreened: 1
    import_source: livejournal.com/fanf/67571
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/67287.html"
format: casual
lj:
  anum: 243
  can_comment: 1
  ditemid: 67571
  event_timestamp: 1163077860
  eventtime: "2006-11-09T13:11:00Z"
  itemid: 263
  logtime: "2006-11-09T13:11:15Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/67571.html"
title: SMTP checkpoint/resume
...

http://fanf.livejournal.com/53255.html

Back in February and March I drafted some SMTP extenstions that were intended to improve its performance and robustness. I've been revisiting this work recently because people from the <a href="http://www.ietf.org/html.charters/lemonade-charter.html">IETF Lemonade</a> working group have expressed interest in them.

Most of the work has been rewriting my REPLAY draft to make it closer to the checkpoint/restart extenstion specified in RFC 1845. I stripped out the excessive rationale sections, and described some of the protocol elements more carefully, and added a 1845 backwards compatibility mode.

https://fanf2.user.srcf.net/hermes/doc/qsmtp/draft-fanf-lemonade-rfc1845bis.html

However it looks like the Lemonade WG are more insteredred in QUICKSTART than in checkpoint/resume, so I've also made a few editorial updates to QUICKSTART, mostly updating references.

https://fanf2.user.srcf.net/hermes/doc/qsmtp/draft-fanf-smtp-quickstart.html
