---
dw:
  anum: 100
  eventtime: "2016-10-01T17:09:00Z"
  itemid: 469
  logtime: "2016-10-01T16:09:44Z"
  props:
    commentalter: 1491292420
    import_source: livejournal.com/fanf/147327
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/120164.html"
format: html
lj:
  anum: 127
  can_comment: 1
  ditemid: 147327
  event_timestamp: 1475341740
  eventtime: "2016-10-01T17:09:00Z"
  itemid: 575
  logtime: "2016-10-01T16:09:44Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 7
  url: "https://fanf.livejournal.com/147327.html"
title: Aperiodic shower curtain
...

<p>We have a periodic table shower curtain. It mostly uses Arial for
its lettering, as you can see from the "R" and "C" in the heading
below, though some of the lettering is Helvetica, like the "t" and "r"
in the smaller caption.</p>

<img src='http://dotat.at/graphics/periodic-table/IMG_0491_360.jpg'><br>
<a href='http://dotat.at/graphics/periodic-table/IMG_0491_1024.jpg'>(bigger)</a> <a href='http://dotat.at/graphics/periodic-table/IMG_0491.jpg'>(huge)</a><br>

<p>The lettering is very inconsistent. For instance, Chromium is set
in a lighter weight than other elements - compare it with Copper</p>

<table>
  <tr>
    <td><img src='http://dotat.at/graphics/periodic-table/IMG_0497_360.jpg'></td>
    <td><img src='http://dotat.at/graphics/periodic-table/IMG_0498_360.jpg'></td>
  </tr>
  <tr>
    <td><a href='http://dotat.at/graphics/periodic-table/IMG_0497_1024.jpg'>(bigger)</a> <a href='http://dotat.at/graphics/periodic-table/IMG_0497.jpg'>(huge)</a></td>
    <td><a href='http://dotat.at/graphics/periodic-table/IMG_0498_1024.jpg'>(bigger)</a> <a href='http://dotat.at/graphics/periodic-table/IMG_0498.jpg'>(huge)</a></td>
  </tr>
</table>

<p>Palladium is particularly special</p>

<img src='http://dotat.at/graphics/periodic-table/IMG_0496_360.jpg'><br>
<a href='http://dotat.at/graphics/periodic-table/IMG_0496_1024.jpg'>(bigger)</a> <a href='http://dotat.at/graphics/periodic-table/IMG_0496.jpg'>(huge)</a><br>

<p>Platinum is Helvetica but Meitnerium is Arial - note the tops of
the "t"s.</p>

<table>
  <tr>
    <td><img src='http://dotat.at/graphics/periodic-table/IMG_0495_360.jpg'></td>
    <td><img src='http://dotat.at/graphics/periodic-table/IMG_0494_360.jpg'></td>
  </tr>
  <tr>
    <td><a href='http://dotat.at/graphics/periodic-table/IMG_0495_1024.jpg'>(bigger)</a> <a href='http://dotat.at/graphics/periodic-table/IMG_0495.jpg'>(huge)</a></td>
    <td><a href='http://dotat.at/graphics/periodic-table/IMG_0494_1024.jpg'>(bigger)</a> <a href='http://dotat.at/graphics/periodic-table/IMG_0494.jpg'>(huge)</a></td>
  </tr>
</table>

<p>Roentgenium is all Arial; Rhenium has a Helvetica "R" but an Arial "e"!</p>

<table>
  <tr>
    <td><img src='http://dotat.at/graphics/periodic-table/IMG_0493_360.jpg'></td>
    <td><img src='http://dotat.at/graphics/periodic-table/IMG_0492_360.jpg'></td>
  </tr>
  <tr>
    <td><a href='http://dotat.at/graphics/periodic-table/IMG_0493_1024.jpg'>(bigger)</a> <a href='http://dotat.at/graphics/periodic-table/IMG_0493.jpg'>(huge)</a></td>
    <td><a href='http://dotat.at/graphics/periodic-table/IMG_0492_1024.jpg'>(bigger)</a> <a href='http://dotat.at/graphics/periodic-table/IMG_0492.jpg'>(huge)</a></td>
  </tr>
</table>

<p>It is a very distracting shower curtain.</p>
