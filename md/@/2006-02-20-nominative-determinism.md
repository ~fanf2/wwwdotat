---
dw:
  anum: 235
  eventtime: "2006-02-20T15:12:00Z"
  itemid: 200
  logtime: "2006-02-20T15:13:56Z"
  props:
    commentalter: 1491292329
    import_source: livejournal.com/fanf/51691
    interface: flat
    opt_backdated: 1
    picture_keyword: silly
    picture_mapid: 6
  url: "https://fanf.dreamwidth.org/51435.html"
format: casual
lj:
  anum: 235
  can_comment: 1
  ditemid: 51691
  event_timestamp: 1140448320
  eventtime: "2006-02-20T15:12:00Z"
  itemid: 201
  logtime: "2006-02-20T15:13:56Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/51691.html"
title: Nominative determinism
...

I just noticed that there's a Swedish Haskell researcher called <a href="http://web.it.kth.se/~lisper/">Björn Lisper</a>...
