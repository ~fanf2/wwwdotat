---
dw:
  anum: 31
  eventtime: "2003-03-28T01:03:00Z"
  itemid: 11
  logtime: "2003-03-27T17:19:03Z"
  props:
    commentalter: 1491292303
    current_moodid: 15
    import_source: livejournal.com/fanf/2827
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/2847.html"
format: casual
lj:
  anum: 11
  can_comment: 1
  ditemid: 2827
  event_timestamp: 1048813380
  eventtime: "2003-03-28T01:03:00Z"
  itemid: 11
  logtime: "2003-03-27T17:19:03Z"
  props:
    current_moodid: 15
  reply_count: 1
  url: "https://fanf.livejournal.com/2827.html"
title: Turned into a good evening
...

The new beta release of MailScanner includes most of my local patches which is always a gratifying thing. It turned up this afternoon so I spent a little time doing a merge which led into a little hacking. I think I have finally killed all the shell evilness which is good.

So I left for the pub feeling unuseless, and spent the evening <b>not narging</b> (<em>shock, horror</em>) [<code>markup++</code>]. Nice chats with <a href="https://simont.livejournal.com/">👤simont</a>, <a href="https://j4.livejournal.com/">👤j4</a>, <a href="https://hoiho.livejournal.com/">👤hoiho</a>.

With any luck I'll still be inclined to do cool stuff over the weekend, either ranting about statistical filtering, or hacking on kernel network stack features.

I'm reading very little fiction at the moment despite a large unread pile. My (escapist?) reading seems to vary inversely with productivity. Instead I'm reading academic papers full of equations containing big sigmas and pis. (pi's? pies? &pi;s?)

Mmm. pie.
