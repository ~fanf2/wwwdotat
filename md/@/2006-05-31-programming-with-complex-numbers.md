---
dw:
  anum: 45
  eventtime: "2006-05-31T10:14:00Z"
  itemid: 233
  logtime: "2006-05-31T09:26:29Z"
  props:
    commentalter: 1491292334
    import_source: livejournal.com/fanf/60040
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/59693.html"
format: casual
lj:
  anum: 136
  can_comment: 1
  ditemid: 60040
  event_timestamp: 1149070440
  eventtime: "2006-05-31T10:14:00Z"
  itemid: 234
  logtime: "2006-05-31T09:26:29Z"
  props: {}
  reply_count: 5
  url: "https://fanf.livejournal.com/60040.html"
title: Programming with complex numbers
...

http://www.cs.berkeley.edu/~wkahan/JAVAhurt.pdf

On pages 11-15 this rant goes off on a tangent about branch cuts in complex arithmetic. It distinguishes between C99-style complex numbers, which have a separate type for bare imaginary numbers, and Fortran-style complex numbers, which do not. The authors assert that it isn't possible for Fortran-style complex numbers to handle signed zeroes correctly and therefore that programs written to use this style of arithmetic necessarily get branch cuts wrong.

This seems unlikely to me, if the programmer has suitable facilities for constructing or decomposing complex numbers from or into two reals. I guess they must be over-simplifying.
