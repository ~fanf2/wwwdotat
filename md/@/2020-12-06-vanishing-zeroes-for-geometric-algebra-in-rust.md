---
dw:
  anum: 136
  eventtime: "2020-12-06T18:28:00Z"
  itemid: 523
  logtime: "2020-12-06T18:28:54Z"
  props:
    interface: flat
    picture_keyword: ""
    revnum: 4
    revtime: 1607443981
  url: "https://fanf.dreamwidth.org/134024.html"
format: html
title: Vanishing zeroes for geometric algebra in Rust
...

<p>These are some notes on what I have done with Rust over the last week,
in the course of doing something highly experimental that could well
turn out to be foolishly over-complicated. (I'll be interested to hear
about other ways of doing this, in other programming languages as well
as Rust!) But it involves some cool ideas - abstract interpretation,
type-level programming in Rust, automatically optimized data
structures, and a bit of geometric algebra for 3D graphics - and I
want to share what I learned.</p>

<p><strong>Health warning:</strong> <a href="https://fanf.dreamwidth.org/134310.html">this <em>did</em> turn out to be foolishly
over-complicated</a> - I suggest
reading the followup before wading in to the rest of these notes, if
you have not already read them.</p>

<cut>

<blockquote>
  <p><em>Aside: There are a lot of code snippets in these notes, and a
couple of Rust Playground links. The code is currently a
barely-working proof-of-concept with very few comments. These notes
are the first piece of documentation.</em></p>
</blockquote>

<h2>context</h2>

<p>Recently I made some animations (involving <a href="http://dotat.at/graphics/truchet.gif">Truchet tiles</a> and the
<a href="http://dotat.at/graphics/hilbert.gif">Hilbert curve</a>), but I was lazy and <a href="http://dotat.at/cgi/git/swirled-series.git/tree/refs/heads/trunk">hacked them together in
C</a>. Foolish! I missed a good opportunity to practise
coding in Rust.</p>

<p>For my next animation I will use Rust, and I want to do some 3D
graphics. I don't think I have done any 3D graphics since I was a
teenager, so I needed to brush up my knowledge and remind myself how
it works.</p>

<p>Back then the standard 3D graphics techniques were based on
<a href="https://en.wikipedia.org/wiki/Projective_geometry">projective geometry</a> and <a href="https://en.wikipedia.org/wiki/Homogeneous_coordinates">homogeneous coordinates</a>. I found out
that nowadays <a href="https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation">quaternions</a> are often preferred for doing rotations
instead of using a matrix. But even quaternions are being superseded.</p>

<h2>geometric algebra</h2>

<p>Geometric algebra is a framework that generalizes things like complex
numbers, quaternions, vectors, matrices, etc. It can be used to model
lots of things, only a few of which I understand: Euclidean and
non-Euclidean geometry, projective geometry, the geometry of
Einsteinian space-time, quantum mechanics, etc. I am going to use it
for 3D projective geometry, as described in <a href="https://slides.com/enkimute/siggraph/">these slides about
geometric algebra for computer graphics, from a tutorial at SIGGRAPH
2019</a>. You can find <a href="https://bivector.net/PROJECTIVE_GEOMETRIC_ALGEBRA.pdf">the SIGGRAPH course notes</a> and a
lot more at <a href="https://bivector.net/">biVector.net</a>. I also found <a href="http://www.euclideanspace.com/maths/algebra/clifford/">the
Euclidean Space website</a> and <a href="https://arxiv.org/abs/1205.5935">Eric Chisolm's
introduction</a> helpful. My friend and colleague Rich Wareham
did a PhD on <a href="https://rjw57.github.io/phd-thesis/rjw-thesis.pdf">geometric algebra for computer graphics</a>
including non-Euclidean and conformal geometry.</p>

<h2>multivectors</h2>

<p>These notes are more about Rust than about geometric algebra; but
geometric algebra is why I am doing wild things with Rust. The wild
things are an answer to the question of how to represent geometric
objects both elegantly and efficiently — but what makes them
inefficient or inelegant?</p>

<p>Geometric algebra works with objects called multivectors. In PGA3D
(projective geometric algebra for 3D graphics) a multivector has 5
parts:</p>

<ul>
<li><p>0: A scalar part, which is just a real number.</p></li>
<li><p>1: A vector part, consisting of four numbers, which in PGA3D
represents a plane or a rotation in that plane, like <a href="https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation">quaternions</a>.</p></li>
<li><p>2: A bivector part, consisting of six numbers, which in PGA3D
represents a line, like <a href="https://en.wikipedia.org/wiki/Pl%C3%BCcker_coordinates">Plücker coordinates</a>.</p></li>
<li><p>3: A trivector part, consisting of four numbers, which in PGA3D
represents a point or direction, like a vector in
<a href="https://en.wikipedia.org/wiki/Homogeneous_coordinates">homogeneous coordinates</a>.</p></li>
<li><p>4: A pseudoscalar part, like an imaginary number, which is used
for things like inversion.</p></li>
</ul>

<p>That's a total of 16 numbers, same as the kind of 4x4 transformation
matrix I used for 3D graphics when I was a teenager.</p>

<h2>a dilemma</h2>

<p>OK, so on the one hand we have an elegant way to describe <a href="https://bivector.net/3DPGA.pdf">all sorts
of geometric objects and operations</a> in terms of
multivectors that contain 16 numbers.</p>

<p>And on the other hand, in a more traditional setup we have specialized
objects for points (4 numbers), lines (6 numbers), and rotations (4
numbers).</p>

<p>In a program that is working with a <em>lot</em> of objects, we want them to
be as compact as possible, so that we don't waste memory or bus
bandwidth.</p>

<p>And the extra space used by multivectors is obnoxiously wasteful: it
is mostly zeroes, and if we know what kind of object the multivector
represents, we know which parts of it must be zero.</p>

<h2>zero-sized zero</h2>

<p>Unlike C and C++, Rust has zero-sized types. I can declare</p>

<pre><code>    struct Zero;
</code></pre>

<p>to create a type representing zeroes that vanish at run time: they
take no space. I can implement <a href="https://doc.rust-lang.org/std/ops/">Rust's operator overloading
traits</a> so that I can use my vanishing zeroes in arithmetic
in the usual way. For example, when I add a number to zero, I get back
the same number:</p>

<pre><code>    impl Add&lt;Num&gt; for Zero {
        type Output = Num;
        fn add(self, other: Num) -&gt; Num {
            other
        }
    }
</code></pre>

<p>but when I multiply a number by zero, the number vanishes and I get
zero:</p>

<pre><code>    impl Mul&lt;Num&gt; for Zero {
        type Output = Zero;
        fn mul(self, _: Num) -&gt; Zero {
            Zero
        }
    }
</code></pre>

<blockquote>
  <p><em>Aside: I have pointlessly declared a type alias <code>type Num = f64</code> in
case I want to change it, even though I probably won't.</em></p>
</blockquote>

<h2>variable-sized multivectors</h2>

<p>I can define a fixed-size PGA3D multivector like</p>

<pre><code>    struct Multivector(V0, V1, V2, V3, V4);
</code></pre>

<p>using some helper types <code>V0</code> ... <code>V4</code> that I have defined to stop me
from muddling up the various parts.</p>

<p>I can make this into a variable-sized multivector If I parameterize
all these types:</p>

<pre><code>    struct Multivec&lt;T0, T1, T2, T3, T4&gt;
        (V0&lt;T0&gt;, V1&lt;T1&gt;, V2&lt;T2&gt;, V3&lt;T3&gt;, V4&lt;T4&gt;);
</code></pre>

<p>Then a full-sized multivector is:</p>

<pre><code>    type Multivector =
             Multivec&lt;Num, Num, Num, Num, Num&gt;;
</code></pre>

<p>And I can make parts of the multivector vanish by setting the
corresponding type parameters to zero.</p>

<blockquote>
  <p><em>Aside: This <code>Multivec&lt;&gt;</code> is variable-sizes in the sense that it can
have different shapes in different parts of the source code, but
each shape is a fixed size at run time. In Rust terms, all the
instances are <code>Sized</code>.</em></p>
</blockquote>

<h2>the big idea</h2>

<p>I want to write geometric algebra code that is both efficient and
elegant.</p>

<p>Efficient means that a multivector is specialized to the geometric
object that it represents, and any parts that are known to be zero
vanish: they are not present in memory and no calculations are
performed on them.</p>

<p>Elegant means that the code I write looks like it is working with
fully-general multivectors: I don't have to tell the compiler in
detail which specialized type each variable has, because the compiler
can work that out for itself.</p>

<p>When my code cares about both efficiency and elegance, I should be
able to write a transformation in elegant style, and just specify the
type of the result. For example, I might want to be sure that a
rotated point is still just a point. The compiler should be able to
tell me when I get it wrong.</p>

<p>I want to do this without too much machinery. Multiplying two
multivectors, a full geometric product, is a beastly 16x16→16 monster
expression. I want to write this once, and get the compiler to
simplify it automatically for the various specialized cases.</p>

<p>I have a proof-of-concept that I think will be able to do this!</p>

<h2>demo test</h2>

<p>What does this idea look like in practice? Let's try a few basic tests
of a simplified version that works on complex numbers instead of
multivectors.</p>

<p>First, efficiency: complex numbers whose real or imaginary parts are
zero should be the same size as a normal number.</p>

<pre><code>    let re = Complex(1.0, Zero);
    assert_eq!(size_of_val(&amp;re), size_of::&lt;Num&gt;());
    let im = Complex(Zero, 1.0);
    assert_eq!(size_of_val(&amp;im), size_of::&lt;Num&gt;());
</code></pre>

<p>Elegance: I can add complex numbers without saying anything about
which parts are zero.</p>

<pre><code>    let c = re + im;
</code></pre>

<p>The compiler has worked out for itself that neither real nor imaginary
part is known to be zero, so the result has to contain two numbers.</p>

<pre><code>    assert_eq!(size_of_val(&amp;c), size_of::&lt;Num&gt;() * 2);
</code></pre>

<p>Back to efficiency: I can add complex numbers without saying anything
about the result type, and my half-size complex numbers do not become
bigger when they don't need to.</p>

<pre><code>    let re2 = re + re;
    assert_eq!(size_of_val(&amp;re2), size_of::&lt;Num&gt;());
    let im2 = im + im;
    assert_eq!(size_of_val(&amp;im2), size_of::&lt;Num&gt;());
</code></pre>

<p>Finally, a few arithmetic expressions work as expected.</p>

<pre><code>    assert_eq!(re2, 2.0 * re);
    assert_eq!(im2, im * 2.0);
    assert_eq!(im * im, -re);
    assert_eq!(re * im, im);
    assert_eq!(c * im, im - re);
    assert_eq!(c, 1.0 + im);
</code></pre>

<p>OK, not too bad!</p>

<p>Here's how it works...</p>

<h2>abstract interpretation</h2>

<p>A compiler will often have some partial information about the inputs
to an expression, and it needs to work out how that information
propagates through the expression. This is <a href="https://en.wikipedia.org/wiki/Abstract_interpretation">abstract interpretation</a>.</p>

<p>In our case, the information we want to track is whether a value is
always guaranteed to be zero or not. When we are operating on simple
scalar values, the rules for propagating zeroness are simple:</p>

<pre><code>    Add  | Zero | Num     Mul  | Zero | Num
    -----+------+-----    -----+------+-----
    Zero | Zero | Num     Zero | Zero | Zero
    Num  | Num  | Num     Num  | Zero | Num
</code></pre>

<p>The zeroness rules for multiplying multivectors are much more
complicated! But most of the difficulty also happens in the same kind
of way when we multiply complex numbers, so I'm going to explain how
to implement complex multiplication with vanishing real and imaginary
parts. Later on we'll come back to the issue of scaling up to
multivectors.</p>

<p>Here's a straightforward multiplication function, without any type
parameterization:</p>

<pre><code>    fn mul(a: Complex, b: Complex) -&gt; Complex {
        let Complex(ar, ai) = a;
        let Complex(br, bi) = b;
        Complex(ar * br - ai * bi,
                ar * bi + ai * br)
    }
</code></pre>

<p>Abstract interpretation is something a compiler will do to this
expression as part of the compiler's built-in analysis. I want to
implement my own zeroness analysis on top of the compiler not inside
it, so I won't be doing abstract interpretation as such. Instead,
alongside this expression which describes how to multiply numbers at
run time, I'm going to write a second expression that calculates its
zeroness at compile time.</p>

<p>This second expression does calculations inside Rust's type system. It
works out the types of each part of the run-time expression, either
<code>Zero</code> or <code>Num</code>, to describe which parts we know at compile time will
always be zero at run time. The compiler then makes <code>Zero</code> values and
expressions vanish.</p>

<h2>type-level function definitions</h2>

<p>OK, how do I write these calculations inside Rust's type system?</p>

<p>A trait is a type-level function declaration:</p>

<pre><code>    trait Sum {
        type Output;
    }
</code></pre>

<p>The type-level function's argument is the (implicit) <code>Self</code> type for
which we will implement the trait. The type-level function's result is
the associated type <code>Output</code>. (It could have any name but <code>Output</code> is
conventional.)</p>

<p>The body of this <code>Sum</code> type-level function is all the <code>impl Sum</code> trait
implementations. Each <code>impl Sum</code> has a self type, which is its
argument pattern, and the body of the implementation is the result for
that argument. So a type-level function is defined using a collection
of pattern-result clauses, like functions in Haskell.</p>

<p>For example, here's a type-level function definition of our zeroness
analysis for addition (or subtraction) of two values, as in the table
above.</p>

<pre><code>    impl Sum for (Zero,Zero) {
        type Output = Zero;
    }
    impl Sum for (Zero,Num) {
        type Output = Num;
    }
    impl Sum for (Num,Zero) {
        type Output = Num;
    }
    impl Sum for (Num,Num) {
        type Output = Num;
    }
</code></pre>

<p>In my <code>Sum</code> type-level function I have used a tuple to bundle two
parameters into a single argument. But we can also define type-level
functions in a sort-of-Curried asymmetrical style, as I am about to
demonstrate with <code>Mul</code>.</p>

<p>Later on I will show how to call a type-level function, but first, it
turns out that some of the type-level functions I need are predefined
by Rust.</p>

<h2>a lesson from rustc</h2>

<p>In the early stages of my struggle to get this to work, at one point I
had my own type-level functions for sums and products (similar to
<code>Sum</code> above) guarding an expression for calculating the run-time
result, but the rest of the type-level machinery was still missing.</p>

<p>Now, in Rust when you are writing a (normal) generic function that
does arithmetic, you need to put trait bounds on the type parameters
to tell the compiler that the arithmetic will work. For example, the
<code>Mul</code> trait bound here tells the compiler that the <code>*</code> operator will
work on values of whatever type <code>T</code> turns out to be.</p>

<pre><code>    struct Bar&lt;T&gt;(T);

    fn babar&lt;T&gt;(a: Bar&lt;T&gt;, b: Bar&lt;T&gt;) -&gt; Bar&lt;T&gt;
    where T: Mul&lt;Output = T&gt;
    {
        Bar(a.0 * b.0)
    }
</code></pre>

<blockquote>
  <p><em>Aside: I expect Rustaceans will think that this is a very strange
description of trait bounds. Yes! Yes, it is. But I hope this odd
perspective will make a bit more sense when we get to type-level
function calls and expressions.</em></p>
</blockquote>

<p>I was getting the Rust compiler to help me fill in the missing type
machinery, and I was amused when I realised that the error messages
were telling me to add trait bounds for <code>Mul</code> that were exactly
parallel to my own type-level functions for abstract zeroness.</p>

<p>But why?</p>

<p>To implement operator-overloaded multiplication with vanished zeroes
at run time (or rather, to tell the compiler that it can optimize them
out), I need a few implementations of the <a href="https://doc.rust-lang.org/std/ops/trait.Mul.html"><code>std::ops::Mul</code></a>
trait:</p>

<pre><code>    impl Mul&lt;Zero&gt; for Zero {
        type Output = Zero;
        fn mul(self, _: Zero) -&gt; Zero {
            Zero
        }
    }

    impl Mul&lt;Zero&gt; for Num {
        type Output = Zero;
        fn mul(self, _: Zero) -&gt; Zero {
            Zero
        }
    }

    impl Mul&lt;Num&gt; for Zero {
        type Output = Zero;
        fn mul(self, _: Num) -&gt; Zero {
            Zero
        }
    }
</code></pre>

<p>Rust does not assume that operators are commutative, so I need two
implementations for <code>Num*Zero</code> and <code>Zero*Num</code>. The <code>Num*Num</code>
case is already covered by Rust's <code>f64*f64</code> implementation.</p>

<p>The Rust compiler is doing its own abstract interpretation of my
run-time expression to prove that the overloaded operators have the
necessary implementations, and to find out what their <code>Output</code> types
are.</p>

<p>My type-level zeroness calculation must correspond to my run-time
expression, and it needs to calculate the same <code>Output</code> types, so it
can piggy-back on the existing operator overloading machinery!</p>

<p>This is really sweet! And I love the way that the Rust compiler nudges
me towards a better design. (This has happened to me a few times now!)</p>

<p>But <em>all</em> was not sweetness and light. I ran into some problems that
made my type expressions explode and become impossibly unwieldy. Let's
look at a simple example, then it will become clear how the types went
wrong.</p>

<h2>type-level function calls</h2>

<p>Normally when defining overloaded operators, you are either dealing
with concrete types (as in the implementations of <code>Mul</code> for <code>Zero</code> and
<code>Num</code> above), or dealing with generic types where the argument and
result types are the same (as in <code>babar()</code> above). Either way, the
type expressions remain fairly simple.</p>

<p>However, my vanishing zeroes need to be fully generic, because every
part of my number might be <code>Zero</code> or a <code>Num</code>. This is what that looks
like when multiplying complex numbers:</p>

<pre><code>    use std::ops::{Add, Mul, Sub};

    struct Complex&lt;R, I&gt;(R, I);

    impl&lt;Ar, Ai, Br, Bi&gt;
        Mul&lt;Complex&lt;Br, Bi&gt;&gt;
        for Complex&lt;Ar, Ai&gt;
    where
        Ar: Copy,
        Ai: Copy,
        Br: Copy,
        Bi: Copy,
        Ar: Mul&lt;Br&gt;,
        Ar: Mul&lt;Bi&gt;,
        Ai: Mul&lt;Br&gt;,
        Ai: Mul&lt;Bi&gt;,
        &lt;Ar as Mul&lt;Br&gt;&gt;::Output: Sub&lt;&lt;Ai as Mul&lt;Bi&gt;&gt;::Output&gt;,
        &lt;Ar as Mul&lt;Bi&gt;&gt;::Output: Add&lt;&lt;Ai as Mul&lt;Br&gt;&gt;::Output&gt;,
    {
        type Output = Complex&lt;
            &lt;&lt;Ar as Mul&lt;Br&gt;&gt;::Output as Sub&lt;
             &lt;Ai as Mul&lt;Bi&gt;&gt;::Output&gt;&gt;::Output,
            &lt;&lt;Ar as Mul&lt;Bi&gt;&gt;::Output as Add&lt;
             &lt;Ai as Mul&lt;Br&gt;&gt;::Output&gt;&gt;::Output,
        &gt;;
        fn mul(self, other: Complex&lt;Br, Bi&gt;)
                -&gt; Self::Output {
            let Complex(ar, ai) = self;
            let Complex(br, bi) = other;
            Complex(ar * br - ai * bi,
                    ar * bi + ai * br)
        }
    }
</code></pre>

<p>[ <a href="https://play.rust-lang.org/?version=stable&amp;mode=debug&amp;edition=2015&amp;gist=a43155c5ef71f20f53321b2aab529325">Rust Playground</a> ]</p>

<p>Our <code>Complex</code> type is fully generic in its real and imaginary parts.</p>

<p>Our <code>impl Mul for Complex</code> is also fully generic, with four
independent type variables for each component of each argument.</p>

<p>The <code>where</code> clause is where most of the action happens.</p>

<pre><code>        Ar: Copy
        ...
</code></pre>

<p>These lines say we need to be able to copy each part of each argument
so they can be used in both parts of the run-time expression. (So, not
interesting for compile time.)</p>

<pre><code>        Ar: Mul&lt;Br&gt;
        ...
</code></pre>

<p>These lines say we need to be able to multiply each part of <code>a</code> with
each part of <code>b</code>. These trait bounds do two things: they allow me to
multiply at run time, as in <code>ar*br</code>, <em>and</em> they allow me to call a
type-level function to work out zeroness at compile time.</p>

<p>How does that work?</p>

<p>The type-level meaning of <code>Ar: Mul&lt;Br&gt;</code> is a declaration that I am
going to call the type-level function <code>Mul</code> with arguments <code>Ar</code> and
<code>Br</code>.</p>

<pre><code>        &lt;Ar as Mul&lt;Br&gt;&gt;::Output
</code></pre>

<p>This expression is the actual type-level function call. It extracts
the result of the call from the trait implementation, which is the
<code>Output</code> associated type. It corresponds to the type (and zeroness) of
the run time expression <code>ar*br</code>.</p>

<p>Given the results of our four multiplications, we need to do an
addition and a subtraction. This is a bit hairy:</p>

<pre><code>        &lt;Ar as Mul&lt;Br&gt;&gt;::Output: Sub&lt;&lt;Ai as Mul&lt;Bi&gt;&gt;::Output&gt;
</code></pre>

<p>If I abbreviate the multiplications like this:</p>

<pre><code>        ArBr = &lt;Ar as Mul&lt;Br&gt;&gt;::Output
        AiBi = &lt;Ai as Mul&lt;Bi&gt;&gt;::Output
</code></pre>

<p>the trait bound looks like this:</p>

<pre><code>        ArBr: Sub&lt;AiBi&gt;
</code></pre>

<p>This allows me to write <code>ar*br - ai*bi</code> in the run time expression,
and at compile time it allows me to calculate the type of the real
part of the result. The abbreviated version of that type looks like:</p>

<pre><code>        &lt;ArBr as Sub&lt;AiBi&gt;&gt;::Output
</code></pre>

<p>And you can see it in its full glory inside the definition of the
<code>Output = Complex&lt;...&gt;</code> associated type:</p>

<pre><code>        &lt;&lt;Ar as Mul&lt;Br&gt;&gt;::Output as Sub&lt;
         &lt;Ai as Mul&lt;Bi&gt;&gt;::Output&gt;&gt;::Output
</code></pre>

<p>Whew!</p>

<h2>quadratic explosion</h2>

<p>OK, that's already pretty unpalatable, if you compare the size of the
run-time calculation to the size of the compile-time calculation. What
happens if we try to scale it up to multivectors?</p>

<p>Well, each multivector argument has 5 generic type parameters, so
<code>impl Mul</code> has 10 type parameters, and its trait bounds list 25
different <code>An: Mul&lt;Bn&gt;</code> combinations.</p>

<p>But the real problem happens with the sums. In a full geometric
product, there are 16 result expressions and many of the expressions
are sums of 16 products.</p>

<p>In the trait bounds, we are required to write the type of every
subexpression, in full, twice, and there are no ways to abbreviate the
types:</p>

<ul>
<li><p>we can't name intermediate types and refer back to them inside
trait bounds;</p></li>
<li><p>hygiene prevents us from using C-style textual macrology to
generate things like multiple trait bounds with a single macro
invocation.</p></li>
</ul>

<p>I struggled a lot trying to find a way to persuade the Rust compiler
that surely it can work out these intermediate types for itself, but no,
it has to be led by the hand through every step of the derivation.</p>

<p>The compiler finds it difficult because it is dealing with an open
world. Contrast pattern matching with Rust <code>enum</code>s: we know the set of
possibilities is fixed, so the compiler can check the pattern is
exhaustive. But with trait bounds on type variables, someone can come
along and define a new type and new trait implementations that break
the assumptions in a generic function. So the assumptions have to be
written down and checked.</p>

<p>I wondered if I could persuade the compiler that the generic type
parameters could only be <code>Zero</code> or <code>Num</code> and no other types, but all
we can say in trait bounds is that the type has to implement a trait,
and traits are open to extension.</p>

<h2>concrete brutalism</h2>

<p>Eventually I found a combination of techniques to get these horrible
type expressions under control:</p>

<ul>
<li><p>Split out the run-time expression for each of the result parts,
(for complex numbers, the real part or the imaginary part) and
make them completely concrete. This means there's no need to
convince the compiler that I am allowed to add an <code>f64</code> to an
<code>f64</code>.</p></li>
<li><p>Choose which concrete version of each result expression to invoke
based on the result types, instead of the argument types. Each
part of the result can be either <code>Zero</code> or a <code>Num</code>. So for each
part I only need two concrete implementations: a no-op <code>Zero</code>, or
the full result as a <code>Num</code> expression.</p></li>
<li><p>Lean on the optimizer more. The <code>Complex</code> <code>Mul</code> above is using
overloaded operators which are defined as no-ops when either of
the arguments is <code>Zero</code>. In the concrete version, the code says
that every <code>Zero</code> in the arguments should be un-vanished into
<code>0.0</code> before doing the calculation; I am assuming that the generic
multiply will be monomorphized, the concrete result expressions
will be inlined, and constant propagation will turn expressions
involving the <code>Zero</code> arguments back into no-ops.</p></li>
<li><p>The type-level zeroness calculation is now decoupled from the
run-time result calculation. This allows me to make the
multivector type-level calculation more abstract and simpler,
because it only needs to talk about the 5x5 combination of the
parts, not the full 16x16 multiplication table. (The complex
number version is too simple for this to make any difference.)</p></li>
<li><p>Make the type-level functions as flat and wide as possible. Every
subexpression requires more clauses in the trait bounds, so wide
and shallow expressions require fewer lines than narrow and deep
expressions.</p></li>
</ul>

<h2>scalable multiplication</h2>

<p>Let's see how these changes look in the simple setting of complex
multiplication. The extra machinery makes the code a fair bit bigger,
but it's a modest linear overhead so for multivectors it ends up being
smaller compared to the quadratic growth of the naive design. These
code snippets are taken from <a href="https://play.rust-lang.org/?version=stable&amp;mode=debug&amp;edition=2015&amp;gist=53add382e07c0fea5dca0bb1e36944f6">the full version on the Rust
Playground</a></p>

<p>For our concrete output expressions, there's a trait which has a
function for each part of the result. (This is an ordinary trait not a
type-level function trait.)</p>

<pre><code>    trait CMul&lt;A, B&gt; {
        fn re(_: A, _: B) -&gt; Self;
        fn im(_: A, _: B) -&gt; Self;
    }
</code></pre>

<p>This trait is implemented for return types of <code>Zero</code> and <code>Num</code>. It is
invoked separately for each part of the result, and each invocation
can have a different result type. The invocation looks simple, because
the types are specified elsewhere:</p>

<pre><code>    fn mul(self, other: Complex&lt;Br, Bi&gt;)
            -&gt; Self::Output {
        Complex(CMul::re(self, other),
                CMul::im(self, other))
    }
</code></pre>

<p>The no-op <code>CMul</code> trait implementation is simple: a <code>Zero</code> result just
makes its arguments vanish.</p>

<pre><code>    impl&lt;A, B&gt; CMul&lt;A, B&gt; for Zero {
        fn re(_: A, _: B) -&gt; Self { Zero }
        fn im(_: A, _: B) -&gt; Self { Zero }
    }
</code></pre>

<p>The <code>CMul for Num</code> implementation does two things: it converts
arguments of a partially-vanished type to the fully-hydrated
<code>Num</code>-everywhere concrete type, then does the calculations on the
concrete type. In the case of multivectors, the expressions are much
bigger than the conversions.</p>

<pre><code>    type ComplexNum = Complex&lt;Num,Num&gt;;

    impl&lt;Ar, Ai, Br, Bi&gt;
        CMul&lt;Complex&lt;Ar, Ai&gt;,
             Complex&lt;Br, Bi&gt;&gt; for Num
    where
        ComplexNum: From&lt;Complex&lt;Ar, Ai&gt;&gt;,
        ComplexNum: From&lt;Complex&lt;Br, Bi&gt;&gt;,
    {
        fn re(a: Complex&lt;Ar, Ai&gt;,
              b: Complex&lt;Br, Bi&gt;) -&gt; Self {
            let Complex(ar, ai) = ComplexNum::from(a);
            let Complex(br, bi) = ComplexNum::from(b);
            ar * br - ai * bi
        }
        fn im(a: Complex&lt;Ar, Ai&gt;,
              b: Complex&lt;Br, Bi&gt;) -&gt; Self {
            let Complex(ar, ai) = ComplexNum::from(a);
            let Complex(br, bi) = ComplexNum::from(b);
            ar * bi + ai * br
        }
    }
</code></pre>

<p>In the scalable <code>impl Mul for Complex</code> the trait bounds start with the
same set of multiplications as the naive version. This time they are
only used for type-level calculation, because we don't need them for
run-time overloading any more.</p>

<pre><code>    Ar: Mul&lt;Br&gt;,
    Ar: Mul&lt;Bi&gt;,
    Ai: Mul&lt;Br&gt;,
    Ai: Mul&lt;Bi&gt;,
</code></pre>

<p>At the top level we have an alias to make it nicer to write the
type-level result of multiplication.</p>

<pre><code>    type Prod&lt;A, B&gt; = &lt;A as Mul&lt;B&gt;&gt;::Output;
</code></pre>

<p>If you remember way back when I introduced type-level function
definitions, I used <code>trait Sum</code> as my example. Well, that
sum-of-a-tuple is how I make the type-level sum calculation wide and
shallow, because a tuple can wrap an arbitrary number of arguments.</p>

<p>Back to the trait bounds, next I declare how I will calculate my
result types. The difference between addition and subtraction doesn't
matter when we only care about zeroness.</p>

<pre><code>    (Prod&lt;Ar, Br&gt;, Prod&lt;Ai, Bi&gt;): Sum,
    (Prod&lt;Ar, Bi&gt;, Prod&lt;Ai, Br&gt;): Sum,
</code></pre>

<p>Now a new part. I need to say that the zeroness calculated by <code>Sum</code>
determines which implementation of my <code>CMul</code> concrete multiplication
to use.</p>

<pre><code>    &lt;(Prod&lt;Ar, Br&gt;, Prod&lt;Ai, Bi&gt;) as Sum&gt;::Output:
        CMul&lt;Complex&lt;Ar, Ai&gt;, Complex&lt;Br, Bi&gt;&gt;,
    &lt;(Prod&lt;Ar, Bi&gt;, Prod&lt;Ai, Br&gt;) as Sum&gt;::Output:
        CMul&lt;Complex&lt;Ar, Ai&gt;, Complex&lt;Br, Bi&gt;&gt;,
</code></pre>

<p>Finally I need to satisfy a couple of run time requirements: I need to
copy my arguments into all of the result parts, and <code>CMul</code> may need to
convert them.</p>

<pre><code>    Complex&lt;Ar, Ai&gt;: Copy + Into&lt;ComplexNum&gt;,
    Complex&lt;Br, Bi&gt;: Copy + Into&lt;ComplexNum&gt;,
</code></pre>

<p>The zeronesses calculated by <code>Sum</code> are also the types of each part of
the output type:</p>

<pre><code>    type Output = Complex&lt;
        &lt;(Prod&lt;Ar, Br&gt;, Prod&lt;Ai, Bi&gt;) as Sum&gt;::Output,
        &lt;(Prod&lt;Ar, Bi&gt;, Prod&lt;Ai, Br&gt;) as Sum&gt;::Output,
    &gt;;
</code></pre>

<p>You can see a complete version of the complex number code on the
<a href="https://play.rust-lang.org/?version=stable&amp;mode=debug&amp;edition=2015&amp;gist=53add382e07c0fea5dca0bb1e36944f6">Rust Playground</a></p>

<h2>exponential growth</h2>

<p>But I should warn you that there are a couple of issues with this
design. Fortunately they don't seem to be troublesome so far, even
though both of them involve exponential growth.</p>

<p>The first is an annoying aspect of the way Rust's conversion traits
are defined. I want to be able to specify how to convert <code>Zero</code> into
<code>Num</code>, then define <code>ComplexNum</code> conversions in terms of anything that
can be converted into a <code>Num</code>, like this:</p>

<pre><code>    impl&lt;R,I&gt; From&lt;Complex&lt;R,I&gt;&gt; for ComplexNum
    where
        R: Into&lt;Num&gt;,
        I: Into&lt;Num&gt;,
    {
        fn from(c: Complex&lt;R, I&gt;) -&gt; Self {
            let Complex(r, i) = c;
            Complex(r.into(), i.into())
        }
    }
</code></pre>

<p>But sadly the Rust compiler complains:</p>

<pre><code>    error[E0119]: conflicting implementations of trait
        `std::convert::From&lt;complex::Complex&lt;f64, f64&gt;&gt;`
        for type `complex::Complex&lt;f64, f64&gt;`:
    = note: conflicting implementation in crate `core`:
            - impl&lt;T&gt; From&lt;T&gt; for T;
</code></pre>

<p>Ugh! Instead I use a macro to implement the three conversions
involving Zero and leave out the no-op <code>From&lt;ComplexNum&gt; for
ComplexNum</code> that conflicts with the standard library.</p>

<p>For multivectors, unfortunately I need 31 of these conversions. Yuck.</p>

<p>The other case where it goes exponential is my wide and shallow <code>Sum</code>
trait. For multivectors, I need to get the zeroness of the sum of up
to 9 (nine!) arguments, each of which can be <code>Zero</code> or <code>Num</code>.</p>

<blockquote>
  <p><em>Aside: I said earlier that the multivector result expressions often
have 16 additions. Why is it only 9 now? This is where the abstract
type-level zeroness expression can be simpler than the concrete
run-time expression, because the type-level expression deals with
the 5x5 parts, whereas the run-time expression deals with the 16x16
numbers.</em></p>
</blockquote>

<p>In total the <code>Sum</code> trait has 820 implementations! (I don't need
argument lists of every possible length.) This seems like <em>a lot</em>, but
the trait implementations don't involve any run time code, so they
cause no slow LLVM codegen / optimization. The compiler seems to
handle it without trouble.</p>

<blockquote>
  <p><em>Aside: in one of my futile detours, I tried using macros to
generate all the 1024 possible concrete implementations of <code>Mul</code> for
multivectors. This <strong>did</strong> make the compiler sad: it took well over
ten seconds to compile or type-check a trivial program. The code
described here compiles in less than a second which is much more
tolerable.</em></p>
</blockquote>

<p>To keep these 820 implementations linear at the source code level, I
have used a little bit of macrology, so I can fit them all into 50
lines of code.</p>

<p>There's one last obvious problem with using this code for more than
wild experiments: my <code>Num</code> type is currently fixed; ideally it should
be generic over numeric types. There are probably a few more
significant hurdles that would make this difficult, but for now I am
going to leave it on the todo list.</p>

<h2>what next</h2>

<p>Well these notes have become rather more than 4000 words according to
<code>wc</code>! Well done if you read this far :-)</p>

<p>As I said earlier, what I have so far is only a proof-of-concept. It
has passed some trivial tests, but I have not yet tried to use it
properly. So the next thing is obviously to try to draw some pictures!</p>
