---
dw:
  anum: 203
  eventtime: "2019-01-02T20:01:00Z"
  itemid: 510
  logtime: "2019-01-02T20:13:07Z"
  props:
    commentalter: 1546516977
    interface: flat
    picture_keyword: ""
    revnum: 2
    revtime: 1546516907
  url: "https://fanf.dreamwidth.org/130763.html"
format: md
lj:
  anum: 107
  can_comment: 1
  ditemid: 154475
  event_timestamp: 1546460040
  eventtime: "2019-01-02T20:14:00Z"
  itemid: 603
  logtime: "2019-01-02T20:14:12Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/154475.html"
...

Notes on DNS server upgrades
============================

Now I have a blogging platform at work, I'm going to use that as the primary place for my work-related articles. I've just added some notes on my server upgrade project which got awkwardly interrupted by the holidays: <https://www.dns.cam.ac.uk/news/2019-01-02-upgrade-notes.html>

(Edited to add) and I have got the Atom feed syndicated by Dreamwidth
at [👤dns_cam_feed](https://dns-cam-feed.dreamwidth.org/)
