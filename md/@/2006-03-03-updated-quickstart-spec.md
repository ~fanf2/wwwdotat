---
dw:
  anum: 140
  eventtime: "2006-03-03T23:14:00Z"
  itemid: 205
  logtime: "2006-03-03T23:16:33Z"
  props:
    import_source: livejournal.com/fanf/52828
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/52620.html"
format: casual
lj:
  anum: 92
  can_comment: 1
  ditemid: 52828
  event_timestamp: 1141427640
  eventtime: "2006-03-03T23:14:00Z"
  itemid: 206
  logtime: "2006-03-03T23:16:33Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/52828.html"
title: Updated QUICKSTART spec
...

https://fanf2.user.srcf.net/hermes/doc/antiforgery/draft-fanf-smtp-quickstart.txt

This version has a basic subset which should be easy to implement, and which brings the MAIL command forward from the client's 9th packet to its 5th in typical use.

Full QUICKSTART allows the client to pipeline commands before the server greeting and after the TLS handshake in a way that should be safe, albeit not so trivial to implement. If all goes well, the MAIL command appears in                                                                                                                                  
the client's 3rd packet, which I think is pretty good :-) If the client has to recover from a cache miss, it should be no slower than basic QUICKSTART.
