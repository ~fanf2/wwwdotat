---
dw:
  anum: 155
  eventtime: "2018-03-22T14:02:00Z"
  itemid: 486
  logtime: "2018-03-22T14:02:51Z"
  props:
    interface: flat
    picture_keyword: ""
    revnum: 1
    revtime: 1521728069
  url: "https://fanf.dreamwidth.org/124571.html"
format: md
lj:
  anum: 246
  can_comment: 1
  ditemid: 151798
  event_timestamp: 1521728100
  eventtime: "2018-03-22T14:15:00Z"
  itemid: 592
  logtime: "2018-03-22T14:16:10Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/151798.html"
...

IETF 101 - Wednesday
====================

After
[Tuesday's revelation about the possibility of lie-ins](https://dotat.at/@/2018-03-21-ietf-101-tuesday.html)
I took it easy yesterday morning, and got to Paddington around lunch
time for the afternoon sessions.

dprive
------

The first afternoon session was the
[DNS privacy](https://datatracker.ietf.org/group/dprive/about/)
working group. I have not been paying as much attention to this work
as I should have, despite being keen on deploying it in production.

So far, this WG has specified
[DNS-over-TLS](https://tools.ietf.org/html/rfc7858),
[DNS-over-DTLS](https://tools.ietf.org/html/rfc8094), and
[EDNS padding](https://tools.ietf.org/html/rfc7830)
(which aims to make traffic analysis harde by quantizing the lengths of DNS messages),
and [more details about server authentication](https://tools.ietf.org/html/rfc8310).

They are working on
[recommendations for DNS privacy service operators](https://tools.ietf.org/html/draft-dickinson-bcp-op)
which looks like it will have some pertinent advice - it's on my list
of documents to review, and I got the impression from the summary
presentation that it's likely to have some helpful ideas I can use
when reviewing my services' privacy policies for GDPR.

[Roland van Rijswijk-Deij](https://twitter.com/reseauxsansfil)
presented a neat application of Bloom filters for privacy-preserving
collection of DNS queries. The idea is that if you have a set of known
bad queries (e.g. botnet C&C, compromised web sites) you can check the
Bloom filter to retrospectively find out if anyone made a bad query,
and that you need to follow up with a more detailed investigation.

Finally, there was some discussion about a second phase of work for
the group. [Stéphane Bortzmeyer](https://twitter.com/bortzmeyer)
has a draft about DNS privacy for the
[resolver-to-authoritative](https://tools.ietf.org/html/draft-bortzmeyer-dprive-resolver-to-auth)
path. This is in need of more discussion and feedback.

acme
----

For the second session I went to the
[acme](https://datatracker.ietf.org/group/acme/about/)
WG meeting. (ACME is the protocol used by [Let's Encrypt](https://letsencrypt.org)).

There was some discussion about authentication mechanisms for IP
address certificates (likely to be of interest for dprive DNS
servers). The draft suggests using the reverse DNS, and there were a
lot of comments in the meeting that this is probabaly not secure
enough: there isn't necessarily a good coupling between authentication
of IP address ownership and authentication of reverse DNS ownership. I
pointed out that in many enterprises, DNS and routing are handled by
different teams; I forgot to mention that in the RIPE databse (for
example) they are also represented by separate objects that can have
separate access control configurations. So this draft needs a rethink.

Another topic of interest was how to fix the
[broken TLS-SNI-01 challenge](https://labs.detectify.com/2018/01/12/how-i-exploited-acme-tls-sni-01-issuing-lets-encrypt-ssl-certs-for-any-domain-using-shared-hosting/).
As I understand it the draft replacement uses TLS ALPN (application
layer protocol negotiation, which allows a TLS client to say it wants
to speak something other than HTTP). This is fiddly, but the idea for
these challenges is to have close integration with an HTTPS web
server, to minimize support glue scripts.

dnsop
-----

Outside working group meetings I discussed the
[IP fragmentation considered fragile](https://tools.ietf.org/html/draft-bonica-intarea-frag-fragile)
draft with a few people, and my idea for a DNS-specific followup.
Idecided I should go ahead and try to get the ball rolling, so I
posted some
[notes on reducing fragmented DNS-over-UDP](https://mailarchive.ietf.org/arch/msg/dnsop/xnJjuOFRE4IiT7uqEFyqhYKKT7c/?qid=513a41746fdf8b9af02a46cd012e73da)
to the dnsop WG list.

Plenary
-------

The final session of the day was the Plenary meeting. This includes a
certain amount of meta-discussion about how the meetings are run -
announcements of future locations, budgeting, changes to memberships
of senior committees, etc. This time there are about 1200 on-site
attendees, 400 remote, and the money to pay for the meeting is about
$804,000 in attendance fees, plus $521,000 in sponsorship.

There were a few presentations on expanding access to the Internet to
sub-Saharan Africa and to areas with low population density. It seems
there is currently a boom in satellite communications, and the
satellite engineers are doing lots of cool things with multi-path
communications to avoid rain fade, and maybe in the not too distant
future, direct sat-to-sat relaying over space lasers. Awesome.

A lot of the plenary is for open mic sessions, where anyone can quiz
the senior committees (the Internet Architecture Board, IAB; the IETF
administrative oversight committee, IAOC; and the Internet Engineering
Steering Group, IESG, which is the committee of IETF area directors).
It struck me that the composition of these committees is about 1/3
women, which is considerably better than the IETF at large - the bulk
of the attendees are white Americans and European middle-aged men.

mfld
----

I had an anti-social lunch, but after I bailed out of the plenary
before the IAOC open mic, I found a pub with a few folks from
[Sinodun](https://www.sinodun.com) and [nic.at](https://nic.at).
We had a pleasant chat, although I managed to knock my beer over,
so I went home unpleasantly moist and smelly. D'oh!
