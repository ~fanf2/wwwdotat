---
format: html
lj:
  anum: 70
  can_comment: 1
  ditemid: 104774
  event_timestamp: 1260551160
  eventtime: "2009-12-11T17:06:00Z"
  itemid: 409
  logtime: "2009-12-11T17:06:35Z"
  props:
    personifi_tags: "2:7,8:33,33:7,10:22,23:3,43:3,32:7,1:29,25:14,19:3,26:14,20:14,9:14"
  reply_count: 1
  url: "https://fanf.livejournal.com/104774.html"
title: Signing the root zone.
...

<p>If you are a DNS, network, or firewall operator, you need to be aware that the root zone of the DNS is going to be signed with DNSSEC in stages during the first half of 2010.</p>

<p>You need to ensure that any packet filters between your recursive DNS resolvers and the public Internet do not block UDP DNS packets larger than 512 bytes, and that they do not block fragmented UDP packets, and that they do not block ICMP "fragmentation needed" packets, and that they do not block DNS-over-TCP.</p>

<p>The reason for this is that DNSSEC makes DNS packets larger, since as well as the answer they must also contain a cryptographic proof that the answer is correct. Misconfigurations that are benign with insecure DNS can cause problems with the move towards DNSSEC. The DNS Operations and Analysis Research Centre has <a href="https://www.dns-oarc.net/oarc/services/replysizetest">a reply size tester</a> which you can use to check that your systems are compatible with large DNS reply packets.</p>

<p>See <a href="http://www.ripe.net/ripe/meetings/ripe-59/presentations/uploads//presentations/Tuesday/Plenary%2014:00/Abley-DNSSEC_for_the_Root_Zone.mId7.pdf">these presentation slides</a> for some details on the process of signing the root zone. See <a href="http://labs.ripe.net/content/preparing-k-root-signed-root-zone">this blog post from RIPE</a>, operators of the K root server, for some information about how they are preparing for the change.</p>

<p>ICANN have published <a href="http://www.icann.org/en/committees/security/sac035.pdf">a paper</a> about the predicted effects of DNSSEC on broadband routers and firewalls. Gaurab Raj Upadhaya has published <a href="http://www.sanog.org/resources/sanog14/sanog14-gaurab-edns.pdf">a few slides</a> about EDNS0, the DNS extension protocol that enables large packets.</p>

<p>Please go out and check your DNS resolvers before they break!</p>
