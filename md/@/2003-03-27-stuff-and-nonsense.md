---
dw:
  anum: 181
  eventtime: "2003-03-27T16:31:00Z"
  itemid: 10
  logtime: "2003-03-27T08:46:24Z"
  props:
    current_moodid: 76
    import_source: livejournal.com/fanf/2592
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/2741.html"
format: casual
lj:
  anum: 32
  can_comment: 1
  ditemid: 2592
  event_timestamp: 1048782660
  eventtime: "2003-03-27T16:31:00Z"
  itemid: 10
  logtime: "2003-03-27T08:46:24Z"
  props:
    current_moodid: 76
  reply_count: 0
  url: "https://fanf.livejournal.com/2592.html"
title: Stuff and nonsense
...

The chain on my Brompton broke on the way into work as I was moving onto a roundabout in the wrong gear. Fortunately there wasn't too much traffic, and I had a spare plastic bag in which I could carry the filthy remnant. I decided to take  the B to the bike shop on Botolph Lane near the office in order to minimise additional lateness, but the place turned out to be closed. Bah. So I trekked down to Drake's (the most-local-but-still-too-far-away B dealer) to get them to fix the chain, move the brake cable (that they routed incorrectly) into the right place, and attend to some minor niggles.

I had a dumb chain incident a few weeks back where I managed to knock it off the front chainwheel and couldn't work out how to get it back on. (The B has lots of narrow gaps for things to get stuck in.) So I split the chain, but I stuffed it up when putting it back together, leaving a link rather stiff. It seemed OK after some competent TLC from the Botolph Lane shop, but I suppose I must have done it more damage than I thought.

I knew there was a good reason I never do anything non-trivial with it.

<em>Anyway</em>, I got into work an additional hour late, and since then I have achieved very little. Sigh. Maybe I'll have some inclination for SpamAssassination when I've cut off a few more of the usenet hydra's heads.
