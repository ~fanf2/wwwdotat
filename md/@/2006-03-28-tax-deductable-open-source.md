---
dw:
  anum: 115
  eventtime: "2006-03-28T21:17:00Z"
  itemid: 212
  logtime: "2006-03-28T21:22:38Z"
  props:
    commentalter: 1491292335
    import_source: livejournal.com/fanf/54780
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/54387.html"
format: casual
lj:
  anum: 252
  can_comment: 1
  ditemid: 54780
  event_timestamp: 1143580620
  eventtime: "2006-03-28T21:17:00Z"
  itemid: 213
  logtime: "2006-03-28T21:22:38Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/54780.html"
title: Tax-deductable open source
...

http://www.interesting-people.org/archives/interesting-people/200603/msg00179.html

Russ Nelson &lt;nelson@crynwr.com&gt; says "<em>I looked into [tax-deductable code donations] in my role as the executive director of <a href="http://publicsoftwarefund.org">the Public Software Fund</a>. If you own copyright on a work, and you donate that work to a 501(c)(3) [non-profit organization], you can deduct the fair market value of that work [from your US taxes].</em>"

Notable relevant organizations include <a href="http://www.freebsdfoundation.org/">the FreeBSD Foundation</a>, <a href="http://www.apache.org/foundation/how-it-works.html">the Apache Software Foundation</a>, and <a href="https://www.fsf.org/about">the Free Software Foundation</a>. They must be projects that will formally take ownership of the copyright from the donor.

Russ continues: "<em>The difficulty here is that you need to determine the fair market value. [...] So what would a contribution to an existing open source project be worth? [...] It seems to me arguable that you can deduct whatever you could sell the first copy for.  When Cygnus Software was an independent company, they would sell the first copy of a gcc port for six or seven figures. A comparable price would be the price of full ownership of any comparable piece of software. A less reliable valuation would be the salary of someone paid to do a comparable work-for-hire.</em>"
