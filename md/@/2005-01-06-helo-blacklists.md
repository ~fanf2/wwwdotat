---
dw:
  anum: 167
  eventtime: "2005-01-06T12:53:00Z"
  itemid: 124
  logtime: "2005-01-06T14:14:28Z"
  props:
    commentalter: 1491292340
    import_source: livejournal.com/fanf/32096
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/31911.html"
format: casual
lj:
  anum: 96
  can_comment: 1
  ditemid: 32096
  event_timestamp: 1105015980
  eventtime: "2005-01-06T12:53:00Z"
  itemid: 125
  logtime: "2005-01-06T14:14:28Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/32096.html"
title: HELO blacklists
...

The other part of <a href="http://www.livejournal.com/users/fanf/31810.html">CSV</a> is DNA - Domain Name Accreditation. A slightly unfortunate abbreviation, I think.

DNA consists of two parts. The first is a system for sites to advertise which accreditation services publish information about the site. The idea is that recipients of email can use this information to reduce the number of accreditation  queries they need to perform. Of course recipients can't trust information provided by senders, so the sender's list of accreditation services must be tempered by the recipient's assessment of the accreditation services' reliability and augmented by a judicious selection of reputation services.

Accreditation and reputation services are very similar from the technical point of view, but have different contractual arrangements. Accreditation services are paid by the sender to vouch for the sender's good standing, and tend to provide positive ratings: current examples include http://www.bondedsender.com/ and http://www.habeas.com/. Reputation services are paid by the recipient (or not - most of them are run on a not-for-profit basis) to track the behaviours of senders, and tend to provide information about those with negative ratings: current examples include http://www.mail-abuse.com/.

The other part of DNA is a standard way to query accreditation and reputation services. This is very similar to existing RHSBL techniques: the sender's HELO name is concatenated with the name of the service and the resulting domain is looked up in the DNS. Instead of encoding information in an A record like most DNS blacklists, DNA specifies a format for a TXT record.

This is all fairly straight-forward, though it's worth noting that DNA depends crucially on an authenticated HELO name in order to work. It can equally well use CSA or traditional HELO verification, though CSA can provide an independent reason to reject a message which traditional HELO verification cannot. IP-based blacklists also depend on authentication: the establishment of the TCP session under SMTP authenticates the sender's IP address.

Without an authenticated HELO name there are only a few <a href="http://www.livejournal.com/users/fanf/30836.html">heuristic HELO checks</a> that DNA can perform without additional knowledge of the SMTP session. Yes, CSV - the combination of CSA and DNA - can replace all but one of my HELO checks: CSA implements the check for senders using the recipient's domain in HELO, and appropriately cunning DNA services can implement the checks for all-numeric and over-long HELO names, and even the check for randomly-generated invalid domain names. The remaining check requires knowledge of the first recipient's local part, which CSV doesn't have.

But could DNA be better?

If the DNA query includes both the client's HELO name and IP address, then the reputation service has everything it needs to implement CSA or traditional HELO verification, as well as the reputation lookup. This implies that the SMTP server doesn't have to implement these functions, and can just rely on maybe only a single DNS lookup to do all the work. This reduces the DNS load caused by the SMTP server significantly. It can also improve cacheing, because SMTP servers are generally not as centralized as DNS servers so  can't usefully cache the results of DNS lookups.

Behind this and the more complicated un-enhanced DNA services I'm thinking of is the idea of a dynamic DNS blacklist service, which - instead of being based on a local database - uses computation and DNS lookups to produce a result. This could plausibly be implemented in Perl by hacking together a Perl DNS server (http://search.cpan.org/dist/Net-DNS-Server/ or http://www.stanford.edu/~riepel/lbnamed/Stanford-DNSserver/) and <a href="http://www.livejournal.com/users/fanf/30648.html">a fixed Net::DNS query library</a>. (Sadly most DNS server back-end APIs don't allow for decent amounts of concurrency, so an easily hackable server is required.) You'd probably want to put a cacheing DNS server in front of the Perl monstrosity :-)

As well as implementing complicated logic like CSA, this kind of blacklist service could also be used to combine multiple DNS blacklists into a single virtual blacklist so that one query from the SMTP server effectively performs multiple blacklist lookups at the same time. With the enhanced DNA these could be both IP blacklists and HELO blacklists.
