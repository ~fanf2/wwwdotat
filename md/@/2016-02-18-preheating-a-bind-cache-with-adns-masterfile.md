---
dw:
  anum: 64
  eventtime: "2016-02-18T15:46:00Z"
  itemid: 444
  logtime: "2016-02-18T15:46:22Z"
  props:
    import_source: livejournal.com/fanf/141030
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/113728.html"
format: html
lj:
  anum: 230
  can_comment: 1
  ditemid: 141030
  event_timestamp: 1455810360
  eventtime: "2016-02-18T15:46:00Z"
  itemid: 550
  logtime: "2016-02-18T15:46:22Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/141030.html"
title: preheating a BIND cache with adns-masterfile
...

<p>tl;dr - you can use my program <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/adns-masterfile.git"><code>adns-masterfile</code></a> to pre-heat a DNS cache as part of a planned failover process.</p>

<h2>adns</h2>

<p>One of my favourite DNS tools is <a href="http://www.chiark.greenend.org.uk/~ian/adns/">GNU <code>adns</code></a>,
an easy-to-use asynchronous stub resolver library.</p>

<p>I started using <code>adns</code> in 1999 when it was still very new. At that time
I was working on web servers at Demon Internet. We had a daily log
processing job which translated IP addresses in web access logs to
host names, and this was getting close to taking more than a day to
process a day of logs. I fixed this problem very nicely with <code>adns</code>, and
my code is included as <a href="http://www.chiark.greenend.org.uk/ucgi/~ianmdlvl/git?p=adns.git;a=blob;f=client/adnslogres.c">an example <code>adns</code> client program</a>.</p>

<p>Usually when I want to do some bulk DNS queries, I end up doing a
clone-and-hack of <code>adnslogres</code>. Although <code>adns</code> comes with a very
capable general-purpose program called <code>adnshost</code>, it doesn't have any
backpressure to control the number of outstanding queries. So if you
try to use it for bulk queries, you will overwhelm the server with
traffic and most of the queries will time out and fail. So, although
my normal approach would be to massage the data with an ad-hoc Perl
script so I can pipe it into a general-purpose program, with <code>adns</code> I
usually end up writing special-purpose C programs which have their own
concurrency control.</p>

<h2>cache preheating</h2>

<p>In our DNS server setup we use
<a href="http://www.keepalived.org/"><code>keepalived</code></a> to do automatic failover of
the recursive server IP addresses. I wrote about <a href="http://fanf.livejournal.com/133294.html">our <code>keepalived</code>
configuration</a> last year,
explaining how we can control which servers are live, which are
standby, and which are not part of the failover pool.</p>

<p>The basic process for config changes, patches, upgrades, etc., is to
take a stanby machine out of the pool, apply the changes, test them,
then shuffle the pool so the next machine can be patched. Patching all
of the servers requires one blip of a few seconds for each live
service address.</p>

<p>However, after moving a service address, the new server has a cold
cache, so (from the point of view of the user) a planned failover
looks like a sudden drop in DNS server performance.</p>

<p>It would be nice if we could pre-heat a server's cache before making
it live, to minimize the impact of planned maintenance.</p>

<h2>rndc dumpdb</h2>

<p>BIND can be told to dump its cache, which it does in almost-standard
textual DNS master file format. This is ideal source data for cache
preheating: we could get one of the live servers to dump its cache,
then use the dump to make lots of DNS queries against a standby server
to warm it up.</p>

<p>The <code>named_dump.db</code> file uses a number of slightly awkward master file
features: it omits owner names and other repeated fields when it can,
and it uses <code>()</code> to split records across multiple lines. So it needs a
bit more intelligence to parse than just extracting a field from a web
log line!</p>

<h2>lex</h2>

<p>I hadn't used <code>lex</code> for years and years before last month. I had
almost forgotten how handy it is, although it does have some very
weird features. (Its treatment of leading whitespace is almost as
special as tab characters in make files.)</p>

<p>But, if you have a problem whose solution involves regular expressions
and a C library, it's hard to do better than <code>lex</code>. It makes C feel
almost like a scripting language!</p>

<p>(According to the Jargon File, Perl was described as a <a href="http://catb.org/~esr/jargon/html/S/Swiss-Army-chainsaw.html">"Swiss-Army
chainsaw"</a>
because it is so much more powerful than Lex, which had been described
as the Swiss-Army knife of Unix. I can see why!)</p>

<h2>adns-masterfile</h2>

<p>So last month, I used <code>adns</code> and <code>lex</code> to write a program called
<a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/adns-masterfile.git"><code>adns-masterfile</code></a>
which parses a DNS master file (in BIND <code>named_dump.db</code> flavour) and
makes queries for all the records it can. It's a reasonably handy way
for pre-heating a cache as part of a planned server swap.</p>

<p>I used it yesterday when I was patching and rebooting everything to
deal with <a href="https://googleonlinesecurity.blogspot.co.uk/2016/02/cve-2015-7547-glibc-getaddrinfo-stack.html">the <code>glibc</code> CVE-2015-7547 vulnerability</a>.</p>

<h2>performance</h2>

<p>Our servers produce a <code>named_dump.db</code> file of about 5.3 million lines
(175 MB). From this <code>adns-masterfile</code> makes about 2.6 million queries,
which takes about 10 minutes with a concurrency limit of 5000.</p>
