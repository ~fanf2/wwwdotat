---
dw:
  anum: 76
  eventtime: "2007-11-20T21:42:00Z"
  itemid: 314
  logtime: "2007-11-20T22:08:53Z"
  props:
    commentalter: 1491292350
    import_source: livejournal.com/fanf/81107
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/80460.html"
format: html
lj:
  anum: 211
  can_comment: 1
  ditemid: 81107
  event_timestamp: 1195594920
  eventtime: "2007-11-20T21:42:00Z"
  itemid: 316
  logtime: "2007-11-20T22:08:53Z"
  props: {}
  reply_count: 13
  url: "https://fanf.livejournal.com/81107.html"
title: Pointer magic
...

<p>The following C type definition can be used for declaring local and global structure objects. You can initialize them as if they were bare structures, because C doesn't mind if you omit curly brackets in initializers (though <tt>gcc -Wall</tt> will complain). You can also use the typedef to declare function arguments, in which case the function will expect a pointer to the structure instead of a copy of it. Furthermore, when you use a variable declared with this typedef, it will be quietly converted into a pointer to the structure just as is expected by the function. This avoids a load of <tt>&</tt> operators and gives you a sort of poor-man's C++ pass-by-reference.</p>
<pre>
        typedef struct mytype {
                /* member declarations */
        } mytype[1];

        mytype var;

        int func(mytype arg);

        func(var);
</pre>
<p><i>ETA</i>: <a href="http://gmplib.org/manual/Parameter-Conventions.html">it seems this trick is used by GMP</a> (see the last paragraph of that page)</p>
[<a href="http://www.livejournal.com/poll/?id=1092168">Poll #1092168</a>]
