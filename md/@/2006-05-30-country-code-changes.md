---
dw:
  anum: 51
  eventtime: "2006-05-30T22:08:00Z"
  itemid: 232
  logtime: "2006-05-30T21:13:39Z"
  props:
    commentalter: 1491292335
    import_source: livejournal.com/fanf/59760
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/59443.html"
format: casual
lj:
  anum: 112
  can_comment: 1
  ditemid: 59760
  event_timestamp: 1149026880
  eventtime: "2006-05-30T22:08:00Z"
  itemid: 233
  logtime: "2006-05-30T21:13:39Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/59760.html"
title: Country code changes
...

It seems that ISO 3166 has been changed so that it is now consistent with the DNS: Jersey, Guernsey, the Isle of Man, and the UK now have their own country codes and are no longer bundled together in the catch-all "GB". This means that the GBR annotations in my passport are obsolete.

RIP ftp.dra.hmg.gb.

Link: http://www.circleid.com/posts/gg_im_je_iso3166_good_bye_gb/
