---
dw:
  anum: 11
  eventtime: "2016-01-22T00:35:00Z"
  itemid: 441
  logtime: "2016-01-22T00:35:13Z"
  props:
    commentalter: 1491292414
    import_source: livejournal.com/fanf/140252
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/112907.html"
format: html
lj:
  anum: 220
  can_comment: 1
  ditemid: 140252
  event_timestamp: 1453422900
  eventtime: "2016-01-22T00:35:00Z"
  itemid: 547
  logtime: "2016-01-22T00:35:13Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 4
  url: "https://fanf.livejournal.com/140252.html"
title: Outside Winter Insulation 7-layer clothing model
...

<p>After being ejected from the pub this evening we had a brief
discussion about the correspondance between winter clothing
and the ISO Open Systems Interconnection 7 layer model. This is
<em>entirely normal</em>.</p>

<p>My coat is OK except it is a bit too large, so even if I bung up the
neck with a woolly scarf, it isn't snug enough around the hips to keep
the chilly wind out. So I had multiple layers on underneath. My
Scottish friend was wearing a t-shirt and a leather jacket – and
actually did up the jacket; I am not sure whether that was because of
the cold or just for politeness.</p>

<p>So the ISO OSI 7 layer model is:</p>

<ol>
<li>physical</li>
<li>link</li>
<li>network</li>
<li>transport</li>
<li>presentation and session or</li>
<li>maybe session and presentation</li>
<li>application</li>
</ol>

<p>The Internet model is:</p>

<ol>
<li>wires or glass or microwaves or avian carriers</li>
<li>ethernet or wifi or mpls or vpn or <em>mille feuille</em> or <br />
how the fuck do we make IP work over this shit</li>
<li>IP</li>
<li>TCP and stuff</li>
<li>dunno</li>
<li>what</li>
<li>useful things and pictures of cats</li>
</ol>

<p>And, no, I wasn't actually wearing OSI depth of clothing; it was more
like:</p>

<ol>
<li>skin</li>
<li>t-shirt</li>
<li>rugby shirt</li>
<li>fleece <br />
You know, OK for outside "transport" most of the year.</li>
<li>...</li>
<li>...    Coat. It has layers but I don't care.</li>
<li>...</li>
</ol>
