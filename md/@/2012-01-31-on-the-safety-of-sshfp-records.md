---
format: html
lj:
  anum: 44
  can_comment: 1
  ditemid: 118060
  event_timestamp: 1328013000
  eventtime: "2012-01-31T12:30:00Z"
  itemid: 461
  logtime: "2012-01-31T12:30:55Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 2
  url: "https://fanf.livejournal.com/118060.html"
title: On the safety of SSHFP records.
...

<p>Proper SSH hygiene is to go through a manual procedure the first time you log into a host to verify that its public key is the one you expect. <a href="http://tools.ietf.org/html/rfc4255">RFC 4255</a> specifies a way to use the DNS to verify ssh host keys so you can skip the manual process. The host key fingerprint is published in an SSHFP record in the DNS and is authenticated using DNSSEC.</p>

<p><a href="http://OpenSSH.org">OpenSSH</a> supports SSHFP lookups if you turn on the <tt>VerifyHostKeyDNS</tt> option (documented towards the end of <a href="http://www.openbsd.org/cgi-bin/man.cgi?query=ssh_config">ssh_config(5)</a>). OpenSSH does not do any DNSSEC validation of its own, and instead relies on the "authenticated data" flag (the AD bit) in the response from a validating resolver. Its SSHFP checking logic is a bit more complicated than you might expect:</p>
<ul>
<li>If the host key does not match then ssh puts up its usual scary banner, regardless of the AD bit.</li>
<li>If the host key matches and the AD bit is set in the DNS response, then ssh connects happily, bypassing the manual check.</li>
<li>If the host key matches but the AD bit is clear, ssh goes into the usual unknown host procedure, with an added note about the presence of a matching SSHFP record.</li>
</ul>

<p>As the RFC explains, none of this is safe unless you are running <a href="http://nlnetlabs.nl/projects/dnssec-trigger/">a validating resolver</a> on the same machine as ssh (or with some other secure communication channel), and the resolver has a chain of trust to your target host's zone.</p>

<p>I am not entirely sure that ssh's behaviour in the no-AD situation is wise. It won't happen in a correct setup, but it's the accidentally incorrect setups that worry me. In particular, if someone has configured their machine to use one of Cambridge University's central validating resolvers and they turn on VerifyHostKeyDNS, then ssh will see the AD bit in DNS replies but it will not be trustworthy because it travelled over the network between resolver and ssh client without verification. There are also quite a lot of non-validating resolvers in the University (e.g. used by the Linux timesharing servers, and handed out by the Eduroam WiFi DHCP servers) and users of those will get ssh's note about matching SSHFP records, and this might give them a false sense of security since it doesn't mention the lack of verification.</p>

<p>So I'm in two minds about deploying SSHFP records. If we add them for hosts such as <tt>hermes.cam.ac.uk</tt> we need to be confident that people who turn on VerifyHostKeyDNS also run their own validating resolver. However there is no warning in the OpenSSH manual pages that this is necessary - in fact no mention of DNSSEC at all, nor any explanation
of the difference between secure and insecure DNS fingerprints.</p>

<p>These worries would not exist if OpenSSH did its own DNSSEC validation of SSHFP records, and if it did not do questionable things with untrusted SSHFP records.</p>
