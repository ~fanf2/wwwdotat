---
dw:
  anum: 113
  eventtime: "2020-09-14T15:47:00Z"
  itemid: 518
  logtime: "2020-09-14T15:10:33Z"
  props:
    interface: flat
    picture_keyword: ""
    revnum: 1
    revtime: 1600096259
  url: "https://fanf.dreamwidth.org/132721.html"
format: md
...

da Vinci bridges
================

tl;dr: <http://dotat.at/random/bridge.html>

<cut>

Last week I found out about [Leonardo da Vinci's design for a self-supporting bridge][core77], which looks rather similar to [Cambridge's Mathematical Bridge][queens] though they work on completely different principles.

[core77]: https://www.core77.com/posts/65043/Leonardo-da-Vincis-Ingenious-Design-for-a-Self-Supporting-Bridge
[queens]: https://en.wikipedia.org/wiki/Mathematical_Bridge

I tried to make one using q-tips, since we have a convenient stash of them, but (being round) they would not stay square and the thing twisted out of shape far too easily.

So I got some lolly sticks, which were much easier. I used little rubber bands from a loom-band toy to hold the sticks in place since I don't have enough hands... I posted pictures on Twitter:

<a href="https://twitter.com/fanf/status/1305251444833038337"><img src="https://dotat.at/graphics/bridge-flat.jpg" width="80%"></a>

<a href="https://twitter.com/fanf/status/1305255864799039489"><img src="https://dotat.at/graphics/bridge-tall.jpg" width="80%"></a>

I was curious about the geometry of the bridge, in particular how far I could expect one to reach, which required a bit of head-scratching since I don't often do this kind of maths. [I even dug out my old A-level book of formulae!][SMP]

[SMP]: https://twitter.com/fanf/status/1305271860792307712

Scribbling on paper was annoying, so I decided to get the computer to draw diagrams for me, and now I have [an interactive da Vinci bridge calculator][bridge]. It's based on some moderately dodgy approximations (explained on the bridge page below the diagrams) - I wanted good diagrams so I could see how good or bad my guesstimates were. Not too bad for sensible cases, since I basically used the small angle approximation.

[bridge]: http://dotat.at/random/bridge.html

<img src="https://dotat.at/graphics/bridge-diag.jpg" width="80%">
