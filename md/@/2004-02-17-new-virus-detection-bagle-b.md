---
dw:
  anum: 64
  eventtime: "2004-02-17T15:14:00Z"
  itemid: 61
  logtime: "2004-02-17T15:25:51Z"
  props:
    commentalter: 1491292308
    import_source: livejournal.com/fanf/15706
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/15680.html"
format: casual
lj:
  anum: 90
  can_comment: 1
  ditemid: 15706
  event_timestamp: 1077030840
  eventtime: "2004-02-17T15:14:00Z"
  itemid: 61
  logtime: "2004-02-17T15:25:51Z"
  props: {}
  reply_count: 6
  url: "https://fanf.livejournal.com/15706.html"
title: "New virus detection: Bagle.b"
...

12:30: I notice lots of odd forged email, which looks like it's a virus but unlike one i've seen before. Save it to a file so I can scan it with a proper virus scanner on another machine. I come up blank. I check http://vil.nai.com/vil/newly-discovered-viruses.asp
but none of the descriptions match this virus.

12:50: I send a few copies to NAI's virus reporting address. Almost immediately get an autoreply saying that they also do not know about this virus. I check my filters, and there appears to be lots of it going through and being zapped on the way because of its executable attachment.

13:50: I get a second reply confirming that this is a new virus, with an extra.dat for detecting it. I test the extra.dat on the file I submitted and it duly detects the virus in each message.

14:00: I install the extra.dat in my email filters and it starts spotting copies almost immediately.

14:30: I send another message to NAI saying that I'm using the extra.dat in anger, and that it appears to be a fast-spreading virus.

14:50: I get a reply thanking me for my feedback and saying that they're keeping a close eye on this threat.

15:20: The threat is upgraded from Low to Medium. So far today we've filtered 4600 infected messages, of which 650 have been Bagle (60% of viruses since 14:00).

15:50: I do a run of my infected host finder. Someone in the University managed to get infected at 15:00.

16:05: 1000 copies now deleted.

17:10: Automated DAT file update gets the official fingerprint from NAI, so the extra.dat is no longer necessary. Still, it has been good for 3 hours and 1800 copies worth of protection.
