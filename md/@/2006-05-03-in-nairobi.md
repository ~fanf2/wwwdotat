---
dw:
  anum: 212
  eventtime: "2006-05-03T22:12:00Z"
  itemid: 225
  logtime: "2006-05-03T22:15:05Z"
  props:
    commentalter: 1491292333
    import_source: livejournal.com/fanf/58107
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/57812.html"
format: casual
lj:
  anum: 251
  can_comment: 1
  ditemid: 58107
  event_timestamp: 1146694320
  eventtime: "2006-05-03T22:12:00Z"
  itemid: 226
  logtime: "2006-05-03T22:15:05Z"
  props: {}
  reply_count: 5
  url: "https://fanf.livejournal.com/58107.html"
title: In Nairobi
...

At <a href="http://www.afnog.org/">AfNOG</a>. Network connectivity is pretty good, and not as bad as the RTT might suggest.
<pre>
traceroute to hermes.cam.ac.uk (131.111.8.59), 64 hops max, 40 byte packets
 1  soekris.ws.afnog.org (196.200.222.2)  2.858 ms  2.514 ms  15.464 ms
 2  196.216.67.253 (196.216.67.253)  4.517 ms  3.173 ms  7.452 ms
 3  217.21.112.4.swiftkenya.com (217.21.112.4)  3.905 ms  3.873 ms  4.796 ms
 4  193.220.225.5 (193.220.225.5)  9.375 ms  4.426 ms  5.115 ms
 5  NO-NIT-TN-7.taide.net (193.219.192.7)  530.815 ms  530.397 ms  530.574 ms
 6  NO-NIT-TN-5.taide.net (193.219.193.145)  533.882 ms  530.595 ms  530.928 ms
 7  nb01b11-pos2-0.nb.telenor.net (217.70.230.45)  531.592 ms  533.452 ms  533.100 ms
 8  nb01b12-ge3-3-0.nb.telenor.net (217.70.228.21)  631.788 ms  678.357 ms  570.329 ms
 9  nb21b12-pos4-2.nb.telenor.net (217.70.227.34)  566.925 ms  615.756 ms  556.670 ms
10  linx-gw1.ja.net (195.66.224.15)  556.974 ms  556.369 ms  555.574 ms
11  po1-1.lond-scr4.ja.net (146.97.35.129)  555.848 ms  555.910 ms  583.818 ms
12  po0-0.lond-scr.ja.net (146.97.33.33)  557.343 ms  564.451 ms  555.941 ms
13  po0-0.cambridge-bar.ja.net (146.97.35.10)  560.323 ms  560.377 ms  561.087 ms
14  route-enet-3.cam.ac.uk (146.97.40.50)  562.861 ms  559.136 ms  559.810 ms
15  route-cent-3.cam.ac.uk (192.153.213.194)  559.727 ms  562.341 ms  559.504 ms
16  hermes-v.csi.cam.ac.uk (131.111.8.59)  559.562 ms  559.973 ms  567.100 ms
</pre>
