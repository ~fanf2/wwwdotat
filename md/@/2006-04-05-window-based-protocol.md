---
dw:
  anum: 79
  eventtime: "2006-04-05T16:57:00Z"
  itemid: 216
  logtime: "2006-04-05T16:59:06Z"
  props:
    commentalter: 1491292332
    import_source: livejournal.com/fanf/55727
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/55375.html"
format: casual
lj:
  anum: 175
  can_comment: 1
  ditemid: 55727
  event_timestamp: 1144256220
  eventtime: "2006-04-05T16:57:00Z"
  itemid: 217
  logtime: "2006-04-05T16:59:06Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/55727.html"
title: "\"window-based\" protocol"
...

http://www.psc.edu/networking/projects/tcptune/

This article says TCP is "window-based" in a way that implies that there are alternative models. <em>Are</em> there alternative models?
