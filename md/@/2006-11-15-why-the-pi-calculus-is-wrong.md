---
dw:
  anum: 209
  eventtime: "2006-11-15T23:36:00Z"
  itemid: 265
  logtime: "2006-11-15T23:36:59Z"
  props:
    commentalter: 1491292339
    import_source: livejournal.com/fanf/68173
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/68049.html"
format: html
lj:
  anum: 77
  can_comment: 1
  ditemid: 68173
  event_timestamp: 1163633760
  eventtime: "2006-11-15T23:36:00Z"
  itemid: 266
  logtime: "2006-11-15T23:36:59Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/68173.html"
title: Why the &pi; calculus is wrong
...

<p>I've recently been pondering models of concurrent programming. One of the simplest, most beautiful and most powerful is the <a href="http://en.wikipedia.org/wiki/Pi-calculus">&pi; calculus</a> - it's effetively the &lambda; calculus of concurrent programming.</p>

<p>The &pi; calculus is oriented around channels, which can be read from, written to, and passed along channels: that is, they can be passed between processes (though processes are implicit). Reading and writing are synchronous: if a process wants to read or write a channel, it blocks until there is another process wanting to do the opposite, and after the communication both processes can proceed with their sequels. The alternative to this is for writes to be asynchronous: they do not block.</p>

<p>The problem with synchronous writes is that they do not translate well into a distributed setting. An asynchronous write translates directly into a network transmission, but a synchronous write requires a round trip to unblock the writer. As a result it is less efficient and more vulnerable to failure. Even in a simple local setting, asynchronous sends may be preferable: in fact, <a href="http://www.cis.upenn.edu/~bcpierce/papers/pict/Html/Pict.html">Pict</a>, a programming language closely based on the &pi; calculus, is restricted to asynchronous writes. On the other hand, with asynchronous writes you may need a queue on the reader's end, and some way to avoid running out of memory because of overflowing queues.</p>

<p>Another design alternative is to focus on processes instead of channels. Messages are sent to processes, and PIDs can be transmitted between processes . In this model the read ends of channels are effectively fixed. This is the <a href="http://en.wikipedia.org/wiki/Actor_model">actor model</a> as used by <a href="http://en.wikipedia.org/wiki/Erlang_%28programming_language%29">Erlang</a>.</p>

<p>The problem with the &pi; calculus's channels, or a distributed-object system like <a href="http://lucacardelli.name/Obliq/Obliq.html">Obliq</a> or <a href="http://www.cypherpunks.to/erights/talks/thesis/markm-thesis.pdf">E (see &sect;17.4 on p. 126)</a>, is that you need distributed garbage collection. If I spawn a process to listen on a channel locally, and send the channel to a remote system, I must keep the process around as long as that remote system may want to write on the channel. Distributed garbage collection is far too difficult. On the other hand, in the actor model a process is the master of its own lifetime, because it lasts until its thread of execution completes. Thus you avoid relying on tricky algorithms, at the cost of needing some way of dealing with failed message sends. But in a distributed system you have to deal with failure anyway, and the same mechanism can deal with the queue overflow problem.</p>

<p>So I conclude that the &pi; calculus is wrong for distributed programming and that Erlang's actor model is right. Those <a href="http://video.google.com/videoplay?docid=-5830318882717959520">Erlang designers</a> are clever chaps.</p>
