---
dw:
  anum: 109
  eventtime: "2016-06-01T19:40:00Z"
  itemid: 459
  logtime: "2016-06-01T18:40:25Z"
  props:
    commentalter: 1491292429
    import_source: livejournal.com/fanf/144696
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/117613.html"
format: html
lj:
  anum: 56
  can_comment: 1
  ditemid: 144696
  event_timestamp: 1464810000
  eventtime: "2016-06-01T19:40:00Z"
  itemid: 565
  logtime: "2016-06-01T18:40:25Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 8
  url: "https://fanf.livejournal.com/144696.html"
title: Experimenting with <code>_Generic()</code> for parametric constness in C11
...

<p>One of the new features in C99 was <code>tgmath.h</code>, the type-generic
mathematics library. (See section 7.22 of
<a href="http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf">n1256</a>.)
This added ad-hoc polymorphism for a subset of the library, but C99
offered no way for programmers to write their own type-generic macros
in standard C.</p>

<p>In C11 the language acquired the <code>_Generic()</code> generic selection
operator. (See section 6.5.1.1 of
<a href="http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf">n1570</a>.)
This is effectively a type-directed switch expression. It can be used
to implement APIs like <code>tgmath.h</code> using standard C.</p>

<p>(The weird spelling with an underscore and capital letter is because
that part of the namespace is reserved for future language extensions.)</p>

<h2>When <code>const</code> is ugly</h2>

<p>It can often be tricky to write const-correct code in C, and
retrofitting constness to an existing API is much worse.</p>

<p>There are some fearsome examples in the standard library. For
instance, <code>strchr()</code> is declared:</p>

<pre><code>    char *strchr(const char *s, int c);
</code></pre>

<p>(See section 7.24.5.2 of
<a href="http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf">n1570</a>.)</p>

<p>That is, it takes a const string as an argument, indicating that it
doesn't modify the string, but it returns a non-const pointer into the
same string. This hidden de-constifying cast allows <code>strchr()</code> to be
used with non-const strings as smoothly as with const strings, since
in either case the implicit type conversions are allowed. But it is an
ugly loop-hole in the type system.</p>

<h2>Parametric constness</h2>

<p>It would be much better if we could write something like,</p>

<pre><code>    const&lt;A&gt; char *strchr(const&lt;A&gt; char *s, int c);
</code></pre>

<p>where <code>const&lt;A&gt;</code> indicates variable constness. Because the same
variable appears in the argument and return types, those strings are
either both const or both mutable.</p>

<p>When checking the function definition, the compiler would have to
treat parametric <code>const&lt;A&gt;</code> as equivalent to a normal const qualifier.
When checking a call, the compiler allows the argument and return
types to be const or non-const, provided they match where the
parametric consts indicate they should.</p>

<p>But we can't do that in standard C.</p>

<h2>Or can we?</h2>

<p>When I mentioned this idea on Twitter a few days ago,
<a href="https://twitter.com/jckarter/status/736687735038054400">Joe Groff said "<code>_Generic</code> to the
rescue"</a>,
so I had to see if I could make it work.</p>

<h2>Example: <code>strchr()</code></h2>

<p>Before wrapping a standard function with a macro, we have to remove
any existing wrapper. (Standard library functions can be wrapped by
default!)</p>

<pre><code>    #ifdef strchr
    #undef strchr
    #endif
</code></pre>

<p>Then we can create a replacement macro which implements parametric
constness using <code>_Generic()</code>.</p>

<pre><code>    #define strchr(s,c) _Generic((s),                    \
        const char * : (const char *)(strchr)((s), (c)), \
        char *       :               (strchr)((s), (c)))
</code></pre>

<p>The first line says, look at the type of the argument <code>s</code>.</p>

<p>The second line says, if it is a <code>const char *</code>, call the function
<code>strchr</code> and use a cast to restore the missing constness.</p>

<p>The third line says, if it is a plain <code>char *</code>, call the function
<code>strchr</code> leaving its return type unchanged from <code>char *</code>.</p>

<p>The <code>(strchr)()</code> form of call is to avoid warnings about attempts to
invoke a macro recursively.</p>

<pre><code>    void example(void) {
        const char *msg = "hello, world\n";
        char buf[20];
        strcpy(buf, msg);

        strchr(buf, ' ')[0] = '\0';
        strchr(msg, ' ')[0] = '\0';
        strchr(10,20);
    }
</code></pre>

<p>In this example, the first call to <code>strchr</code> is always OK.</p>

<p>The second call typically fails at runtime with the standard <code>strchr</code>,
but with parametric constness you get a compile time error saying that
you can't modify a const string.</p>

<p>Without parametric constness the third call gives you a type
conversion warning, but it still compiles! With parametric constness
you get an error that there is no matching type in the <code>_Generic()</code>
macro.</p>

<h2>Conclusion</h2>

<p>That is actually pretty straightforward, which is nice.</p>

<p>As well as parametric constness for functions, in the past I have also
wondered about parametric constness for types, especially structures.
It would be nice to be able to use the same code for read-only static
data as well as mutable dynamic data, and have the compiler enforce
the distinction. But <code>_Generic()</code> isn't powerful enough, and in any
case I am not sure how such a feature should work!</p>
