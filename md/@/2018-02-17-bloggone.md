---
dw:
  anum: 105
  eventtime: "2018-02-17T16:17:00Z"
  itemid: 480
  logtime: "2018-02-17T16:40:36Z"
  props:
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/122985.html"
format: html
lj:
  anum: 18
  can_comment: 1
  ditemid: 150034
  event_timestamp: 1518895800
  eventtime: "2018-02-17T19:30:00Z"
  itemid: 586
  logtime: "2018-02-17T19:30:52Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/150034.html"
title: Bloggone
...

<p>For various bad reasons I haven't blogged here since I said I was
moving to Dreamwidth last year. Part of it is that I haven't found a
satisfactory page layout / style sheet (sigh).</p>

<p>I have ended up writing blog-type things in other contexts, mostly
work-related. In the last couple of months I have been posting them On
Jackdaw: one page of
<a href="https://jackdaw.cam.ac.uk/ipreg/future.html">plans for the future</a>
and another with
<a href="https://jackdaw.cam.ac.uk/ipreg/progress.html">rants and raves about work in progress</a>.</p>

<p>This work will include a lot more web development than I have done in
the past. I expect that one side-effect will be to turn those
not-quite-blog pages into a more complete blog setup with RSS/Atom
feeds etc. which will I will probably reuse for personal blogging on
my own web site. (Something I have vaguely wanted to do for ages but
have not had the tuits.)</p>

<p>Anyway, in the mean time I have been posting stuff on Twitter, mostly
interesting links, silly retweets (and sometimes political ones, tho I
try not to overdo politics), and other randomness. I posted
<a href="https://twitter.com/fanf/status/961742383410008064">a brief status update</a>
the week before last, I'm still getting occasional notifications about a
<a href="https://twitter.com/fanf/status/961347069872148480">nerdy observation about CamelCase</a>
which became surprisingly popular - my usual standard for
<a href="https://twitter.com/fanf/status/964286266396332032">a popular tweet</a>
is orders of magnitude smaller :-)</p>
