---
dw:
  anum: 42
  eventtime: "2003-03-19T14:10:00Z"
  itemid: 4
  logtime: "2003-03-19T06:34:19Z"
  props:
    import_source: livejournal.com/fanf/1040
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/1066.html"
format: casual
lj:
  anum: 16
  can_comment: 1
  ditemid: 1040
  event_timestamp: 1048083000
  eventtime: "2003-03-19T14:10:00Z"
  itemid: 4
  logtime: "2003-03-19T06:34:19Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/1040.html"
title: More hacking
...

I've put the work I did last night <a href="http://people.freebsd.org/~fanf/FreeBSD/NETALIAS-5">here</a>.
