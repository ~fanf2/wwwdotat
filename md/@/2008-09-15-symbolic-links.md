---
dw:
  anum: 8
  eventtime: "2008-09-15T14:40:00Z"
  itemid: 361
  logtime: "2008-09-15T15:40:53Z"
  props:
    commentalter: 1491292360
    import_source: livejournal.com/fanf/93483
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/92424.html"
format: html
lj:
  anum: 43
  can_comment: 1
  ditemid: 93483
  event_timestamp: 1221489600
  eventtime: "2008-09-15T14:40:00Z"
  itemid: 365
  logtime: "2008-09-15T15:40:53Z"
  props: {}
  reply_count: 9
  url: "https://fanf.livejournal.com/93483.html"
title: Symbolic links
...

<p>What happens if you try to create a symlink to a zero-length target, as in <tt>ln -s "" foo</tt>?</p>

<p><b>Linux</b>: ENOENT.</p>

<p><b>Mac OS X 10.4</b>: EINVAL.</p>

<p><b>FreeBSD</b>: works, and any attempt to resolve the symlink returns ENOENT.</p>

<p><b>Solaris</b>: works, and the resulting symlink behaves the same as <tt>ln -s . foo</tt></p>
