---
dw:
  anum: 205
  eventtime: "2005-11-11T19:36:00Z"
  itemid: 164
  logtime: "2005-11-11T19:38:47Z"
  props:
    commentalter: 1491292326
    import_source: livejournal.com/fanf/42443
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/42189.html"
format: casual
lj:
  anum: 203
  can_comment: 1
  ditemid: 42443
  event_timestamp: 1131737760
  eventtime: "2005-11-11T19:36:00Z"
  itemid: 165
  logtime: "2005-11-11T19:38:47Z"
  props: {}
  reply_count: 19
  url: "https://fanf.livejournal.com/42443.html"
title: Signing up for more work
...

Foolish, I know, but I'll get stale if I do <em>nothing</em> but email. This week I are been mostly writing a draft proposal to be given to my senior management team, suggesting that I should implement a Jabber service for Cambridge University. All comments and suggestions welcome!

http://www.cus.cam.ac.uk/~fanf2/hermes/doc/jabber/proposal.txt
