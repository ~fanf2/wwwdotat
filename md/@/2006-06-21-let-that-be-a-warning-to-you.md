---
dw:
  anum: 12
  eventtime: "2006-06-21T15:35:00Z"
  itemid: 241
  logtime: "2006-06-21T15:43:27Z"
  props:
    commentalter: 1491292335
    import_source: livejournal.com/fanf/62190
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/61708.html"
format: casual
lj:
  anum: 238
  can_comment: 1
  ditemid: 62190
  event_timestamp: 1150904100
  eventtime: "2006-06-21T15:35:00Z"
  itemid: 242
  logtime: "2006-06-21T15:43:27Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/62190.html"
title: Let that be a warning to you
...

In the course of cleaning up after the departmental email server failure, I've noticed that we have an enormous number of messages on our queues for one user, like about 14 000 out of about 18 000 on each of the machines.

It turns out that this user has a vanity domain which forwards email for any local part at that domain to his Hermes account. This vanity domain is being spammed to death, with the result that his quota is entirely used up with 30 000 spam messages and he has another 70 000 waiting.

What is worse, he isn't even attached to this University any more: his Hermes account seems to be a left-over from when he was a lecturer here. It looks like he's given up on it - he stopped popping his email from it last month - but of course he hasn't got in touch with us about the problem. Aaargh!

Never accept email to invalid local parts! It is just asking for trouble!
