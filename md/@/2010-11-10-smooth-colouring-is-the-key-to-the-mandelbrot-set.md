---
lj:
  anum: 201
  can_comment: 1
  ditemid: 110025
  event_timestamp: 1289404440
  eventtime: "2010-11-10T15:54:00Z"
  itemid: 429
  logtime: "2010-11-10T15:54:21Z"
  props:
    personifi_tags: "15:30,2:7,17:4,18:8,8:42,33:3,23:7,32:4,16:4,1:12,3:10,4:2,20:14,9:14,nterms:yes"
  reply_count: 12
  url: "https://fanf.livejournal.com/110025.html"
title: Smooth colouring is the key to the Mandelbrot set
...

At [Cambridge Geek Night 6](http://cambridgegeeknights.net/) on Monday
I gave a brief talk about what has been occupying some of my spare
time in recent weeks. Since [Benoît
Mandelbrot](https://en.wikipedia.org/wiki/Beno%C3%AEt_Mandelbrot) died
last month I have been playing around with making pretty pictures of
Mandelbrot sets, culminating (so far) in a movie of a deep zoom
targeted near <i>z</i> = -0.15 + 1.04<i>i</i>. The software and
materials for my talk are at <https://dotat.at/prog/mandelbrot/>.

One of the nicest renderings I did when I was a teenager was a 300dpi
laser-printed [binary decomposition of the level sets of the exterior
of the Mandelbrot
set](https://en.wikibooks.org/wiki/File:Mandel_bd.jpg). It took a
whole afternoon to generate using BBC BASIC on my Archimedes, which
was the best tool for numeric code I had available. I wanted to be
able to produce smoothly coloured renderings like the ones in the
fractal art books by [Heinz-Otto
Peitgen](https://en.wikipedia.org/wiki/Heinz-Otto_Peitgen) and his
collaborators, but a few things prevented me. Firstly the lack of a
computer that could display true colour images :-) and secondly the
difficulty of the mathematics. If I remember correctly, I just about
grasped enough of the [distance estimation
method](https://dotat.at/prog/mandelbrot/distance.pdf) to understand
that it requires significantly more work per iteration, which also
made it unattractive on the slow computers I had to hand.

Revisiting the Mandelbrot set now, I have the luxury of computers that
are thousands of times faster and thousands of times more colourful
and millions of times more accurate with their arithmetic. The
Internet also provides more accessible resources on the mathematics of
the Mandelbrot set. [A page by Linas
Vepstas](https://linas.org/art-gallery/escape/escape.html) was
particularly helpful with the smooth colouring problem. Even better,
it turns out that his simple recipe arises from the mathematics that
gives us our deepest understanding of the Mandelbrot set.

First some basic notation to make the formulae less cluttered.

<math>
 <mtable>
  <mtr>
   <mtd columnalign="right">
    <msub><mi>f</mi><mi>c</mi></msub><mo>(</mo><mi>z</mi><mo>)</mo>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd>
    <msup><mi>z</mi><mn>2</mn></msup><mo>+</mo><mi>c</mi>
   </mtd>
  </mtr>
  <mtr>
   <mtd columnalign="right">
    <msub><mi>z</mi><mi>n</mi></msub>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd>
    <msubsup><mi>f</mi><mi>c</mi><mi>n</mi></msubsup>
    <mo>(</mo><mi>z</mi><mo>)</mo>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <msub><mi>f</mi><mi>c</mi></msub><mo>(</mo>
    <mo>⋯</mo>
    <msub><mi>f</mi><mi>c</mi></msub><mo>(</mo><mi>z</mi><mo>)</mo>
    <mo>)</mo>
   </mtd>
  </mtr>
  <mtr>
   <mtd columnalign="right">
    <msub><mi>r</mi><mi>n</mi></msub>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd>
    <mo>|</mo>
    <msub><mi>z</mi><mi>n</mi></msub>
    <mo>|</mo>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <mo>|</mo>
    <msubsup><mi>f</mi><mi>c</mi><mi>n</mi></msubsup>
    <mo>(</mo><mi>z</mi><mo>)</mo>
    <mo>|</mo>
   </mtd>
  </mtr>
 </mtable>
</math>

To generate a Julia set, <i>c</i> is a fixed parameter and <i>z</i>
varies across the complex plane. For the Mandelbrot set, <i>c</i>
varies across the plane and <i>z</i> starts its iteration at 0. In
both cases we iterate until <i>z</i> escapes to infinity, in which
case it is outside the set, or remains bounded, in which case it is
inside. Because we are most interested in the behaviour of <i>z</i> as
it is iterated, we usually leave the parameter <i>c</i> implicit.

The simplest Julia set, when <i>c</i> = 0, is just the unit circle. In
this case we can write down a direct formula for
<i>z<sub>n</sub></i>:

<math>
 <mtable>
  <mtr>
   <mtd columnalign="right">
    <msub><mi>f</mi><mn>0</mn></msub><mo>(</mo><mi>z</mi><mo>)</mo>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <msup><mi>z</mi><mn>2</mn></msup>
   </mtd>
  </mtr>
  <mtr>
   <mtd columnalign="right">
    <msub><mi>z</mi><mi>n</mi></msub>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <msubsup><mi>f</mi><mi>c</mi><mn>0</mn></msubsup>
    <mo>(</mo><mi>z</mi><mo>)</mo>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <msubsup>
     <mi>z</mi>
     <mrow><mi>n</mi><mo>-</mo><mn>1</mn></mrow>
     <mn>2</mn>
    </msubsup>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <msubsup>
     <mi>z</mi>
     <mrow><mi>n</mi><mo>-</mo><mn>2</mn></mrow>
     <mn>4</mn>
    </msubsup>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <msubsup>
     <mi>z</mi>
     <mrow><mi>n</mi><mo>-</mo><mn>3</mn></mrow>
     <mn>8</mn>
    </msubsup>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <msup>
     <mi>z</mi>
     <msup>
      <mn>2</mn>
      <mi>n</mi>
     </msup>
    </msup>
   </mtd>
  </mtr>
 </mtable>
</math>

There is something called a Böttcher map which transforms the exterior
of Julia set <i>c</i> = 0 into the exterior of other connected Julia
sets and back again. It's like a
[Riemann map](https://en.wikipedia.org/wiki/Riemann_mapping_theorem),
but it operates on the complement of the set.

<math>
 <mtable>
  <mtr>
   <mtd columnalign="right">
    <msub><mi>f</mi><mi>c</mi></msub><mo>(</mo><mi>z</mi><mo>)</mo>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <msubsup><mi>𝚽</mi><mi>c</mi><mrow><mo>-</mo><mn>1</mn></mrow></msubsup>
    <mo>(</mo><msub><mi>f</mi><mi>0</mi></msub>
    <mo>(</mo><msub><mi>𝚽</mi><mi>c</mi></msub>
    <mo>(</mo><mi>z</mi><mo>)</mo>
    <mo>)</mo><mo>)</mo>
   </mtd>
  </mtr>
 </mtable>
</math>

The definition of the map is as follows. Observe that
Φ<sub>0</sub>(<i>z</i>) is the identity function.

<math>
 <mtable>
  <mtr>
   <mtd columnalign="right">
    <msub><mi>𝚽</mi><mi>c</mi></msub><mo>(</mo><mi>z</mi><mo>)</mo>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <munder>
     <mo>lim</mo>
     <mrow><mi>n</mi><mo>→</mo><mn>∞</mn></mrow>
    </munder>
    <mroot>
     <mrow>
      <msub><mi>z</mi><mi>n</mi></msub>
      <mspace height="1em"/>
     </mrow>
     <msup mathsize="0.8em">
      <mn>2</mn><mi>n</mi>
     </msup>
    </mroot>
   </mtd>
  </mtr>
 </mtable>
</math>

Adrien Douady and John Hubbard showed that [the Böttcher map
Φ<sub><i>c</i></sub>(0)](https://en.wikipedia.org/wiki/File:Jung50e.png)
is a conformal mapping from the exterior of the Mandelbrot set M to
the exterior of the unit disk, and therefore the Mandelbrot set is
connected.

The Böttcher map allows us to treat the exterior of the set as an
electrostatic field. Imagine an infinitely long crinkly cylinder whose
cross-section is the set. If you electrically charge the cylinder it
creates a field in the surrounding space. The Böttcher map tells you
how the simple circular equipotential curves and radial field lines of
the Julia set <i>c</i> = 0 correspond to those surrounding the
Mandelbrot set and the other connected Julia sets.

This is a powerful tool for understanding the Mandelbrot set. The
binary decomposition makes a lot of the field structure visible, since
the boundaries between the black and white areas are formed from
segments of field lines and equipotential curves. If you express the
angle of a field line as a binary fraction of a whole turn, you get
the sequence of black and white segments that the field line crosses
in the binary decomposition. Field lines with rational angles
correspond to pinch points on the Mandelbrot set.

For our purposes we are less interested in field lines and more
interested in the Douady-Hubbard potential function. We use the
Böttcher map to warp the space around the set into its simplest form.
This allows us to use the standard formula for the electrostatic
potential around an infinite wire, which is the log of the distance
from the wire. The base of the logarithm is related to how highly
charged the wire is; we can choose it for our convenience.

<math>
 <mtable>
  <mtr>
   <mtd columnalign="right">
    <msub><mi>𝜙</mi><mi>c</mi></msub><mo>(</mo><mi>z</mi><mo>)</mo>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <mo>log</mo>
    <mo>|</mo>
    <msub><mi>𝚽</mi><mi>c</mi></msub><mo>(</mo><mi>z</mi><mo>)</mo>
    <mo>|</mo>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <munder>
     <mo>lim</mo>
     <mrow><mi>n</mi><mo>→</mo><mn>∞</mn></mrow>
    </munder>
    <mo>log</mo>
    <mo>|</mo>
    <msubsup>
     <mi>z</mi>
     <mi>n</mi>
     <mrow>
      <mn>1</mn>
      <mo>/</mo>
      <msup><mn>2</mn><mi>n</mi></msup>
     </mrow>
    </msubsup>
    <mo>|</mo>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <munder>
     <mo>lim</mo>
     <mrow><mi>n</mi><mo>→</mo><mn>∞</mn></mrow>
    </munder>
    <mo>log</mo>
    <msubsup>
     <mi>r</mi>
     <mi>n</mi>
     <mrow>
      <mn>1</mn>
      <mo>/</mo>
      <msup><mn>2</mn><mi>n</mi></msup>
     </mrow>
    </msubsup>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd><mo>=</mo></mtd>
   <mtd columnalign="left">
    <munder>
     <mo>lim</mo>
     <mrow><mi>n</mi><mo>→</mo><mn>∞</mn></mrow>
    </munder>
    <mspace  width="0.5em"/>
    <mfrac>
     <mn>1</mn>
      <msup><mn>2</mn><mi>n</mi></msup>
    </mfrac>
    <mo>log</mo>
    <msub>
     <mi>r</mi>
     <mi>n</mi>
    </msub>
   </mtd>
  </mtr>
 </mtable>
</math>

Since we don't have time to count to infinity, in practice we define a
bail-out radius <i>R</i> and stop iterating points that get further
than that from the origin. The iteration count <i>n</i> is used for
the flat blocky level-set colouring scheme.

<math>
 <mtable>
  <mtr>
   <mtd>
    <msub>
     <mi>r</mi>
     <mrow><mi>n</mi><mo>-</mo><mn>1</mn></mrow>
    </msub>
   </mtd>
   <mtd><mo>≤</mo></mtd>
   <mtd>
    <mi>R</mi>
   </mtd>
   <mtd><mo>&lt;</mo></mtd>
   <mtd>
    <msub>
     <mi>r</mi>
     <mi>n</mi>
    </msub>
   </mtd>
  </mtr>
 </mtable>
</math>

The way to get a smooth colouring is to ask what fractional iteration
value <i>ν</i> would make the point land exactly on the bail-out
radius.

<math>
 <mtable>
  <mtr>
   <mtd>
    <mi>n</mi><mo>-</mo><mn>1</mn>
   </mtd>
   <mtd><mo>≤</mo></mtd>
   <mtd>
    <mi>𝜈</mi>
   </mtd>
   <mtd><mo>&lt;</mo></mtd>
   <mtd>
    <mi>n</mi>
   </mtd>
  </mtr>
 </mtable>
</math>

We can use the potential function to formalize this idea. If <i>R</i>
is large, varying it will not make much difference to the potential we
calculate for a point. So to derive 𝜈 we assume that we get the
same result from the two expressions for the potential in the first
line below. (I think this is what causes the small errors and
discontinuities discussed by Linas.)

<math>
 <mtable>
  <mtr>
   <mtd>
    <mfrac>
     <mrow><mo>log</mo><mi>R</mi></mrow>
     <msup><mn>2</mn><mi>𝜈</mi></msup>
    </mfrac>
   </mtd>
   <mtd><mo>≃</mo></mtd>
   <mtd>
    <mfrac>
     <mrow>
      <mo>log</mo>
      <msub><mi>r</mi><mi>n</mi></msub>
     </mrow>
     <msup><mn>2</mn><mi>n</mi></msup>
    </mfrac>
   </mtd>
  </mtr>
  <mtr>
   <mtd>
    <msup>
     <mn>2</mn>
     <mrow><mi>n</mi><mo>-</mo><mi>𝜈</mi></mrow>
    </msup>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd>
    <mfrac>
     <mrow>
      <mo>log</mo>
      <msub><mi>r</mi><mi>n</mi></msub>
     </mrow>
     <mrow><mo>log</mo><mi>R</mi></mrow>
    </mfrac>
   </mtd>
  </mtr>
  <mtr>
   <mtd>
    <mi>𝜈</mi>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd>
    <mi>n</mi>
    <mo>-</mo>
    <msub><mo lspace="0em" rspace="0em">log</mo><mn>2</mn></msub>
    <mrow>
     <mo>(</mo>
     <mfrac>
      <mrow>
       <mo>log</mo>
       <msub><mi>r</mi><mi>n</mi></msub>
      </mrow>
      <mrow><mo>log</mo><mi>R</mi></mrow>
     </mfrac>
     <mo>)</mo>
    </mrow>
   </mtd>
  </mtr>
  <mtr>
   <mtd></mtd>
   <mtd><mo>=</mo></mtd>
   <mtd>
    <mi>n</mi>
    <mo>-</mo>
    <msub><mo lspace="0em" rspace="0em">log</mo><mn>2</mn></msub>
    <mrow><mo>(</mo>
     <mo lspace="0em">log</mo>
     <msub><mi>r</mi><mi>n</mi></msub>
    <mo>)</mo></mrow>
    <mo>+</mo>
    <msub><mo lspace="0em" rspace="0em">log</mo><mn>2</mn></msub>
    <mrow><mo>(</mo>
     <mo lspace="0em">log</mo><mi>R</mi>
    <mo>)</mo></mrow>
   </mtd>
  </mtr>
 </mtable>
</math>

The final term in that equation is a constant, since <i>R</i> is a
fixed parameter of our implementation. We might as well throw it away
since it does not add any useful information.

<math>
 <mtable>
  <mtr>
   <mtd>
    <mi>n</mi>
    <mo lspace="0em">ʹ</mo>
   </mtd>
   <mtd><mo>=</mo></mtd>
   <mtd>
    <mi>n</mi>
    <mo>-</mo>
    <msub><mo lspace="0em" rspace="0em">log</mo><mn>2</mn></msub>
    <mrow><mo>(</mo>
     <mo lspace="0em">log</mo>
     <msub><mi>r</mi><mi>n</mi></msub>
    <mo>)</mo></mrow>
   </mtd>
  </mtr>
 </mtable>
</math>

And thus we get the equation to use for smoothly colouring of the
exterior of the Mandelbrot set.
