---
dw:
  anum: 210
  eventtime: "2004-11-14T18:19:00Z"
  itemid: 111
  logtime: "2004-11-14T11:00:46Z"
  props:
    commentalter: 1491292323
    import_source: livejournal.com/fanf/28474
    interface: flat
    opt_backdated: 1
    picture_keyword: silly
    picture_mapid: 6
  url: "https://fanf.dreamwidth.org/28626.html"
format: casual
lj:
  anum: 58
  can_comment: 1
  ditemid: 28474
  event_timestamp: 1100456340
  eventtime: "2004-11-14T18:19:00Z"
  itemid: 111
  logtime: "2004-11-14T11:00:46Z"
  props: {}
  reply_count: 6
  url: "https://fanf.livejournal.com/28474.html"
title: Brilliant Brassicas
...

We've been getting lots of <a href="http://discworld.atuin.net/lpc/links/cabbage/info/cauliflower.html">cauliflower</a> in the organic veg boxes recently, and in the last couple of weeks they have been getting odder.

<a href="http://www.purplecauliflower.com.au/images/pur2.jpg">Purple cauliflower:</a> This vareity apparently resulted from the discovery of a purple coloured spontaneous mutant plant in a cauliflower field in the late 1980s, and is supposed to have an excellent flavour. The purple colour is related to the colouring of red cabbage. This Aussie distributor has an interesting web site: http://www.purplecauliflower.com.au/

<a href="http://www.superherodesigns.com/journal/romanesque_400.jpg">Romanesque Cauliflower or Broccoli:</a> This is even more extravagantly fractal than your usual cauliflower or broccoli. Unfortunately Google's not coming up with a decent web site about this most mathematical of vegetables. So here are some miscellaneous links: http://www.livejournal.com/users/pipu/215266.html  http://cbsnewyork.com/tony/tonytanillo_story_283153435.html  http://www.artonshow.biz/info/notes/note2pineapple.htm  http://en.wikipedia.org/wiki/Broccoli  http://alt.venus.co.uk/weed/fractals/romanesco.htm  http://www.samcooks.com/relish/cauliflower.htm
