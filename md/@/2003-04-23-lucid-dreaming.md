---
dw:
  anum: 110
  eventtime: "2003-04-23T13:06:00Z"
  itemid: 17
  logtime: "2003-04-23T06:27:59Z"
  props:
    commentalter: 1491292304
    current_moodid: 93
    current_music: Total Euphoria CD2 mixed by Dave Pearce
    import_source: livejournal.com/fanf/4417
    interface: flat
    opt_backdated: 1
    picture_keyword: photo
    picture_mapid: 3
  url: "https://fanf.dreamwidth.org/4462.html"
format: casual
lj:
  anum: 65
  can_comment: 1
  ditemid: 4417
  event_timestamp: 1051103160
  eventtime: "2003-04-23T13:06:00Z"
  itemid: 17
  logtime: "2003-04-23T06:27:59Z"
  props:
    current_moodid: 93
    current_music: Total Euphoria CD2 mixed by Dave Pearce
  reply_count: 0
  url: "https://fanf.livejournal.com/4417.html"
title: Lucid dreaming
...

I've been dreaming a lot more than usual recently. I had a particularly odd one on Saturday shortly before I started having control over what was going on. I was merging a patch against Mutt and I needed to check the CVS history, so I ran my cvslog script and things went all Tron on me: There were lots and lots of branches like ropes forming a kind of carpet. I could see the revisions to each file as annotations along the ropes like tool tips. Ulrich Drepper was surfing down the slope formed by this carpet, as if he had made the majority of the changes (I never was very good at names).

In other news, I went climbing for the first time yesterday evening. I was invited by Rachel Coleman and we went with Cormac O'Connell and Stephanie Riach to the <a href="http://www.mepal.co.uk/Start.htm">Mepal Outdoor Centre</a> out in the fens of Cambridgeshire, where there are a couple of climbing walls. I didn't have the strength or stamina to get very far up the taller walls, and I can definitely feel several of my muscles this morning, but it was great fun. Definitely worth doing more of, I think.

(Earworm of the evening was Castles in the Sky by Ian Van Dahl.)
