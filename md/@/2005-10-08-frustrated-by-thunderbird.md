---
dw:
  anum: 63
  eventtime: "2005-10-08T12:20:00Z"
  itemid: 153
  logtime: "2005-10-08T14:55:14Z"
  props:
    commentalter: 1491292370
    import_source: livejournal.com/fanf/39428
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/39231.html"
format: casual
lj:
  anum: 4
  can_comment: 1
  ditemid: 39428
  event_timestamp: 1128774000
  eventtime: "2005-10-08T12:20:00Z"
  itemid: 154
  logtime: "2005-10-08T14:55:14Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 7
  url: "https://fanf.livejournal.com/39428.html"
title: Frustrated by Thunderbird
...

We were sorry to hear earlier this week that Cyrusoft, the publishers of Mulberry, have <a href="http://www.cyrusoft.com/">gone bankrupt</a>. Cyrus Daboo started writing Mulberry at Cambridge (when I was an undergraduate) with close co-operation from the Computing Service and our Mac users. Mulberry was the first really good GUI IMAP client so it's a shame to see it die an unnaturally early death. (More about the early days of Mulberry <a href="http://web.archive.org/web/20050428125201/http://www.cyrusoft.com/company/philosophy.html">here</a>.)

For the above reasons we have in the past recommended Mulberry to our users; we can continue to support it for the rest of this academic year, but we need to find something to replace it. To this end I have been playing around with Thunderbird. I have been sadly disappointed. It is buggy, its user interface is clunky, and it encourages users to send their passwords across the net in the clear. The latter in particular is inexcusable for a modern piece of software.

<h3>How not to do it</h3><em>In which Tony goes off on one about how Thunderbird is far too difficult to configure...</em>

When you start Thunderbird for the first time, the first thing you see is a broken "import settings" dialogue which fails to list anywhere to import settings from. Nice start!

Then Thunderbird puts you through a new account setup wizard. On the first pane you can choose from email, rss, or usenet. Having chosen the first you then go to the "identity" pane where you enter your name and email address.

Next is the slightly confusing server information pane. This allows you to choose between POP and IMAP, and type in the host names of the incoming POP/IMAP server and the outgoing SMTP server. The confusion arises from an option that appears if you select POP (which is the default), in which case another option appears which seems to be about whether or not to merge multiple POP feeds into a single INBOX - the description isn't very clear; what is worse is that there is a hrizontal line between the incoming server hostname and this option, and no line between this option and the outgoing server hostname, so the visual cues give you the wrong idea about what this relates to until you flip the POP/IMAP switch.

Note that this pane does not have any security or port number options!

Next is the user names pane in which you type in your user names for the incoming and outgoing servers. The text boxes default to the local part of your email address, which is quite nice. However there are still no security options.

The final pane allows you to give this account a friendly label for the GUI. After this the main window opens and you get asked to type in your password to log into the IMAP server.

At this point I'm pretty suspicious. Surely they can't be so stupid as to send the password in the clear? After all, the IMAP standard requires TLS support. But paranoia means I fire up tcpdump and type in a bogus password. Lo! the password appears in the tcpdump output. I peevishly cancel the login and go looking for the account settings.

The first pane of the preferences dialogue box has a button for configuring how Thunderbird connects to the Internet [sic]. Clicking that reveals that this means proxy configuration, but it doesn't use the correct terminology until you open the subordinate dialogue box. The settings for the actual mail servers are nowhere to be found in the remaining preferences panes. (This is not unprecedented, because Mac OS X Mail also does not include account settings in the preferences dialogue box.) Further digging through the menus reveals that account settings lives in the tools menu.

The primary pane for the account deals with identity information (name, email address, signature, etc.) and, somewhat out of place, the SMTP server. The latter option is in the form of a useless drop-down menu with no direct way to change the detailed settings.

The next pane is for server settings, which is only for the incoming server. This allows you to change the host name, and - mirabile dictu! - the port number. There's a security settings panel which allows you to choose how to use TLS, including the "expose my password" (i.e. "never") and "man-in-the-middle vulnerability" (i.e. "optional") settings. There's also a "secure authentication" tickybox, which like the Internet connection preferences button is clearly a meaningless euphemism for something that the developers think is too technical for users. Googling reveals that it means challenge-response authentication, i.e. CRAM-MD5 - it might also include Kerberos, but since Thunderbird has no documentation I can't easily find out.

So I turn on TLS and continue to look for the SMTP server options. Further down there is a security pane. This reveals itself to be about message encryption, not security in general. Another stupid euphemism. I eventually find the SMTP server options, entirely divorced from the rest of the account settings in a separate sub-tree of the dialogue box. This pane has a just list of one SMTP server host name, which I have to select before clicking the edit button in order to be transferred to a separate dialogue box which - at last - allows me to set the port number and TLS options.

After all that, Thunderbird is finally configured correctly, and indeed it works. However I was put through three panes of the wizard to enter the settings in the first place, which resulted in a dangerously insecure (and broken) configuration, after which I had to dig through four panels of the account settings dialogue box, after going on a wild goose chase through five panels of the preferences dialogue and one in the account settings dialogue.

This is by far the worst MUA configuration user interface I have seen - even Outlook is better. However all of them are bad, because they expose too much complexity to the user and encourage the use of insecure settings.

<h3>The right way to do it</h3><em>In which Tony expounds on secure user interface design...</em>

The following is based on a couple of simple principles: be as secure as possible by default; and never ask the user something that can be found out automatically.

The account setup wizard needs at least the following information:

<ul><li>The user's name</li>
<li>The user's email address</li>
<li>The incoming server name</li>
<li>The outgoing server name</li>
<li>The user's password.</li>
<li>By default the user's login name is the local part of their email address. There must be an option to edit this, but in many cases that will not be necessary.</li></ul>
When this has been entered, the software probes the servers to automatically find out the rest of the information, including port numbers, TLS settings, and authentication protocols. The algorithm goes like this:

(1) Connect to the IMAP port (143) of the incoming server and check for the STARTTLS capability. If it is available, set the incoming server options to IMAP with TLS required.

(2) If STARTTLS is not available, try the IMAPS port (993) instead. If this works, set the incoming server options to IMAPS.

(3) If either (1) or (2) succeed, do the TLS negotiation and cache the server certificate.

(4) If neither IMAP connection works, perform a similar process with the POP3 (110) and POP3S (995) ports.

Note that IMAP is preferred to POP because it works better if you use multiple MUAs, and because if you try out a POP client and forget to set the keep mail on server option, it will delete everything. However if the user's email provider doesn't give them enough storage space they may wish to override this policy; see below.

(5) Look at what authentication protocols are supported. The order of preference should be (a) Kerberos over TLS or in the clear; (b) password auth over TLS; (c) CRAM-MD5 over TLS or in the clear; (d) password auth in the clear.

CRAM-MD5 has dubious security properties, because it is vulnerable to brute-forcing attacks that password-over-TLS is not, and because the server must have a cleartext-equivalent copy of the users' passwords.

(6) Check that login works, but only if secure authentication is possible. This may require prompting for a password.

(7) Perform a similar process with the outgoing SMTP server. This is slightly more complicated because there are more possibilities. First try the submission port 587; if that doesn't work try the SMTPS port 465; if that doesn't work, try port 25. If secure authentication isn't offered, attempt to send a message from the user to the user (but abort before sending the DATA) to see if the server works as an unauthenticated relay.

I've described this process as being sequential, but in practice the software will want to perform all seven connection attempts concurrently to minimise delays that may be caused by firewalls dropping packets.

When this is complete, present to the user the results of the probe for confirmation or alteration. This will be the usual account settings dialogue box. This should include the following information and controls:

<ul><li>incoming server name, editable</li>
<li>incoming server port number, editable</li>
<li>incoming server protocol, switchable between POP and IMAP</li>
<li>incoming server TLS setting (STARTTLS, SSL-on-connect, optional, none)</li>
<li>incoming server user name</li>
<li>If secure authentication worked, there should be a friendly OK indicator, and an option for the user to view the cached server TLS certificate. If the certificate is not signed by a CA, the software should present it to the user for checking without being asked.</li>
<li>If only insecure authentication was offered, there should be a warning note. This should always appear if the user selects either of the dangerous TLS settings (the last two).</li>
<li>There should be a note if a working login has not been confirmed.</li>
<li>outgoing server name, editable</li>
<li>outgoing server port number, editable</li>
<li>outgoing server TLS setting</li>
<li>outgoing server user name</li>
<li>A tickybox to indicate whether authentication is required; if it is not checked the TLS and user name options are greyed out.</li>
<li>The outgoing server has a similar OK/unconfirmed flag and security note as the incoming server. If TLS and AUTH were not offered by the server, the security note should say that roaming may not work, but should not be as scary as for cleartext password authentication.</li>
<li>Finally, there is a button to trigger test connections using the given settings to confirm that they are working OK, a button to re-do autoconfiguration from scratch, and the usual OK/Cancel pair (though the latter should probably be greyed out for initial configuration!)</li>
</ul>
(There should probably also be options for client TLS certificate authentication as an alternative to password auth.)

As well as the initial setup, the software must allow for configuration changes caused by alterations to the server or by roaming, and should assist the user in these cases.

If secure authentication was not previously possible but the server now advertises it, the software should check that it works OK then present a dialogue box to the user saying "Secure authentication is now possible, and your settings have been upgraded to use it." with an OK button and a button that takes the user to the account settings dialogue box. The software must be very paranoid about the server's TLS certificate in this case, in order to ensure that this isn't actually a man-in-the-middle attack.

If secure authentication was previously possible but is not advertised on this connection, the software should warn the user that they may be under attack and abort the connection. The other possibility is that the user's email provider has one of those misguided setups that only allow secure connections from the public Internet, so the warning should also advise them to contact their support staff. The software should not encourage the user to turn on the "optional TLS" setting in this situation, and should cause extra work for email providers with bad setups.

If the TLS certificate changes, the user should be warned gently - particularly gently if the CA signatures on the old and new cert are both trusted.

<h3>A few final words...</h3>
I have previously considered that perhaps there should be a standard protocol for email software autoconfiguration, such as specially formatted records in the DNS that contain all the necessary information, so that all users would have to do is type in their email address. However, although SRV records can already solve most of this problem, they are not quite enough because although they tell you the host name and port number, they don't tell you about the higher-level security settings. So such a protocol is not entirely trivial; it also has the problem that it would be redundant wrt the in-band feature negotiation of IMAP and SMTP which might cause objections to the idea on architectural grounds. So the above approach of intelligent autoconfiguration is pretty close to ideal; perhaps you could make it even slicker by guessing the IMAP and SMTP server names based on the user's email address, instead of hoping that the SRV idea gets standardized.

Update: see also the expired Internet-Draft http://www.watersprings.org/pub/id/draft-hall-email-srv-02.txt
