---
dw:
  anum: 141
  eventtime: "2006-03-17T18:28:00Z"
  itemid: 210
  logtime: "2006-03-17T18:40:09Z"
  props:
    commentalter: 1491292335
    import_source: livejournal.com/fanf/54134
    interface: flat
    opt_backdated: 1
    picture_keyword: silly
    picture_mapid: 6
  url: "https://fanf.dreamwidth.org/53901.html"
format: casual
lj:
  anum: 118
  can_comment: 1
  ditemid: 54134
  event_timestamp: 1142620080
  eventtime: "2006-03-17T18:28:00Z"
  itemid: 211
  logtime: "2006-03-17T18:40:09Z"
  props: {}
  reply_count: 9
  url: "https://fanf.livejournal.com/54134.html"
title: Irony! Schadenfreude!
...

This has to be the funniest support request I have seen in a long time. How can I reply without making sarcastic comments about there obviously being no problem because the user's <a href="http://www.goldmark.org/jeff/stupid-disclaimers/">stupid email disclaimer</a> tells the recipient to delete the embarrassing message? Extra points, of course, for sending an "urgent" message after 17:00 on a Friday.

[fx: fanf files off the serial numbers]

<em>Help!

I wonder if you can help me please. I have sent an email to someone by mistake. It has confidential information on it that the recipient shouldn't see.

[...]

This e-mail (together with any files transmitted with it) is intended only for the use of the individual(s) or the organisation to whom it is addressed. It may contain information which is strictly confidential or privileged. If you are not the intended recipient, you are notified that any dissemination, distribution or copying of this e-mail is strictly prohibited. If you have received this e-mail in error, please notify the sender by return e-mail (or telephone) and delete the original message.
</em>
