---
dw:
  anum: 25
  eventtime: "2005-07-05T21:52:00Z"
  itemid: 146
  logtime: "2005-07-05T21:59:36Z"
  props:
    commentalter: 1491292323
    import_source: livejournal.com/fanf/37678
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/37401.html"
format: casual
lj:
  anum: 46
  can_comment: 1
  ditemid: 37678
  event_timestamp: 1120600320
  eventtime: "2005-07-05T21:52:00Z"
  itemid: 147
  logtime: "2005-07-05T21:59:36Z"
  props: {}
  reply_count: 6
  url: "https://fanf.livejournal.com/37678.html"
title: Typesetting music
...

I've spent a fun evening getting to grips with <a href="http://www.quercite.com/pmw.html">Philip's Music Writer</a>, the music typesetting software written by my esteemed colleague Philip Hazel. This is for the hymn music on the service sheet for our wedding. So far I have done <a href="http://dotat.at/hymns/marching.ps">hymn 2</a> and <a href="http://dotat.at/hymns/cwmrhondda.ps">hymn 3</a>. The result is quite nice, but the <a href="http://dotat.at/hymns/marching.pmw">source</a> <a href="http://dotat.at/hymns/cwmrhondda.pmw">files</a> remind me strongly of the 1970s Bell Labs Unix typesetting utilities, especially because of the amount of fiddling you have to do to work around quirks in the source language.
