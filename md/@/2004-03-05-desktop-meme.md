---
dw:
  anum: 93
  eventtime: "2004-03-05T09:50:00Z"
  itemid: 69
  logtime: "2004-03-05T01:59:11Z"
  props:
    import_source: livejournal.com/fanf/17915
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/17757.html"
format: casual
lj:
  anum: 251
  can_comment: 1
  ditemid: 17915
  event_timestamp: 1078480200
  eventtime: "2004-03-05T09:50:00Z"
  itemid: 69
  logtime: "2004-03-05T01:59:11Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/17915.html"
title: Desktop meme
...

From <a href="https://marnanel.livejournal.com/">👤marnanel</a>

<b>1. Why did you choose the background color/image you have?</b>

It's a field semy of dotats (representing my email address).

<b>2. Why is your toolbar is where it is?</b>

I don't have a toolbar. My window manager menu pops up when I press the Microsoft menu button on the keyboard (and other window manager functions are controlled with the Microsoft Windows key), which avoids clashes with application keyboard controls.

<b>3. Why are the choices on your toolbar are present?</b>

I don't have a toolbar because they are a waste of space.

<b>4. What are the desktop icons?</b>

Nonexistent.
