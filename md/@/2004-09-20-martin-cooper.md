---
dw:
  anum: 13
  eventtime: "2004-09-20T14:34:00Z"
  itemid: 103
  logtime: "2004-09-20T07:40:29Z"
  props:
    commentalter: 1491292314
    import_source: livejournal.com/fanf/26492
    interface: flat
    opt_backdated: 1
    picture_keyword: passport
    picture_mapid: 4
  url: "https://fanf.dreamwidth.org/26381.html"
format: casual
lj:
  anum: 124
  can_comment: 1
  ditemid: 26492
  event_timestamp: 1095690840
  eventtime: "2004-09-20T14:34:00Z"
  itemid: 103
  logtime: "2004-09-20T07:40:29Z"
  props: {}
  reply_count: 6
  url: "https://fanf.livejournal.com/26492.html"
title: Martin Cooper
...

More news from Helen and Debbie:

http://w3.cambridge-news.co.uk/classified/bmds.asp                                                                                                    
                                                                                                                                                      
"COOPER - Martin James, of Girton and Hills Road, aged 29 years, died suddenly on Thursday, September 9th, 2004, sadly missed by parents Brian and Val, sister Annette, Mary and many others. Service at Girton Parish Church, at 1.30pm, on Wednesday, September 22nd, 2004, followed by burial in churchyard. Family flowers only, donations for charity (to be decided), may be sent to Richard Stebbings Funeral Service, Kendal House, Cambridge Road, Impington. CB4 9NU"

====

"The botanic gardens have said to his parents that if we specify the area Martin liked best, the monkey tree, I think its called, they will try and put the bench in sight of that. Any left over monies from the bench can be donated to the botanical gardens, who will use it to plant an area not already planted and send us a notice to tell us what we planted. His parents would like a copy of that notice."
