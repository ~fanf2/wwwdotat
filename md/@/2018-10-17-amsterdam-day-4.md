---
dw:
  anum: 245
  eventtime: "2018-10-17T00:36:00Z"
  itemid: 504
  logtime: "2018-10-16T22:39:36Z"
  props:
    commentalter: 1539761577
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/129269.html"
format: md
...

Amsterdam day 4
===============

The excitement definitely caught up with me today, and it was a bit of
a struggle to stay awake. On Monday I repeated [the planning error I
made at IETF101](https://dotat.at/@/2018-03-21-ietf-101-tuesday.html)
and missed a lie-in which didn't help! D'oh! So I'm having a quiet
evening instead of going to the RIPE official nightclub party.

Less DNS stuff on the timetable today, but it has still been keeping
me busy:

CDS
---

During the DNS-OARC meeting I spoke to Ondřej Caletka of CESNET (the
Czech national academic network) about his work on automatically
updating DS records for reverse DNS delegations in the RIPE database.
He had some really useful comments about the practicalities of
handling CDS records and how
[`dnssec-cds`](https://jpmens.net/2017/09/21/parents-children-cds-cdnskey-records-and-dnssec-cds/)
does or does not fit into a bigger script, which is kind of important
because I intended `dnssec-cds` to encapsulate the special CDS
validation logic in a reusable, scriptable way.

Today [Anand
Buddhdev](https://www.ripe.net/about-us/press-centre/publications/speakers/anand-buddhdev)
of RIPE NCC caught me between coffees to give me some sage advice on
how to help get the CDS automation to happen on the parent side of the
delegation, at least for the reverse DNS zones for which RIPE is the
parent.

The RIPE vs RIPE NCC split is important for things like this: As I
understand it, RIPE is the association of European ISPs, and it's a
consensus-driven organization that develops policies and
recommendations; RIPE NCC is the secretariat or bureaucracy that
implements RIPE's policies. So Anand (as a RIPE NCC employee) needs
to be told by RIPE to implement CDS checking, he can't do it without
prior agreement from his users.

So I gather there is going to be some opportunity to get this onto the
agenda at the DNS working group meetings tomorrow and on Thursday.


ANAME
-----

As planned, I went through Matthijs's comments today, and grabbed some
time to discuss where clarification is needed. There are several
points in the draft which are really matters of taste, so it'll be
helpful if I note them in the draft as open to suggestions. But there
are other aspects that are core to the design, so it's really
important (as Evan told me) to make it easy for readers to understand
them.


Jon Postel
----------

Today was the 20th anniversary of Jon Postel's death.

Daniel Karrenberg spoke about why it is important to remember Jon,
with a few examples of his approach to Internet governance.

[RFC 2468 - "I remember IANA"](https://tools.ietf.org/html/rfc2468)


Women in Tech
-------------

I skipped the Women in Tech lunch, even though
[Denesh](https://denesh.uk/) suggested I could go - I didn't want to
add unnecessary cis-male to a women's space. But I gather there
were some good discussions about overthrowing the patriarchy, so I
regret missing an opportunity to learn by listening to the arguments.


VXLAN / EVPN / Geneve
---------------------

Several [talks
today](https://ripe77.ripe.net/programme/meeting-plan/plenary/#tues1)
about some related networking protocols that I am not at all familiar
with.

The first talk by Henrik Kramshoej on VXLAN injection attacks looks
like it is something my colleagues need to be aware of (if they are
not already!)

The last talk was by Ignas Bagdonas on the Geneve which is a possible
replacement for VXLAN. Most informative question was "why not MPLS?"
and the answer seemed to be that Geneve (like VXLAN) is supposed to be
easier since it includes more of the control plane as part of the
package.

Flemming Heino from LINX talked about "deploying a disaggregated
network model using EVPN technology". This was interesting because of
the discussion of the differences between data centre networks and
exchange point networks. I think the EVPN part was to do with some of
the exchange point features, which I didn't really understand. The
physical side of their design is striking, though: 1U switches, small
number of SKUs, using a leaf + spine design, with a bit of careful
traffic modelling, instead of a big chassis with a fancy backplane.


Other talks
-----------

At least two used [LaTeX Beamer](https://en.wikipedia.org/wiki/Beamer_(LaTeX)) :-)

Lorenzo Cogotti on the high performance isolario.it BGP scanner

  - "dive right into C which is not pleasant but necessary"

  - keen on C99 VLAs!

  - higher level wrappers allow users to avoid C

Florian Streibelt - BGP community attacks

  - 14% of transit providers propagate BGP communities which is enough
    to propagate widely because the network is densely connected

  - high potential for attack!

  - leaking community 666 remotely-triggered black hole; failing to
    filter 666 announcements

  - he provided lots of very good motivation for his safety recommendations

Constanze Dietrich - human factors of security misconfigurations

  - really nice summary of her very informative research

Niels ten Oever - Innovation and Human Rights in the Internet Architecture

  - super interesting social science analysis of the IETF

  - much more content in the talk than the slides, so it's probably
    worth looking at the video (high bandwidth talking!)

Tom Strickx - Cloudflare - fixing some anycast technical debt

  - nice description of a project to overhaul their BGP configuration

Andy Wingo - 8 Ways Network Engineers use Snabb

  - nice overview of the Lua wire-speed software network toolkit
    project started by Luke Gorrie

  - I had a pleasant chat with Andy on the sunny canalside
