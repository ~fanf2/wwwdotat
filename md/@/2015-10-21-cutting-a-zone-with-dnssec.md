---
dw:
  anum: 234
  eventtime: "2015-10-21T15:49:00Z"
  itemid: 434
  logtime: "2015-10-21T14:49:39Z"
  props:
    commentalter: 1491292413
    import_source: livejournal.com/fanf/138451
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/111338.html"
format: html
lj:
  anum: 211
  can_comment: 1
  ditemid: 138451
  event_timestamp: 1445442540
  eventtime: "2015-10-21T15:49:00Z"
  itemid: 540
  logtime: "2015-10-21T14:49:39Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 4
  url: "https://fanf.livejournal.com/138451.html"
title: Cutting a zone with DNSSEC
...

<p>This week we will be delegating <code>newton.cam.ac.uk</code> (the Isaac Newton
Institute's domain) to the Faculty of Mathematics, who have been
running their own DNS since the very earliest days of Internet
connectivity in Cambridge.</p>

<p>Unlike most new delegations, the <code>newton.cam.ac.uk</code> domain already
exists and has a lot of records, so we have to keep them working
during the process. And for added fun, <code>cam.ac.uk</code> is signed with
DNSSEC, so we can't play fast and loose.</p>

<p>In the absence of DNSSEC, it is mostly OK to set up the new zone, get
all the relevant name servers secondarying it, and then introduce the
zone cut. During the rollout, some servers will be serving the domain
from the old records in the parent zone, and other servers will serve
the domain from the new child zone, which occludes the old records in
its parent.</p>

<p>But this won't work with DNSSEC because validators are aware of zone
cuts, and they check that delegations across cuts are consistent with
the answers they have received. So with DNSSEC, the process you have
to follow is fairly tightly constrained to be basically the opposite
of the above.</p>

<p>The first step is to set up the new zone on name servers that are
completely disjoint from those of the parent zone. This ensures that a
resolver cannot prematurely get any answers from the new zone - they
have to follow a delegation from the parent to find the name servers
for the new zone. In the case of <code>newton.cam.ac.uk</code>, we are lucky that
the Maths name servers satisfy this requirement.</p>

<p>The second step is to introduce the delegation into the parent
zone. Ideally this should propagate to all the authoritative servers
promptly, using NOTIFY and IXFR.</p>

<p>(I am a bit concerned about DNSSEC software which does validation as a
separate process after normal iterative resolution, which is most of
it. While the delegation is propagating it is possible to find the
delegation when resolving, but get a missing delegation when
validating. If the validator is persistent at re-querying for the
delegation chain it should be able to recover from this; but quick
propagation minimizes the problem.)</p>

<p>After the delegation is present on all the authoritative servers, and
old data has timed out of caches, the new child zone can (if
necessary) be added to the parent zone's name servers. In our case the
central <code>cam.ac.uk</code> name servers and off-site secondaries also serve
the Maths zones, so this step normalizes the setup for
<code>newton.cam.ac.uk</code>.</p>
