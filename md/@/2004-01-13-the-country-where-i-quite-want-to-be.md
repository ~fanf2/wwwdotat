---
dw:
  anum: 85
  eventtime: "2004-01-13T17:36:00Z"
  itemid: 49
  logtime: "2004-01-13T17:38:53Z"
  props:
    commentalter: 1491292307
    current_moodid: 66
    import_source: livejournal.com/fanf/12789
    interface: flat
    opt_backdated: 1
    picture_keyword: photo
    picture_mapid: 3
  url: "https://fanf.dreamwidth.org/12629.html"
format: casual
lj:
  anum: 245
  can_comment: 1
  ditemid: 12789
  event_timestamp: 1074015360
  eventtime: "2004-01-13T17:36:00Z"
  itemid: 49
  logtime: "2004-01-13T17:38:53Z"
  props:
    current_moodid: 66
  reply_count: 2
  url: "https://fanf.livejournal.com/12789.html"
title: "\"The country where I quite want to be\""
...

<em>Fast Shown</em> ystävät naureskelevat kuullessaan Ralphista ja Tedistä tai sanottavan, "which was nice", "scorchio!" tai "nimbocumulus", mutta ne, jotka eivät katsoneet kyseistä TV-sarjaa, eivät koe kyseisiä hokemia hauskoiksi.

<a href="http://www.uta.fi/~mt63532/docs/merkitys.html">(link)</a>
