---
dw:
  anum: 59
  eventtime: "2005-05-06T20:22:00Z"
  itemid: 140
  logtime: "2005-05-06T20:27:31Z"
  props:
    commentalter: 1491292322
    import_source: livejournal.com/fanf/36178
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/35899.html"
format: casual
lj:
  anum: 82
  can_comment: 1
  ditemid: 36178
  event_timestamp: 1115410920
  eventtime: "2005-05-06T20:22:00Z"
  itemid: 141
  logtime: "2005-05-06T20:27:31Z"
  props: {}
  reply_count: 9
  url: "https://fanf.livejournal.com/36178.html"
title: Flood protection
...

I've just written a proposal for implementing rate limits on outgoing email via ppswitch (our central email relay). The aim is to detect and stop floods of viruses or spam from a compromised machine on the University's network.

The document includes a description of the simple mathematical model I'm planning to use to compute a client's sending rate. It seems to satisfy my requirements but if anyone has any better ideas then I'm all ears.

http://www.cus.cam.ac.uk/~fanf2/hermes/doc/antiforgery/ratelimit.html
