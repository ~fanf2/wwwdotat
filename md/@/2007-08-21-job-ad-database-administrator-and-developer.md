---
dw:
  anum: 118
  eventtime: "2007-08-21T16:35:00Z"
  itemid: 298
  logtime: "2007-08-21T16:42:35Z"
  props:
    commentalter: 1491292344
    import_source: livejournal.com/fanf/76742
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/76406.html"
format: html
lj:
  anum: 198
  can_comment: 1
  ditemid: 76742
  event_timestamp: 1187714100
  eventtime: "2007-08-21T16:35:00Z"
  itemid: 299
  logtime: "2007-08-21T16:42:35Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/76742.html"
title: Job ad - database administrator and developer
...

<p>The University Computing Service is forming a small team of software developers to work on a variety of projects.  We are recruiting a database specialist to work in this group.  The successful applicant will design database schemas, write stored procedures for the application developers and configure and maintain database systems where needed.</p>

<p>Most but not all development will be done on Linux platforms with databases such as MySQL, Oracle and PostgreSQL.  The majority of projects are anticipated to be web applications, typically using Java.</p>

<p>Much of the development will use open source software and improvements and modifications to the software will be fed back to the international development teams responsible for these pieces of software.</p>

<p>Candidates must have:</p>
<ul>
	<li>A degree in a numerate discipline, or equivalent experience.</li>
	<li>Substantial experience of relational database design.</li>
	<li>Substantial knowledge of SQL and writing stored procedures.</li>
	<li>Good experience in software development.</li>

	<li>Familiarity with administering a relational database.</li>
	<li>Good English skills, both verbal and written.</li>
</ul>

<p>This is a permanent appointment at Grade 9, subject to a nine months satisfactory probation period, on a salary scale of &pound;33,779-&pound;42,791 with initial salary depending on experience. <a href="http://www.cam.ac.uk/cs/jobs/further/dbadmin.html">More information can be found on the CS's web site</a>.</p>

<p><i>PS. fanf notes that the dev group's offices are on the 5th floor with a nice view of King's College chapel.</i></p>
