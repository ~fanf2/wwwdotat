---
dw:
  anum: 93
  eventtime: "2008-07-30T13:54:00Z"
  itemid: 352
  logtime: "2008-07-30T14:54:18Z"
  props:
    import_source: livejournal.com/fanf/91170
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/90205.html"
format: html
lj:
  anum: 34
  can_comment: 1
  ditemid: 91170
  event_timestamp: 1217426040
  eventtime: "2008-07-30T13:54:00Z"
  itemid: 356
  logtime: "2008-07-30T14:54:18Z"
  props:
    verticals_list: science
  reply_count: 0
  url: "https://fanf.livejournal.com/91170.html"
title: selog-8.7.30 - selective logging library
...

<p>I have released a new version of <a href="http://dotat.at/prog/selog/">selog</a>. The only significant change is C++ support, requested by <a href="https://filecoreinuse.livejournal.com/">👤filecoreinuse</a>.</p>

<p>One thing I would like to be able to do in C++ is call a selector, so that <code>sel(fmt, ...)</code> works like <code>selog(sel, fmt, ...)</code>. (The Lua API supports something like this.) However as far as I can tell this would require variadic templates to implement efficiently (i.e. to inline the <code>selog_on()</code> check) and this is probably reaching too far into voodoo territory to be sensible.</p>
