---
dw:
  anum: 221
  eventtime: "2004-03-16T16:58:00Z"
  itemid: 74
  logtime: "2004-03-16T09:00:44Z"
  props:
    commentalter: 1491292310
    import_source: livejournal.com/fanf/19014
    interface: flat
    opt_backdated: 1
    picture_keyword: passport
    picture_mapid: 4
  url: "https://fanf.dreamwidth.org/19165.html"
format: casual
lj:
  anum: 70
  can_comment: 1
  ditemid: 19014
  event_timestamp: 1079456280
  eventtime: "2004-03-16T16:58:00Z"
  itemid: 74
  logtime: "2004-03-16T09:00:44Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/19014.html"
title: SF memories
...

What was that Asimov satirical story about a future US election where there was one man who voted and all the rest was inferred using magic statistical software on <a href="http://www-personal.washtenaw.cc.mi.us/~multivac/multivac.html">Multivac</a>?
