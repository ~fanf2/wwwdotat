---
dw:
  anum: 180
  eventtime: "2009-04-23T20:55:00Z"
  itemid: 382
  logtime: "2009-04-23T20:01:02Z"
  props:
    commentalter: 1491292377
    import_source: livejournal.com/fanf/99349
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
    taglist: "dvcs, hg, bzr, git, version control"
  url: "https://fanf.dreamwidth.org/97972.html"
format: html
lj:
  anum: 21
  can_comment: 1
  ditemid: 99349
  event_timestamp: 1240520100
  eventtime: "2009-04-23T20:55:00Z"
  itemid: 388
  logtime: "2009-04-23T20:01:02Z"
  props:
    personifi_tags: "15:10,17:1,35:6,8:62,10:3,23:5,42:10,32:1,16:1,1:51,3:10,25:6,19:1,20:1,9:10,nterms:no"
    taglist: "hg, git, bzr, version control, dvcs"
    verticals_list: "computers_and_software,technology"
  reply_count: 6
  url: "https://fanf.livejournal.com/99349.html"
title: Some thoughts about git
...

<p>I was originally planning to witter about distributed version control vs. centralized version control, especially the oft-neglected problem of breaking up a large <a href="http://www.nongnu.org/cvs/">cvs</a> / <a href="http://subversion.tigris.org/">svn</a> / <a href="http://www.perforce.com/">p4</a> repository. This was partly triggered by <a href="http://www.youtube.com/watch?v=4XpnKHJAok8">Linus's talk about git at Google</a> in which he didn't really address a couple of questions about how to migrate a corporate source repository to distributed version control. But in the end I don't think I have any point other than the fairly well-known one that distributed version control systems work best when your systems are split into reasonably modestly-sized and self-contained modules, one per repository. Most systems are modular, even if all the modules are in one huge central repository, but the build and system integration parts can often get tightly coupled to the repository layout making it much harder to decentralize.</p>

<p>Instead I'm going to wave my hands a bit about the ways in which <a href="http://git-scm.com/">git</a> has unusual approaches to distributed version control, and how <a href="http://bazaar-vcs.org/">bzr</a> in particular seems to take diametrically opposing attitudes. I'm not saying one is objectively better than the other, because most of these issues are fairly philosophical and for practical purposes they are dominated by things like quality of implementation and documentation and support.</p>

<h3>Bottom-up</h3>

<p>Git's design is very bottom-up. Linus started by designing a repository structure that he thought would support his goals of performance, semantics, and features, and worked upwards from there. The upper levels, especially the user interface, were thought to be of secondary importance and something that could be worked on and improved further down the line. As a result it has a reputation for being very unfriendly to use, but that problem is pretty much gone now.</p>

<p>Other VCSs take a similar approach, for example <a href="http://www.selenic.com/mercurial/">hg</a> is based on its <a href="http://www.selenic.com/mercurial/wiki/index.cgi/Presentations?action=AttachFile&amp;do=get&amp;target=ols-mercurial-paper.pdf">revlog</a> data structure, and <a href="http://darcs.net/">darcs</a> has its <a href="http://darcs.net/manual/node9.html">patch algebra</a>. However bzr seems to be designed from the top down, starting with a user interface and a set of supported workflows, and viewing its repository format and performance characteristics as of secondary importance and something that can be improved further down the line. As a result it has a reputation for being very slow.</p>

<h3>Amortization</h3>

<p>Most VCSs have a fairly intricate repository format, and every operation that writes to the repository eagerly keeps it in the canonical efficient form. Git is unusual because its write operations add data to the repository in an unpacked form which makes writing cheaper but makes reading from the repository gradually less and less efficient - until you repack the repo in a separate heavy-weight operation to make reads faster again. (Git will do this automatically for you every so often.) The advantage of this is that the packed repository format isn't constrained by any need for incremental updates, so it can optimise for read performance at the expense of greater pack write complexity because this won't slow down common write operations. Bzr being the opposite of git seems to do a lot more up-front work when writing to its repository than other VCSs, e.g. to make annotation faster.</p>

<p>Thus git has two parallel repository formats, loose and packed. Other VCSs may have multiple repository formats, but only one at a time, and new formats are introduced to satisfy feature or performance requirements. Repository format changes are a pain and happily git's stabilized very early on - unlike bzr's.</p>

<h3>Laziness</h3>

<p>As well as being slack about <i>how</i> it writes to its repository, git is also slack about <i>what</i> it writes. There has been an inclination in recent VCSs towards richer kinds of changeset, with support for file copies and renames or even things like token renames in darcs. The bzr developers think this is <a href="http://www.markshuttleworth.com/archives/123">vital</a>. Git, on the other hand, doesn't bother storing that kind of information at all, and instead lazily calculates it when necessary. There are <a href="http://permalink.gmane.org/gmane.comp.version-control.git/217">some good reasons</a> for this, in particular that developers will often not bother to be explicit about rich change information, or the information might be lost when transmitting a patch, or the change might have come from a different VCS that doesn't encode the information. This implies that even VCSs that can represent renames <a href="https://lists.canonical.com/archives/bazaar/2009q1/052948.html">still need to be able to infer them</a> in some situations.</p>

<p>Git's data structure helps to make this efficient: it identifies files and directories by a hash of their contents, so if the hash is the same it doesn't need to look any closer to find differences because there aren't any - and this implies a copy or rename. This means that you should not rename or copy a file and modify it in the same commit, because that makes git's rename inference harder. Similarly if you rename a directory, don't modify any of its contents (including renames and permissions changes) in the same commit.</p>

<p>Mercurial also uses hashes to identify things, but they aren't pure content hashes: they include historical information, so they can't be used to identify files with the same contents but different histories. Thus efficiency forces hg to represent copies explicitly.</p>

<h3>Any more?</h3>

<p>I should say that I know very little about bzr, and nothing about <a href="http://www.gnu.org/software/gnu-arch/">tla</a>, <a href="http://monotone.ca/">mtn</a>, or <a href="http://www.bitkeeper.com/">bk</a>, so if any of the above is off the mark or over-states git's weirdness, then please correct me in a comment!</p>
