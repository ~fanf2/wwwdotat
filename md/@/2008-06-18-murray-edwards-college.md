---
dw:
  anum: 146
  eventtime: "2008-06-18T21:22:00Z"
  itemid: 347
  logtime: "2008-06-18T22:08:23Z"
  props:
    commentalter: 1491292418
    import_source: livejournal.com/fanf/89717
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/88978.html"
format: html
lj:
  anum: 117
  can_comment: 1
  ditemid: 89717
  event_timestamp: 1213824120
  eventtime: "2008-06-18T21:22:00Z"
  itemid: 350
  logtime: "2008-06-18T22:08:23Z"
  props:
    personifi_tags: "nterms:yes"
    verticals_list: culture
  reply_count: 18
  url: "https://fanf.livejournal.com/89717.html"
title: Murray Edwards College
...

<p>Today brings us the cheerful news that <a href="http://www.newhall.cam.ac.uk/">New Hall</a> has been refounded with an endowment of &pound;30 million and finally been given the proper name that it has been waiting for since 1954.</p>

<p>This led to a discussion on IRC about people who have colleges at both Cambridge and Oxford named after them. The list is:</p>

<dl>
<dt>God</dt>
<dd><ul>
<li>Trinity College, Cambridge
<li>Trinity Hall, Cambridge
<li><strike>God's House</strike> Christ's College, Cambridge
<li>Trinity College, Oxford
</ul></dd>

<dt>Jesus</dt>
<dd><ul>
<li>Christ's College, Cambridge
<li>Corpus Christi College, Cambridge
<li>Emmanuel College, Cambridge
<li>Jesus College, Cambridge
<li>Christ Church, Oxford
<li>Corpus Christi College, Oxford
<li>Jesus College, Oxford
</ul></dd>

<dt>the Virgin Mary modestly hides in the full names of several colleges</dt>
<dd><ul>
<li>The College of the Blessed Virgin Mary, Saint John the Evangelist, and the glorious virgin Saint Radegund, within the City and University of Cambridge, commonly called Jesus College, in the University of Cambridge
<li>The King's College of Our Lady and Saint Nicholas in Cambridge
<li>The College of Corpus Christi and the Blessed Virgin Mary in Cambridge
<li><strike>Hall of the Annunciation of the Blessed Virgin Mary</strike> Gonville and Caius College, Cambridge
<li>The College of the Blessed Mary and All Saints, Lincoln (in Oxford)
<li>The Warden and Scholars of St Mary's College of Winchester in Oxford, commonly called New College in Oxford
<li>The Provost and Scholars of the House of the Blessed Mary the Virgin in Oxford, commonly called Oriel College, of the Foundation of Edward the Second of famous memory, sometime King of England
</ul></dd>

<dt>Mary Magdalene</dt>
<dd><ul>
<li>Magdalene College, Cambridge
<li>Magdalen College, Oxford
</ul></dd>

<dt>St Peter</dt>
<dd><ul>
<li>Peterhouse, Cambridge
<li>St Peter's College, Oxford
</ul></dd>

<dt>St Catherine</dt>
<dd><ul>
<li>St Catharine's College, Cambridge
<li>St Catherine's College, Oxford
</ul></dd>

<dt>Edmund Rich of Abingdon</dt>
<dd><ul>
<li>St Edmund's College, Cambridge
<li>St Edmund Hall, Oxford
</ul></dd>

<dt>Lady Margaret Beaufort</dt>
<dd><ul>
<li>Lady Margaret Hall, Oxford
<li>Margaret Beaufort Institute of Theology, Cambridge
</ul></dd>

<dt>Sir Isaac Wolfson</dt>
<dd><ul>
<li>Wolfson College, Cambridge
<li>Wolfson College, Oxford
</ul></dd>

</dl>

<p>Cambridge's Pembroke College is named after Marie de St Pol, Countess of Pembroke (widow of the 1st Earl of Pembroke); Oxford's Pembroke College is named after William Herbert, 3rd Earl of Pembroke.</p>

<p>Cambridge has two colleges named after St John the Evangelist, and Oxford has one named after St John the Baptist.</p>

<p>Cambridge has two colleges endowed by Lady Margaret Beaufort (Christ's and John's) whereas Oxford has a college named after her. <b>Edited to add</b> four and a half years later: Cambridge also has the Margaret Beaufort Institute for Theology, which is member of the theological federation so only loosely affiliated with the University and not much like a proper college, but still worth noting.</p>

<p>Cambridge's University College lasted 7 years before it was renamed Wolfson College. Oxford's University College is over 700 years old.</p>

<p>Both universities have benefited from money obtained from manufacturing cars: Cambridge via the Cripps foundation, and Oxford via the Nuffield foundation.</p>
