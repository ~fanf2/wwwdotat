---
dw:
  anum: 138
  eventtime: "2004-10-22T13:23:00Z"
  itemid: 109
  logtime: "2004-10-22T06:25:55Z"
  props:
    commentalter: 1491292315
    import_source: livejournal.com/fanf/28118
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/28042.html"
format: casual
lj:
  anum: 214
  can_comment: 1
  ditemid: 28118
  event_timestamp: 1098451380
  eventtime: "2004-10-22T13:23:00Z"
  itemid: 109
  logtime: "2004-10-22T06:25:55Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/28118.html"
title: Apple / FreeBSD / Unix events
...

On 1st Nov I will be going to http://www.ukuug.org/events/apple04/

On 2nd Nov Jordan Hubbard is coming up to Cambridge to talk to Apply Unixy people in the Hopkinson Lecture Theatre at 2pm. I'll be at that talk as well. If you want to come, email me and I'll inform the appropriate people.
