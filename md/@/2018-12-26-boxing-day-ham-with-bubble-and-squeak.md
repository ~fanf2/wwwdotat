---
dw:
  anum: 101
  eventtime: "2018-12-26T20:06:00Z"
  itemid: 509
  logtime: "2018-12-26T20:06:55Z"
  props:
    commentalter: 1546192781
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/130405.html"
format: md
lj:
  anum: 196
  can_comment: 1
  ditemid: 154308
  event_timestamp: 1545857280
  eventtime: "2018-12-26T20:48:00Z"
  itemid: 602
  logtime: "2018-12-26T20:48:18Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/154308.html"
...

Boxing day ham with bubble and squeak
=====================================

This was a good way to eat up the left-over veg from xmas day.

Yesterday we had chicken with roast potatoes, leek sauce, peas, and
pigs in blankets - not a complicated meal because there was just the
four of us.

I cooked a 1kg bag of roasties, which we ate a bit more than half of,
and I made leek sauce with two leeks of which about half was left
over, plus about 100g of left-over peas. Obviously something like
bubble and squeak or veggie potato fritter things is the way to use
them up.

Here are some outline recipes. No quantities, I'm afraid, because I am
not a precision cook.

Leek sauce
----------

I dunno how well-known this is, but it's popular in my family.

Slice a couple of leeks into roundels, and gently fry them in LOTS of
butter until they are soft.

Then make a white sauce incorporating the leeks. So add a some flour,
and stir it all together making a leeky roux.

Then add milk a glug at a time, stirring to incorporate in between
(it's much more forgiving than a plain white sauce) until it is runny.
Season with nutmeg and black pepper.

Keep stirring while it cooks and the sauce thickens.

Ham
---

I like to cook a ham like this on Boxing Day.

Get a nice big gammon, which will fit in one of your pots while
covered in liquid. It's best to soak it in cold water in the fridge
overnight, to make it less salty.

Drain it and rinse it, then put it on the hob to simmer in cider
(diluted to taste / budget with apple juice and / or water). I added
some peppercorns and bay leaves, but there are [lots of spice
options](https://www.bbc.com/food/recipes/exceptional_gammon_with_50001).

For the glaze I mix the most fierce mustard I have with honey, and
(after peeling off the skin) paint it all over the gammon. Then roast
in an oven long enough to caramelize the glaze (15 minutes ish).

Let the ham stand - it doesn't need to be hot when you serve it.

Cider gravy
-----------

In the past I have usually neglected to soak the ham before cooking,
so the cider ended up impossibly salty afterwards. This time I tried
reducing the cooking liquid to see if I could make a cider gravy, but
it was still too salty.

Apparently [I could try making a potato soup with
it](https://twitter.com/swampers/status/1077996480412221440) because
that neutralizes the salt. I think I will try this because we also
have loads of (unsalted) chicken stock to be used.

Bubble and squeak fritters
--------------------------

This might have worked better if I had got the leftovers out of the
fridge some time before preparation, so that they could warm up and
soften!

I mashed the potatoes, then mixed them together with the leek sauce
(which I zapped in the microwave to make it less solid!), and finally
added the peas (to avoid smashing them up too much). I also added an
egg, but actually the mixture had enough liquid already and the egg
made it a bit too soft.

After some experimentation I found that the best way to cook it was to
put a dollop of mixture directly in the frying pan with a wooden
spoon, so (after flattening) the fritters were about 2cm thick and a bit
smaller than palm sized. I could comfortably cook a few at a time.

I fried them in oil in a moderately hot pan, so they were hot all the
way through and browned nicely on the outside.

Verdict
-------

I had just enough fritters to feed three adults and a child, and most
of the ham has gone! Enough left for a few sandwiches, I think.

I will try to aim for left-over leek sauce and potatoes more often :-)
