---
comments:
  Cohost: https://cohost.org/fanf/post/1573033-on-my-blog
  Dreamwidth: https://fanf.dreamwidth.org/142513.html
  Fediverse: https://mendeddrum.org/@fanf/110435364199507263
  Twitter: https://twitter.com/fanf/status/1662143991314366476
...
Where does my computer get the time from?
=========================================

This week I was in Rotterdam for [a RIPE meeting](https://ripe86.ripe.net/).
On Friday morning I gave a lightning talk called [where does my
computer get the time from?](https://ripe86.ripe.net/archives/video/1126/)
The RIPE meeting website has a copy of my slides and a video of the
talk; this is a blogified low-res version of the slides with a rough
and inexact transcript.

> I wrote a follow-up note, ["Where does 'where does my computer get
> the time from?' come from?"][whence-whence] about some things I left
> out of the talk.

[whence-whence]: https://dotat.at/@/2023-05-28-whence-whence-time.html

<cut>

<style>
main img, article img { border: 1px solid var(--fg); }
</style>

------------------------------------------------------------------------

![Where does my computer get the time from?](https://dotat.at/@/2023-04-whence-time.001.jpeg)

Where does my computer get the time from?

------------------------------------------------------------------------

![from NTP](https://dotat.at/@/2023-04-whence-time.002.jpeg)

from NTP - here's a picture of an NTP packet

------------------------------------------------------------------------

![David Mills surrounded by clocks](https://dotat.at/@/2023-04-whence-time.003.jpeg)

and here's a picture of David Mills who invented NTP

------------------------------------------------------------------------

![Where does my computer get the time from? NTP](https://dotat.at/@/2023-04-whence-time.004.jpeg)

simple question, easy answer, end of talk? No!

let's peel off some layers...

------------------------------------------------------------------------

![Where does NTP get the time from? NTP](https://dotat.at/@/2023-04-whence-time.005.jpeg)

stratum 3 NTP servers get the time from stratum 2 NTP servers,

stratum 2 NTP servers get the time from stratum 1 NTP servers,

stratum 1 NTP servers get the time from some reference clock

maybe a radio signal such as MSF in Britain or DCF77 in Germany

but in most cases the reference clock is probably a GPS receiver

------------------------------------------------------------------------

![Where does NTP get the time from? GPS](https://dotat.at/@/2023-04-whence-time.006.jpeg)

here's a GPS timing receiver

------------------------------------------------------------------------

![Where does NTP get the time from? GPS](https://dotat.at/@/2023-04-whence-time.007.jpeg)

and here's a GPS satellite

------------------------------------------------------------------------

![Where does GPS get the time from?](https://dotat.at/@/2023-04-whence-time.008.jpeg)

where does GPS get the time from?

------------------------------------------------------------------------

![Schriever space force base entrance sign](https://dotat.at/@/2023-04-whence-time.009.jpeg)

Schriever Space Force Base in Colorado

they look after a lot of different top secret satellites and other
stuff at Schriever, as you can see from all the mission logos

------------------------------------------------------------------------

![Schriever space force base aerial photo](https://dotat.at/@/2023-04-whence-time.010.jpeg)

so you can't get close enough to take a nice photo

------------------------------------------------------------------------

![Where does Schriever SFB get the time from?](https://dotat.at/@/2023-04-whence-time.011.jpeg)

Where does Schriever SFB get the time from?

------------------------------------------------------------------------

![racks of time-keeping equipment with smart blue blanking panels](https://dotat.at/@/2023-04-whence-time.012.jpeg)

the US Naval Observatory Alternate Master Clock
is on site at Schriever in Colorado

------------------------------------------------------------------------

![the USNO main building](https://dotat.at/@/2023-04-whence-time.013.jpeg)

the US Naval Observatory Alternate Master Clock
gets the time from the US Naval Observatory in Washington DC

------------------------------------------------------------------------

![where does the USNO get the time from?](https://dotat.at/@/2023-04-whence-time.014.jpeg)

there are three answers

------------------------------------------------------------------------

![USNO atomic clocks](https://dotat.at/@/2023-04-whence-time.015.jpeg)

the first answer is atomic clocks, lots of atomic clocks

in the background there are dozens of rack mounted caesium beam clocks

in the foreground the black boxes house hydrogen masers

------------------------------------------------------------------------

![more USNO atomic clocks](https://dotat.at/@/2023-04-whence-time.016.jpeg)

these shiny cylinders are [rubidium fountains](https://www.cnmoc.usff.navy.mil/Our-Commands/United-States-Naval-Observatory/Precise-Time-Department/The-USNO-Master-Clock/The-USNO-Master-Clock/Rubidium-Fountain-Clocks/)

------------------------------------------------------------------------

![aerial photograph of the USNO campus, which is a perfect circle](https://dotat.at/@/2023-04-whence-time.017.jpeg)

the USNO has so many atomic clocks they have entire buildings dedicated to them

When I was preparing this talk I noticed on Apple Maps that there's a
huge building site in the middle of the USNO campus. It turns out they
are building a fancy new clock house; the main limit on the accuracy
of their clocks is environmental stability: temperature, humidity,
etc. so the new building will have serious air handling.

------------------------------------------------------------------------

![where does the USNO get the time from?](https://dotat.at/@/2023-04-whence-time.018.jpeg)

the second answer is that UTC is a horrible compromise between time
from atomic clocks and time from earth rotation

------------------------------------------------------------------------

![the Paris observatory](https://dotat.at/@/2023-04-whence-time.019.jpeg)

so the USNO gets the time from the international earth rotation
service, which is based at the Paris Observatory

twice a year the IERS sends out
[Bulletin C](https://datacenter.iers.org/availableVersions.php?id=16),
which says whether or not there will be a leap second in six months
time; leap seconds are added (or maybe removed) from UTC to keep it in
sync with earth rotation

------------------------------------------------------------------------

![where does the IERS get the time from?](https://dotat.at/@/2023-04-whence-time.020.jpeg)

the IERS is spread across several organizations which contribute
to its scientific work

for example, you can subscribe to [IERS Bulletin A](https://datacenter.iers.org/availableVersions.php?id=6),
which is a weekly notice with precise details of the earth orientation parameters

------------------------------------------------------------------------

![the USNO main building](https://dotat.at/@/2023-04-whence-time.021.jpeg)

Bulletin A is sent out by the US Naval observatory

they need to know the exact orientation of the earth under the GPS
satellites, so they can provide precise positioning

------------------------------------------------------------------------

![where does the USNO get the time from?](https://dotat.at/@/2023-04-whence-time.022.jpeg)

the third answer is, how does the USNO know its atomic clocks are working well?

------------------------------------------------------------------------

![the pavillion de breteuil](https://dotat.at/@/2023-04-whence-time.023.jpeg)

that information comes from the international bureau of weights and
measures in Paris, who maintain the global standard UTC

------------------------------------------------------------------------

![where does the BIPM get the time from?](https://dotat.at/@/2023-04-whence-time.024.jpeg)

how does the BIPM determine what UTC is?

------------------------------------------------------------------------

![Circular T](https://dotat.at/@/2023-04-whence-time.025.jpeg)

the BIPM collects time measurements from national timing laboratories
around the world, and uses those measurements to determine official
UTC

periodically they send out [Circular T](https://www.bipm.org/en/time-ftp/circular-t)
which has information about the discrepencies between official UTC and
UTC from the various national time labs

------------------------------------------------------------------------

![diagram of the new SI](https://dotat.at/@/2023-04-whence-time.026.jpeg)

the BIPM is responsible for maintaining the international system of
units, which is defined by the general conference on weights and
measures

the CGPM is an international treaty organization established by the
metre convention of 1875

------------------------------------------------------------------------

![diagram of the SI highlighting the second](https://dotat.at/@/2023-04-whence-time.027.jpeg)

UTC is an implementation of the SI unit of time, based on quantum
measurements of caesium atoms

------------------------------------------------------------------------

![where did the CGPM get the time from?](https://dotat.at/@/2023-04-whence-time.028.jpeg)

where did this magic number, about 9.2 GHz, come from?

------------------------------------------------------------------------

![Essen and Parry standing next to their atomic clock](https://dotat.at/@/2023-04-whence-time.029.jpeg)

in 1955, Louis Essen (on the right) and Jack Parry (left) built
the first caesium atomic clock

the current definition of the second came from the calibration of this clock

------------------------------------------------------------------------

![where did Essen and Parry get the time from?](https://dotat.at/@/2023-04-whence-time.030.jpeg)

before atomic clocks, the definition of the second was based on
astronomy, so Essen and Parry needed help from astronomers to find out
how fast their clock ticks according to the existing standard of time

------------------------------------------------------------------------

![the USNO main building](https://dotat.at/@/2023-04-whence-time.031.jpeg)

they got help from the astronomers at the US Naval Observatory

------------------------------------------------------------------------

![Markowitz and Essen](https://dotat.at/@/2023-04-whence-time.032.jpeg)

the way it worked was William Markowitz measured time by looking at
the skies, and Louis Essen measured time by looking at his atomic
clock, and to correlate their measurements, they both listened to the
WWV radio time signal broadcast by the national bureau of standards in
Washington DC

this project took 3 years, 1955 - 1958

------------------------------------------------------------------------

![where did Markowitz get the time from?](https://dotat.at/@/2023-04-whence-time.033.jpeg)

Markowitz was measuring the "ephemeris second"

in 1952 the international astronomical union changed the definition of
time so that instead of being based on the rotation of the earth about
its axis, it was based on the orbit of the earth around the sun

in the 1930s they had discovered that the earth's rotation is not
perfectly even: it slows down and speeds up slightly

clocks were now more precise than the rotation of the earth, so the
ephemeris second was a new more precise standard of time

------------------------------------------------------------------------

![where did the IAU get the time from?](https://dotat.at/@/2023-04-whence-time.034.jpeg)

the ephemeris second is based on an astronomical ephemeris, which is a
mathematical model of the solar system

------------------------------------------------------------------------

![tables of the motion of the earth on its axis and around the sun](https://dotat.at/@/2023-04-whence-time.035.jpeg)

the standard ephemeris was produced by Simon Newcomb in the late 1800s

he collected a vast amount of historical astronomical data to create
his mathematical model

it remained the standard until the mid 1980s

------------------------------------------------------------------------

![Simon Newcomb](https://dotat.at/@/2023-04-whence-time.036.jpeg)

here's a picture of Simon Newcomb

he is a fine-looking Victorian gentleman

where did he work?

------------------------------------------------------------------------

![the USNO main building](https://dotat.at/@/2023-04-whence-time.037.jpeg)

at the US naval observatory!

(and the US nautical almanac office)

------------------------------------------------------------------------

![the Royal Greenwich Observatory](https://dotat.at/@/2023-04-whence-time.038.jpeg)

I have now run out of layers: before this point, clocks were set more
straightforwardly by watching stars cross the sky

so, to summarise my talk, where does my computer get the time from?

it does _not_ get it from the Royal Greenwich Observatory!
