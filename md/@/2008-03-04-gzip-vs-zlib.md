---
dw:
  anum: 110
  eventtime: "2008-03-04T20:08:00Z"
  itemid: 327
  logtime: "2008-03-04T21:08:15Z"
  props:
    commentalter: 1491292351
    import_source: livejournal.com/fanf/84334
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/83822.html"
format: casual
lj:
  anum: 110
  can_comment: 1
  ditemid: 84334
  event_timestamp: 1204661280
  eventtime: "2008-03-04T20:08:00Z"
  itemid: 329
  logtime: "2008-03-04T21:08:15Z"
  props:
    personifi_tags: "nterms:yes"
    verticals_list: "computers_and_software,technology"
  reply_count: 4
  url: "https://fanf.livejournal.com/84334.html"
title: gzip vs. zlib
...

I'm baffled. <tt>gzip</tt> does its thing by calling the <tt>zlib</tt> <tt>deflate()</tt> function, though it wraps the compressed stream differently than <tt>deflate()</tt> normally does. I have an application where I don't need <tt>gzip</tt>'s extra metadata, so I wrote a small program called <tt>deflate</tt>. They should perform the same apart from a small constant overhead for <tt>gzip</tt>, but they don't. As far as I can tell the only differences are minor details of buffer handling.
<pre>
$ jot 40000 | gzip -9c | wc -c
   87733
$ jot 40000 | ./deflate | wc -c
   86224
$ jot 20000 | sed 's/.*/print "hello, world &"/' | ./lua-5.1.2/src/luac -s -o - - | gzip -9c | wc -c
   81470
$ jot 20000 | sed 's/.*/print "hello, world &"/' | ./lua-5.1.2/src/luac -s -o - - | ./deflate | wc -c
   82687
</pre>
