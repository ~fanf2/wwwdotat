---
dw:
  anum: 27
  eventtime: "2018-10-14T01:50:00Z"
  itemid: 501
  logtime: "2018-10-13T23:52:15Z"
  props:
    commentalter: 1539693867
    interface: flat
    picture_keyword: ""
    revnum: 2
    revtime: 1540482287
  url: "https://fanf.dreamwidth.org/128283.html"
format: md
...

Amsterdam day 1
===============

During IETF 101 I wrote up the day's activity on the way home on the
train, which worked really well. My vague plan of doing the same in
Amsterdam might be less successful because it's less easy to hammer
out notes while walking across the city.

Some semi-chronological notes ... I'm going to use [the workshop
programme](https://indico.dns-oarc.net/event/29/timetable/) as a
starting point, but I'm not going to write particularly about the
contents of the talks (see the workshop web site for that) but more
about what I found relevant to me and the follow-up hallway
discussions.

Morning
-------

The first half of the morning was CENTR members only, so I went to get
a few bits from the local supermarket [via the "I Amsterdam"
sign](https://twitter.com/fanf/status/1051009863348887552) before
heading to the conference hotel.

The second half of the morning was DNS-OARC business. It's a small
organization that provides technical infrastructure, so most of the
presentation time was about technical matters. The last sessopn before
lunch was Matt Pounsett talking about sysadmin matters. He's
impressively enthusiastic about cleaning up a mountain of technical
debt. We had a chat at the evening social and I tried to convince him
that Debian without `systemd` is much less painful than the
[Devuan](https://devuan.org/) he has been struggling with.

Jerry Lundstrom talked about his software development projects. A
couple of these might be more useful to me than I was previously aware:

  * `drool` - DNS replay tool. I have a crappy `dnsmirror` script
    which I wrote after my previous test procedure failed to expose
    [CVE-2018-5737](https://kb.isc.org/docs/aa-01606) because I wasn't
    repeating queries.

	`drool respdiff` sounds like it might be a lot of what I need to
    automate my testing procedures between deploying to staging and
    production.

  * `dnsjit` - suite of DNS-related tools in Lua. If this has the
    right facilities then I might be able to delete the horrid DNS
    packet parsing code from
    [`doh101`](https://github.com/fanf2/doh101).

Cloudflare DoH and DoT
----------------------

Ólafur Guðmundsson said a few noteworthy things:

  * Cloudflare's DoT implementation is built-in to [Knot
    Resolver](https://www.knot-resolver.cz/). They are heavy users of
    my [qp trie](https://dotat.at/prog/qp/README.html) data structure,
    and at the evening social Petr Špaček told me they are planning to
    merge the knot-reolver-specific fixes with my [beer festival
    COW](https://dotat.at/@/2018-06-28-beer-festival-week-hacking-notes-epilogue.html) work.

  * Their DoH implementation uses Lua scripting in NGINX which sounds
    eerily familiar :-) (Oversimplifying to the point of being wrong,
    Cloudflare's web front end is basically OpenResty.)

  * He mentioned a problem with hitting quotas on the number of http2
    channels opened by Firefox clients, which I need to double check.

  * Cloudflare are actively working on DoT for recursive to
    authoritative servers. Sadly, although the IETF DNS Privacy
    working group has been discussing this recently, there hasn't been
    much comment from people who are doing practical work. Cloudflare
    likes the advantages of holding persistent connections from their
    big resolvers to popular authoritative servers, which is basically
    optimizing for a more centralized web. It's the modern version of
    leased lines between mainframes.

I submitted my DoH / DoT lightning talk (2 slides!) to the program
committee since it includes stuff which Ólafur didn't mention that is
relevant to other recursive server operators.

ANAME
-----

I merged my revamped draft into [Evan Hunt's aname
repository](https://github.com/each/draft-aname) and collaboration is
happening. I've talked about it with at least one big DNS company who
have old proprietary ANAME-like facilities, and they are keen to use
standardization as a push towards removing unused features and
cleaning up other technical debt. I've put some "mighty weasel words"
in the draft, i.e. stealing the "as if" idea from C, with the idea
that it gives implementers enough freedom to make meaningful zone file
portability possible, provided the zone is only relying on some common
subset of features.

Other small notes
-----------------

Matt Larson (ICANN) - rollover was a "pleasant non-event"

  * several ICANN staff are at DNS-OARC, so they travelled to NL
    before the rollover, and used a conference room at NLnet Labs as
    an ad-hoc mission control room for the rollover

Matt Weinberg (Verisign) - bad trust anchor telemetry signal
investigation - culprit was Monero! I wonder if this was what caused
Cambridge's similar weirdness back in June.

Duane Wessels (Verisign) talked about abolishing cross-TLD glue.
Gradually the registry data model becomes less insane! [fanf's rant censored]

Jaromír Talíř (CZ.NIC) on TLD algorithm rollovers. Don't fear ECDSA P256!
I definitely want to do this for Cambridge's zones.

Bert Hubert (PowerDNS) talked about the DNS Camel and his
[Hello DNS](https://github.com/ahupowerdns/hello-dns) effort to make
the RFCs easier to understand. He's very pleased with the name
compression logic he worked out for
[tdns](https://github.com/ahupowerdns/hello-dns/tree/master/tdns),
a teaching DNS server. I want to have a proper look at it...
