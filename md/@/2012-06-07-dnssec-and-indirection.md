---
format: html
lj:
  anum: 161
  can_comment: 1
  ditemid: 121505
  event_timestamp: 1339091640
  eventtime: "2012-06-07T17:54:00Z"
  itemid: 474
  logtime: "2012-06-07T16:54:39Z"
  props:
    give_features: 1
    personifi_tags: "nterms:no"
  reply_count: 0
  url: "https://fanf.livejournal.com/121505.html"
title: DNSSEC and indirection
...

<p>What can DNSSEC do, apart from provide full employment for DNS nerds? Most of the work so far has been on securing the DNS itself, which is important but <a href="http://dnssexy.net/">not particularly sexy</a>. Looking back at <a href="http://fanf.livejournal.com/113732.html">a DNSSEC article I wrote a year ago</a>, the main changes in the last year have been the very unsexy business of rolling out and improving DNSSEC support. A lot of this work is still some distance from end users - TLD and registrar support, better software tools. I recently registered a domain name with <a href="http://www.gandi.net">Gandi</a> and had it up and running DNSSEC very quickly - but I had to host it on my own servers since Gandi's hosting services don't support DNSSEC. So there's still a long way to go. Better large-scale DNS management software such as <a href="http://atomiadns.com/">AtomiaDNS</a> should help with deployment at hosting providers.</p>

<p>Infrastructure is interesting to the extent that you can build interesting things on it. Perhaps the most important thing about DNSSEC is that it is a new, very cheap and scalable, public key infrastructure. The <a href="http://tools.ietf.org/wg/dane/">IETF DANE working group</a> has been working on a specification for <a href="http://tools.ietf.org/html/draft-ietf-dane-protocol">TLSA resource records</a> which use the DNSSEC PKI to constrain the X.509 PKI in order to reduce the problems caused by insecure certification authorities. This spec is now in the last stages of the IETF process before being passed to the RFC editor for publication.</p>

<p>The next job is to tie this into the many application protocols that use TLS. For instance I have submitted a draft describing <a href="http://tools.ietf.org/html/draft-fanf-dane-smtp">how to use DNSSEC to secure SMTP between MTAs</a>. and Matt Miller has one on <a href="http://tools.ietf.org/html/draft-miller-xmpp-dnssec-prooftype">DNSSEC and XMPP</a>. This ought to be straightforward, but there's a knotty question about how to deal with indirection records such as MX and SRV (and to a lesser extent CNAME and DNAME).</p>

<p>The present situation is that our security protocols ignore any indirection in the DNS, because it can't be trusted. So if you connect to the gmail.com XMPP servers, their certificates authenticate themselves as gmail.com rather than as the targets of the respective SRV records. This is rather inconvenient for hosting providers, which either need certificates that can authenticate huge numbers of names, or <a href="http://tools.ietf.org/html/rfc6120#section-4.7.2">protocols</a> or <a href="http://tools.ietf.org/html/rfc6066#section-3">extensions</a> that allow the client to tell the server which name it is expecting.</p>

<p>DNSSEC can change this. A signed SRV or MX record is a trustworthy delegation of responsibility from a domain to its hosting provider. When it finds a signed indirection, a client only needs to check the target server host name against the certificate it presents. Service providers only need certificates for their server host names, not for every customer domain. My draft for email and Matt Miller's for XMPP allow clients and servers to trust the DNS in this way.</p>

<p>Unfortunately getting from where we are to this shiny future is going to make correct configuration harder, along the lines of <a href="http://fanf.livejournal.com/120855.html">Google's cockup</a> that I reported yesterday.</p>
