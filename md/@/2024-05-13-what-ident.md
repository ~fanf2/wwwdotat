---
comments:
  Dreamwidth: https://fanf.dreamwidth.org/147315.html
  Fediverse: https://mendeddrum.org/@fanf/112434663686247046
  Lobsters: https://lobste.rs/s/elj5qw/unix_version_control_lore_what_ident
...
Unix version control lore: what, ident
======================================

There are a couple of version control commands that deserve wider
appreciation: [SCCS `what`][what] and [RCS `ident`][ident]. They allow
you to find out what source a binary was built from, without having to
run it -- handy if it is a library! They basically scan a file looking
for magic strings that contain version control metadata and print out
what they discover.

[what]: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/what.html
[ident]: https://www.gnu.org/software/rcs/manual/html_node/ident.html

<cut>

keyword expansion
-----------------

SCCS, RCS, `cvs`, and `svn` all have a way to expand keywords in a
file when it is checked out of version control.

The POSIX [SCCS `get`][get] documentation describes its runes under
the "identification keywords" heading. The relevant one is `%W%` which
inserts the magic marker `@(#)` used by `what`.

[RCS / `cvs` / `svn` keyword substitution][cvs] uses more descriptive
markers like `$Revision$`.

[get]: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/get.html#tag_20_52_13_02
[cvs]: https://www.gnu.org/software/trans-coord/manual/cvs/cvs.html#Keyword-substitution


a berkeley example
------------------

It was a lonstanding BSD practice to use keyword expansion everywhere.
I first encountered it when I got involved in the Apache httpd project
in the late 1990s -- Apache's CVS repository was hosted on a FreeBSD
box and used a version of FreeBSD's CVS administrative scripts.

Here's an example from [`res_send.c`][res_send] in FreeBSD's libc
resolver.

[res_send]: https://cgit.freebsd.org/src/tree/lib/libc/resolv/res_send.c?id=e45764721aedfa6460e1767664864bda9457c10e

    static const char sccsid[] = "@(#)res_send.c	8.1 (Berkeley) 6/4/93";
    static const char rcsid[] = "$Id: res_send.c,v 1.22 2009/01/22 23:49:23 tbox Exp $";
    __FBSDID("$FreeBSD$");

There are geological strata of version control ident strings here:

  * the `sccsid` from the Berkeley CSRG SCCS repository
  * the `rcsid` from ISC's BIND repository
    (`tbox` was ISC's tinderbox build / CI system)
  * the `FBSDID` from FreeBSD's `cvs` and later `svn` repositories
    (which has not been expanded)


an unifdef example
------------------

When `unifdef` was uplifted to git, I wanted to keep its embedded
version control keywords -- I have a sentimental liking for this old
tradition. If you've installed [`cssc`][cssc], `rcs`, and `unifdef` on
a Debian box, you can run,

[cssc]: https://www.gnu.org/software/cssc/

        :; sccs what /usr/bin/unifdef
        :; ident /usr/bin/unifdef

Both of those will produce similar output to

        :; unifdef -V

On a Mac with the developer command-line tools installed,

        :; what /Library/Developer/CommandLineTools/usr/bin/unifdef

You get the output twice because it's a fat binary!


versioning three ways
---------------------

In [`unifdef.c`][unifdef], the embedded version string looks like,

        static const char copyright[] =
            "@(#) $Version: unifdef-2.12 $\n"
            "@(#) $Date: 2020-02-14 16:49:56 +0000 $\n"
            "@(#) $Author: Tony Finch (dot@dotat.at) $\n"
            "@(#) $URL: http://dotat.at/prog/unifdef $\n"
        ;

Each line is prefixed with an SCCS magic marker `@(#)` so that `what`
can find it, and wrapped in an RCS-style `$Keyword$` so that `ident`
can find it. There's a fairly trivial `version()` function that
spits out the `copyright[]` string when you run `unifdef -V`.

[unifdef]: https://dotat.at/cgi/git/unifdef.git/blob/HEAD:/unifdef.c


embedding versions from git
---------------------------

My projects have various horrible build scripts for embedding the
version number from `git`. The basic idea is,

  * use an annotated or signed tag to mark a release,
    i.e. `git tag -a` or `git tag -s`

  * use `git describe` to get a version string that includes an
    extra number counting commits since the last release

  * maybe use `git show --pretty=format:%ai -s HEAD` to get a release date

  * stuff the outputs from `git` into the `$Version$` and `$Date$` RCS
    keywords


retro cool
----------

I enjoy keeping this old feature working, even though it isn't very
useful if no-one knows about it! Maybe if I blog about it, it'll
become more widespread?
