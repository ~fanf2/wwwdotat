---
dw:
  anum: 63
  eventtime: "2016-03-11T02:03:00Z"
  itemid: 448
  logtime: "2016-03-11T02:03:09Z"
  props:
    commentalter: 1491292420
    import_source: livejournal.com/fanf/141891
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/114751.html"
format: html
lj:
  anum: 67
  can_comment: 1
  ditemid: 141891
  event_timestamp: 1457661780
  eventtime: "2016-03-11T02:03:00Z"
  itemid: 554
  logtime: "2016-03-11T02:03:09Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 4
  url: "https://fanf.livejournal.com/141891.html"
title: Confidentiality vs privacy
...

<p>I have a question about a subtle distinction in meaning between words.
I want to know if this distinction is helpful, or if it is just
academic obfuscation. But first let me set the scene...</p>

<p>Last week I attempted to explain DNSSEC to a few undergraduates. One
of the things I struggled with was justifying its approach to privacy.</p>

<p>(HINT: it doesn't have one!)</p>

<p>Actually, DNSSEC has a bit more reasoning for its lack of privacy than
that, so here's a digression:</p>

<h2>The bullshit reason why DNSSEC doesn't give you privacy</h2>

<p>Typically when you query the DNS you are asking for the address of a
server; the next thing you do is connect to that server. The result of
the DNS query is necessarily revealed, in cleartext, in the headers of
the TCP connection attempt.</p>

<p>So anyone who can see your TCP connection has a good chance of
inferring your DNS queries. (Even if your TCP connection went to
Cloudflare or some other IP address hosting millions of websites, a
spy can still guess based on the size of the web page and other
clues.)</p>

<h2>Why this reasoning is bullshit</h2>

<p>DNSSEC turns the DNS into a general-purpose public key infrastructure,
which means that it makes sense to use the DNS for a lot more
interesting things than just finding IP addresses.</p>

<p>For example, people might use DNSSEC to publish their PGP or S/MIME
public keys. So your DNS query might be a prelude to sending encrypted
email rather than just connecting to a server.</p>

<p>In this case the result of the query is <em>not</em> revealed in the TCP
connection traffic - you are always talking to your mail server! The
DNS query for the PGP key reveals who you are sending mail to -
information that would be much more obscure if the DNS exchange were
encrypted!</p>

<h2>That subtle distinction</h2>

<p>What we have here is a distinction between</p>

<blockquote>
  <p>who you are talking to</p>
</blockquote>

<p>and</p>

<blockquote>
  <p>what you are talking about</p>
</blockquote>

<p>In the modern vernacular of political excuses for the police state,
<em>who</em> you talk to is "metadata" and this is "valuable" information
which "should" be collected. (For example, <a href="http://abcnews.go.com/blogs/headlines/2014/05/ex-nsa-chief-we-kill-people-based-on-metadata/">America uses metadata to
decide who to
kill</a>.)
Nobody wants to know <em>what</em> you are talking about, unless <a href="https://google.com/search?q=apple+vs+fbi">getting
access to your data gives them a precedent in favour of more spying
powers</a>.</p>

<p>Hold on a sec! Let's drop the politics and get back to nerdery.</p>

<h2>Actually it's more subtle than that</h2>

<p>Frequently, "who you are talking to" might be obscure at a lower
layer, but revealed at a higer layer.</p>

<p>For example, when you query for a correspondent's public key, that
query might be encrypted from the point of view of a spy sniffing your
network connection, so the spy can't tell whose key you asked for. But
if the spy has cut a deal with the people who run the keyserver, they
can find out which key you asked for and therefore who you are talking
to.</p>

<h2>Why DNS privacy is hard</h2>

<p>There's a fundamental tradeoff here.</p>

<p>You can have privacy against an attacker who can observe your network
traffic, by always talking to a few trusted servers who proxy your
actions to the wider Internet. You get extra privacy bonus points if a
diverse set of other people use the same trusted servers, and provide
covering fire for your traffic.</p>

<p>Or you can have privacy against a government-regulated ISP who
provides (and monitors) your default proxy servers, by talking
directly to resources on the Internet and bypassing the monitored
proxies. But that reveals a lot of information to network-level
traffic analsis.</p>

<h2>The question I was building up to</h2>

<p>Having written all that preamble, I'm even less confident that this is
a sensible question. But anyway,</p>

<p>What I want to get at is the distinction between metadata and content.
Are there succinct words that capture the difference?</p>

<p>Do you think the following works? Does this make sense?</p>

<p>The <em>weaker</em> word is</p>

<blockquote>
  <p>"confidential"</p>
</blockquote>

<p>If something is confidential, an adversary is likely to know who you
are talking to, but they might not know what you talked about.</p>

<p>The <em>stronger</em> word is</p>

<blockquote>
  <p>"private"</p>
</blockquote>

<p>If something is private, an adversary should not even know who you are
dealing with.</p>

<p>For example, a salary is often "confidential": an adversary (a rival
colleague or a prospective employer) will know who is paying it but
not how big it is.</p>

<p>By contrast, an adulterous affair is "private": the adversary (your
spouse) shouldn't even know you are spending a lot of time with
someone else.</p>

<p>What I am trying to get at with this choice of words is the idea that
even if your communication is "confidential" (i.e. encrypted so spies
can't see the contents), it probably isn't "private" (i.e. it doesn't
hide who you are talking to or suppress scurrilous implications).</p>

<p>SSL gives you confidentiality (it hides your credit card number)
whereas TOR gives you privacy (it hides who you are buying drugs
from).</p>
