---
dw:
  anum: 142
  eventtime: "2004-03-03T19:01:00Z"
  itemid: 67
  logtime: "2004-03-03T11:04:18Z"
  props:
    commentalter: 1491292309
    current_moodid: 104
    import_source: livejournal.com/fanf/17194
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/17294.html"
format: casual
lj:
  anum: 42
  can_comment: 1
  ditemid: 17194
  event_timestamp: 1078340460
  eventtime: "2004-03-03T19:01:00Z"
  itemid: 67
  logtime: "2004-03-03T11:04:18Z"
  props:
    current_moodid: 104
  reply_count: 5
  url: "https://fanf.livejournal.com/17194.html"
title: "It's a virus-eat-virus world out there..."
...

The situation is getting silly.
See [my link log](https://dotat.at/:/?q=f+secure+av+research+weblog)
for a link to <a href="http://www.f-secure.com/weblog/">the F-Secure
anti-virus research weblog</a>. I've done a little analysis of McAfee
virus signature file updates over the last six months, and it clearly
shows how their usual weekly release schedule just isn't frequent
enough any more.

To the nearest hour:

4287 19 Aug 2003 16:00:00
4288 20 Aug 2003 23:00:00
4289 27 Aug 2003 19:00:00
4290 28 Aug 2003 14:00:00
4291 03 Sep 2003 20:00:00
4292 10 Sep 2003 21:00:00
4293 17 Sep 2003 22:00:00
4294 18 Sep 2003 19:00:00
4295 24 Sep 2003 20:00:00
4296 01 Oct 2003 19:00:00
4297 08 Oct 2003 19:00:00
4298 15 Oct 2003 20:00:00
4299 22 Oct 2003 19:00:00
4300 29 Oct 2003 19:00:00
4301 31 Oct 2003 17:00:00
4302 05 Nov 2003 20:00:00
4303 13 Nov 2003 02:00:00
4304 14 Nov 2003 13:00:00
4305 19 Nov 2003 21:00:00
4306 26 Nov 2003 19:00:00
4307 03 Dec 2003 20:00:00
4308 10 Dec 2003 20:00:00
4309 17 Dec 2003 20:00:00
4310 22 Dec 2003 03:00:00
4311 24 Dec 2003 17:00:00
4312 31 Dec 2003 17:00:00
4313 07 Jan 2004 19:00:00
4314 14 Jan 2004 22:00:00
4315 16 Jan 2004 16:00:00
4316 19 Jan 2004 06:00:00
4317 21 Jan 2004 20:00:00
4318 26 Jan 2004 17:00:00
4319 27 Jan 2004 05:00:00
4320 28 Jan 2004 19:00:00
4321 29 Jan 2004 22:00:00
4322 04 Feb 2004 22:00:00
4323 11 Feb 2004 19:00:00
4324 17 Feb 2004 17:00:00
4325 18 Feb 2004 16:00:00
4326 18 Feb 2004 22:00:00
4327 24 Feb 2004 01:00:00
4328 25 Feb 2004 18:00:00
4329 28 Feb 2004 02:00:00
4330 01 Mar 2004 01:00:00
4331 02 Mar 2004 18:00:00
4332 03 Mar 2004 03:00:00
