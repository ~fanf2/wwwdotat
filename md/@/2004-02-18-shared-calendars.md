---
dw:
  anum: 191
  eventtime: "2004-02-18T17:33:00Z"
  itemid: 63
  logtime: "2004-02-18T17:43:00Z"
  props:
    commentalter: 1491292309
    import_source: livejournal.com/fanf/16130
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/16319.html"
format: casual
lj:
  anum: 2
  can_comment: 1
  ditemid: 16130
  event_timestamp: 1077125580
  eventtime: "2004-02-18T17:33:00Z"
  itemid: 63
  logtime: "2004-02-18T17:43:00Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/16130.html"
title: Shared Calendars
...

One of the projects that we've assiduously avoided doing is a shared calendar system for the whole University, on the grounds that there aren't any plausible open standards and implementations, and we fear lurking scalability problems in the human side of things.

The iCal/vCal standard is fairly mature, but the associated Calendar Access Protocol is mired in committee bikeshedding (it's on revision 14). In the mean time people like <a href="http://www.apple.com/ical/">Apple</a> and <a href="http://www.mozilla.org/projects/calendar/">Mozilla</a> are using WebDAV as the publication and sharing mechanism; there's also <a href="http://www.ietf.org/internet-drafts/draft-dusseault-caldav-00.txt">an internet draft on this topic</a>.

Perhaps this provides a plausible way forward for us, if the interoperability comes together...

(The small matter of a directory rears its ugly head too.)
