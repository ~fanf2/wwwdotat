---
comments:
  Dreamwidth: https://fanf.dreamwidth.org/148367.html
  Fediverse: https://mendeddrum.org/@fanf/112808606328870714
  Hacker News: https://news.ycombinator.com/item?id=40997704
  Lobsters: https://lobste.rs/s/qsaevf/semaphores_golang_gnu_make
...
semaphores in Golang and GNU make
=================================

[Semaphores][] are one of the oldest concurrency primitives in
computing, invented over 60 years ago. They are weird: usually the
only numbers of concurrent processes we care about are [zero, one, or
many][] – but semaphores deal with those fussy finite numbers in
between.

[Semaphores]: https://en.wikipedia.org/wiki/Semaphore_(programming)
[zero, one, or many]: https://en.wikipedia.org/wiki/Zero_one_infinity_rule

Yesterday I was writing code that needed to control the number of
concurrent operating system processes that it spawned so that it
didn't overwhelm the computer. One of those rare situations when a
semaphore is just the thing!


a Golang channel is a semaphore
-------------------------------

A Golang channel has a buffer size -- a number of free slots -- which
corresponds to the initial value of the semaphore. We don't care about
the values carried by the channel: any type will do.

        var semaphore := make(chan any, MAXPROCS)

The `acquire` operation uses up a slot in the channel. It is
traditionally called `P()`, and described as decrementing the value of
the semaphore, i.e. decrementing the number of free slots in the
channel. When the channel is full this will block, waiting for another
goroutine to release the semaphore.

        func acquire() {
            semaphore <- nil
        }

The `release` operation, traditionally called `V()`, frees a slot in
the channel, incrementing the value of the semaphore.

        func release() {
            <-semaphore
        }

That's it!


the GNU make jobserver protocol is a semaphore
----------------------------------------------

The GNU `make -j` parallel builds feature uses a semaphore in the
form of its [jobserver protocol][]. Occasionally, other programs
support the jobserver protocol too, such as [Cargo][]. BSD `make -j`
uses basically the same semaphore implementation, but is not
compatible with the GNU make jobserver protocol.

[jobserver protocol]: https://www.gnu.org/software/make/manual/html_node/POSIX-Jobserver.html
[Cargo]: https://doc.rust-lang.org/cargo/reference/environment-variables.html

The `make` jobserver semaphore works in a similar manner to a Golang
channel semaphore, but:

  * instead of a channel, `make` uses a unix pipe;

  * because `make` can't control the amount of _free_ space in a
    pipe's buffer, the value of the semaphore is represented as the
    amount of _used_ space in the pipe's buffer;

  * the value of the semaphore can't be more than [PIPE_BUF][], to
    ensure that `release()` will never block.

[PIPE_BUF]: https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/limits.h.html

Here's a C-flavoured sketch of how it works. To create a semaphore and
initialize its value, create a pipe and write that many characters to
it, which are buffered in the kernel:

        int fd[2];
        pipe(fd);

        char slots[MAXPROCS] = {0};
        write(fd[1], slots, sizeof(slots));

To acquire a slot, read a character from the pipe. When the pipe is
empty this will block, waiting for another process to release the
semaphore.

        char slot;
        read(fd[0], &slot, 1);

To release a slot, the worker must write the same character back to
the pipe:

        write(fd[1], &slot, 1);

Error handling is left as an exercise for the reader.


bonus: waiting for concurrent tasks to complete
-----------------------------------------------

If we need to wait for everything to finish, we don't need any extra
machinery. We don't even need to know how many tasks are still
running! It's enough to acquire all possible slots, which will block
until the tasks have finished, then release all the slots again.

        func wait() {
            for range MAXPROCS {
                acquire()
            }
            for range MAXPROCS {
                release()
            }
        }

That's all for today! Happy hacking :-)
