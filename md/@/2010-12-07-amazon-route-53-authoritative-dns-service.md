---
format: casual
lj:
  anum: 177
  can_comment: 1
  ditemid: 111025
  event_timestamp: 1291710060
  eventtime: "2010-12-07T08:21:00Z"
  itemid: 433
  logtime: "2010-12-07T08:21:51Z"
  props:
    personifi_tags: "17:35,18:7,8:42,27:28,1:42,3:7,25:7,26:7,9:7,nterms:yes"
  reply_count: 20
  url: "https://fanf.livejournal.com/111025.html"
title: "Amazon \"Route 53\" authoritative DNS service"
...

What a disappointing offering.

To be fair, they are providing a cheap, high volume, globally distributed, authoritative DNS service. I expect they will do this as superbly as their other cloud hosting services.

But they are using <a href="http://cr.yp.to/djbdns.html">obsolete unmaintained software</a> for the DNS servers. This has two clearly bad consequences:

Firstly, they don't support DNSSEC.

Secondly, they have invented their own API for updating the DNS instead of using the standard dynamic update protocol.

<i>ETA:</i> Thirdly (but not a djbdns problem) they don't support AXFR, let alone NOTIFY and IXFR, which means that Route 53 cannot be used as a secondary DNS service for a primary you run, nor can you set up a local slave of your Route 53 zones.

I admit that they would have to have their own API for provisioning zones, since there is not yet a standard way to do that - and in fact BIND is only just getting its own API for dynamic zone provisioning. But I doubt I'll see Amazon staff on the IETF lists working to help standardize a provisioning protocol if they don't even support DNSSEC or standard dynamic updates.
