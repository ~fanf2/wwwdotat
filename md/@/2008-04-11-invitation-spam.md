---
dw:
  anum: 14
  eventtime: "2008-04-11T10:45:00Z"
  itemid: 333
  logtime: "2008-04-11T10:45:33Z"
  props:
    commentalter: 1491292361
    import_source: livejournal.com/fanf/86218
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/85262.html"
format: html
lj:
  anum: 202
  can_comment: 1
  ditemid: 86218
  event_timestamp: 1207910700
  eventtime: "2008-04-11T10:45:00Z"
  itemid: 336
  logtime: "2008-04-11T10:45:33Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/86218.html"
title: invitation spam
...

<p>I got two variants that slipped past my filters overnight:</p>

<b>Yahoo! groups</b>

<blockquote>tmatt087dten@yahoo.com has invited you to join Build_Instant_Money_Now on Yahoo! Groups, the best way to discover and share information and advice with others.</blockquote>

<b>Google Calendar</b>

<blockquote>Description: Dear Good Friend,<br>
I am very happy to inform you about my success in getting those funds transferred under the cooperation of a new partner in London,Presently,I'm in saudi arabia for the purpose of investing my own share of the money.</blockquote>

<p>An interesting way to deliver spam. It reminds me a bit of a key weakness of DJB's IM2000, where the message availability notifications can be used to carry spam payloads regardless of their intended purpose.</p>
