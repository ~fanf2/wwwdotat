---
dw:
  anum: 30
  eventtime: "2016-05-08T15:30:00Z"
  itemid: 456
  logtime: "2016-05-08T14:30:06Z"
  props:
    commentalter: 1491292421
    import_source: livejournal.com/fanf/143907
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/116766.html"
format: html
lj:
  anum: 35
  can_comment: 1
  ditemid: 143907
  event_timestamp: 1462721400
  eventtime: "2016-05-08T15:30:00Z"
  itemid: 562
  logtime: "2016-05-08T14:30:06Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 7
  url: "https://fanf.livejournal.com/143907.html"
title: "Apple calendar apps amplifying .ics VALARM ACTION:EMAIL"
...

<p>This took me hours to debug yesterday so I thought an article would be a good idea.

<h2>The vexation</h2>

<p>My phone was sending email notifications for events in our shared calendar.

<p>Some of my Macs were trying and failing to send similar email notifications. (Mac OS Mail knows about some of my accounts but it doesn't know the passwords because I don't entirely trust the keychain.)

<h2>The confusion</h2>

<p>There is no user interface for email notifications in Apple's calendar apps.
<ul>
<li>They do not allow you to create events with email notifications.
<li>They do not show the fact that an event (created by another client) has an email notification configured.
<li>They do not have a preferences setting to control email notifications.
</ul>

<h2>The escalation</h2>

<p>It seems that if you have a shared calendar containing an event with an email notification, each of your Apple devices which have this calendar will separately try to send their own duplicate copy of the notification email.

<p>(I dread to think what would happen in an office environment!)

<h2>The explanation</h2>

<p>I couldn't get a CalDAV client to talk to Fastmail's CalDAV service in a useful way. I managed to work out what was going on by exporting a <tt>.ics</tt> file and looking at the contents.

<p>The VEVENT clause saying ACTION:EMAIL was immediately obvious towards the end of the file.

<h2>The solution</h2>

<p>Sadly I don't know of a way to stop them from doing this other than to avoid using email  notification on calendar events.
