---
dw:
  anum: 59
  eventtime: "2008-09-10T00:12:00Z"
  itemid: 360
  logtime: "2008-09-10T01:37:30Z"
  props:
    commentalter: 1491292360
    import_source: livejournal.com/fanf/93326
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/92219.html"
format: html
lj:
  anum: 142
  can_comment: 1
  ditemid: 93326
  event_timestamp: 1221005520
  eventtime: "2008-09-10T00:12:00Z"
  itemid: 364
  logtime: "2008-09-10T01:37:30Z"
  props: {}
  reply_count: 9
  url: "https://fanf.livejournal.com/93326.html"
title: Counting the days
...

<p>Nearly 10 years ago I developed a neat function for converting a Gregorian date into a count of days since some epoch. There was nothing particularly new about it; I just tweaked the numbers until the formula was as simple as possible. I've re-used this code a fair number of times since then without re-examining it. This evening I worked out that it can be distilled even more.</p>

<p>The function adds together some expressions that count the number of days indicated by the three components of the date, plus a constant to set the epoch.</p>

<p>The number of normal days before the year <i>y</i> (full four digit year) is just <i>y</i>*365.</p>

<p>The number of leap days up to and including the year <i>y</i> is <i>y</i>/4-<i>y</i>/100+<i>y</i>/400. This is the usual "add a leap day in every fourth year, but not in every hundredth year, but do in every 400th year" rule. I'm using integer division that discards any remainder, i.e. rounds down.</p>

<p>The previous two paragraphs are inconsistent about including or excluding the current year. This is because leap days occur inside a leap year, not at the end, so their contribution to the day count does not depend solely on the year. We can fix this by re-assigning January and February to the previous year using a small administrative fiddle, as follows (where <i>m</i> counts months starting from 1). Then a leap day falls just before the start of years that are multiples of four (etc.) and the expression in the previous paragraph magically does the right thing. The fiddle also requires an adjustment to the epoch constant.</p>

<pre>
        if (m > 2) m -= 2; else m += 10, y -= 1;
</pre>

<p>Months are the part that I spent most time fiddling with. Section 1.12 of <a href="http://emr.cs.iit.edu/home/reingold/calendar-book/third-edition/">Calendrical Calculations</a> develops various equations for calendars (such as the Julian calendar) that spread their leap years evenly, like Bresenham's line drawing algorithm spreads out raster steps. The equations also work for month lengths in the Julian and Gregorian calendars, if you treat 30-day months as normal years and 31 day months as leap years, and ignore February. We want to calculate the number of days in a year before the start of a given month, which is calculated by equation 1.73:</p>

<pre>
        (L*y - L + D*L % C) / C + (y - 1) * N
</pre>

<p>where <i>L</i> is the number of leap years in a cycle, <i>C</i> is the total number of years in a cycle, <i>D</i> is the offset of the start of the cycle, <i>N</i> is the number of days in a normal year, and years are actually months. Yuck. It turns out that if we perform the right administrative fiddling, we can arrange that <i>D</i>=0, and we can substitute <i>m</i>=<i>y</i>-1. This gives us the greatly simplified:</p>

<pre>
        m*L/C + m*N = m * (N*C+L) / C
</pre>

<p>For Julian and Gregorian months, <i>N</i>=30, <i>C</i>=12, and <i>L</i>=7, giving <i>m</i>*367/12. This pretends that February has 30 days, but the pretence doesn't matter because we have moved February to the end of the year so we never count days after February 28th or 29th . The complete function is then as follows (with the epoch set so that 1st January, 1 A.D. is day 1).</p>

<pre>
    int A(int y, int m, int d) {
        if (m > 2) m -= 2; else m += 10, y -= 1;
        return y*365 + y/4 - y/100 + y/400 + m*367/12 + d - 336;
    }
</pre>

<p>In fact you don't have to treat a whole year as a cycle for the purpose of the month calculation. The most basic pattern is a five month cycle containing three long months, as in March-July and August-December. (January and February are the start of a third truncated cycle.) However when I tried to make <i>m</i>*153/5 work in 1999 I could not eliminate the <i>D</i> offset to keep the equations simple. My problem was that I only allowed myself to count months starting from zero or one, and I was flailing around without the sage guidance of messrs. Dershowitz and Reingold. (I didn't use their equation 1.73 when I was developing this code, but it's a good example of what I was trying to avoid.)</p>

<p>It turns out that we can play around with the administrative fiddle to adjust <i>D</i> so that the cycle falls in the right place, so long as we compensate by adjusting the epoch constant. For the Julian and Gregorian five-month cycle <i>D</i>=4, i.e. we need to shift March (the start of the cycle) to month number 4, which gives us the following code.</p>

<pre>
    int B(int y, int m, int d) {
        if (m > 2) m += 1; else m += 13, y -= 1;
        return y*365 + y/4 - y/100 + y/400 + m*153/5 + d - 428;
    }
</pre>

<p>I mentioned above that equation 1.73 works for Julian leap years, so we can use it for years as well as months. This combines first two terms of the return expression leaving the Gregorian correction separate. No additional fiddling is necessary. We can't merge in the Gregorian correction as well, because evenly spreading the leap years gives intervals of 4 or 5, not 4 or 8.</p>

<pre>
    int C(int y, int m, int d) {
        if (m > 2) m += 1; else m += 13, y -= 1;
        return y*1461/4 - y/100 + y/400 + m*153/5 + d - 428;
    }
</pre>

<p>There's a related function for calculating the number of days in a month. It uses the remainders from the divisions to find the length of the current month, instead of using the results to count the preceding days. My old code re-used the same administrative fiddle:</p>

<pre>
    int D(int y, int m) {
        if (m > 2) m -= 2; else m += 10;
        return m*367%12 > 4 ? 31
             : m != 12     ? 30
             : y % 4      ? 28
             : y % 100   ? 29
             : y % 400  ? 28
                       : 29;
    }
</pre>

<p>In fact you don't need a fiddle in this case, because there's no need to move February to the end of the year. You can just pick values of <i>C</i> and <i>L</i> which make the pattern fall in the right place.</p>

<pre>
    int E(int y, int m) {
        return m*275%9 > 3 ? 31
             : m != 2     ? 30
             : y % 4     ? 28
             : y % 100  ? 29
             : y % 400 ? 28
                      : 29;
    }
</pre>
