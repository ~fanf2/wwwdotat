---
dw:
  anum: 71
  eventtime: "2008-08-27T13:48:00Z"
  itemid: 356
  logtime: "2008-08-27T13:48:00Z"
  props:
    commentalter: 1491292359
    hasscreened: 1
    import_source: livejournal.com/fanf/92248
    interface: flat
    opt_backdated: 1
    picture_keyword: silly
    picture_mapid: 6
  url: "https://fanf.dreamwidth.org/91207.html"
format: casual
lj:
  anum: 88
  can_comment: 1
  ditemid: 92248
  event_timestamp: 1219844880
  eventtime: "2008-08-27T13:48:00Z"
  itemid: 360
  logtime: "2008-08-27T13:48:00Z"
  props: {}
  reply_count: 7
  url: "https://fanf.livejournal.com/92248.html"
title: Birthday present
...

I arrived in the office today to find that <a href="http://www.srcf.ucam.org/">some fans</a> had given me <a href="http://www.spamgift.com/ProductDetail.aspx?Product={7172D74B-1AD4-4A55-9F64-5015FA4D2D33}">a birthday present</a>, which rather amused me :-)
