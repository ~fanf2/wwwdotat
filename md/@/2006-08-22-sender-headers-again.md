---
dw:
  anum: 81
  eventtime: "2006-08-22T20:03:00Z"
  itemid: 250
  logtime: "2006-08-22T20:13:32Z"
  props:
    commentalter: 1491292339
    import_source: livejournal.com/fanf/64303
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/64081.html"
format: casual
lj:
  anum: 47
  can_comment: 1
  ditemid: 64303
  event_timestamp: 1156276980
  eventtime: "2006-08-22T20:03:00Z"
  itemid: 251
  logtime: "2006-08-22T20:13:32Z"
  props: {}
  reply_count: 5
  url: "https://fanf.livejournal.com/64303.html"
title: "Sender: headers again"
...

http://fanf.livejournal.com/44733.html

I just found out an explanation for Outlook's irritating behaviour that I was working around back in January. It turns out that Sendmail has never followed the standard's requirements for the <tt>Sender:</tt> header (it never adds or fixes it), so the developers of Outlook assumed that you would only see a <tt>Sender:</tt> header when the message had been transmitted via a mailing list. This makes the "from [list] on behalf of [author]" wording look relatively sensible. However, they should have used the bloody <tt>List-ID:</tt> which is supposed to be used for exactly this kind of thing.

What is worse is that Exim is out on a limb in this respect. Postfix used to follow the standard but was changed in 2000 to be bug-compatible with Sendmail. This is very irritating, because the standard behaviour is much more useful. Grr.
