---
dw:
  anum: 139
  eventtime: "2007-11-08T17:14:00Z"
  itemid: 312
  logtime: "2007-11-08T17:16:41Z"
  props:
    commentalter: 1491292349
    import_source: livejournal.com/fanf/80401
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/80011.html"
format: html
lj:
  anum: 17
  can_comment: 1
  ditemid: 80401
  event_timestamp: 1194542040
  eventtime: "2007-11-08T17:14:00Z"
  itemid: 314
  logtime: "2007-11-08T17:16:41Z"
  props: {}
  reply_count: 7
  url: "https://fanf.livejournal.com/80401.html"
title: Hot off the presses
...

<pre>
Network Working Group                                         C. Hutzler
<a href="http://www.ietf.org/rfc/rfc5068.txt">Request for Comments: 5068</a>
BCP: 134                                                      D. Crocker
Category: Best Current Practice              Brandenburg InternetWorking
                                                              P. Resnick
                                                   QUALCOMM Incorporated
                                                               E. Allman
                                                          Sendmail, Inc.
                                                                <b>T. Finch</b>
                               University of Cambridge Computing Service
                                                           November 2007

  Email Submission Operations: Access and Accountability Requirements

Status of This Memo

   This document specifies an Internet Best Current Practices for the
   Internet Community, and requests discussion and suggestions for
   improvements.  Distribution of this memo is unlimited.

Abstract

   Email has become a popular distribution service for a variety of
   socially unacceptable, mass-effect purposes.  The most obvious ones
   include spam and worms.  This note recommends conventions for the
   operation of email submission and transport services between
   independent operators, such as enterprises and Internet Service
   Providers.  Its goal is to improve lines of accountability for
   controlling abusive uses of the Internet mail service.  To this end,
   this document offers recommendations for constructive operational
   policies between independent operators of email submission and
   transmission services.

   Email authentication technologies are aimed at providing assurances
   and traceability between internetworked networks.  In many email
   services, the weakest link in the chain of assurances is initial
   submission of a message.  This document offers recommendations for
   constructive operational policies for this first step of email
   sending, the submission (or posting) of email into the transmission
   network.  Relaying and delivery entail policies that occur subsequent
   to submission and are outside the scope of this document.
</pre>
