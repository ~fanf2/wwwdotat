---
dw:
  anum: 219
  eventtime: "2004-03-05T18:37:00Z"
  itemid: 70
  logtime: "2004-03-05T10:37:51Z"
  props:
    import_source: livejournal.com/fanf/17973
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/18139.html"
format: casual
lj:
  anum: 53
  can_comment: 1
  ditemid: 17973
  event_timestamp: 1078511820
  eventtime: "2004-03-05T18:37:00Z"
  itemid: 70
  logtime: "2004-03-05T10:37:51Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/17973.html"
title: UKUUG LISA Winter Conference paper
...

Now available on the web at http://www.cus.cam.ac.uk/~fanf2/hermes/doc/talks/2004-02-ukuug/
