---
dw:
  anum: 13
  eventtime: "2006-05-04T20:08:00Z"
  itemid: 227
  logtime: "2006-05-04T20:15:11Z"
  props:
    import_source: livejournal.com/fanf/58378
    interface: flat
    opt_backdated: 1
    picture_keyword: weather
    picture_mapid: 5
  url: "https://fanf.dreamwidth.org/58125.html"
format: casual
lj:
  anum: 10
  can_comment: 1
  ditemid: 58378
  event_timestamp: 1146773280
  eventtime: "2006-05-04T20:08:00Z"
  itemid: 228
  logtime: "2006-05-04T20:15:11Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/58378.html"
title: AfNOG pictures
...

Our "localhost" spent most of today struggling to get shipments through customs, including the routers for {E,F}2 and various other equipment. Most of the instructors are here now, and we are very international crowd. USA, Canada, Ausralia, New Zealand, Chile, France, Denmark, Nigeria, Ghana, South Africa, and I'm sure I've forgotten some. A few people represent several countries by themselves :-)

http://www.ws.afnog.org/pictures/index.html

Amanda's pictures from today (4th May) include one of Y.T. editing slides and handouts.

The Guinness brewed under licence in Kenya is 6.5%. The coffee is still excellent.
