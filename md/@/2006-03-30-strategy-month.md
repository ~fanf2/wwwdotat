---
dw:
  anum: 54
  eventtime: "2006-03-30T18:34:00Z"
  itemid: 214
  logtime: "2006-03-30T18:45:42Z"
  props:
    commentalter: 1491292332
    import_source: livejournal.com/fanf/55126
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/54838.html"
format: casual
lj:
  anum: 86
  can_comment: 1
  ditemid: 55126
  event_timestamp: 1143743640
  eventtime: "2006-03-30T18:34:00Z"
  itemid: 215
  logtime: "2006-03-30T18:45:42Z"
  props: {}
  reply_count: 19
  url: "https://fanf.livejournal.com/55126.html"
title: Strategy month
...

According to <strike>Our Glorious Leader</strike> the Computing Service's director, March is "strategy month" and we are supposed to come up with ideas. So, this is my long-term wish list. No restraint has been exerted in its compilation :-)<ul><li>Get the Chat service deployed!
<li>Hermes projects:
<ul><li>Re-do and improve the anti-spam and anti-virus infrastructure. This has three parts:
<ol><li>Replace MailScanner with exiscan, so that we can scan messages at SMTP time, and reject those we don't like instead of discarding them. If we don't want to rely totally on ClamAV we'd need to replace McAfee with a better commercial AV scanner, because McAfee is not fast enough for non-batch-mode scanning; it's also pretty bad at promptly issuing new signatures. This would allow us to have a default SpamAssassin threshold (without the lost-email consent problems caused by the way MailScanner works) which in turn would help with our AOL problems.
<li>Allow per-user options at SMTP time, e.g. spam threshold, stricter sender verification, greylisting, etc. This requires some data-shifting infrastructure which I have made a small start at, and user-interface extensions to Prayer.
<li>Add a new component for per-user statistical spam filtering. This would live on the message store (with other per-user state), and hook into message delivery for initial filtering, and copying to or from the spam folder for training. Again we need user-interface changes to Prayer to present the results of the filter and allow users to train it - similar functionality to full MUAs like Mac OS X Mail or Thunderbird.
</ol><li>Shared folders. Requires a new IMAP proxy (like the Cyrus Murder) and changes to the replication system. Does Cyrus 2.3 have the necessary functionality?
<li>Internationalized webmail. Replace (or run alongside) Prayer with some better-known software?
<li>Finish the ratelimit project. Will require time to work on a user interface, so that it is sufficiently friendly. My current idea for how it would work requires the double-bounce patch mentioned below.
<li>Deploy DKIM (server-side message signatures for anti-spam).
</ul><li>Exim projects:
<ul><li>Improved hints DB infrastructure. The aim is to remove the performance bottleneck caused by whole-table locking, and make the back-end more pluggable so that we could have distributed hints databases. PPswitch would then act as a cluster with the machines sharing knowledge about other hosts on the net.
<li>Concurrent DNS lookups during routing, to improve mailing list performance. I've done some work on this already.
<li>Better handling of double bounces. If we can't deliver a bounce to recipient@domain, try postmaster@domain then postmaster@cam instead of freezing the message.
<li>Finish the bounce address protection code.
</ul><li>Service integration:
<ul><li>Kerberos. Proper single-sign-on! Users really hate typing in passwords.
<li>Raven-authenticated webmail for more single-sign-on.
<li>Mailing lists generated from Lookup groups. Lookup groups generated from data in CamSIS/CHRIS.
<li>Web presence: an extension to the chat service which allows other Raven-authenticated sites (such as lookup and webmail) to display a presence icon next to a username. If I am looking at your lookup page and you are on my chat buddy list, then I can see whether you are currently on-line and I can click on the presence icon to chat with you. Depends crucially on transparent single-sign-on. In particular, the default for Raven at the moment is confirmed sign-on, which isn't transparent enough.
<li>Short URL service, like go.warwick.ac.uk, which doesn't host content but instead redirects to full URLs. Mainly for use on printed material such as business cards, academic papers, posters, press releases, etc. Namespace divided into ~CRSID for homepages (using data from lookup) and ad-hoc names managed by user-admin. Could include societies.
</ul><li>Web stuff, etc.:
<ul><li>Blogging service.
<li>Web forums.
<li>The two could be aspects of the same thing (e.g. Livejournal's blogs and communities).
<li>Society information in Lookup, similar to the institution information.
<li>More effective use of talks.cam.ac.uk.
<li>Shared calendars / meeting manager.
</ul><li>Networking:
<ul><li>Make it easier for depts and colleges to host conferences. e.g. at the Durham UKUUG I was given a short-term uid which gave me access to their PWF and wifi.
</ul></ul>
