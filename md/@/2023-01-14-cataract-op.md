---
comments:
    Dreamwidth: https://fanf.dreamwidth.org/141228.html
title: Cataract surgery
...

Previously, I wrote about [my cataract][] and its assessment at
Addenbrooke's [cataract clinic][].

[my cataract]: https://dotat.at/@/2021-09-22-my-cataract.html
[cataract clinic]: https://dotat.at/@/2022-02-23-cataract-clinic.html

I had my cataract removed a couple of weeks ago, and so far things are
going well, though there is still some follow-up work needed.

<cut>

timing
------

My cataract op was originally planned for the end of November, but it
got delayed to the 30th December. I wasn't planning to do anything for
new year's eve, so nbd - but I _did_ feel that xkcd was trolling me
personally with a couple of cartoons areound the turn of the year:

  * [I'm regretting my New Year's Eve novelty "2023" laser eye surgery.](https://xkcd.com/2718/)

  * [Is it true that if someone makes a hole in you, it just closes up on its own?](https://xkcd.com/2720/)


clothing
--------

I had the op under general anaesthetic, because my cataract was
unusually difficult. The cataract clinic normally avoids the extra
risks of a general anaesthetic becase their typical patients are much
older than me.

So I was kitted out in a hospital gown (I was allowed to keep my
underpants on), compression socks, and the ugliest non-slip slipper
socks I have ever seen.


preparation
-----------

I first spoke to the more junior of the surgeons, who examined my
cataract and discussed thigs like the target focal length.

Because my cataract was so dense, it was difficult for them to
accurately measure the size of my eye, so there's a greater chance
than usual of a "refractive surprise", in which case I will need more
correction in the left lens of my specs. The surgeons seemed a lot
more concerned about my need for specs than I am!

After some waiting, a nurse conspiratorially said to me "the
consultant is coming!" and I was ushered into the examination room
again.

The consultant surgeon examined my eye. "Interesting cataract," he
said. He had a disconcerting habit of sucking his teeth when looking
at my eye, like a plumber who is about to give you some
wallet-lightening bad news.

The surgeons had a high-bandwith discussion involving lots of ocular
anatomical jargon, as they made a plan for how they would go about the
operation.

I was given a list of probabilities of possible bad outcomes; in some
cases I might have had to return in a week or so for a second fixup
operation.


bedside manner
--------------

Everyone adhered to their assigned stereotypes.

The nurses were cheerful and appreciated my little jokes at my own
expense.

The anaesthetist was more business-like. When he asked if I had
previously had a general anaesthetic, I said, "not since I had my
wisdom teeth out 30 years ago", and told him a little anecdote:

> A large and serious nurse made me lie on my front, and gave me a
> pre-med injection in my bottom. I'm not sure if it was the drugs or
> the indignity, but I was giggling foolishly until they wheeled me
> through.

"In your bum!", he responded with surprise.

The surgeons were very serious, seeming to focus more on the
technicalities than on the patient.


cross checking
--------------

There was a reassuring amount of double- and triple-checking.

In the first examination, the surgeon drew an arrow on my forehead
pointing at my left eye, and got me to sign a consent form.

I was asked several times, "do you have any allergies?" (no).

I was asked a few times, by the anaesthetist and the surgeouns, "what
are we going to do to you?" (I did not attempt to use any jargon when
replying!)

In the operating room, one of the nurses found that I had signed the
wrong consent form (it did not cover the general anaesthetic), so I
had to sign a replacement. (A bit awkward with a cannula in the back
of my writing hand!)

And when they got the new lens the surgeons examined the specification
on its box and agreed with each other that it matched what they
expected.

I admired the attention to detail.


afterwards
----------

The consultant came round to chat after I had woken up.

He was much more cheerful than before, and seemed quite pleased with
himself - a definite improvement compared to his demeanor before the
op!

He told me that out of caution they had not removed absolutely all of
the cataract, because there was too much risk of damaging the lens's
supporting structures. The remaining "plaques" can be removed by laser
surgery after my eye has healed up. No need for a second invasive op.

A nurse gave me some eye drops (antibiotics, 4x daily for a week,
steriods, 4x daily for 4 weeks) and instructions on looking after my
eye - when to wear the eye guard, how to clean the eye, etc.

Rachel came to help me collect my things and chaperone me home in a
taxi.


recovery
--------

The first day or so after the op, my eye was sore enough that I needed
painkillers. But after then it was tolerable, mainly itchy /
irritated, and the eye drops helped with that.

I had a glimpse through the eye when I came round in the operating
room, before they put the eye patch on, and it was immediately
apparent that the eye could see better than it ever did before.

There were some odd visual effects at first:

  * Different colour balance: my left eye saw everything as a lot more
    _blue_ and high contrast.

  * Bright light was actually painful: my eye could not comfortably
    adjust to it.

  * Some odd double vision, like my left eye saw at a 30° angle to my
    right eye.

I spent a lot of the first week with my left eye closed except when
the light was subdued enough. I usually have a lot of bright lights in
my study, but they have remained off!

The surgeon I spoke to back in February had been very cautious about
whether my eyes would be able to work together after surgery, so the
double vision was not a surprise, though it was disappointing.

But after a week or so, it settled down: colours appear the same and
up is in the same direction with both eyes. My left eye still gets
tired much more quickly, but it can cope with normal indoor lighting
levels comfortably.

I have got some extra "lubricating" eye drops to help soothe the eye
when it gets irritated between steroid drops.


what's next
-----------

At the moment, my left eye can see relatively well, much better than
it could ever see before. (I had the cataract since birth; I had
surgery because it became worse in recent years.) It is still blurry,
though, comparable to my right eye without my specs, and not good
enough to read with. (My right eye is very short-sighted, about -7.5
diopters.)

As far as I can tell, my brain is now doing a reasonable job of
integrating vision from both eyes. Based on a brief game of catch with
the offspring, my depth perception seems to be working. Yay!

I am due back at Addenbrooke's in a couple of weeks for a checkup, and
I expect that in February I will return again to have the remnants of
the cataract removed with LASERS (_pew pew_). After that, I got the
impression from the surgeons that I might be able to see well enough
to read, which would be truly magical.

After that I expect I will get some new specs. I think I will get a
general purpose pair, and separate pair for looking at computer
screens.
