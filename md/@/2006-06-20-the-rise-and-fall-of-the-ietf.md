---
dw:
  anum: 40
  eventtime: "2006-06-20T14:52:00Z"
  itemid: 240
  logtime: "2006-06-20T14:57:28Z"
  props:
    commentalter: 1491292339
    import_source: livejournal.com/fanf/61875
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/61480.html"
format: casual
lj:
  anum: 179
  can_comment: 1
  ditemid: 61875
  event_timestamp: 1150815120
  eventtime: "2006-06-20T14:52:00Z"
  itemid: 241
  logtime: "2006-06-20T14:57:28Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/61875.html"
title: The rise and fall of the IETF?
...

http://www.acmqueue.com/modules.php?name=Content&pa=showpage&pid=396

I can't help thinking of the IETF when reading this article about CORBA, and how its actual behaviour can deviate from its self-image. Some people refer to this disparagingly as the "IVTF" (V for vendor).
