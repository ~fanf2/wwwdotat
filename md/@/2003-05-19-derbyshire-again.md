---
dw:
  anum: 61
  eventtime: "2003-05-19T18:02:00Z"
  itemid: 21
  logtime: "2003-05-19T11:20:18Z"
  props:
    commentalter: 1491292304
    current_moodid: 126
    current_music: Tubular Bells III
    import_source: livejournal.com/fanf/5455
    interface: flat
    opt_backdated: 1
    picture_keyword: photo
    picture_mapid: 3
  url: "https://fanf.dreamwidth.org/5437.html"
format: casual
lj:
  anum: 79
  can_comment: 1
  ditemid: 5455
  event_timestamp: 1053367320
  eventtime: "2003-05-19T18:02:00Z"
  itemid: 21
  logtime: "2003-05-19T11:20:18Z"
  props:
    current_moodid: 126
    current_music: Tubular Bells III
  reply_count: 6
  url: "https://fanf.livejournal.com/5455.html"
title: Derbyshire again
...

We went up for the whole weekend this time, Friday evening to Sunnday evening, camping near Hope. We started off with breakfast at Longlands Eating House, then went to check the rocks in the old quarry at <a href="http://www.streetmap.co.uk/streetmap.dll?G2M?X=425265&Y=379770&A=Y&Z=3">Lawrence Field</a> for climbability. Unfortunately they were saturated by rain and remained so because the weather didn't clear up; we couldn't climb so we had to find other things to do. Lawrence Field has a beautiful lawn wooded with silver birch and grazed by sheep, which we could easily imagine to be populated by elves as we walked around it. We then went to see <a href="http://www.streetmap.co.uk/streetmap.dll?G2M?X=413505&Y=382610&A=Y&Z=3">a couple of the caves near Castleton</a>, namely Blue John Cavern (which is spectacular) and Speedwell Cavern (which includes an underground boat ride but is otherwise disappointing). On Sunday we did some walking around the Derwent Reservoir (where they tested the bouncing bomb), and went to see the Dambusters museum which is housed in one of the towers in the dam.

Maybe next time the weather will be better.
