---
format: casual
lj:
  anum: 155
  can_comment: 1
  ditemid: 126875
  event_timestamp: 1367547720
  eventtime: "2013-05-03T02:22:00Z"
  itemid: 495
  logtime: "2013-05-03T01:22:02Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 7
  url: "https://fanf.livejournal.com/126875.html"
title: Two compelling applications for universal surveillance
...

Face recognition is pretty good now: you can upload a picture to Facebook and it will automatically identify who is in the frame. Combine this with Google Glass and the idea of lifelogging: what can these technologies do as prosthetic memories?

I am at a party and I meet someone I am sure I have met before, but I can&#39;t remember. I could try talking to them until I get enough context to remind myself of their name, or I could just wink to take a photo, which is uploaded and annotated, and then I know their name, employer, marital status, and I have a picture of our last photographed encounter. Oh yes, now I remember! And we have saved a few minutes of embarrassing protocol negotiation and impedance matching.

But why wink? If I am lifelogging, everything I do gets uploaded. So when I see someone, I can be automatically reminded to say happy birthday, or that I need to give them the thing I offered to lend them. Contextual social cues!

How much embarrassment we could avoid! How sooner we could get to the fun part of the conversation!

(This post was inspired by a pub conversation with Ian Jackson and Mark Wooding; Simon Tatham suggested the auto-reminder feature.)
