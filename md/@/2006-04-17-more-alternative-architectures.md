---
dw:
  anum: 172
  eventtime: "2006-04-17T22:54:00Z"
  itemid: 220
  logtime: "2006-04-17T21:59:46Z"
  props:
    commentalter: 1491292332
    import_source: livejournal.com/fanf/56642
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/56492.html"
format: casual
lj:
  anum: 66
  can_comment: 1
  ditemid: 56642
  event_timestamp: 1145314440
  eventtime: "2006-04-17T22:54:00Z"
  itemid: 221
  logtime: "2006-04-17T21:59:46Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/56642.html"
title: More alternative architectures
...

Mike O'Dell's alternative IPv6 architecture, known as 8+8, is neat. His idea of NATting at site boundaries as a matter of course is something I have been wondering about. Nice to see that someone has already worked it out in detail.

http://www.watersprings.org/pub/id/draft-odell-8+8-00.txt

Previously: http://fanf.livejournal.com/53662.html

<b>Update:</b> 8+8 later became the "Global/Site/End-system" addressing architecture: http://ietfreport.isoc.org/idref/draft-ietf-ipngwg-gseaddr/
