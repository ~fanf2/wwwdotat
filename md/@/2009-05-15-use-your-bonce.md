---
dw:
  anum: 140
  eventtime: "2009-05-15T09:44:00Z"
  itemid: 385
  logtime: "2009-05-15T08:55:18Z"
  props:
    commentalter: 1491292378
    import_source: livejournal.com/fanf/100245
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/98700.html"
format: html
lj:
  anum: 149
  can_comment: 1
  ditemid: 100245
  event_timestamp: 1242380640
  eventtime: "2009-05-15T09:44:00Z"
  itemid: 391
  logtime: "2009-05-15T08:55:18Z"
  props:
    personifi_tags: "2:11,35:2,8:52,33:2,11:2,23:8,42:2,32:5,1:52,3:2,25:2,19:2,20:8,9:32,nterms:yes"
    verticals_list: "computers_and_software,life"
  reply_count: 15
  url: "https://fanf.livejournal.com/100245.html"
title: Use your bonce
...

<p><a href="http://www.vestasys.org/">Vesta</a> includes a purely functional programming language for specifying build rules. It has <a href="http://www.vestasys.org/doc/pubs/pldi-00-04-20.pdf">an interesting execution model</a> which avoids unnecessary rebuilds. Unlike <tt>make</tt>, it automatically works out dependencies in a way that is independent of your programming language or tools - no manually maintained dependencies or parsing source for <tt>#include</tt> etc. Also unlike make, it doesn't use timestamps to decide if dependencies are still valid, but instead uses a hash of their contents; it can do this efficiently because of its underlying version control repository. Vesta assumes that build tools are essentially purely functional, i.e. that their output files depend only on their input files, and that any differences (e.g. embedded timestamps) don't affect the functioning of the output.</p>

<p>I've been wondering if Vesta's various parts can be unpicked. It occurred to me this morning that its build-once functionality could make a quite nice stand-alone tool. So here's an outline of a program called <tt>bonce</tt> that I don't have time to write.</p>

<p><tt>bonce</tt> is an adverbial command, i.e. you use it like <tt>bonce gcc -c foo.c</tt>. It checks if the command has already been run, and if so it gets the results from its build results cache. It uses Vesta's dependency cache logic to decide if a command has been run. In the terminology of <a href="http://www.vestasys.org/doc/pubs/pldi-00-04-20.pdf">the paper</a>, the primary key for the cache is a hash of the command line, and the secondary keys are all the command's dependencies as recorded in the cache. If there is a cache miss, the command is run in dependency-recording mode. (Vesta does this using its magic NFS server, which is the main interface to its repository.) This can be done using an <tt>LD_PRELOAD</tt> hack that intercepts system calls, e.g. <tt>open(O_RDONLY)</tt> is a dependency and <tt>open(O_WRONLY)</tt> is probably an output file, and <tt>exec()</tt> is modified to invoke <tt>bonce</tt> recursively. When the command completes, its dependencies and outputs are recorded in the cache.</p>

<p><tt>bonce</tt> is likely to need some heuristic cleverness. For example, Vesta has some logic that simplifies the dependencies of higher-level build functions so that the dependency checking work for a top-level build invocation scales less than linearly with the size of the project. It could also be useful to look into git repositories to get SHA-1 hashes and avoid computing them.</p>

<p>It should then be reasonable to write very naive build scripts or makefiles, with simplified over-broad dependencies that would normally cause excessive rebuilds - e.g. every object file in a module depends on every source file - which <tt>bonce</tt> can reduce to the exact dependencies and thereby eliminate redundant work. No need for a special build language and no need to rewrite build scripts.</p>
