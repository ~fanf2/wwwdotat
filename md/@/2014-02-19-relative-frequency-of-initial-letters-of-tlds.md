---
dw:
  anum: 30
  eventtime: "2014-02-19T11:20:00Z"
  itemid: 403
  logtime: "2014-02-19T11:20:38Z"
  props:
    commentalter: 1491292407
    import_source: livejournal.com/fanf/130428
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/103198.html"
format: html
lj:
  anum: 124
  can_comment: 1
  ditemid: 130428
  event_timestamp: 1392808800
  eventtime: "2014-02-19T11:20:00Z"
  itemid: 509
  logtime: "2014-02-19T11:20:38Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 6
  url: "https://fanf.livejournal.com/130428.html"
title: Relative frequency of initial letters of TLDs
...

<p>Compare <a href="https://en.wikipedia.org/wiki/Letter_frequency#Relative_frequencies_of_the_first_letters_of_a_word_in_the_English_language">Wikipedia's table of the relative frequencies of initial letters in English</a>.

<pre>
$ dig axfr . @f.root-servers.net |
  perl -ne '
	next unless /^(([a-z])[a-z0-9-]+)[.][  ].*/;
	$label{$1} = 1; $letter{$2}++; $total++;
	END {
		for my $x (sort keys %letter) {
			my $p = 100.0*$letter{$x}/$total;
			printf "&lt;tr>&lt;td>$x&lt;/td>
				&lt;td align=right>%5.2f&lt;/td>
				&lt;td>&lt;span style=\"
					display: inline-block;
					background-color: gray;
					width: %d;\">
				&amp;nbsp;&lt;/span>&lt;/td>&lt;/tr>\n",
			    $p, 32*$p;
		}
	}'
</pre>

<table>
<tr><td>a</td><td align=right> 5.24</td><td><span style="display: inline-block; background-color: gray; width: 167;">&nbsp;</span></td></tr>
<tr><td>b</td><td align=right> 5.96</td><td><span style="display: inline-block; background-color: gray; width: 190;">&nbsp;</span></td></tr>
<tr><td>c</td><td align=right>10.02</td><td><span style="display: inline-block; background-color: gray; width: 320;">&nbsp;</span></td></tr>
<tr><td>d</td><td align=right> 2.45</td><td><span style="display: inline-block; background-color: gray; width: 78;">&nbsp;</span></td></tr>
<tr><td>e</td><td align=right> 3.28</td><td><span style="display: inline-block; background-color: gray; width: 104;">&nbsp;</span></td></tr>
<tr><td>f</td><td align=right> 2.43</td><td><span style="display: inline-block; background-color: gray; width: 77;">&nbsp;</span></td></tr>
<tr><td>g</td><td align=right> 5.03</td><td><span style="display: inline-block; background-color: gray; width: 161;">&nbsp;</span></td></tr>
<tr><td>h</td><td align=right> 1.78</td><td><span style="display: inline-block; background-color: gray; width: 57;">&nbsp;</span></td></tr>
<tr><td>i</td><td align=right> 3.38</td><td><span style="display: inline-block; background-color: gray; width: 108;">&nbsp;</span></td></tr>
<tr><td>j</td><td align=right> 1.14</td><td><span style="display: inline-block; background-color: gray; width: 36;">&nbsp;</span></td></tr>
<tr><td>k</td><td align=right> 2.71</td><td><span style="display: inline-block; background-color: gray; width: 86;">&nbsp;</span></td></tr>
<tr><td>l</td><td align=right> 3.74</td><td><span style="display: inline-block; background-color: gray; width: 119;">&nbsp;</span></td></tr>
<tr><td>m</td><td align=right> 6.69</td><td><span style="display: inline-block; background-color: gray; width: 213;">&nbsp;</span></td></tr>
<tr><td>n</td><td align=right> 4.26</td><td><span style="display: inline-block; background-color: gray; width: 136;">&nbsp;</span></td></tr>
<tr><td>o</td><td align=right> 0.72</td><td><span style="display: inline-block; background-color: gray; width: 23;">&nbsp;</span></td></tr>
<tr><td>p</td><td align=right> 5.68</td><td><span style="display: inline-block; background-color: gray; width: 181;">&nbsp;</span></td></tr>
<tr><td>q</td><td align=right> 0.65</td><td><span style="display: inline-block; background-color: gray; width: 20;">&nbsp;</span></td></tr>
<tr><td>r</td><td align=right> 2.81</td><td><span style="display: inline-block; background-color: gray; width: 90;">&nbsp;</span></td></tr>
<tr><td>s</td><td align=right> 6.15</td><td><span style="display: inline-block; background-color: gray; width: 196;">&nbsp;</span></td></tr>
<tr><td>t</td><td align=right> 6.02</td><td><span style="display: inline-block; background-color: gray; width: 192;">&nbsp;</span></td></tr>
<tr><td>u</td><td align=right> 1.91</td><td><span style="display: inline-block; background-color: gray; width: 61;">&nbsp;</span></td></tr>
<tr><td>v</td><td align=right> 2.66</td><td><span style="display: inline-block; background-color: gray; width: 85;">&nbsp;</span></td></tr>
<tr><td>w</td><td align=right> 1.94</td><td><span style="display: inline-block; background-color: gray; width: 61;">&nbsp;</span></td></tr>
<tr><td>x</td><td align=right>12.19</td><td><span style="display: inline-block; background-color: gray; width: 389;">&nbsp;</span></td></tr>
<tr><td>y</td><td align=right> 0.41</td><td><span style="display: inline-block; background-color: gray; width: 13;">&nbsp;</span></td></tr>
<tr><td>z</td><td align=right> 0.75</td><td><span style="display: inline-block; background-color: gray; width: 23;">&nbsp;</span></td></tr>
</table>
