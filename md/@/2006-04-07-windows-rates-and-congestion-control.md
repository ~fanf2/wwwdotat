---
dw:
  anum: 198
  eventtime: "2006-04-07T18:19:00Z"
  itemid: 217
  logtime: "2006-04-07T18:49:04Z"
  props:
    commentalter: 1491292332
    import_source: livejournal.com/fanf/56026
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/55750.html"
format: casual
lj:
  anum: 218
  can_comment: 1
  ditemid: 56026
  event_timestamp: 1144433940
  eventtime: "2006-04-07T18:19:00Z"
  itemid: 218
  logtime: "2006-04-07T18:49:04Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/56026.html"
title: "Windows, rates, and congestion control"
...

<a href="http://fanf.livejournal.com/55727.html">On Wednesday I asked about alternatives to window-based protocols.</a> An answer is "rate-based". For example, see <a href="http://www.cse.wustl.edu/~jain/papers/charny.htm">this paper from 1995</a> which discusses congestion control in ATM.

Of course TCP has an implicit measure of its sending rate, because it maintains both a congestion window and an RTT, so its average sending rate is cwnd/rtt - but this is not explicit in the way it is for ATM. And in fact TCP's rate can be very bursty and is often unstable in adverse conditions. However the cwnd does give you a direct measurement of the amount of buffering you need in case it is necessary to retransmit lost packets.

<b>Edit:</b> There's a really nice summary of the burstiness of TCP in <a href="http://nms.lcs.mit.edu/papers/index.php?detail=111">this paper</a> which brilliantly turns what is commonly viewed as a disadvantage into a benefit for dynamic traffic splitting.

Someone on #cl pointed me to <a href="http://www.ana.lcs.mit.edu/dina/XCP/">XCP</a> which is a really clever congestion control protocol, which has been implemented to work under TCP but could in principle work with any unicast transport protocol. One thing that I quite like about it is that it agrees with my intuition that the Internet would work better if routers and hosts co-operated more (<a href=" http://fanf.livejournal.com/53662.html">one</a> <a href="http://fanf.livejournal.com/53976.html">two</a>). In XCP, senders annotate their packets with their current RTT and cwnd parameters (measured by TCP in the usual way) plus their desired cwnd increase. Routers along the way can adjust the increase according to how busy the links are, and even make it negative if there is too much traffic. The receiver then returns the feedback to the sender in its acks. The really brilliant thing is that routers do not need to keep any per-flow state - compare the ATM paper above, which does require per-flow state, and is therefore hopelessly unscalable. Imagine the number of concurrent flows and the flow churn rate of a router on the LINX! Furthermore, XCP separates aggregate efficiency from fair allocation of bandwidth between flows. The fairness controller can implement different policies, which could allow for more precise QOS guarantees, or bandwidth allocation based on price paid, etc.

Really really cool, but really really hard to deploy widely. It's the kind of thing that I suppose would fit in with David D. Clark's <a href="http://www.google.com/search?q=%22reinventing+the+internet%22+economist">Future Internet Network Design</a> research programme, which is supposed to come up with new ideas for the long term. "To do that, you have to free yourself from what the world looks like now."
