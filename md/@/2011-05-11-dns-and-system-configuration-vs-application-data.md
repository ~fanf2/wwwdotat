---
format: html
lj:
  anum: 109
  can_comment: 1
  ditemid: 112749
  event_timestamp: 1305158040
  eventtime: "2011-05-11T23:54:00Z"
  itemid: 440
  logtime: "2011-05-11T22:54:03Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/112749.html"
title: DNS and system configuration vs. application data.
...

<p>There's a rough distinction to be made between system configuration data and application managed data. The former is altered by the development / operations staff, and the latter is altered by application users. The former is typically maintained in a revision control repository and deployed to the live systems using configuration management tools such as rdist or puppet; the latter is maintained on the live systems by the application from which it is backed up. The former tends to be more ad hoc and the latter more standardized.</p>

<p>So for email the application data includes messages in flight (SMTP/LMTP) or in store (IMAP/POP), users' sieve filters (MANAGESIEVE), and the shared address book (LDAP). [I mention the protocols to emphasize the standardization.] System configuration data includes things like which domains are hosted where, system aliases, central filtering and access control rules, and suchlike. The distinction is rough because in an environment that is more ISPish than corporate, big chunks of system configuration get delegated to customers and sold as "virtual mail domains", with their own custom management applications in place of sysadmin tools, and SQL or LDAP database storage.</p>

<p>The DNS is more like a system configuration component than a user-facing application. Even so, it's a bit surprising that BIND has remained so firmly based on traditional flat files (at least, superficially). But in fact for a corporate environment (with one frequently-updated main zone and not a lot of churn in which zones are being served) the DNS has some really nice application data management features. In fact it had loads of NoSQL buzzwords before NoSQL was cool. It's a distributed replicated in-memory database with eventual consistency. The combination of UPDATE, NOTIFY, and IXFR mean that the lag between a user requesting a modification and it becoming visible at the slaves can be tiny. UPDATE itself has very nice transactional semantics which allow you to implement read-modify-write operations with optimistic concurrency. Swish.</p>

<p>It isn't actually too painful to bridge the gap between static zone files in revision control and live zones managed by BIND. <a href="https://fanf2.user.srcf.net/hermes/conf/bind/bin/nsdiff">Here's a little tool called <tt>nsdiff</tt></a> which does the job. If you are running on the master server you can just pipe the output into <tt>nsupdate -l</tt>. (This is not much harder than updating the live zone file in place and running <tt>rndc reload</tt>.) Note that if the diff is too large, <tt>nsdiff</tt> breaks it up into multiple update operations - perhaps it should have an option to fail if it can't use a single atomic transaction.</p>

<p>Whereas dynamic updates have been around for many years, BIND has only recently become able to incrementally add and delete zones from its configuration, as opposed to re-reading the entire thing to find out which zones it should serve. This is a bit surprising since BIND configuration updates have been a pain point for ISPs for a very long time. <a href="http://www.uit.co.uk/BK-ADNS/HomePage">Other nameservers</a> with more accessible database backends probably make provisioning rather less clunky than my little <a href="https://fanf2.user.srcf.net/hermes/conf/bind/bin/addzone-master"><tt>addzone-master</tt></a> <a href="https://fanf2.user.srcf.net/hermes/conf/bind/bin/addzone-slave"><tt>addzone-slave</tt></a> <a href="https://fanf2.user.srcf.net/hermes/conf/bind/bin/delzone"><tt>delzone</tt></a> scripts.</p>

<p>It might be interesting to see what comes of the IETF <a href="http://tools.ietf.org/wg/dnsop/">DNSOP</a> work on standardized name server management. So far there is just a <a href="http://tools.ietf.org/html/rfc6168">requirements document</a> but sadly not much sign of follow-up work.</p>
