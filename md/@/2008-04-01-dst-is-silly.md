---
dw:
  anum: 8
  eventtime: "2008-04-01T10:45:00Z"
  itemid: 330
  logtime: "2008-04-01T10:45:51Z"
  props:
    commentalter: 1491292351
    import_source: livejournal.com/fanf/85472
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/84488.html"
format: casual
lj:
  anum: 224
  can_comment: 1
  ditemid: 85472
  event_timestamp: 1207046700
  eventtime: "2008-04-01T10:45:00Z"
  itemid: 333
  logtime: "2008-04-01T10:45:51Z"
  props: {}
  reply_count: 5
  url: "https://fanf.livejournal.com/85472.html"
title: DST is silly
...

Around the end of October I read David Prerau's book "Saving the Daylight". It changed my opinion of DST somewhat. I used to think that it was unmitigated stupidity, and that everyone should make seasonal adjustments to their timetables instead, or something. (For example, my secondary school had afternoon lessons just after lunch in the summer, but later after tea in the winter.) What I hadn't considered was the human-factors effects that this anarchy would have. In the USA for much of the 20th century DST was a popular idea but it was not consistently enforced by governments at any level. The result was prolonged chaos, documented at length in Prerau's book. DST only works well when applied consistently over large areas, and enforcing it by law is the best way of achieving it.

I'm not entirely convinced that the goals of DST are foolish. I'm by nature the kind of person that Benjamin Franklin and (with less humour) William Willett wanted to persuade to get up earlier. But I like long summer evenings, and it's hard to make the most of them without awkward winter daylight times if your clock is fixed relative to mean solar time.

<em>However</em> I think mean solar time is a fetish that should now be discarded, especially in the guise of UTC. Leap seconds are an abomination: civil time does not need to be synchronized that closely to the Earth's rotational angle. Ideally we'd just define civil time as a timezone offset from atomic time, without the intermediate layer that keeps the astronomers happy. (The UK abolished the RGO because responsibility for timekeeping had moved from the astronomers to the physicists. The rest of the world hasn't yet made the same break with history.)

Having abolished unpredictable leap second disruptions in our timekeeping, we would still be dependent for stability on politicians not arsing around with the timezones. They have proved themselves incapable of keeping their grubby hands off. Perhaps the solution is to make a further break from the noon-oriented mean solar time fetish and use a more practical benchmark as the basis of civil time: sunrise. If the sun always rises at 07:30, then we get nice long summer evenings, sensible daylight in winter, and a time system based on natural philosophy not political tinkering. Actually, rather than using local sunrise as your benchmark, you want northern, southern, and perhaps tropical sunrise time zones. For example, civil times in the northern hemisphere might be defined by one-hour offsets from local sunrise time at the intersection of the Tropic of Cancer and the Greenwich Meridian. Perfect!

So about five months ago I wrote a brief description of how this could work. But I quickly realised (with Prerau's book in mind) that the practicalities of persuading people to put this into practice made the idea ridiculous. So it became <a href="http://catless.ncl.ac.uk/Risks/25.10.html#subj1">an April 1st RISKS Digest item</a>, which I kept under my hat for all that time. I was greatly amused by <a href="https://simont.livejournal.com/">👤simont</a>'s <a href="http://simont.livejournal.com/198405.html?thread=1480709#t1480709&style=mine">recent comment</a> advocating something very similar, though I couldn't follow up until today :-)
