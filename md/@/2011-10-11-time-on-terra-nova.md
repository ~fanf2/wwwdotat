---
format: html
lj:
  anum: 0
  can_comment: 1
  ditemid: 116480
  event_timestamp: 1318364040
  eventtime: "2011-10-11T20:14:00Z"
  itemid: 455
  logtime: "2011-10-11T19:15:06Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 6
  url: "https://fanf.livejournal.com/116480.html"
title: Time on Terra Nova
...

<p>There is a new sf series on Fox in the US called "Terra Nova". The
premise is that the protagonists have travelled back 85 million years
in time from 2149 to escape environmental collapse. Back then in the
time of the dinosaurs the length of the day was one or two percent
shorter than it is today. This led <a href="http://six.pairlist.net/pipermail/leapsecs/2011-October/003311.html">Daniel
Tobias to ask the leapsecs mailing list</a> how time should be handled
by the colonists. A Mars colony would also have to answer a similar
question, because they would have to cope with a day that is a couple
of percent longer.</p>

<p>The problem is that our base unit of time does not conveniently
divide the length of the day. This is inevitable however we choose to
fix our units, because the length of the day is not constant.
Travellers to other times or planets have to deal with an exaggerated
version of the problem, but it's also true on present-day Earth. The
length of the second is based on the length of the day over 100 years
ago, and it is now off by about 10<sup>-8</sup>. As well as tidal
slowing, there are also periodic and random variations in the length
of day, which also happen to be on the order of 10<sup>-8</sup>.</p>

<p>There are about five ways of dealing with this problem which I'll
divide into two and a half categories..<p>

<h3>Digital schemes</h3>

<p>When we're dividing up longer time periods into days we use
calendars. We can also view schemes for dividing days into seconds as
simple calendars.</p>

<h4>Observational calendars</h4>

<p>The simplest calendars are based on observation of astronomical
phenomena. For instance in the Islamic calendar, when the new moon
occurs the day counter is reset and the month counter is incremented.</p>

<p>Our current system of leap seconds in UTC is essentially an
observational calendar, where what is being observed is the difference
between mean solar time (aka UT1) and atomic time. Leap seconds are
inserted to keep this difference less than 0.9 seconds.</p>

<p>A time or space colony could use a similar system, though their day
is unlikely to be close to a round number of seconds in length, so
they will probably need to switch back and forth frequently between
short and long days. The disadvantage of observational calendars is
that they are not predictable, so it is not possible to schedule
events in the future with any precision.</p>

<h4>Arithmetical calendars</h4>

<p>If you have enough astronomical sophistication to measure periods
of rotation and revolution accurately, you can set up a calendar with
fixed arithmetic rules. For example, the Julian and Gregorian
calendars.</p>

<p>There has been a lengthy discussion over the last ten years on <a href="http://www.cl.cam.ac.uk/~mgk25/time/metrologia-leapsecond.pdf">the
possible discontinuation of leap seconds</a>. This would turn UTC into
a very simple arithmetic calendar.</p>

<p>Arithmetic calendars tend to drift out of sync, either because of
errors in their initial setup, or because the relevant periods are no
longer what they were. Reforming a calendar to fix it is incredibly
painful - think of the 350 year transition period required by the
Gregorian reform.</p>

<p>There is a work-around available when you are dealing with a
mis-match between the nominal length of day and the actual length of
day, which is not available for normal calendars. Provided the
difference is small enough, less than about 10<sup>-5</sup>, you can
accommodate mismatches by adjusting timezone boundaries. This is easy
to cope with if your timezone system is already handling random
political fluctuations, and will probably happen without the need for
any central co-ordination.</p>

<p>The difficulty with this scheme is that time of day is not a good
approximation of planetary angle of rotation relative to the sun, so
astronomical and navigational systems will need a source of UTC-UT1
data (aka DUT1).</p>

<h3>Fractional schemes</h3>

<p>In Kim Stanley Robinson's Mars trilogy the colonists stop the
clocks between 24:00 and 00:00 to allow for the extra forty minutes in
a Martian day beyond 24 hours. (I presume they don't actually stop the
clocks inside their support systems since they still need useful
telemetry logging during this "timeslip", amongst other things.) There
is a similar arrangement in David Weber's Honor Harrington books.</p>

<p>You could perhaps allow for a partial second at the end of each
day, to make the nominal length of day exactly match the actual length
of day. The disadvantage is this would cause an awkward glitch in time
and frequency reference broadcasts.</p>

<p><i>ETA:</i> The fractional second idea is another trick that time-of-day
calendrical systems can use which more common calendars can't, because
the second is an artificial unit of time not a measurement of the position of a
celestial body.</p>

<h3>Analogue schemes</h3>

<p>Instead of having a variable number of fixed-length seconds in a
day, we can have a fixed number of variable-length seconds in a day.
There are a couple of ways of doing this.</p>

<h4>Rubber seconds</h4>

<p>In the 1960s, time and frequency reference broadcasts were matched
to the length of day using a combination of frequency adjustments and
occasional jumps of 0.1 or 0.2 seconds. This allowed them to track UT2
more closely than modern UTC tracks UT1.</p>

<p>However it had the disadvantage of requiring difficult adjustments
to the broadcast equipment make the frequency changes, and it made it
more difficult for users of the broadcasts to obtain a precise
reference frequency. So it was abandoned in favour of the simpler UTC
scheme.</p>

<p>A modern variant of this
is <a href="http://www.cl.cam.ac.uk/~mgk25/time/utc-sls/">smoothed
leap seconds</a>
or <a href="http://googleblog.blogspot.com/2011/09/time-technology-and-leaping-seconds.html">leap
smear</a>, where rubber seconds are used temporarily to avoid glitches
caused by leap seconds.</p>

<h4>Two timescales</h4>

<p>All the above schemes start with a timescale based on seconds, and
try to accommodate the variable length of days within this timescale
(except for rubber seconds which are the other way round). Instead of
trying to reconcile the irreconcilable, we could instead work with two
separate timescales.</p>

<p>For precise time and frequency applications, establish an atomic
timescale that is as stable as possible. It might be sensible to use a
different base unit, say the <a href="http://en.wikipedia.org/wiki/Planck_time">Planck time</a>, or
rename the atomic second to say the <a href="http://en.wikipedia.org/wiki/Louis_Essen">essen</a>, in order to
avoid confusion with subdivisions of the day. Instants in this
timescale should be labelled as a count of seconds since an epoch, not
in YYYY-MM-DD HH:MM:SS form to avoid confusion with time of day.</p>

<p>For civil time of day, use UT1. All calculations involving civil
time should be done inside this system, ignoring its relationship to
atomic time. This timescale is not suitable for high precision
applications, since there's an inherent instability of about
10<sup>-8</sup>. It retains all the properties of pre-atomic time:
fixed number of seconds per day and synchronization with Earth
rotation.</p>

<p>Time and frequency broadcasts should be based on the atomic
timescale. In order to obtain civil time, these broadcasts should
include the atomic time when the current civil day started, and its
length in atomic seconds (and possibly also the current rate of change
of the length of day).</p>

<p>In terms of the POSIX <a href="http://pubs.opengroup.org/onlinepubs/9699919799/functions/clock_gettime.html">clock_gettime()</a>
interface, the atomic timescale roughly corresponds to
CLOCK_MONOTONIC, and civil time corresponds to CLOCK_REALTIME i.e.
time_t seconds since the epoch.</p>

<h3>Back in the real world</h3>

<p>I think the two timescales arrangement is the best way to model
what is actually going on. However it is difficult to see how it could
be deployed. It requires us to separate out the high-precision time
and civil time functions of a lot of critical systems, such as the MSF
/ DCF77 / WWV / etc. time signals, GPS and other navigation systems,
and NTP.</p>

<p>We should find out within the next year what will happen to leap
seconds. I think time geeks underestimate the problems they cause by
breaking deeply embedded cultural assumptions about time. The notation
we use for time of day is many thousands of years old, and changing it
is possibly a reform of Gregorian scope. So I tend to be in favour of
abolishing them even though it'll cost a lot for astronomers to fix.
Cheaper than fixing POSIX time, though.</p>

<p>(Previously: <a href="http://fanf.livejournal.com/104586.html">iCalendar is wrong</a>.)</p>
