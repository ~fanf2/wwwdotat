---
format: html
lj:
  anum: 78
  can_comment: 1
  ditemid: 108878
  event_timestamp: 1285241700
  eventtime: "2010-09-23T11:35:00Z"
  itemid: 425
  logtime: "2010-09-23T11:35:11Z"
  props:
    personifi_tags: "28:27,17:5,18:5,8:16,23:27,16:5,1:16,3:5,19:16,9:72,nterms:yes"
  reply_count: 14
  url: "https://fanf.livejournal.com/108878.html"
title: The University and College Union are spammers
...

<p>The <a href="http://www.uss.co.uk">Universities Superannuation Scheme</a> is currently in the process of changing its terms and conditions from a final salary pension scheme to a career average scheme. Unsurprisingly the UCU are campaigning against this.</p>

<p>The last two nights they have been spamming university staff with a campaign mailshot. (They tried to send us 3450 messages last night and 650 the previous night.) The mailing list they are using is VERY dodgy. At our site more than 9% of the addresses are invalid, and 1% of the addresses are role addresses that should not be receiving the kind of confidential personal information that a union might send to a member.</p>
