---
dw:
  anum: 107
  eventtime: "2005-04-04T10:05:00Z"
  itemid: 138
  logtime: "2005-04-04T10:59:22Z"
  props:
    commentalter: 1491292321
    import_source: livejournal.com/fanf/35713
    interface: flat
    opt_backdated: 1
    picture_keyword: weather
    picture_mapid: 5
  url: "https://fanf.dreamwidth.org/35435.html"
format: casual
lj:
  anum: 129
  can_comment: 1
  ditemid: 35713
  event_timestamp: 1112609100
  eventtime: "2005-04-04T10:05:00Z"
  itemid: 139
  logtime: "2005-04-04T10:59:22Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/35713.html"
title: ""
...

So this morning I had a remarkably sensible non-lucid (or semi-lucid) dream.

I was remembering a mathematical (or perhaps cryptographic) joke involving two entities, labelled A and 1. (Or maybe it was A and 2 or 1 and B; my memory is vague when asleep at 6 in the morning.) Shortly after I first encountered this joke I found out about typesetting software that auto-numbers sections and lists such as: a. first item; a. second item; a. third item; which is fixed up to a. b. c. You can also provide a style hint such as 1. for numerical lists or A. for capitalized lists or i. or I. for roman numerals. (I think I've seen <a href="https://jwz.livejournal.com/">👤jwz</a> writing lists in this style but without the fix up.)

I dreamt talking about this to <a href="https://rmc28.livejournal.com/">👤rmc28</a> and generalizing it to a list of things that are first in some series (aleph, alpha, red, etc.), to which dream-Rachel responded "Oh, that's Newton's list." (Actually, s/list/set/g. I think the Newton part came from me reading lots of fiction about the English enlightenment recently.)

After waking it occurred to me that the set wouldn't be all that interesting, though you could argue endlessly about the choice of sequences from which to take first items for inclusion. The Guinness Book of Records might be a good source, but it's far too heavy on people. The sequence of British Prime Ministers is worth including, but what about people who ran a mile in less than 4 minutes? Who came after Roger Bannister? Lists of abstractions are much more fun, mainly because of the obscurity.

Maybe filling in subsequent items from the lists would make a good source of quiz questions :-)
