---
dw:
  anum: 160
  eventtime: "2006-02-06T14:02:00Z"
  itemid: 195
  logtime: "2006-02-06T14:09:58Z"
  props:
    commentalter: 1491292329
    import_source: livejournal.com/fanf/50257
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/50080.html"
format: casual
lj:
  anum: 81
  can_comment: 1
  ditemid: 50257
  event_timestamp: 1139234520
  eventtime: "2006-02-06T14:02:00Z"
  itemid: 196
  logtime: "2006-02-06T14:09:58Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/50257.html"
title: Virus names
...

Virus naming is generally a matter of consensus between the AV vendors, but occasionally that breaks down. A good example is "blackworm" aka "blackmal" aka "blueworm" aka "mywife" aka "nyxem". Our email AV system keys off the virus name to decide whether to delete a message or mangle it. (Sadly our current system can't reject messages at SMTP time.) This depends on us getting a reasonably unique name from the virus scanners, so that we treat messages appropriately. Sadly at the moment there's something nasty going around which McAfee is calling "the Generic Malware.a!zip trojan" and ClamAV is calling "Worm.VB-9". Can I have a proper name please so I can delete it and stop irritating people with mangled junk?
