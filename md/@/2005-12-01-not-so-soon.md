---
dw:
  anum: 165
  eventtime: "2005-12-01T18:26:00Z"
  itemid: 165
  logtime: "2005-12-01T19:02:58Z"
  props:
    commentalter: 1491292326
    import_source: livejournal.com/fanf/42585
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/42405.html"
format: casual
lj:
  anum: 89
  can_comment: 1
  ditemid: 42585
  event_timestamp: 1133461560
  eventtime: "2005-12-01T18:26:00Z"
  itemid: 166
  logtime: "2005-12-01T19:02:58Z"
  props: {}
  reply_count: 6
  url: "https://fanf.livejournal.com/42585.html"
title: Not so soon...
...

So, while I was away <a href="http://www.livejournal.com/users/rmc28/145840.html">seeing my dad and going to EuroBSDcon in Basel</a>, my boss Bob presented <a href="http://www.livejournal.com/users/fanf/42443.html">my Jabber proposal</a> to the senior management team. It was sent back to me for further work.

There were a few tactical errors, I think. The original reason for writing the proposal was that I had got to the stage of needing real DNS records in order to do some realistic testing. Bob said that I couldn't have them without SMT approval, hence the proposal write-up. (For good reasons there is a bit of a hurdle for the creation of domains immediately under <code>cam.ac.uk</code>.)

What I failed to emphasize to Bob or in my proposal, was that there had been no discussion of requirements or of development/release milestones beyond what I had sketched out for myself. So I was perfectly happy for the SMT to say "do things in this order please, and we won't formally announce until X, Y, and Z are done". Our Glorious Leader demanded a web interface with <a href="http://raven.cam.ac.uk/">Raven</a> authentication, and our Deputy Director thought that we'd need an MSN gateway to maximize take-up. There was also some discussion about how we could guage that the service was successful. This was all fine by me.

However there was much more of an argument than was warranted, especially over the Raven requirement. This is irritating because I'd prefer to make progress than fight political battles over unimportant details. Maybe we'll be successful with a revised proposal at the SMT next week.

The Raven requirement does have some slightly awkward implications which are worth noting.

The security model for Raven requires that Raven passwords are only typed into Raven, which means that they cannot be used by native Jabber clients. So my original plan to use the Hermes password file will still go ahead. But we've been told that any web-based Jabber client must use Raven authentication, which means that Jabber users must use different passwords in different contexts, which is a bit ugly. It's also different from the way Hermes passwords are used for email - though OGL says that if our webmail service was being done now it would also use Raven.

It implies some customization of the software, rather than just installation and configuration of the standard version. The Jabber HTTP binding uses clever AJAX techniques to get much less latency and bandwidth than a simple polling solution, and the protocol tunnels most of the Jabber protocol through to the web client. So I may have to make modifications to both the web client part as well as the web server part. Fitting an AJAX application into the Raven model may be tricky too.

Then there's the problem of turning Raven authentication at the web server into SASL authentication at the Jabber server. The easiest way to do this is for the Jabber server to trust the web server, and for them to use SASL EXTERNAL authentication. The EXTERNAL mechanism just states the username, and the server uses some implicit context to authenticate it. It is designed for lifting TLS client authentication up to the SASL layer, but it also works for turning trust defined by the system administrator into a standardized protocol. This kind of trust is not great for security, so I'd prefer something better; we'll see if Jon Warbrick, the author of Raven, has any bright ideas.
