---
dw:
  anum: 24
  eventtime: "2007-09-05T15:50:00Z"
  itemid: 302
  logtime: "2007-09-05T15:53:37Z"
  props:
    commentalter: 1491292344
    import_source: livejournal.com/fanf/77783
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/77336.html"
format: casual
lj:
  anum: 215
  can_comment: 1
  ditemid: 77783
  event_timestamp: 1189007400
  eventtime: "2007-09-05T15:50:00Z"
  itemid: 303
  logtime: "2007-09-05T15:53:37Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/77783.html"
title: Even more jobs
...

<b>2 Software Development Specialists</b>

The University Computing Service is forming a small team of software developers to work on a variety of projects. We are recruiting two people to work in this group.

The successful applicants will develop a variety of new applications with the majority of projects being developed in a modern web application framework based on Java, with associated unit tests, build tools and documentation. There will also be enhancement and customisation work on existing applications written in a variety of languages. The successful candidates will be able to adapt rapidly to new environments and technologies. Most of the development will be done on Linux platforms with occasional work on Mac OS X or Windows servers.

Much of the development will use open source software and improvements and modifications to the software will be fed back to the international development teams responsible for these pieces of software.

Candidates must have:

    * A good degree in a numerate discipline, or equivalent experience.
    * Substantial experience of software development environments.
    * Substantial experience in web application development.
    * Good knowledge of Java, preferably in a web context.
    * Good knowledge of at least one interpreted language.
    * Good communication skills, both verbal and written.

These are permanent appointments at Computer Officer Grade 8, subject to a nine months satisfactory probation period, on a salary scale of £30,012-£40,335 with initial salary depending on experience.   <a href="http://www.cam.ac.uk/cs/jobs/further/softwaredev.html">More information can be found on the CS's web site.</a> The closing date for applications is Friday 5 October 2007. It is intended that interviews will be held during the week beginning Monday 15 October 2007.
