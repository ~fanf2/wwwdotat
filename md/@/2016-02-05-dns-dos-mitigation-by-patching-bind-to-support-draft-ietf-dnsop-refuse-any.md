---
dw:
  anum: 175
  eventtime: "2016-02-05T21:48:00Z"
  itemid: 443
  logtime: "2016-02-05T21:48:46Z"
  props:
    commentalter: 1491292431
    hasscreened: 1
    import_source: livejournal.com/fanf/140566
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/113583.html"
format: html
lj:
  anum: 22
  can_comment: 1
  ditemid: 140566
  event_timestamp: 1454708880
  eventtime: "2016-02-05T21:48:00Z"
  itemid: 549
  logtime: "2016-02-05T21:48:46Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/140566.html"
title: DNS DoS mitigation by patching BIND to support draft-ietf-dnsop-refuse-any
...

<p>Last weekend one of our authoritative name servers
(<code>authdns1.csx.cam.ac.uk</code>) suffered a series of DoS attacks which made
it rather unhappy. Over the last week I have developed a patch for
BIND to make it handle these attacks better.</p>

<h2>The attack traffic</h2>

<p>On <code>authdns1</code> we provide off-site secondary name service to a number
of other universities and academic institutions; the attack targeted
<code>imperial.ac.uk</code>.</p>

<p>For years we have had a number of defence mechanisms on our DNS
servers. The main one is <a href="http://www.redbarn.org/dns/ratelimits">response rate
limiting</a>, which is designed to
reduce the damage done by DNS reflection / amplification attacks.</p>

<p>However, our recent attacks were different. Like most reflection /
amplification attacks, we were getting a lot of QTYPE=ANY queries, but
unlike reflection / amplification attacks these were not spoofed, but
rather were coming to us from a lot of recursive DNS servers. (A large
part of the volume came from Google Public DNS; I suspect that is just
because of their size and popularity.)</p>

<p>My guess is that it <em>was</em> a reflection / amplification attack, but we
were not being used as the amplifier; instead, a lot of <a href="http://openresolverproject.net">open
resolvers</a> were being used to amplify,
and they in turn were making queries upstream to us. (Consumer routers
are often open resolvers, but usually forward to their ISP's resolvers
or to public resolvers such as Google's, and those query us in turn.)</p>

<h2>What made it worse</h2>

<p>Because from our point of view the queries were coming from real
resolvers, RRL was completely ineffective. But some other
configuration settings made the attacks cause more damage than they
might otherwise have done.</p>

<p>I have configured our authoritative servers to avoid sending large UDP
packets which get fragmented at the IP layer. <a href="http://www.cs.ru.nl/~rijswijk/pub/ieee-commag-dnssec-2014.pdf">IP fragments often get
dropped and this can cause problems with DNS
resolution.</a>
So I have set</p>

<pre><code>    max-udp-size 1420;
    minimal-responses yes;
</code></pre>

<p>The first setting limits the size of outgoing UDP responses to an MTU
which is very likely to work. (The ethernet MTU minus some slop for
tunnels.) The second setting reduces the amount of information that
the server tries to put in the packet, so that it is less likely to be
truncated because of the small UDP size limit, so that clients do not
have to retry over TCP.</p>

<p>This works OK for normal queries; for instance a <code>cam.ac.uk IN MX</code>
query gets a svelte 216 byte response from our authoritative servers
but a chubby 2047 byte response from our recursive servers which do
not have these settings.</p>

<p>But ANY queries blow straight past the UDP size limit: the attack
queries for <code>imperial.ac.uk IN ANY</code> got obese 3930 byte responses.</p>

<p>The effect was that the recursive clients retried their queries over
TCP, and consumed the server's entire TCP connection quota. (Sadly
BIND's TCP handling is not up to the standard of good web servers, so
it's quite easy to nadger it in this way.)</p>

<h2>draft-ietf-dnsop-refuse-any</h2>

<p>We might have coped a lot better if we could have served all the
attack traffic over UDP. Fortunately there was some pertinent
discussion in the <a href="https://datatracker.ietf.org/wg/dnsop/">IETF DNSOP working
group</a> in <a href="https://mailarchive.ietf.org/arch/search/?start_date=2015-03-01&amp;end_date=2015-03-31&amp;email_list=dnsop&amp;q=any">March last
year</a>
which resulted in
<a href="https://tools.ietf.org/html/draft-dnsop-refuse-any">draft-ietf-dnsop-refuse-any</a>,
"providing minimal-sized responses to DNS queries with QTYPE=ANY".</p>

<p>This document was instigated by
<a href="https://www.cloudflare.com/dns/">Cloudflare</a>, who have a DNS server
architecture which makes it unusually difficult to produce traditional
comprehensive responses to ANY queries. Their approach is instead to
send just one synthetic record in response, like</p>

<pre><code>    cloudflare.net.  HINFO  ( "Please stop asking for ANY"
                              "See draft-jabley-dnsop-refuse-any" )
</code></pre>

<p>In the discussion, Evan Hunt (one of the BIND developers) suggested an
alternative approach suitable for traditional name servers. They can
reply to an ANY query by picking one arbitrary RRset to put in the
answer, instead of all of the RRsets they have to hand.</p>

<p>The draft says you can use either of these approaches. They both allow
an authoritative server to make the recursive server go away happy
that it got an answer, and without breaking odd applications like
qmail that foolishly rely on ANY queries.</p>

<p>I did a few small experiments at the time to demonstrate that it
really would work OK in the real world (unlike some of the earlier
proposals) and they are both pretty neat solutions (unlike some of the
earlier proposals).</p>

<h2>Attack mitigation</h2>

<p>So draft-ietf-dnsop-refuse-any is an excellent way to reduce the
damage caused by the attacks, since it allows us to return small UDP
responses which reduce the downstream amplification and avoid pushing
the intermediate recursive servers on to TCP. But BIND did not have
this feature.</p>

<p>I did a very quick hack on Tuesday to strip down ANY responses, and I
deployed it to our authoritative DNS servers on Wednesday morning for
swift mitigation. But it was immediately clear that I had put my patch
in completely the wrong part of BIND, so it would need substantial
re-working before it could be more widely useful.</p>

<p>I managed to get back to the patch on Thursday. The right place to put
the logic was in the fearsome
<a href="https://source.isc.org/cgi-bin/gitweb.cgi/bind9.git/blob/bin/named/query.c?h=3b0ef962#l6422"><code>query_find()</code></a>
which is the top-level query handling function and nearly 2400 lines
long! I finished the first draft of the revised patch that afternoon
(using none of the code I wrote on Tuesday), and I spent Friday
afternoon debugging and improving it.</p>

<p>The result is <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/bind9.git/commitdiff/f8c420dd8e">this patch which adds a <code>minimal-qtype-any</code>
option</a>.
I'm currently running it on my toy nameserver, and I plan to deploy it
to our production servers next week to replace the rough hack.</p>

<p>I have submitted the patch to the ISC; hopefully something like it will
be included in a future version of BIND. And it prompted <a href="http://www.ietf.org/mail-archive/web/dnsop/current/msg16561.html">a couple of questions about draft-ietf-dnsop-refuse-any</a> that I posted to the DNSOP working group mailing list.</p>
