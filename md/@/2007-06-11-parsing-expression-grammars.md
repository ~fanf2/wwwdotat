---
dw:
  anum: 121
  eventtime: "2007-06-11T09:56:00Z"
  itemid: 288
  logtime: "2007-06-11T09:39:11Z"
  props:
    commentalter: 1491292360
    import_source: livejournal.com/fanf/74193
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/73849.html"
format: casual
lj:
  anum: 209
  can_comment: 1
  ditemid: 74193
  event_timestamp: 1181555760
  eventtime: "2007-06-11T09:56:00Z"
  itemid: 289
  logtime: "2007-06-11T09:39:11Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 10
  url: "https://fanf.livejournal.com/74193.html"
title: Parsing expression grammars
...

<a href="http://pdos.csail.mit.edu/~baford/packrat/">PEGs</a> are an interesting new formalism for grammars. Unlike most older formalisms, which are based on Chomsky's generative grammars, their starting point is <em>recognizing</em> strings that are in a language, not <em>generating</em> them. As such they are a closer match to what we usually want a grammar for. The practical effect of this is that they naturally avoid common ambiguities without external rules, such as C's if/else ambiguity or the various rules about greediness imposed on regexes (e.g. perl's matching rules versus POSIX's longest-leftmost rule, discussed in <a href="http://regex.info/">Friedl's book</a>). Even though PEGs can recognize some non-context-free languages (e.g. a<sup>n</sup>b<sup>n</sup>c<sup>n</sup>) they can be matched in linear time using a <a href="http://pdos.csail.mit.edu/~baford/packrat/icfp02">packrat parser</a> (which can be implemented very beautifully in Haskell).

<a href="http://pdos.csail.mit.edu/~baford/packrat/popl04">Bryan Ford's 2004 POPL paper</a> establishes the formal foundations of PEGs and defines a concrete syntax for them, fairly similar to <a href="http://www.ietf.org/rfc/rfc4234.txt">ABNF</a>. The key differences are: the choice operator is <em>ordered</em> (prefers to match its left-hand sub-expression); repetition operators are maximally greedy and don't backtrack (so the second <tt>a</tt> in <tt>a*a</tt> can never match); and it includes positive and negative lookahead assertions of the form <tt>&a</tt> and <tt>!a</tt> (like <tt>(?=a)</tt> and <tt>(?!a)</tt> in perl).

It occurs to me that there is a useful analogy hidden in here, that would be made more obvious with a little change in syntax. Instead of <tt>a / b</tt> write <tt>a || b</tt>, and instead of <tt>&a b</tt> write <tt>a && b</tt>.  With || and && I am referring to C's short-cutting "orelse" and "andalso" boolean operators - or rather the more liberal versions that can return more than just a boolean, since a PEG returns the amount of input matched when it succeeds. This immediately suggests some new identities on grammars based on De Morgan's laws, e.g. <tt>!a || !b === !(a && b)</tt>. Note that <tt>!!a =/= a</tt> because the former never consumes any input, so not all of De Morgan's laws work with PEGs. This also suggests how to choose the operators to overload when writing a PEG parser combinator library for C++ (which has a much wider range of possibilities than <a href="http://www.inf.puc-rio.br/~roberto/lpeg.html">Lua</a>).
