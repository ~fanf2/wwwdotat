---
dw:
  anum: 109
  eventtime: "2008-09-22T23:03:00Z"
  itemid: 363
  logtime: "2008-09-22T22:55:48Z"
  props:
    commentalter: 1491292361
    import_source: livejournal.com/fanf/94043
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/93037.html"
format: html
lj:
  anum: 91
  can_comment: 1
  ditemid: 94043
  event_timestamp: 1222124580
  eventtime: "2008-09-22T23:03:00Z"
  itemid: 367
  logtime: "2008-09-22T22:55:48Z"
  props: {}
  reply_count: 13
  url: "https://fanf.livejournal.com/94043.html"
title: The Corpus Christi Chronophage Clock
...

<p>I have been admiring the new clock on <a href="http://maps.google.com/?ll=52.203705,0.117624&z=21">the corner of Trumpington St and Bene't St</a>. Instead of having hands, the clock has three concentric rings hidden behind its face. The outer ring marks seconds, the middle ring marks minutes, and the inner ring marks hours. Each ring is driven by the next outer ring via a gearing mechanism that makes the rings tick from one mark to the next. It is fairly normal for the second hand's motion to be quantized in this manner, but more unusual for the minute and hour hands to tick like this too.</p>

<p>Each ring sits in front of a circle of 60 blue LEDs, and the face has three circles of 60 slots which would allow the LEDs to shine through if the rings weren't in the way. The position of each disk is indicated by a slot which allows one LED to shine through the disk and the face to be seen by the viewers.</p>

<p>In fact, each ring has 61 slots, one of which is usually aligned with a slot in the face to indicate the time, and 60 of which are normally out of alignment. When a ring ticks from one position to the next, its 60 normally-unaligned slots line up with the face in turn before the normally-aligned slot lines up with the next slot in the face. This has the effect of making it look like the ring has flown around 366&deg; whereas it only moved by 6&deg;</p>

<p>I have made <a href="http://dotat.at/random/clock.html">a little animation</a> which shows how this works. It ticks once every 4 seconds and it only has 12 slots on the face and 13 on the disk, so that you can clearly see the mechanism behind the effect. I have drawn the face's slots in the outer circle and the ring's slots in the inner circle. It uses the new-fangled &lt;canvas&gt; HTML element, so you may need to upgrade your browser to view it.</p>

<p>There is <a href="http://www.admin.cam.ac.uk/offices/communications/1522.html">a video of the clock</a> where some of its features are described by the designer, John Taylor, who made his fortune from his invention of the kettle thermostat.</p>
