---
dw:
  anum: 151
  eventtime: "2018-03-28T10:11:00Z"
  itemid: 489
  logtime: "2018-03-28T09:12:08Z"
  props:
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/125335.html"
format: md
lj:
  anum: 147
  can_comment: 1
  ditemid: 152467
  event_timestamp: 1522231920
  eventtime: "2018-03-28T10:12:00Z"
  itemid: 595
  logtime: "2018-03-28T09:12:50Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/152467.html"
...

My comments on the ICANN root DNSSEC key rollover
=================================================

ICANN are currently requesting
[public comments on restarting the root DNSSEC KSK rollover](https://www.icann.org/public-comments/ksk-rollover-restart-2018-02-01-en).
I thought I didn't have anything particularly useful to say, but when
I was asked to contribute a comment I found that I had more opinions
than I realised! So I sent in some thoughts which you can see on
[the KSK public comments list](https://mm.icann.org/pipermail/comments-ksk-rollover-restart-01feb18/2018q1/thread.html)
and which are duplicated below.

Please go ahead and roll the root KSK as planned on the 11th October 2018.
--------------------------------------------------------------------------

The ongoing work on trust anchor telemetry and KSK sentinel might be
useful for making an informed decision, but there is the risk of
getting into the trap that there is never enough data. It would be bad
to delay again and again for just one more experiment. KSK sentinel
might provide better data than trust anchor telemetry, but I fear it
is too late for this rollover, and it may never be deployed by the
problem sites that cause concern. So maybe the quest for data is not
absolutely crucial.

I increasingly think RFC 5011 is insufficient and not actually very
helpful. It was implemented rather late and it seems many deployments
don’t use it at all. It only solves the easy problem of online
rollovers: it doesn’t help with bootstrapping or recovery.

RFC 7958 moves the problem around rather than solving it. It suggests
that we can treat personal PGP keys or self-signed X.509 keys with
unclear provenance as more trusted than the root key, with all its
elaborate documentation, protections, and ceremonial. It adds more
points of failure, whereas a proper solution should disperse trust.

I think the best short term option is to put more emphasis on using
software updates for distributing the root trust anchors. The software
is already trusted, so using it for key distribution doesn't introduce
a new point of failure. Most vendors have a plausible security update
process. Software updates can solve the same problems as RFC 5011, in
a straight-forward and familiar way. Across the ecosystem as a whole
it disperses trust amongst the vendors.

Longer term I would like a mechanism that addresses bootstrap and
recovery (because you can’t get your software update without DNS)
but that is not doable before the rollover later this year.
