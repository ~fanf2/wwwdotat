---
dw:
  anum: 4
  eventtime: "2005-12-07T19:56:00Z"
  itemid: 168
  logtime: "2005-12-07T20:07:35Z"
  props:
    commentalter: 1491292330
    import_source: livejournal.com/fanf/43399
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/43012.html"
format: casual
lj:
  anum: 135
  can_comment: 1
  ditemid: 43399
  event_timestamp: 1133985360
  eventtime: "2005-12-07T19:56:00Z"
  itemid: 169
  logtime: "2005-12-07T20:07:35Z"
  props: {}
  reply_count: 5
  url: "https://fanf.livejournal.com/43399.html"
title: Meta Matters
...

Today I have been doing some project support work for the Chat service. (Its domain name is chat.cam.ac.uk, and people might not know what Jabber is, so "the Chat service" is how we will refer to it, unless anyone comes up with a better idea.)

I have a role address for support matters, <tt>chat-support@ucs.cam.ac.uk</tt>.

I have <a href="https://lists.cam.ac.uk/mailman/listinfo/cs-chat-users">a mailing list for interested people</a>. That link only works for Cambridge people, but anyone can send a subscribe message to <tt>cs-chat-users-request@lists.cam.ac.uk</tt> (though I doubt it'll be of much interest to outsiders).

I have <a href="https://wiki.cam.ac.uk/chat">a wiki</a> kindly set up for me by <a href="https://furrfu.livejournal.com/">👤furrfu</a> (again the link is restricted to Cambridge people).

I have a logo!
<img src="https://fanf2.user.srcf.net/hermes/doc/jabber/logo.png">
