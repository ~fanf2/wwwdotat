---
dw:
  anum: 158
  eventtime: "2018-03-07T23:13:00Z"
  itemid: 481
  logtime: "2018-03-07T23:14:37Z"
  props:
    commentalter: 1546551146
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/123294.html"
format: html
lj:
  anum: 178
  can_comment: 1
  ditemid: 150450
  event_timestamp: 1520464500
  eventtime: "2018-03-07T23:15:00Z"
  itemid: 587
  logtime: "2018-03-07T23:16:20Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/150450.html"
title: "Secure Let's Encrypt ACME DNS challenges with BIND"
...

<p>Last week, the EFF wrote about
<a href="https://www.eff.org/deeplinks/2018/02/technical-deep-dive-securing-automation-acme-dns-challenge-validation">how to safely allow web servers to update ACME DNS challenges</a>.
Whereas non-wildcard <a href="https://letsencrypt.org">Let's Encrypt</a>
certificates can be authorized by the web server itself, wildcard
certs require the ACME client to put the challenge token in the DNS.</p>

<p>The EFF article outlined a few generic DNS workarounds (which I won't
describe here), and concluded by suggesting delegating the
<code>_acme-challenge</code> subdomain to a special ACME-DNS server.</p>

<p>But, if your domain is hosted with BIND, it's much easier.</p>

<p>First, you need to generate a TSIG key (a shared secret) which will be
used by your ACME client to update the DNS. The <code>tsig-keygen</code> command
takes the name of the key as its argument; give the key the same name
as the domain it will be able to update. I write TSIG keys to files
named <code>tsig.&lt;keyname&gt;</code> so I know what they are.</p>

<pre><code>    $ tsig-keygen _acme-challenge.dotat.at \
        &gt;tsig._acme-challenge.dotat.at
</code></pre>

<p>This file needs to be copied to the ACME client - I won't go into the
details of how to get that part working.</p>

<p>The key needs to be included in the primary BIND server config:</p>

<pre><code>    include "tsig._acme-challenge.dotat.at";
</code></pre>

<p>You also need to modify your zone's dynamic update configuration. My
zones typically have:</p>

<pre><code>    update-policy local;
</code></pre>

<p>The new configuration needs both the expanded form of <code>local</code> plus the
<code>_acme-challenge</code> permissions, like this:</p>

<pre><code>    update-policy {
        grant local-ddns zonesub any;
        grant _acme-challenge.dotat.at self _acme-challenge.dotat.at TXT;
    };
</code></pre>

<p>You can test that the key has restricted permissions using <code>nsupdate</code>.
The following transcript shows that this ACME TSIG key can only add
and delete TXT records at the <code>_acme-challenge</code> subdomain - it isn't
able to update TXT records at other names, and isn't able to update
non-TXT records at the <code>_acme-challenge</code> subdomain.</p>

<pre><code>    nsupdate -k tsig._acme-challenge.dotat.at
    &gt; add thing.dotat.at 3600 txt thing
    &gt; send
    update failed: REFUSED
    &gt; add _acme-challenge.dotat.at 3600 a 127.0.0.1
    &gt; send
    update failed: REFUSED
    &gt; add _acme-challenge.dotat.at 3600 txt thing
    &gt; send
    &gt; del _acme-challenge.dotat.at 3600 txt thing
    &gt; send
</code></pre>

<p>That's it!</p>
