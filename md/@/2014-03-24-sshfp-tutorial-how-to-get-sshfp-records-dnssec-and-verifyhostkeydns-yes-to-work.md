---
dw:
  anum: 205
  eventtime: "2014-03-24T19:18:00Z"
  itemid: 404
  logtime: "2014-03-24T19:19:04Z"
  props:
    commentalter: 1491292406
    import_source: livejournal.com/fanf/130577
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/103629.html"
format: html
lj:
  anum: 17
  can_comment: 1
  ditemid: 130577
  event_timestamp: 1395688680
  eventtime: "2014-03-24T19:18:00Z"
  itemid: 510
  logtime: "2014-03-24T19:19:04Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 1
  url: "https://fanf.livejournal.com/130577.html"
title: "SSHFP tutorial: how to get SSHFP records, DNSSEC, and VerifyHostKeyDNS=yes to work."
...

<p>One of the great promises of DNSSEC is to provide a new public key
infrastructure for authenticating Internet services. If you are a
relatively technical person you can try out this brave new future now
with <a href="http://openssh.com/">ssh</a>.
</p><p>There are a couple of advantages of getting SSHFP host
authentication to work. Firstly you get easier-to-use security, since
you longer need to rely on manual host authentication, or better
security for the brave or foolhardy who trust leap-of-faith
authentication. Secondly, it becomes feasible to do host key
rollovers, since you only need to update the DNS - the host&#39;s key is
no longer wired into thousands of <tt>known_hosts</tt> files. (You can
probably also get the latter benefit using ssh certificate
authentication, but why set up another PKI if you already have one?)
</p><p>In principle it should be easy to get this working but there are a
surprising number of traps and pitfalls. So in this article I am going
to try to explain all the whys and wherefores, which unfortunately
means it is going to be long, but I will try to make it easy to
navigate. In the initial version of this article I am just going to
describe what the software does by default, but I am happy to add
specific details and changes made by particular operating systems.
</p><p>The outline of what you need to do on the server is:
</p><ul>
<li>Sign your DNS zone. I will not cover that in this article.</li>
<li>Publish SSHFP records in the DNS</li>
</ul>

<p>The client side is more involved. There are two versions, depending
on whether ssh has been compiled to use ldns or not. Run <tt>ldd
$(which ssh)</tt> to see if it is linked with libldns.
</p><ul>
<li>Without ldns:<ul>
<li>Install a validating resolver (BIND or Unbound)</li>
<li>Configure the stub resolver <tt>/etc/resolv.conf</tt></li>
<li>Configure ssh</li>
</ul></li>
<li>With ldns:<ul>
<li>Install <tt>unbound-anchor</tt></li>
<li>Configure the stub resolver <tt>/etc/resolv.conf</tt></li>
<li>Configure ssh</li>
</ul></li>
</ul>


<h2>Publish SSHFP records in the DNS</h2>

<p>Generating SSHFP records is quite straightforward:
</p>
<pre>
    <b>demo:~#</b> cd /etc/ssh
    <b>demo:/etc/ssh#</b> ssh-keygen -r $(hostname)
    demo IN SSHFP 1 1 21da0404294d07b940a1df0e2d7c07116f1494f9
    demo IN SSHFP 1 2 3293d4c839bfbea1f2d79ab1b22f0c9e0adbdaeec80fa1c0879dcf084b72e206
    demo IN SSHFP 2 1 af673b7beddd724d68ce6b2bb8be733a4d073cc0
    demo IN SSHFP 2 2 953f24d775f64ff21f52f9cbcbad9e981303c7987a1474df59cbbc4a9af83f6b
    demo IN SSHFP 3 1 f8539cfa09247eb6821c645970b2aee2c5506a61
    demo IN SSHFP 3 2 9cf9ace240c8f8052f0a6a5df1dea4ed003c0f5ecb441fa2c863034fddd37dc9
</pre>


<p>Put these records in your zone file, or you can convert them into
an <tt>nsupdate</tt> script with a bit of seddery:
</p>
<pre>
    ssh-keygen -r $(hostname -f) |
        sed &#39;s/^/update add /;s/ IN / 3600 IN /;/ SSHFP . 1 /d;&#39;
</pre>


<p>The output of <tt>ssh-keygen -r</tt> includes hashes in both SHA1
and SHA256 format (the shorter and longer hashes). You can discard the
SHA1 hashes.
</p><p>It includes hashes for the different host key authentication algorithms:</p><ul>
<li>1: ssh-rsa</li>
<li>2: ssh-dss</li>
<li>3: ecdsa</li>
</ul>

<p>I believe <a href="http://tools.ietf.org/html/rfc6594">ecdsa covers
all three key sizes</a> which OpenSSH gives separate algorithm names:
ecdsa-sha2-nistp256, ecdsa-sha2-nistp384, ecdsa-sha2-nistp521</p>

<p>OpenSSH supports
other <a href="http://www.openssh.com/cgi-bin/man.cgi?query=ssh_config">host
key authentication algorithms</a>, but unfortunately they cannot be authenticated using SSHFP records because
they do not have algorithm numbers allocated.</p>

<p>The problem is actually worse than that, because most of the extra
algorithms are the same as the three listed above, but with added
support for certificate authentication. The ssh client is able to
convert certificate to plain key authentication, but
<a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=742513">a
bug in this fallback logic breaks SSHFP authentication</a>.
</p><p>So I recommend that if you want to use SSHFP authentication your
server should only have host keys of the three basic algorithms listed
above.</p>

<p><b>NOTE (added 26-Nov-2014):</b> if you are running OpenSSH-5, it does not support ECDSA SSHFP records. So if your servers need to support older clients, you might want to stick to just RSA and DSA host keys.</p>

<p>You are likely to have an SSHFP algorithm compatibility problem if you get the message: "Error calculating host key fingerprint."</p>

<p><b>NOTE (added 12-Mar-2015):</b> if you are running OpenSSH-6.7 or later, it has support for <a href="https://tools.ietf.org/html/rfc7479">Ed25519 SSHFP records</a> which use algorithm number 4.</p>

<h2>Install a validating resolver</h2>

<p>To be safe against active network interception attacks you need to
do DNSSEC validation on the same machine as your ssh client. If you
don&#39;t do this, you can still use SSHFP records to provide a marginal
safety improvement for leap-of-faith users. In this case I recommend
using <tt>VerifyHostKeyDNS=ask</tt> to reinforce to the user that they
ought to be doing proper manual host authentication.
</p><p>If ssh is not compiled to use ldns then you need to run a local
validating resolver, either BIND or unbound.</p>

<p>If ssh is compiled to use ldns, it can do its own validation, and
you do not need to install BIND or Unbound.</p>

<p>Run <tt>ldd $(which ssh)</tt> to see if it is linked with libldns.</p>

<h3>Install a validating resolver - BIND</h3>

<p>The following configuration will make <tt>named</tt> run as a local
validating recursive server. It just takes the defaults for
everything, apart from turning on validation. It automatically uses
BIND&#39;s built-in copy of the root trust anchor.
</p><p><b>/etc/named/named.conf</b></p>
<pre>
    options {
        dnssec-validation auto;
        dnssec-lookaside auto;
    };
</pre>


<h3>Install a validating resolver - Unbound</h3>

<p>Unbound comes with a utility <tt>unbound-anchor</tt> which sets up
the root trust anchor for use by the <tt>unbound</tt> daemon. You can
then configure <tt>unbound</tt> as follows, which takes the defaults
for everything apart from turning on validation using the trust anchor
managed by <tt>unbound-anchor</tt>.
</p><p><b>/etc/unbound/unbound.conf</b></p>
<pre>
    server:
        auto-trust-anchor-file: &quot;/var/lib/unbound/root.key&quot;
</pre>



<h3>Install a validating resolver - dnssec-trigger</h3>

<p>If your machine moves around a lot to dodgy WiFi hot spots and
hotel Internet connections, you may find that the nasty middleboxes
break your ability to validate DNSSEC. In that case you can
use <a href="http://www.nlnetlabs.nl/projects/dnssec-trigger/">dnssec-trigger</a>,
which is a wrapper around Unbound which knows how to update its
configuration when you connect to different networks, and which can
work around braindamaged DNS proxies.</p>


<h2>Configure the stub resolver - without ldns</h2>

<p>If ssh is compiled without ldns, you need to add the following line
to <tt>/etc/resolv.conf</tt>; beware your system&#39;s automatic resolver
configuration software, which might be difficult to persuade to leave
resolv.conf alone.
</p>
<pre>
    options edns0
</pre>

<p>For testing purposes you can add <tt>RES_OPTIONS=edns0</tt> to
ssh&#39;s environment.</p>

<p>On some systems (including Debian and Ubuntu), ssh is patched to
force EDNS0 on, so that you do not need to set this option. See the
section on RRSET_FORCE_EDNS0 below for further discussion.</p>


<h2>Configure the stub resolver - with ldns</h2>

<p>If ssh is compiled with ldns, you need to
run <tt>unbound-anchor</tt> to maintain a root trust anchor, and add
something like the following line to <tt>/etc/resolv.conf</tt>
</p>
<pre>
    anchor /var/lib/unbound/root.key
</pre>

<p>Run <tt>ldd $(which ssh)</tt> to see if it is linked with libldns.</p>


<h2>Configure ssh</h2>

<p>After you have done all of the above, you can add the following to
your ssh configuration, either <tt>/etc/ssh/ssh_config</tt>
or <tt>~/.ssh/config</tt>
</p>
<pre>
    VerifyHostKeyDNS yes
</pre>

<p>Then when you connect to a host for the first time, it should go
straight to the <tt>Password:</tt> prompt, without asking for manual
host authtentication.</p>

<p>If you are not using certificate authentication, you might also
want to disable that. This is because ssh prefers the certificate
authentication algorithms, and if you connect to a host that offers
a more preferred algorithm, ssh will try that and ignore the DNS. 
This is not very satisfactory; hopefully it will improve
when <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=742513">the
bug</a> is fixed.</p>
<pre>
    HostKeyAlgorithms ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-rsa,ssh-dss
</pre>


<h2>Troubleshooting</h2>

<p>Check that your resolver is validating and getting a secure result
for your host. Run the following command and check for &quot;ad&quot;
in the flags. If it is not there then either your resolver is not
validating, or /etc/resolv.conf is not pointing at the validating
resolver.
</p>
<pre>
    $ dig +dnssec <i>&lt;hostname&gt;</i> sshfp | grep flags
    ;; flags: qr rd ra <b>ad</b>; QUERY: 1, ANSWER: 3, AUTHORITY: 0, ADDITIONAL: 1
    ; EDNS: version: 0, flags: do; udp: 4096
</pre>


<p>See if ssh is seeing the AD bit. Use <tt>ssh -v</tt> and look for
messages about secure or insecure fingerprints in the DNS. If you are
getting secure answers via dig but ssh is not, perhaps you are missing
&quot;options edns0&quot; from <tt>/etc/resolv.conf</tt>.
</p>
<pre>
    debug1: found 6 <b>secure</b> fingerprints in DNS
    debug1: matching host key fingerprint found in DNS
</pre>

<p>Try using specific host key algorithms, to see if ssh is trying to
authenticate a key which does not have an SSHFP record of the
corresponding algorithm.
</p>
<pre>
    $ ssh -o HostKeyAlgorithms=ssh-rsa <i>&lt;hostname&gt;</i>
</pre>

<h2>Summary</h2>

<p>What I use is essentially:
</p><p><b>/etc/named/named.conf</b></p>
<pre>
    options {
        dnssec-validation auto;
        dnssec-lookaside auto;
    };
</pre>

<p><b>/etc/resolv.conf</b></p>
<pre>
    nameserver 127.0.0.1
    options edns0
</pre>

<p><b>/etc/ssh/ssh_config</b></p>
<pre>
    VerifyHostKeyDNS yes
</pre>


<h2>Background on DNSSEC and non-validating stub resolvers</h2>

<p>When ssh is not compiled to use ldns, it has to trust the recursive
DNS server to validate SSHFP records, and it trusts that the
connection to the recursive server is secure. To find out if an SSHFP
record is securely validated, ssh looks at the AD bit in the DNS
response header - AD stands for &quot;authenticated data&quot;.</p>

<p>A resolver will not set the AD bit based on the security status of
the answer unless the client asks for it. There are two ways to do
that. The simple way (from the perspective of the DNS protocol) is
to <a href="http://tools.ietf.org/html/rfc6840#section-5.7">set the AD
bit in the query</a>, which gets you the AD bit in the reply without
other side-effects. Unfortunately the standard resolver API makes it
very hard to do this, so it is only simple in theory.</p>

<p>The other way is to add
an <a href="http://tools.ietf.org/html/rfc6891">EDNS0 OPT record</a>
to the query with <a href="http://tools.ietf.org/html/rfc3225">the DO
bit</a> set - DO stands for &quot;DNSSEC OK&quot;. This has a number
of side-effects: EDNS0 allows large UDP packets which provide the
extra space needed by DNSSEC, and DO makes the server send back the
extra records required by DNSSEC such as RRSIGs.</p>

<p>Adding &quot;options edns0&quot; to <tt>/etc/resolv.conf</tt> only
tells it to add the EDNS0 OPT record - it does not enable DNSSEC.
However ssh itself observes whether EDNS0 is turned on, and if so also
turns on the DO bit.</p>


<h2>Regarding &quot;options edns0&quot; vs RRSET_FORCE_EDNS0</h2>

<p>At first it might seem annoying that <tt>ssh</tt> makes you add
&quot;options edns0&quot; to <tt>/etc/resolv.conf</tt> before it will
ask for DNSSEC results. In fact on some systems, ssh is patched to
add a DNS API flag called RRSET_FORCE_EDNS0 which forces EDNS0 and
DO on, so that you do not need to explicitly configure the stub
resolver. However although this seems more convenient, it is
less safe.</p>

<p>If you are using the standard portable OpenSSH then you can safely
set <tt>VerifyHostKeyDNS=yes</tt>, provided your stub resolver is
configured correctly. The rule you must follow is to only add "options
edns0" if /etc/resolv.conf is pointing at a local validating resolver.
SSH is effectively treating "options edns0" as a signal that it can
trust the resolver. If you keep this rule you can change your resolver
configuration without having to reconfigure ssh too; it will
automatically fall back to <tt>VerifyHostKeyDNS=ask</tt> when
appropriate.</p>

<p>If you are using a version of ssh with the RRSET_FORCE_EDNS0 patch
(such as Debian and Ubuntu) then it is sometimes NOT SAFE to
set <tt>VerifyHostKeyDNS=yes</tt>. With this patch ssh has no way to
tell if the resolver is trustworthy or if it should fall back
to <tt>VerifyHostKeyDNS=ask</tt>; it will blindly trust a remote
validating resolver, which leaves you vulnerable to MitM attacks. On
these systems, if you reconfigure your resolver, you may also have to
reconfigure ssh in order to remain safe.</p>

<p>Towards the end of February there
was <a href="http://www.ietf.org/mail-archive/web/dane/current/msg06469.html">a
discussion on an IETF list about stub resolvers and DNSSEC</a> which
revolved around exactly this question of how an app can tell if it is
safe to trust the AD bit from the recursive DNS server.</p>

<p>One proposal was for the stub resolver to strip the AD bit in replies
from untrusted servers, which (if it were implemented) would allow ssh
to use the RRSET_FORCE_EDNS0 patch safely. However this proposal means
you have to tell the resolver if the server is trusted, which might
undo the patch's improved convenience. There are ways to avoid that,
such as automatically trusting resolvers running on the local host,
and perhaps having a separate configuration file listing trusted
resolvers, e.g. those reachable over IPSEC.</p>
