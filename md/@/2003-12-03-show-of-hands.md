---
dw:
  anum: 115
  eventtime: "2003-12-03T08:59:00Z"
  itemid: 42
  logtime: "2003-12-03T09:05:37Z"
  props:
    commentalter: 1491292306
    current_moodid: 44
    import_source: livejournal.com/fanf/10824
    interface: flat
    opt_backdated: 1
    picture_keyword: photo
    picture_mapid: 3
  url: "https://fanf.dreamwidth.org/10867.html"
format: casual
lj:
  anum: 72
  can_comment: 1
  ditemid: 10824
  event_timestamp: 1070441940
  eventtime: "2003-12-03T08:59:00Z"
  itemid: 42
  logtime: "2003-12-03T09:05:37Z"
  props:
    current_moodid: 44
  reply_count: 4
  url: "https://fanf.livejournal.com/10824.html"
title: Show of Hands
...

So this folk duo were doing a gig at the Swan in High Wycombe, and they were being shown to their dressing room. The door had their names on it which impressed them, since they were more accustomed to dressing rooms that said "gentlemen" on the door. However they were less impressed when they saw inside.

"Oh no, they haven't cleaned it!" the manager moaned, as they looked over the strewn bottles, clothes, spilt drinks, odd stains, and general wreckage.

"What happened?" asked a musician.

"It was an end-of tour party."

"Who?" they asked, imagining some debauched rock band.

"Sooty," said the manager. "It got a bit out of hand."
