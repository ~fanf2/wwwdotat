---
dw:
  anum: 8
  eventtime: "2007-11-19T21:40:00Z"
  itemid: 313
  logtime: "2007-11-19T22:32:45Z"
  props:
    commentalter: 1491292349
    import_source: livejournal.com/fanf/80822
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/80136.html"
format: casual
lj:
  anum: 182
  can_comment: 1
  ditemid: 80822
  event_timestamp: 1195508400
  eventtime: "2007-11-19T21:40:00Z"
  itemid: 315
  logtime: "2007-11-19T22:32:45Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/80822.html"
title: Chain of distraction
...

I was catching up with <a href="http://listbox.com/member/archive/247">Dave Farber's Interesting People list</a> this evening when I read <a href="http://www.listbox.com/member/archive/247/2007/11/sort/time_rev/page/1/entry/14:67/20071117114131:E9093E3A-952B-11DC-9F40-50AC66D9244B/">a brief post</a> about Ray Kurzweil's graph of a 100-year generalization of Moore's law. This caught my interest, so I <a href="http://www.google.com/search?q=kurzweil">googled for "Kurzweil"</a> to see if I could find what it was referring to. I found the graph as an illustration of <a href="http://en.wikipedia.org/wiki/Accelerating_change">the Wikipedia article about "accelerating change"</a> which in turn led me to <a href="http://www.kurzweilai.net/articles/art0134.html?printable=1">Kurzweil's long essay</a> on the subject. He says that Moore's Law is just a particularly quantitative instance of a general rule of exponential progress, encompassing not just technology in general, but also biological evolution, and leading to an AI singularity that he expects before 2050. The <a href="http://www.kurzweilai.net/">Kurzweil AI</a> site has lots of interesting articles on the subject, one of which is <a href="http://www.kurzweilai.net/articles/art0476.html?printable=1">a log of an on-line chat between Kurzweil and Vernor Vinge</a>, in which VV plugs a collection of his short stories that was published a few years ago, of which I did not remember having a copy. In fact, it has been languishing on <a href="http://www.amazon.co.uk/gp/registry/registry.html?id=20JUWH1EOSTNK">my Amazon wish list</a> because <a href="http://www.amazon.co.uk/dp/0312875843">it isn't available new in the UK</a>. However, <a href="http://www.amazon.com/dp/0312875843">it is available in the US</a>, and <a href="http://www.google.com/search?q=GBP in USD">the exchange rate</a> is currently very favourable. So I ordered the book along with a few others, including a copy of <a href="http://www.amazon.co.uk/dp/0192142313">the Oxford Companion to the Year</a>, which at $80 is two thirds of its price over here, and <a href="http://www.amazon.co.uk/dp/0750306408">a history of the atomic clock</a> which is similarly cut-price. They should go well on my shelves next to <a href="http://www.amazon.co.uk/dp/0521003970">The Measurement of Time</a> and <a href="http://www.amazon.co.uk/dp/0521777526">Calendrical Calculations</a> (aargh, now there's <a href="http://www.amazon.co.uk/dp/0521702380">a third edition</a>!). I was lucky enough to be able to buy the latter two books from <a href="http://www.cambridge.org/uk/bookshop/">the Cambridge University Press bookshop</a> which gives a <a href="http://www.admin.cam.ac.uk/offices/personnel/staff/benefits/discounts.html#heading1">20% discount</a> to <a href="http://www.admin.cam.ac.uk/offices/misd/univcard/">University Card</a> holders.
