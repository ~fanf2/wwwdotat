---
dw:
  anum: 215
  eventtime: "2016-01-25T20:26:00Z"
  itemid: 442
  logtime: "2016-01-25T20:26:29Z"
  props:
    commentalter: 1491292415
    import_source: livejournal.com/fanf/140505
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/113367.html"
format: html
lj:
  anum: 217
  can_comment: 1
  ditemid: 140505
  event_timestamp: 1453753560
  eventtime: "2016-01-25T20:26:00Z"
  itemid: 548
  logtime: "2016-01-25T20:26:29Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 2
  url: "https://fanf.livejournal.com/140505.html"
title: A rant about <tt>whois</tt>
...

<p>I have been fiddling around with <a href="http://svnweb.freebsd.org/base/head/usr.bin/whois/">FreeBSD's <code>whois</code>
client</a>. Since I
have become responsible for Cambridge's Internet registrations, it's
helpful to have a <code>whois</code> client which isn't annoying.</p>

<p>Sadly, <code>whois</code> is an unspeakably crappy protocol. In fact it's barely
even a protocol, more like a set of vague suggestions. Ugh.</p>

<h2>The first problem...</h2>

<p>... is to work out which server to send your <code>whois</code> query to. There
are a number of techniques, most of which are necessary and none of
which are sufficient.</p>

<ol start="0">
<li><p>Rely on a knowledgable user to specify the server.</p>

<p>Happily we can do better than just this, but the
feature has to be available for special queries.</p></li>
<li><p>Have a built-in curated mapping from query patterns
to servers.</p>

<p>This is the approach used by <a href="https://github.com/rfc1036/whois">Debian's <code>whois</code>
client</a>. Sadly in
the era of vast numbers of new gTLDs, this requires
software updates a couple of times a week.</p></li>
<li><p>Send the query to <code>TLD.whois-servers.net</code> which maps TLDs to whois servers using CNAMEs
in the DNS.</p>

<p>This is a brilliant service, particularly good for the
wild and wacky two-letter country-class TLDs.
Unfortunately it has also failed to keep up with the
new gTLDs, even though it only needs a small amount of
extra automation to do so.</p></li>
<li><p>Try <code>whois.nic.TLD</code> which is the standard required
for new gTLDs.</p>

<p>In practice a combination of (2) and (3) is extremely
effective for domain name <code>whois</code> lookups.</p></li>
<li><p>Follow referrals from a server with broad but shallow data
to one with narrower and deeper data.</p>

<p>Referrals are necessary for domain queries in "thin"
registries, in which the TLD's registry does not
contain all the details about registrants (domain
owners), but instead refers queries to the registrar
(i.e. reseller).</p>

<p>They are also necessary for IP address lookups, for
which ARIN's database contains registrations in North
America, plus referrals to the other regional Internet
registries for IP address registrations in other parts
of the world.</p></li>
</ol>

<p>Back in May I added (3) to FreeBSD's <code>whois</code> to fix its support for
new gTLDs, and I added a bit more (1).</p>

<p>One motivation for the latter was for looking up <code>ac.uk</code> domains: (4)
doesn't work because Nominet's <code>.uk</code> <code>whois</code> server doesn't provide
referrals to JANET's <code>whois</code> server; and (2) is a bit awkward, because
although there is an entry for <code>ac.uk.whois-servers.net</code> you have to
have some idea of when it makes sense to try DNS queries for 2LDs.
(<code>whois-servers.net</code> would be easier to use if it had a wildcard for
each entry.)</p>

<p>The other motivation for extending the curated server list was to
teach it about more NIC handle formats, such as <code>-RIPE</code> and <code>-NICAT</code>
handles; and the same mechanism is useful for special-case domains.</p>

<p>Last week I added support for AS numbers, moving them from (0) to (1).
After doing that I continued to fiddle around, and soon realised that
it is possible to dispense with (3) and (2) and a large chunk of (1),
by relying more on (4). The IANA <code>whois</code> server knows about most things
you might look up with <code>whois</code> - domain names, IP addresses, AS
numbers - and can refer you to the right server.</p>

<p>This allowed me to throw away a lot of query syntax analysis and
trial-and-error DNS lookups. Very satisfying.</p>

<p>(I'm not sure if this excellently comprehensive data is a new feature
of IANA's <code>whois</code> server, or if I just failed to notice it before...)</p>

<h2>The second problem...</h2>

<p>... is that the output from <code>whois</code> servers is only vaguely
machine-readable.</p>

<p>For example, FreeBSD's <code>whois</code> now knows about 4 different referral
formats, two of which occur with varying spacing and casing from
different servers. (I've removed support for one amazingly ugly and
happily obsolete referral format.)</p>

<p>My code just looks for a match for any referral format without trying
to be knowledgable about which servers use which syntax.</p>

<p>The output from <code>whois</code> is basically a set of <code>key: value</code> pairs, but
often these will belong to multiple separate objects (such as a domain
name or a person or a net block); servers differ about whether blank
lines separate objects or are just for pretty-printing a single
object. I'm not sure if there's anything that can be done about this
without huge amounts of tedious work.</p>

<p>And servers often emit a lot of rubric such as terms and conditions or
hints and tips, which might or might not have comment markers.
FreeBSD's whois has a small amount of rudimentary rubric-trimming code
which works in a lot of the most annoying cases.</p>

<h2>The third problem...</h2>

<p>... is that the syntax of whois queries is enormously variable. What
is worse, some servers require some non-standard complication to get
useful output.</p>

<p>If you query Verisign for <code>microsoft.com</code> the server does fuzzy
matching and returns a list of dozens of spammy name server names. To
get a useful answer you need to ask for <code>domain microsoft.com</code>.</p>

<p>ARIN also returns an unhelpfully terse list if a query matches
multiple objects, e.g. a net block and its first subnet. To make it
return full details for all matches (like RIPE's whois server) you
need to prefix the query with a <code>+</code>.</p>

<p>And for <code>.dk</code> the verbosity option is <code>--show-handles</code>.</p>

<p>The best one is DENIC, which requires a different query syntax
depending on whether the domain name is a non-ASCII internationalized
domain name, or a plain ASCII domain (which might be a
punycode-encoded internationalized domain name). Good grief, can't it
just give a useful answer without hand-holding?</p>

<h2>Conclusion</h2>

<p>That's quite a lot of bullshit for a small program to cope with, and
it's really only scratching the surface. Debian's whois implementation
has attacked this mess with a lot more sustained diligence, but I
still prefer FreeBSD's because of its better support for new gTLDs.</p>
