---
dw:
  anum: 78
  eventtime: "2008-04-17T13:08:00Z"
  itemid: 336
  logtime: "2008-04-17T12:08:38Z"
  props:
    commentalter: 1491292352
    import_source: livejournal.com/fanf/86855
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/86094.html"
format: html
lj:
  anum: 71
  can_comment: 1
  ditemid: 86855
  event_timestamp: 1208437680
  eventtime: "2008-04-17T13:08:00Z"
  itemid: 339
  logtime: "2008-04-17T12:08:38Z"
  props:
    verticals_list: technology
  reply_count: 4
  url: "https://fanf.livejournal.com/86855.html"
title: some ports are more equal than others
...

<p>This has to be the strangest (or stupidest) way that I have seen of treating ports < 1024 specially.</p>

<p>It seems that the nameservers for spacely.net drop any queries that use a source port < 1024. (They also serve domains such as ChristopherReeve.org, which is a brain research funding charity and the reason I found out about the problem.) It was a complete bugger to diagnose, because <tt>dig</tt> of course uses a high source port, so it was able to resolve names without a problem. My nameservers (and chiark's) use the traditional source port 53, so they were in trouble. By default <tt>bind</tt> now uses high source ports so most sites would not trip over the stupidity.</p>

<p>I guess it's time to remove that bit of ancient paranoia from my usual <tt>named.conf</tt> options section.</p>

<pre>
; <<>> DiG 9.3.4 <<>> -b 131.111.8.132#1023 mx ChristopherReeve.org @ns.spacely.net
; (1 server found)
;; global options:  printcmd
;; connection timed out; no servers could be reached

; <<>> DiG 9.3.4 <<>> -b 131.111.8.132#1024 mx ChristopherReeve.org @ns.spacely.net
; (1 server found)
;; global options:  printcmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 5873
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 2

;; QUESTION SECTION:
;ChristopherReeve.org.          IN      MX

;; ANSWER SECTION:
ChristopherReeve.org.   14400   IN      MX      10 mail.ChristopherReeve.org.
ChristopherReeve.org.   14400   IN      MX      5 mail1.ChristopherReeve.org.

;; ADDITIONAL SECTION:
mail.ChristopherReeve.org. 14400 IN     A       66.215.16.66
mail1.ChristopherReeve.org. 14400 IN    A       67.151.7.145

;; Query time: 185 msec
;; SERVER: 64.209.141.253#53(64.209.141.253)
;; WHEN: Thu Apr 17 12:53:08 2008
;; MSG SIZE  rcvd: 113
</pre>
