---
dw:
  anum: 178
  eventtime: "2004-04-20T07:42:00Z"
  itemid: 82
  logtime: "2004-04-20T00:45:18Z"
  props:
    commentalter: 1491292312
    import_source: livejournal.com/fanf/21196
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/21170.html"
format: casual
lj:
  anum: 204
  can_comment: 1
  ditemid: 21196
  event_timestamp: 1082446920
  eventtime: "2004-04-20T07:42:00Z"
  itemid: 82
  logtime: "2004-04-20T00:45:18Z"
  props: {}
  reply_count: 11
  url: "https://fanf.livejournal.com/21196.html"
title: Gmail and privacy
...

I still fail to understand the problem some people have with gmail.

(1) It's opt-in. If you don't like it, don't use it.

(2) Email is already scanned to protect users from spam and viruses. Why not scan for advertising?

(3) EU Data protection officers seem to agree.

http://www.interesting-people.org/archives/interesting-people/200404/msg00152.html
