---
format: html
lj:
  anum: 253
  can_comment: 1
  ditemid: 124413
  event_timestamp: 1354297500
  eventtime: "2012-11-30T17:45:00Z"
  itemid: 485
  logtime: "2012-11-30T17:56:40Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 6
  url: "https://fanf.livejournal.com/124413.html"
title: "Can't send mail from an Apple Mac via a BT Internet connection."
...

<p>This morning we got a problem report from a user who couldn't send mail, because our message submission servers were rejecting their MUA's EHLO command because the host name was syntactically invalid. The symptoms were a lot like <a href="http://business.forums.bt.com/t5/Email/email-on-Mac-problem-sending/td-p/46630">this complaint on the BT forums</a> except our user was using Thunderbird. In particular the hostname had the same form, <tt>unknown-<i>11</i>:<i>22</i>:<i>33</i>:<i>44</i>:<i>55</i>:<i>66</i>.home</tt>, where the part in the middle looked like an embedded ethernet address including the colons that triggered the syntax error.</p>

<p>I wondered where this bogus hostname was coming from so I asked the user to look at their Mac's various hostname settings. I had a look in our logs to see if other people were having the same problem. Yes, a dozen or two, and <i>all</i> of them were using BT Internet, and <i>all</i> of the ethernet addresses in their hostnames were allocated to Apple.</p>

<p>A lot of our BT users have hostnames like <tt>unknown-<i>11</i>-<i>22</i>-<i>33</i>-<i>44</i>-<i>55</i>-<i>66</i>.home</tt> where the colons in the ethernet address have been replaced by hyphens. But it seems that some versions of the BT hub have broken hostname allocation that fails to use the correct syntax. You can <a href="http://business.forums.bt.com/t5/Email/365-Emails-on-Macs-using-a-Hub-3-not-sending/td-p/46868">change the settings on the hub to allocate a hostname with valid syntax</a> but you have to do this separately for each computer that connects to your LAN. I believe (but I haven't checked) that the hub implements a fake <tt>.home</tt> TLD so that DNS "works" for computers on the LAN.</p>

<p>When a Mac OS computer connects to a network it likes to automatically adjust its hostname to match the network's idea of its name, so on a LAN managed by a broken BT hub it ends up with a bogus hostname containing colons. You can <a href="http://business.forums.bt.com/t5/Email/Problems-Sending-from-Mac-Mail-using-a-Hub-3/td-p/47328">force the Mac to use a sensible hostname</a> either globally or (as in those instructions) just in a particular location.</p>

<p>Most MUAs construct their EHLO commands using the computer's hostname. Most mail servers reject EHLO commands with invalid syntax (though underscores are often given a pass). So, if you try to send mail from a Mac on a BT connection then it is likely to fail in this way.</p>

<p>This is a stupid and depressing bug, and the workarounds available to the user are all rather horrible. It would be a waste of time to ask the affected users to do their own workarounds and complaints to BT - and it seems most of them are oblivious to the failure.</p>

<p>So I decided to tweak the configuration on our message submission servers to ignore the syntax of the client hostname. In Exim you can do this using <tt>helo_accept_junk_hosts = *</tt> preceded by a comment explaining why BT Hubs suck.</p>
