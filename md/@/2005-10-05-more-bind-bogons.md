---
dw:
  anum: 138
  eventtime: "2005-10-05T12:47:00Z"
  itemid: 152
  logtime: "2005-10-05T12:54:10Z"
  props:
    commentalter: 1491292324
    import_source: livejournal.com/fanf/39352
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/39050.html"
format: casual
lj:
  anum: 184
  can_comment: 1
  ditemid: 39352
  event_timestamp: 1128516420
  eventtime: "2005-10-05T12:47:00Z"
  itemid: 153
  logtime: "2005-10-05T12:54:10Z"
  props: {}
  reply_count: 9
  url: "https://fanf.livejournal.com/39352.html"
title: more bind bogons
...

Following <a href="http://www.livejournal.com/users/fanf/38942.html?style=mine">my previous entry</a>, I emailed &lt;bind-suggest&#64;isc.org&gt; to suggest an improvement which would allow me to do what I want directly, without fiddling with features that aren't quite the right thing. I got a reply from Mark Andrews who says that bind-9.4.0 will do what I want, according to this changelog entry:
<pre>
  1798. [func] The server syntax has been extended to support a
               range of servers.  [RT #11132]
</pre>
This will allow me to say the following, which is exactly what I want.
<pre>
  server 192.168.0.0/16 {
    bogus yes;
  };
</pre>
