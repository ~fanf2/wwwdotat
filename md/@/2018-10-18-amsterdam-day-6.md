---
dw:
  anum: 212
  eventtime: "2018-10-18T18:56:00Z"
  itemid: 506
  logtime: "2018-10-18T16:58:21Z"
  props:
    commentalter: 1540218214
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/129748.html"
format: md
...

Amsterdam day 6
===============

([Fri](https://dotat.at/@/2018-10-12-amsterdam-day-0.html)
[Sat](https://dotat.at/@/2018-10-14-amsterdam-day-1.html)
[Sun](https://dotat.at/@/2018-10-14-amsterdam-day-2.html)
[Mon](https://dotat.at/@/2018-10-16-amsterdam-day-3.html)
[Tue](https://dotat.at/@/2018-10-17-amsterdam-day-4.html)
[Wed](https://dotat.at/@/2018-10-18-amsterdam-day-5.html))

I'm posting these notes earlier than usual because it's the RIPE
dinner later. As usual there are links to the presentation materials
from the [RIPE77 meeting plan](https://ripe77.ripe.net/programme/meeting-plan/).

One hallway conversation worth noting: I spoke to Colin Petrie of RIPE
NCC who mentioned that they are rebooting the Wireless APs every day
because they will not switch back to a DFS channel after switching
away to avoid radar interference, so they gradually lose available
bandwidth.


DNS WG round 2
--------------

Anand Buddhdev - RIPE NCC update

  - k-root: 80,000 qps, 75% junk, 250 Mbit/s on average, new 100Gbit/s node

  - RIPE has a new DNSSEC signer. Anand gave a detailed examination of
    the relative quality of the available solutions, and explained why
    they chose Knot DNS. Their migration is currently in progress
    using a key rollover.

  - Anand also spoke supportively about CDS/CDNSKEY automation

Ondřej Caletka - DS updates in the RIPE DB

  - Some statistics from the RIPE database to help inform decisions
    about CDS automation.

Benno Overeinder - IETF DNSOP update

  - Overview of work in progress, including ANAME. I spoke at the mic
    to explain that there is a "camel-sensitive" revamped draft that
    has not yet been submitted

  - Matthijs Mekking has started a prototype provisioning-side
    implementation of ANAME <https://github.com/matje/anamify>

Sara Dickinson - performance of DNS over TCP

  - With multithreading, TCP performance is 67% of UDP performance for
    Unbound, and only 25% for BIND

  - Current DNS load generation tools are not well suited to TCP, and
    web load generation tools also need a lot of adaptation (e.g. lack
    of pipelining)

  - There's a lack of good models for client behaviour, which is much
    more pertinent for TCP than UDP. Sara called for data collection
    and sharing to help this project.

Petr Špaček - DNSSEC and geoIP in Knot DNS

  - Details of how this new feature works with performance numbers.
    Petr emphasized how this king of thing is outside the scope of
    current DNS standards. It's kind of relevant to ANAME because many
    existing ANAME-like features are coupled to geoIP features. I've
    been saying to several people this week that the key challege in
    the ANAME spec is to have a clearly described an interoperable
    core, which also allows tricks like these.

Ondřej Surý - ISC BIND feature telemetry

  - Ondřej asked what is the general opinion on adding a phone home
    feature to BIND which allows ISC to find out what features people
    are not using and which could be removed.

  - NLnet Labs and CZ.NIC said they were also interested in this idea;
    PowerDNS is already doing this and their users like the warnings
    about security updates being available.


Open Source
-----------

Sasha Romijn on IRRd v4

  - Nice to hear a success story about storing JSON in PostgreSQL

  - RPSL has horrid 822 line continuations and interleaved comments, oh dear!

Mircea Ulinic (Cloudflare) Salt + Napalm for network automation

  - Some discussion about why they chose Salt: others "not
    event-driven nor data-driven"

Andy Wingo - a longer talk about Snabb - choice quotes:

  - "rewritable software"

  - "network functions in the smallest amount of code possible"

Peter Hessler on OpenBSD and OpenBGPD - a couple of notable OpenBSD points

  - they now have zero ROP gadgets in `libc` on `arm64`

  - they support arbitrary prefix length for SLAAC

Martin Hoffman - "Oxidising RPKI" - NLnet Labs Routinator 3000 written in Rust:

  - write in C? "why not take advantage of the last 40 years of
    progress in programming languages?"


IPv6
----

Jen Linkova on current IETF IPv6 activity:

  - IPv6 only RA flag

  - NAT64 prefix in RA

  - path MTU discovery "a new hope?" - optional packet truncation
    and/or MTU annotations in packet header

  - [Indefensible Neighbour
    Discovery](https://tools.ietf.org/html/draft-jaeggli-v6ops-indefensible-nd) -
    Jen recommends this summary of mitigations for layer 2 resource
    exhaustion

Oliver Gasser on how to discover IPv6 addresses:

  - You can't brute-force scan IPv6 like you can IPv4 :-)

  - Use a "hitlist" of known IPv6 addresses instead, obtained from
    DNS, address assignment policies, crowdsourcing, infering nerby
    addresses, ...

  - It's possible to cover 50% of prefixes using their methods

  - Cool use of entropy clustering to discover IPv6 address assignment schemes.

Jens Link talked about IPv6 excuses, and Benedikt Stockebrand talked
about how to screw up an IPv6 addressing plan. Both quite amusing and
polemical :-)
