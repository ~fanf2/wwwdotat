---
comments:
  APNIC: https://blog.apnic.net/2024/06/20/the-osi-deprogrammer/#comments
  Fediverse: https://mendeddrum.org/@fanf/112161930711762156
  Dreamwidth: https://fanf.dreamwidth.org/145551.html
...
On "the OSI deprogrammer"
=========================

Back in December, George Michaelson posted an item on the APNIC blog titled
["That OSI model refuses to die"](https://blog.apnic.net/2023/12/12/that-osi-model-refuses-to-die/),
in reaction to Robert Graham's ["OSI Deprogrammer"](https://docs.google.com/document/d/1iL0fYmMmariFoSvLd9U5nPVH1uFKC7bvVasUcYq78So/mobilebasic)
published in September. I had [discussed the OSI Deprogrammer on Lobsters](https://lobste.rs/s/lkb3g6/osi_deprogrammer),
and George's blog post prompted me to write an email. He and I agreed
that I should put it on my blog, but I did not get a round tuit until
now...

The main reason that OSI remains relevant is Cisco certifications require
network engineers to learn it. This makes OSI part of the common technical
vocab and basically unavoidable, even though (as Rob Graham correctly
argues) it's deeply unsuitable.

It would be a lot better if the OSI reference model were treated as a
model of OSI in particular, not a model of networking in general, [as
Jesse Crawford argued in 2021][computer.rip]. OSI ought to be taught
as an example alongside similar reference models of protocol stacks
that are actually in use.

[computer.rip]: https://computer.rip/2021-03-27-the-actual-osi-model.html

One of OSI's big problems is how it enshrines layering as _the_
architectural pattern, but there are other patterns that are at least
as important:

  * The hourglass narrow waist pattern, where a protocol stack provides a
    simple abstraction and only really cares about how things work on one
    side of the waist.

    For instance, IP is a narrow waist and the Internet protocol stack
    only really cares about the layers above it. And Ethernet's addressing
    and framing are another narrow waist, where IEEE 802 only really cares
    about the layers below.

  * Recursive layering of entire protocol stacks. This occurs when
    tunnelling, e.g. MPLS or IPSEC. It works in concert with narrow waists
    that allow protocol stacks to be plugged together.

    Tunneling starkly highlights what nonsense OSI's fixed layers are,
    leading to things like network engineers talking about "layer 2.5"
    when talking about tunneling protocols that present Ethernet's narrow
    waist at their endpoints.

Speaking of Ethernet, it's very poorly served by the OSI model. Ethernet
actually has three layers:

  * PHY (cables, [MAU][], [SFP][])
  * MAC ([AUI][], [MII][])
  * LLC ([LLDP][], [LACP][])

[MAU]: https://en.wikipedia.org/wiki/Medium_Attachment_Unit
[SFP]: https://en.wikipedia.org/wiki/Small_Form-factor_Pluggable
[AUI]: https://en.wikipedia.org/wiki/Attachment_Unit_Interface
[MII]: https://en.wikipedia.org/wiki/Media-independent_interface
[LLDP]: https://en.wikipedia.org/wiki/Link_Layer_Discovery_Protocol
[LACP]: https://en.wikipedia.org/wiki/Link_aggregation

Then there's WiFi which looks like Ethernet from IP's point of view,
but is even more complicated. And almost everything non-ethernet has gone
away or been adapted to look more like ethernet...

Whereas OSI has too few lower layers, it has too many upper layers:
its session and presentation layers don't correspond to anything in
the Internet stack. I think Rob Graham said that they came from IBM
SNA, and were related to terminal-related things like simplex or
block-mode, and character set translation. [Jack Haverty said
something similar on the Internet History mailing list in
2019][haverty]. The closest the ARPANET / Internet protocols get is
Telnet's feature negotiation; a lot of the problem solved by the OSI
presentation layer is defined away by the Internet's ASCII-only
network virtual terminal. Graham also said that when people assign
Internet functions to layers 5 and 6, they do so based on the names
not based on how the OSI describes what they do.

[haverty]: https://elists.isoc.org/pipermail/internet-history/2019-July/005376.html

One of the things that struck me when reading
[Mike Padlipsky](https://en.wikipedia.org/wiki/Michael_A._Padlipsky)'s
[Elements of Networking Style](https://archive.org/details/elementsofnetwor00padl)
is the amount of argumentation that was directed at terminal handling
back then. I guess in that light it's not entirely surprising that OSI
would dedicate two entire layers to the problem.

Padlipsky also drew the
[ARPANET layering as a fan](https://twitter.com/fanf/status/1392218230144569346)
instead of a skyscraper, with intermediate layers shared by some but
not all higher-level protocols, e.g. the NVT used by Telnet, FTP,
SMTP. I expect if he were drawing the diagram later there might be
layers for 822 headers, MIME, SASL -- though they are more like design
patterns than layers since they get used rather differently by SMTP,
NNTP, HTTP. The notion of pick-and-mix protocol modules seems more
useful than fixed layering.

Anyway, if I could magically fix the terminology, I would prefer
network engineers to talk about specific protocols (e.g. ethernet,
MPLS) instead of bogusly labelling them as layers (e.g. 2, 2.5). If
they happen to be working with a more diverse environment than usual
(hello [DOCSIS][]) then it would be better to talk about sub-IP
protocol stacks. But that's awkwardly sesquipedalian so I can't see it
catching on.

[DOCSIS]: https://en.wikipedia.org/wiki/DOCSIS
