---
dw:
  anum: 163
  eventtime: "2017-01-08T00:58:00Z"
  itemid: 472
  logtime: "2017-01-08T00:58:30Z"
  props:
    commentalter: 1491292429
    import_source: livejournal.com/fanf/148164
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/120995.html"
format: html
lj:
  anum: 196
  can_comment: 1
  ditemid: 148164
  event_timestamp: 1483837080
  eventtime: "2017-01-08T00:58:00Z"
  itemid: 578
  logtime: "2017-01-08T00:58:30Z"
  props:
    og_image: "http://l-files.livejournal.net/og_image/936728/578?v=1483877774"
    personifi_tags: "nterms:yes"
  reply_count: 2
  url: "https://fanf.livejournal.com/148164.html"
title: Compact encoding of the leap seconds list
...

<p>The list of leap seconds is published in a number of places. Most
authoritative is <a href="https://hpiers.obspm.fr/eoppc/bul/bulc/Leap_Second.dat">the IERS list published from the Paris
Observatory</a>
and most useful is <a href="ftp://time.nist.gov/pub/leap-seconds.list">the version published by
NIST</a>. However neither of
them are geared up for distributing leap seconds to (say) every NTP
server.</p>

<p>For a couple of years, I have published the leap second list in the
DNS as a set of fairly human-readable AAAA records. There's an example
in <a href="https://pairlist6.pair.net/pipermail/leapsecs/2015-January/005438.html">my message to the LEAPSECS
list</a>
though I have changed the format to avoid making any of them look like
real IPv6 addresses.</p>

<p><a href="http://phk.freebsd.dk/time/20151122.html">Poul-Henning Kamp also publishes leap second information in the
DNS</a> though he only
publishes an encoding of the most recent <a href="https://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat">Bulletin
C</a> rather than
the whole history.</p>

<p>I have recently added a set of PHK-style A records at
<code>leapsecond.dotat.at</code> listing every leap second, plus an "illegal"
record representing the point after which the difference between TAI
and UTC is unknown. I have also added a <code>next.leapsecond.dotat.at</code>
which should be the same as PHK's <code>leapsecond.utcd.org</code>.</p>

<p>There are also some HINFO records that briefly explain the other
records at <code>leapsecond.dotat.at</code>.</p>

<p>One advantage of using the DNS is that the response can be signed and
validated using DNSSEC. One disadvantage is that big lists of
addresses rapidly get quite unwieldy. There can also be problems with
DNS messages larger than 512 bytes. For <code>leapsecond.dotat.at</code> the
answers can get a bit chunky:</p>

<table>
<tr><td><th>plain<th>DNSSEC
<tr><th>A<td>485<td>692
<tr><th>AAAA<td>821<td>1028
</table>

<h2>Terse</h2>

<p>I've thought up a simple and brief way to list the leap seconds, which
looks like this:</p>

<pre><small>    6+6+12+12+12+12+12+12+12+18+12+12+24+30+24+12+18+12+12+18+18+18+84+36+42+36+18+5?
</small></pre>

<p><a href="https://tools.ietf.org/html/rfc5234">ABNF</a> syntax:</p>

<pre><code>    leaps  =  *leap end
    leap   =  gap delta
    end    =  gap "?"
    delta  =  "-" / "+"
    gap    =  1*DIGIT
</code></pre>

<p>Each leap second is represented as a decimal number, which counts the
months since the previous leap second, followed by a "+" or a "-" to
indicate a positive or negative leap second. The sequence starts at
the beginning of 1972, when TAI-UTC was 10s.</p>

<p>So the first leap is "6+", meaning that 6 months after the start of
1972, a leap second increases TAI-UTC to 11s. The "84+" leap
represents the gap between the leap seconds at the end of 1998 and end
of 2005 (7 * 12 months).</p>

<p>The list is terminated by a "?" to indicate that the IERS have not
announced what TAI-UTC will be after that time.</p>

<h2>Rationale</h2>

<p><a href="http://www.itu.int/rec/R-REC-TF.460-6-200202-I/en">ITU recommendation
TF.460-6</a> specifies
in paragraph 2.1 that leap seconds can happen at the end of any month,
though in practice the preference for the end of December or June has
never been overridden. Negative leap seconds are also so far only a
theoretical possibility.</p>

<p>So, counting months between leaps is the largest radix permitted,
giving the smallest (shortest) numbers.</p>

<p>I decided to use decimal and mnemonic separators to keep it simple.</p>

<h2>Binary</h2>

<p>If you want something super compact, then bit-banging is the way to do
it. Here's a binary version of the leap second list, 29 bytes
displayed as hexadecimal in network byte order.</p>

<pre><code>46464c4c 4c4c4c4c 4c524c4c 585e584c 524c4c52 52523c58 646a6452 85
</code></pre>

<p>Each byte has a 2-bit two's complement signed delta in the most
significant bits, and a 6-bit unsigned month count in the least
significant bits.</p>

<p>The meaning of the delta field is as follows (including the hex,
binary, and decimal values):</p>

<ul>
<li>0x40 = 01 = +1 = positive leap second</li>
<li>0x00 = 00 = 0 = no leap second</li>
<li>0xC0 = 11 = -1 = negative leap second</li>
<li>0x80 = 10 = -2 = end of list, like "?"</li>
</ul>

<p>So, for example, a 6 month gap followed by a positive leap second is
0x40 + 6 == 0x46. A 12 month gap is 0x40 + 12 == 0x4c.</p>

<p>The "no leap second" option comes into play when the gap between leap
seconds is too large to fit in 6 bits, i.e. more than 63 months, e.g.
the 84 month gap between the ends of 1998 and 2005. In this situation
you encode a number of "no leap second" gaps until the remaining gap
is less than 63.</p>

<p>I have encoded the 84 month gap as 0x3c58, i.e. 0x00 + 0x3c is 60
months followed by no leap second, then 0x40 + 0x18 is 24 months
followed by a positive leap second.</p>

<h2>Compression</h2>

<p>There's still quite a lot of redundancy in the binary encoding.
It can be reduced to 24 bytes using <a href="https://tools.ietf.org/html/rfc1951">RFC 1951 DEFLATE
compression</a>.</p>

<h2>Publication</h2>

<p>There is now a TXT record at <code>leapsecond.dotat.at</code> containing the
human-readable terse form of the leap seconds list. This is gives you
a 131 byte plain DNS response, or a 338 byte DNSSEC signed response.</p>

<p>I've published the deflated binary version using a private-use
TYPE65432 record which saves 58 bytes.</p>

<p>There is code to download and check the consistency of the leapseconds
files from the IERS, NIST, and USNO, generate the DNS records, and
update the DNS if necessary, at
<a href="http://dotat.at/cgi/git/leapseconds.git">http://dotat.at/cgi/git/leapseconds.git</a>.</p>
