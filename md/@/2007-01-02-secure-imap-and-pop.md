---
dw:
  anum: 239
  eventtime: "2007-01-02T17:46:00Z"
  itemid: 271
  logtime: "2007-01-02T17:46:37Z"
  props:
    commentalter: 1491292339
    import_source: livejournal.com/fanf/69867
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/69615.html"
format: casual
lj:
  anum: 235
  can_comment: 1
  ditemid: 69867
  event_timestamp: 1167759960
  eventtime: "2007-01-02T17:46:00Z"
  itemid: 272
  logtime: "2007-01-02T17:46:37Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/69867.html"
title: Secure IMAP and POP
...

I've just rolled out some changes to our POP and IMAP servers which have become possible because all our users are now logging in over TLS. For the last year while we have been working towards getting all users' settings secured there has been a small weakness in the way we enforced secure logins. The server had to receive the username before it could decide whether or not to allow an insecure login, which means that when a supposedly secure user accidentally uses insecure settings (e.g. because they are reconfiguring a new MUA) they are likely to expose their password because the login conversation goes too far before it is aborted.

The changes are just a few pedantic tweaks to the way our servers speak IMAP and POP.

In POP, the client logs in by issuing a USER command, which states the username, followed by a PASS command with the password. The server now rejects insecure login attempts after the USER command, before the password is transmitted. There is a corresponding change to the server's capability list, which omits the USER command until TLS has been established.

IMAP is less helpful to us because its LOGIN command transmits the username and password all at once. (However this saves a round trip so is faster.) However we can still put LOGINDISABLED in the server's capability list, which will help clients that use it. The other IMAP improvement is to include the capability list in the server banner which saves another round trip - a noticeable improvement for GPRS users.

This should be invisible to users, but makes the service safer especially for wireless access.
