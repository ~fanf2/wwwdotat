---
dw:
  anum: 61
  eventtime: "2006-03-01T11:24:00Z"
  itemid: 203
  logtime: "2006-03-01T11:39:06Z"
  props:
    commentalter: 1491292392
    import_source: livejournal.com/fanf/52429
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/52029.html"
format: casual
lj:
  anum: 205
  can_comment: 1
  ditemid: 52429
  event_timestamp: 1141212240
  eventtime: "2006-03-01T11:24:00Z"
  itemid: 204
  logtime: "2006-03-01T11:39:06Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/52429.html"
title: Email volume
...

Our email volume is following a strange curve. The following numbers come from a week in mid-February (specifically the 11th-17th) for the last four years:
<pre>
        messages     GB
2003   1 143 641    30.97
2004   2 480 215    62.11
2005   2 199 303   117.86
2006   2 925 334   162.70
</pre>Why the drop in message count in 2005? Architectural changes to our email systems meant that fewer messages were going through ppswitch more than once; this year that has increased again because of the unbundling of the mailinglist system. It may also be a result in the changes of behaviour of email viruses (which contribute to the count).
