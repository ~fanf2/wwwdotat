---
dw:
  anum: 197
  eventtime: "2006-04-21T17:37:00Z"
  itemid: 222
  logtime: "2006-04-21T17:40:26Z"
  props:
    commentalter: 1491292341
    import_source: livejournal.com/fanf/57296
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/57029.html"
format: casual
lj:
  anum: 208
  can_comment: 1
  ditemid: 57296
  event_timestamp: 1145641020
  eventtime: "2006-04-21T17:37:00Z"
  itemid: 223
  logtime: "2006-04-21T17:40:26Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/57296.html"
title: chat service update
...

I sent this to the <a href="https://lists.cam.ac.uk/mailman/listinfo/cs-chat-users">chat-users list</a> yesterday...

A few people have been wondering what's been going on since my last message in January, so here's an update.

We got approval from the IT Syndicate at the end of February, so the Chat service will be going ahead as planned. Our Hostmaster staff did some tests to ensure that SRV records in the cam.ac.uk DNS would not be a problem, and these were passed OK. My colleagues in Unix Support arranged the purchase of two computers to host the Chat service (as part of a larger order) and racked them in the machine room.

During that time I have mostly been working on email-related things, hence the lack of news... However I have been keeping an eye on the Jabber mailing lists. The discussions there have helped me to decide which server software to use. I originally selected jabberd-2, which is nice and clean, but is not quite finished and development is slow. What's worse is its reputation for flakiness and poor integration with multi-user chat. I also considered jabberd-1.4, which is still being actively developed, but it is crufty and lacks some important XMPP features such as privacy lists. Then in February, the JSF announced that jabber.org (the first Jabber server) had switched to ejabberd-1.0, the Erlang Jabber server. It is featureful, implements all of XMPP, and gets some serious performance, clustering, and robustness technology from Erlang.

This week I have been getting to grips with Erlang, and next week I'll tackle Unix Support's new Linux installation service so that I can get Wogan and Parky (the Chat service computers) running. After that I'll be away for two weeks in Kenya, teaching people about Exim. After that I hope to get a prototype Jabber service running.

As ever, there are more details on the Chat wiki at https://wiki.csx.cam.ac.uk/chat/ (for those with raven accounts)
