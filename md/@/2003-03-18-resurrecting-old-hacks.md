---
dw:
  anum: 177
  eventtime: "2003-03-18T23:50:00Z"
  itemid: 3
  logtime: "2003-03-18T16:16:13Z"
  props:
    commentalter: 1491292303
    current_moodid: 126
    current_music: computer fans
    import_source: livejournal.com/fanf/874
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/945.html"
format: casual
lj:
  anum: 106
  can_comment: 1
  ditemid: 874
  event_timestamp: 1048031400
  eventtime: "2003-03-18T23:50:00Z"
  itemid: 3
  logtime: "2003-03-18T16:16:13Z"
  props:
    current_moodid: 126
    current_music: computer fans
  reply_count: 0
  url: "https://fanf.livejournal.com/874.html"
title: Resurrecting old hacks
...

People are showing interest in <a href="http://www.freebsd.org/cgi/query-pr.cgi?pr=12071">my NETALIAS patch</a> for FreeBSD machines with lots of IP addresses (e.g. I used to run machines with 96000 addresses). So I'm looking at how easy the patch will be to port forwards across two major versions and a number of years...
