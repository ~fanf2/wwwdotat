---
dw:
  anum: 195
  eventtime: "2004-11-24T18:06:00Z"
  itemid: 115
  logtime: "2004-11-24T10:17:46Z"
  props:
    commentalter: 1491292319
    import_source: livejournal.com/fanf/29644
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/29635.html"
format: casual
lj:
  anum: 204
  can_comment: 1
  ditemid: 29644
  event_timestamp: 1101319560
  eventtime: "2004-11-24T18:06:00Z"
  itemid: 115
  logtime: "2004-11-24T10:17:46Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/29644.html"
title: Bleeding edge
...

I've just rolled out the as-yet-unreleased Exim 4.44 on our central email servers (ppswitch). We're the first people to run it in anger, and I think it's the first time ppswitch has been quite this close to the forefront of technology :-)

A lot of the features in Exim 4.4x have been requested by me in order to improve the service on ppswitch, especially stuff related to submission mode, callout verification, logging, and DNS blacklisting. This helps to make the configuration nice and tidy as well as implementing the correct functionality.

A particularly nice feature in 4.44 is the ability to reject on the basis that a domain's name servers are operated by known spammers:
<pre>
  deny message  = The name servers for the domain ${sender_address_domain} \
                  are listed at ${dnslist_domain} ($dnslist_value); \
                  See ${dnslist_text}
       dnslists = sbl.spamhaus.org/${lookup dnsdb{>:a=\
                                   ${lookup dnsdb{>:zns=\
                                     $sender_address_domain}}}}
</pre>

My configuration file is now over 1000 lines, about half of which are blank or comments. I have another 200 lines of prototype configuration (which implement cryptographic bounce authentication) to test and roll out...
