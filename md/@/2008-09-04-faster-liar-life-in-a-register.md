---
dw:
  anum: 171
  eventtime: "2008-09-04T18:27:00Z"
  itemid: 359
  logtime: "2008-09-04T18:27:46Z"
  props:
    import_source: livejournal.com/fanf/93032
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/92075.html"
format: html
lj:
  anum: 104
  can_comment: 1
  ditemid: 93032
  event_timestamp: 1220552820
  eventtime: "2008-09-04T18:27:00Z"
  itemid: 363
  logtime: "2008-09-04T18:27:46Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/93032.html"
title: Faster LIAR (Life in a register)
...

<p>This week I have been discussing the Game of Life with the amazingly enthusiastic Brice Due, after <a href="http://fanf.livejournal.com/81169.html?thread=415505">he commented</a> on <a href="http://fanf.livejournal.com/81169.html">my original post about LIAR</a>. He put me in touch with <a href="http://tomas.rokicki.com/">Tom Rokicki</a> who, as well as writing <a href="http://golly.sourceforge.net/">probably the best implementation of Life</a>, recently featured in <a href="http://www.newscientist.com/channel/fundamentals/mg19926681.800-cracking-the-last-mystery-of-the-rubiks-cube.html">New Scientist</a> for his work on God's solution to Rubik's Cube.</p>

<p>Tom pointed me at <a href="http://groups.google.com/group/comp.arch.embedded/msg/d835361b253897b5">a better bit-twiddling implementation of Life</a> which comes in at 26 operations compared to my code's 34 - a significant improvement. However his code is ultra-compressed and really hard to understand. So I wrote <a href="http://dotat.at/prog/life/liar2.c">a comprehensible version</a>. This code has some rather clever features.</p>

<pre>
	left = bmp << 1;
	right = bmp >> 1;
	side1 = left ^ right;
	side2 = left & right;
	mid1 = side1 ^ bmp;
	mid2 = side1 & bmp;
	mid2 = side2 | mid2;
</pre>

<p>In the first stage he uses a normal full adder to do the horizontal 3-to-2 compression which counts the number of cells in each row (bits <tt>mid1</tt> and <tt>mid2</tt>). The first two ops of the full adder form a half adder which he uses to count the cells to either side of the middle cell (bits <tt>side1</tt> and <tt>side2</tt>). Thus he gets four useful outputs from just five ops!</p>

<pre>
	upper1 = mid1 << 8;
	lower1 = mid1 >> 8;
	upper2 = mid2 << 8;
	lower2 = mid2 >> 8;
</pre>

<p>The <tt>side</tt> bits are kept in the middle row, whereas the <tt>mid</tt> bits are shifted to become the sums for the upper and lower rows.</p>

<pre>
#define REDUCE(g,f,a,b,c) do {	\
		uint64_t d, e;	\
		d = a ^ b;	\
		e = b ^ c;	\
		f = c ^ d;	\
		g = d | e;	\
	} while(0)

	REDUCE(sum12, sum13, upper1, side1, lower1);
	REDUCE(sum24, sum26, upper2, side2, lower2);
</pre>

<p>The second stage is to compress the three rows into separate sums of the low bits and the high bits. Tom does not use a normal adder to do this. It turns out that there are 24 adder-like boolean functions of three arguments, which produce two values that together encode the number of input bits. Only one of them requires four operations - the others require five or more. The encoding of the result is a bit strange:</p>

<table><tr><th>count</th><th>result</th></tr>
<tr><td>0</td><td>f=0 g=0</td></tr>
<tr><td>1</td><td>f=1 g=1</td></tr>
<tr><td>2</td><td>f=0 g=1</td></tr>
<tr><td>3</td><td>f=1 g=0</td></tr>
</table>

<p>As well as saving two ops compared to using a full adder, this function makes the final twiddling particularly brief.</p>

<pre>
	tmp = sum12 ^ sum13;
	bmp = (bmp | sum13) & (tmp ^ sum24) & (tmp ^ sum26);
	return(bmp & 0x007E7E7E7E7E7E00);
</pre>

<p>I wondered if it's possible to use the 4-op adder-alike in the first stage too. The problem with adding together all nine cells is overflowing the three bit result. This is OK when using a full adder because the overflow happens to confuse a very large count (therefore a dead result) with a very small count (again a dead result) so it makes no difference. The modified adder confuses a middling small count (live result) with a middling big count (dead result) so it fails. The dual-purpose half+full-adder trick neatly avoids overflow problems.</p>

<p>Despite successfully unpicking this code I still find it rather unintuitive, especially the final twiddle. I am in awe of people who can invent such cunning tricks.</p>
