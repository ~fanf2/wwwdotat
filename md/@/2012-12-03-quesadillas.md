---
format: html
lj:
  anum: 148
  can_comment: 1
  ditemid: 124564
  event_timestamp: 1354562880
  eventtime: "2012-12-03T19:28:00Z"
  itemid: 486
  logtime: "2012-12-03T19:28:07Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 3
  url: "https://fanf.livejournal.com/124564.html"
title: Quesadillas
...

<p>Last week I was inspired by <a href="https://twitter.com/clanwilliam/status/273064491263090688">a tweet about a chilli quesadilla</a> and since then I have cooked the following about three times for Rachel and I.</p>

<p>Filling (enough for four quesadillas):
 <ul>
  <li>An onion, cut in half and sliced to make strips;
  <li>Two sweet peppers, cut into strips
  <li>A couple of cloves of garlic
  <li>Two fresh chillis, deseeded and sliced
  <li>Diced chorizo (Aldi sell little packs, two of which is about right)
  <li>Olive oil
 </ul>
Heat the oil in a pan then bung the lot in and fry until the onion and pepper are cooked - stop before the onion and peppers get too soft. Since Rachel isn't such a fan of spicy food, I keep the chillis on one side and add them when I assemble my quesadillas.</p>

<p>To make a quesadilla, take a plain wheat flour tortilla wrap and fold in half and unfold to make a crease. Cover each half with a single layer of cheese slices (I like Emmental). Put the tortilla in a dry frying pan and put on a moderate heat. Put some filling on one half, and when the cheese has started to melt, fold over the other half and press so the cheese glues in the folling. Cook for a while until the underside is gently browned (less than a minute, I think) then flip over to brown the other side.</p>

<p>Enjoy!</p>
