---
dw:
  anum: 22
  eventtime: "2008-08-28T14:48:00Z"
  itemid: 358
  logtime: "2008-08-28T14:48:12Z"
  props:
    commentalter: 1491292361
    import_source: livejournal.com/fanf/92745
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/91670.html"
format: html
lj:
  anum: 73
  can_comment: 1
  ditemid: 92745
  event_timestamp: 1219934880
  eventtime: "2008-08-28T14:48:00Z"
  itemid: 362
  logtime: "2008-08-28T14:48:12Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 3
  url: "https://fanf.livejournal.com/92745.html"
title: More postcodes
...

<p>Thanks for all the interesting comments on <a href="http://fanf.livejournal.com/92539.html">my previous post</a>.</p>

<p>The reason I'm investigating this is to work around false positives caused by SpamAssassin's obfuscation rules. These are intended to match deliberate misspellings of commonly spammed goods such as Viagra. The specific instance that caused the bug report was a Reading postcode being treated as an obfuscated Rolex.</p>

<p>Therefore I'm not particularly worried about missing out obscure special cases like GIR 0AA and the overseas territories AAAA 1ZZ. However it might be worth tightening up the outcode regex, based on <a href="http://en.wikipedia.org/wiki/List_of_postcode_areas_in_the_United_Kingdom">the list of UK postcode areas</a>, to reduce the chance of matching a bogus postcode.</p>

<p>Also, the <a href="http://www.royalmail.com/portal/rm/content1?catId=400044&mediaId=9200078">Post Office's postcode FAQ</a> mentions that only London uses the ANA and AANA outcode formats. (In fact it's only the E, EC, SW, W, WC areas.) I managed to find a list of postcode districts which includes these outcodes (Wikipedia omits them) and it shows that the third position rule is wrong: it says M does not appear there but there is a poscode district London W1M. Rule Three also allows A and E which are not in fact used.</p>

<pre>qr{\b
  ([BGLMNS][1-9][0-9]?
  |[A-PR-UWYZ][A-HK-Y][1-9]?[0-9]
  |([EW]C?|NW?|S[EW])[1-9][0-9A-HJKMNPR-Y]
  )[ ]{0,2}
  ([0-9][ABD-HJLNP-UW-Z]{2})
\b}x</pre>
