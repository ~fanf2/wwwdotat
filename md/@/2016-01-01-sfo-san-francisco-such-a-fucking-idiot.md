---
dw:
  anum: 11
  eventtime: "2016-01-01T08:36:00Z"
  itemid: 439
  logtime: "2016-01-01T08:36:21Z"
  props:
    commentalter: 1491292414
    import_source: livejournal.com/fanf/139763
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/112395.html"
format: html
lj:
  anum: 243
  can_comment: 1
  ditemid: 139763
  event_timestamp: 1451637360
  eventtime: "2016-01-01T08:36:00Z"
  itemid: 545
  logtime: "2016-01-01T08:36:21Z"
  props:
    give_features: 1
    personifi_tags: "nterms:no"
  reply_count: 6
  url: "https://fanf.livejournal.com/139763.html"
title: SFO / San Francisco / Such a Fucking idiOt
...

<p>On a flight to San Francisco in 2000 or 2001 I had a chat with a
British woman sitting next to me. She was a similar age to me,
20-something, also aiming to try her hand at the Silly Valley thing,
but, you know, MONTHS behind me. She asked me if I knew of a Days Inn
or something like that. There was (is) one in the Tenderloin district
literally round the corner from my flat, but that part of the city was
such a shithole I was embarrassed to say I lived there. And then (I
kid you not) I managed to leave my box of tea behind on the plane, and
I lost touch with my seat-mate trying to retrieve it. What a chump.

<p>Another time, I was on a taxi from Cambridge to Heathrow (ridiculous
wasteful expense) when I realised my passport had fallen out of my
back pocket while I was sitting on my heels during a party. I missed
my flight, had to go back to Cambridge to recover my passport, and my
employer's travel agent put me on another flight the next day. I felt
like a fool; I'm amazed my employer handled that so reasonably (not
to mention the other ways I took advantage of them).

<p>My sojourn in San Francisco was not a success. I was amazingly lucky
to catch the tail end of the dot-com boom in 2000 but I burned out
badly less than a year later. I was stupid in so many ways.

<p>I failed because I overestimated my own capabilities, and I
underestimated the importance of my friends. It's enormously difficult
to establish a social network in a new place from scratch. I was lucky
working for Demon Internet in London (1997-2000) and for Covalent in
San Francisco (2000-2001) that in both cases my colleagues were a social bunch. But I was often back to Cambridge for parties with my mates, and there's a big difference between 60 miles and 6000 miles.

<p>And, honestly, I was too arrogant to ask my colleagues for feedback
and support. (I'm still crap at that.)

<p>But!

<p>I recovered from the breakdown. Though it took a long time, I moved
back closer to my friends, spent my savings writing code for fun, and
in the end got a job which has kept body and soul together (and
better) for 13 years.

<p>My failure was painful and difficult, but I learned valuable lessons
about myself, and it WAS NOT (in the end) a disaster.

<p>This year has brought that time back to me in interesting ways.

<p>A friend of ours went back to work in South America, in a place she
knew and loved, in a job that was made for her. But the place had
changed - the old friends were no longer there - and the job wasn't as
happy as expected. She was back here much sooner than planned. But our
mutual friends told her about my crash and burn and recovery, and this
helped her recover.

<p>Another friend did the dot-com thing with a much greater success than
me: it took him a lot more than a year to burn out. His was a more
controlled flight into terrain than mine, but similarly abrupt.
However he already knew about my past, and he says he also took
strength from my story.

<p>It is enormously touching to know that my friends have seen my
failure, seen that it wasn't a catastrophe, and that helped them
to get back on their feet.

<p>So, happy new year, and know that if things don't work out as you
hoped, if you fucked it up, it isn't the end of the world. Keep
talking to the people you love and keep doing your thing.
