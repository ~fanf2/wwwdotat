---
dw:
  anum: 97
  eventtime: "2009-03-25T11:51:00Z"
  itemid: 379
  logtime: "2009-03-25T12:26:31Z"
  props:
    commentalter: 1491292363
    import_source: livejournal.com/fanf/98711
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/97121.html"
format: html
lj:
  anum: 151
  can_comment: 1
  ditemid: 98711
  event_timestamp: 1237981860
  eventtime: "2009-03-25T11:51:00Z"
  itemid: 385
  logtime: "2009-03-25T12:26:31Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/98711.html"
title: Ada Lovelace Day
...

<p>This is a bit late, but I've been inspired to post after reading about the women other people admire. Here are a few of mine.</p>
<ul>

<li><a href="http://www.eecs.harvard.edu/~margo/">Margo Seltzer</a>: developer of the BSD log-structured filesystem and Berkeley DB. An open source entrepreneur - founder and CTO of Sleepycat Software - who also has a successful academic career.</li>

<li>Dina Katabi invented <a href="http://www.ana.lcs.mit.edu/dina/XCP/">XCP, the eXplicit Congestion control Protocol</a>, which is a brilliant way for routers to make each individual flow adjust to the level of congestion without the need for any per-flow state. I think it was one of the first papers I read about advanced transport protocol research, and I have continued to follow the subject since then.</li>

<li><a href="http://nih.blogspot.com/">Lisa Dusseault</a> is a WebDAV expert and IETFer, currently one of the Applications Area Directors and therefore a member of the IESG. She was one of the funner people I met at the Paris IETF meeting a few years ago. I like the new ideas she's brought to the IESG, such as monthly updates on her work as Apps AD.</li>

<li>And of course <a href="https://rmc28.livejournal.com/">👤rmc28</a>, who got the University Card working properly and now slaves away on the student information system.</li>

</ul>
