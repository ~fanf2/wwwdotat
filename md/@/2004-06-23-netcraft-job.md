---
dw:
  allowmask: 1
  anum: 229
  eventtime: "2004-06-23T19:48:00Z"
  itemid: 90
  logtime: "2004-06-23T12:53:39Z"
  props:
    commentalter: 1491292312
    import_source: livejournal.com/fanf/23103
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  security: usemask
  url: "https://fanf.dreamwidth.org/23269.html"
format: casual
lj:
  allowmask: 1
  anum: 63
  can_comment: 1
  ditemid: 23103
  event_timestamp: 1088020080
  eventtime: "2004-06-23T19:48:00Z"
  itemid: 90
  logtime: "2004-06-23T12:53:39Z"
  props: {}
  reply_count: 3
  security: usemask
  url: "https://fanf.livejournal.com/23103.html"
title: Netcraft Job
...

Ever since I was working at Demon, Mike Prettejohn &lt;mhp@netcraft.com&gt; has been trying to recruit me to work for <a href="http://www.netcraft.com">Netcraft</a>. I've been reluctant because they were too far away from where I wanted to be (originally Bath, now Bradford-upon-Avon). He now tells me he's thinking of opening a Cambridge office because of the number of people he'd like to employ who have put down roots here.

So if you are on the lookout for something internetty/consultanty/webby/newsy, email him. See also 
http://news.netcraft.com/archives/2004/03/15/software_development_opportunities_at_netcraft.html
