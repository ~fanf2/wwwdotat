---
dw:
  anum: 211
  eventtime: "2006-07-05T13:55:00Z"
  itemid: 244
  logtime: "2006-07-05T14:03:35Z"
  props:
    commentalter: 1491292368
    import_source: livejournal.com/fanf/62930
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/62675.html"
format: casual
lj:
  anum: 210
  can_comment: 1
  ditemid: 62930
  event_timestamp: 1152107700
  eventtime: "2006-07-05T13:55:00Z"
  itemid: 245
  logtime: "2006-07-05T14:03:35Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/62930.html"
title: Anonymous arseholes
...

It soon becomes obvious to people who spend much time communicating online that people tend to behave worse than they do in person. This is probably due to the absence of the usual cues you get from being able to see or hear who you are talking to. The problem becomes much worse when you add anonymity into the mix.

We've recently had a dickhead on the exim-users list abusing anonymous remailers to make one of the regular posters appear bad. As a consequence I've blacklisted all the anonymous remailers I could find - only 35 of them, which was fewer than I expected. I did this based on their sender addresses, since they tend to live on dynamic IP addresses or behind legitimate email systems.

I'm feeling very cross because of this bad behaviour.
