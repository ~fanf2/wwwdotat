---
dw:
  anum: 107
  eventtime: "2003-04-07T20:21:00Z"
  itemid: 15
  logtime: "2003-04-07T13:37:08Z"
  props:
    commentalter: 1491292303
    current_moodid: 74
    import_source: livejournal.com/fanf/3977
    interface: flat
    opt_backdated: 1
    picture_keyword: photo
    picture_mapid: 3
  security: private
  url: "https://fanf.dreamwidth.org/3947.html"
format: casual
lj:
  anum: 137
  can_comment: 1
  ditemid: 3977
  event_timestamp: 1049746860
  eventtime: "2003-04-07T20:21:00Z"
  itemid: 15
  logtime: "2003-04-07T13:37:08Z"
  props:
    current_moodid: 74
  reply_count: 2
  security: private
  url: "https://fanf.livejournal.com/3977.html"
title: And in other news
...

It's pretty clear now that she's more interested than just friendship would warrant, however I don't want it to go any further. I'm happy with the way things are right now: things are reasonably straightforward and productive and undisruptive. I also don't want to upset her still-besotted ex, who is a friend I'd like to keep. So, can I write this email in a way that continues nice friendly activities without cultivating wrong ideas?
