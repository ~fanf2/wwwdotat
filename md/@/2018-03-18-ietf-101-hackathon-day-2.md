---
dw:
  anum: 90
  eventtime: "2018-03-18T23:06:00Z"
  itemid: 483
  logtime: "2018-03-18T23:09:27Z"
  props:
    interface: flat
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/123738.html"
format: md
lj:
  anum: 199
  can_comment: 1
  ditemid: 150983
  event_timestamp: 1521414540
  eventtime: "2018-03-18T23:09:00Z"
  itemid: 589
  logtime: "2018-03-18T23:10:23Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/150983.html"
...

IETF 101 Hackathon, day 2
=========================

Yesterday, on
[IETF 101 hackathon day 1](https://dotat.at/@/2018-03-17-ietf-101-hackathon-day-1.html),
I made a proof of concept DNS-over-HTTPS server. Today I worked on
separating it from my prototyping repository, documenting it, and
knocking out some interoperability bugs.

You can get `doh101` from <https://github.com/fanf2/doh101> and
<https://dotat.at/cgi/git/doh101.git>, and you can send me feedback via
GitHub or email to <dot@dotat.at>.

doh101 vs doh-proxy
-------------------

Yesterday’s problem with the
[doh-proxy client](https://facebookexperimental.github.io/doh-proxy/)
turned out to be very simple: my server only did HTTP/1.1 whereas
doh-proxy only does HTTP/2. The simple fix was to enable HTTP/2: I
added `http2` to the `listen ssl` line in my `nginx.conf`.

doh101 vs Firefox
-----------------

Daniel Stenberg of [cURL](https://curl.haxx.se) fame suggested I
should try out `doh101` with the
[DoH support in Firefox Nightly](https://twitter.com/bagder/status/975315082879127552).
It mysteriously did not work, for reasons that were not immediately
obvious.

I could see Firefox making its initial probe query to check that my
server worked, after which Firefox clearly decided that my server was
broken. After some experimentation with Firefox debugging telemetry,
and cURL tracing mode, and fiddling with my code to make sure it was
doing the right thing with `Content-Length` etc. I noticed that I was
sending the response with `ngx.say()` instead of `ngx.print()`: `say`
appends a newline, so I had a byte of stray garbage after my DNS
packet.

Once I fixed that, Firefox was happy! It’s useful to have such a pedantic
client to test against :-)

doh101 vs HTTP
--------------

It became clear yesterday that the current DoH draft is a bit unclear
about the dividing line between the DNS part and the HTTP part. I
wasn't the only person that noticed this *lacuna*: on the way into
London this morning I wrote up some
[notes on error handling in DNS over HTTPS](https://www.ietf.org/mail-archive/web/doh/current/msg00283.html),
and by the time I was ready to send my notes to the list I found that
Ted Hardie and Patrick McManus had already started discussing the
topic. I think my notes had some usefully concrete suggestions.

Still to do
-----------

The second item on yesterday's TODO list was to improve the connection
handling on the back end of my DoH proxy. I did not make any progress
on that today; at the moment I don't know if it is worth spending more
time on this code, or whether it would be better to drop to C and help
to make an even more light-weight
[NGINX DoH module](https://github.com/jedisct1/nginx-doh-module).
