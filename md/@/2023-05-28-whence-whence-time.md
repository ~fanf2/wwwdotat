---
comments:
  Cohost: https://cohost.org/fanf/post/1573033-on-my-blog
  Dreamwidth: https://fanf.dreamwidth.org/142513.html
  Fediverse: https://mendeddrum.org/@fanf/110435364199507263
  Twitter: https://twitter.com/fanf/status/1662143991314366476
...
Where does "where does my computer get the time from?" come from?
=================================================================

I am pleased that so many people enjoyed
[my talk about time at RIPE86](https://dotat.at/@/2023-05-26-whence-time.html).
I thought I would write a few notes on some of the things I left out.

<toc>

genesis
-------

There were a couple of things that I thought would make a fun talk:

* Just how many layers can I peel back? At least 11, apparently! I
  thought of doing a joke based on the [9 layer model][] and [Spın̈al
  Tap][eleven], but in the end I kept the intro straight.

[9 layer model]: https://shop.isc.org/collections/shirts/products/osi-9-layer-model-t-shirt
[eleven]:https://en.wikipedia.org/wiki/Up_to_eleven

* It's surprising how important the US Naval Observatory has been.
  What was relatively new to me were the details of how Essen and
  Markowitz calibrated the first atomic clock, which was enough to
  give the story a good dose of "repeat until funny".


Essen & Markowitz
-----------------

A curious detail I have not found an answer to is why Louis Essen
teamed up with William Markowitz to calibrate his clock, not someone
more local such as the Astronomer Royal. It's clear that Markowitz had
a good deal of enthusiasm for the project, but I wonder if there were
logistical reasons too: at that time the Royal Greenwich Observatory
was moving to [Herstmonceux][].

[Herstmonceux]: https://en.wikipedia.org/wiki/Royal_Observatory,_Greenwich#The_Royal_Observatory_at_Herstmonceux


Galileo
-------

I did not mention any satellite navigation systems other than GPS. I
don't know much about [Galileo's ground segment][galileo] (its
equivalent to Schriever SFB and the USNO) - I should really read more
of [Bert Hubert][berthub]'s writing on the topic!

[galileo]: https://berthub.eu/articles/posts/galileos-ground-segment-problem/
[berthub]: https://berthub.eu/


zones
-----

The layers I was peeling back are all below the complications of time
zones; I couldn't fit them neatly into the narrative. But there's an
interesting parallel (as it were) with the [International Meridian
Conference][], which is the basis of the standard time zones.

Years ago I read the proceedings of the meridian conference, and it
struck me that the French were quite opinionated, and grumpy that
Greenwich had the lead. The Americans, who were the conveners,
preferred Greenwich as the status quo. The British were relatively
quiet - perhaps they lobbied off the record, or were just smug?

In the end, Greenwich won largely because it was already the meridian
used by the majority of nautical charts. But it's another example of
international science being led by the French and Americans.

[International Meridian Conference]: https://en.wikipedia.org/wiki/International_Meridian_Conference


BIH
---

I think I was a bit unfair to the Paris Observatory, which had a more
important role than my talk suggested.

Before responsibility for international time was split in the 1980s
between the [IERS][IERS] and the [BIPM][CCTF], there was the [Bureau
International de l'Heure][BIH], which was responsible for both earth
rotation and atomic time. It was based at the Paris Observatory.

Relatedly, I skipped over the awkward history of atomic time between
the 1950s and 1970s (and today). I previously wrote about that in my
[update on leap seconds][leapsecs] last year.

[IERS]: https://en.wikipedia.org/wiki/International_Earth_Rotation_and_Reference_Systems_Service
[CCTF]: https://www.bipm.org/en/committees/cc/cctf
[BIH]: https://en.wikipedia.org/wiki/International_Time_Bureau
[leapsecs]: https://dotat.at/@/2022-12-04-leap-seconds.html


irony
-----

Finally, the last slide has the punch line that the Royal Greenwich
Observatory did not show up in the story at all. I could have
delivered the joke better: I think it needed a bit more context to
land effectively.

I think it's funny to deliver this story as an English person who
lives almost on top of the Greenwich meridian; in fact for the last
few years of its life, the rump of the RGO was based in Cambridge, and
after it finally closed, my spouse Rachel worked in [Greenwich House][].

[Greenwich House]: https://en.wikipedia.org/wiki/Royal_Observatory,_Greenwich#The_Royal_Observatory_at_Cambridge
