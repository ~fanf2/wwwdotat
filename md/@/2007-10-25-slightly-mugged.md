---
dw:
  anum: 66
  eventtime: "2007-10-25T22:06:00Z"
  itemid: 310
  logtime: "2007-10-25T21:55:17Z"
  props:
    commentalter: 1491292378
    hasscreened: 1
    import_source: livejournal.com/fanf/80125
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/79426.html"
format: casual
lj:
  anum: 253
  can_comment: 1
  ditemid: 80125
  event_timestamp: 1193349960
  eventtime: "2007-10-25T22:06:00Z"
  itemid: 312
  logtime: "2007-10-25T21:55:17Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 39
  url: "https://fanf.livejournal.com/80125.html"
title: Slightly mugged
...

On Thursday evenings I go to <a href="http://www.thecarltonarms.co.uk/">the pub</a> for a few beers with friends. The route I walk from work goes <a href="http://maps.google.com/maps?t=h&ll=52.211295,0.120903&z=17">across Jesus Green from Lower Park St to Carlyle Rd</a>. This evening it was darker than usual, because of cloud cover and the lamp on the path being broken. There's usually a few people walking across the park but this evening it was quiet.

As I was approaching the Jesus Lock end at about 20:30, I saw three youths cutting across from the right-hand avenue onto then path. They walked towards me widely spread abreast, so that two of them were on the grass and I had plenty of space to pass between them. As I did so, one of them pushed me over onto the grass. I said "cunt!" or something like that, and he started to have a bit of a go with feet and fists saying "giz ya phone". He wasn't hitting hard, so I told him it was worthless to him. I tried briefly to get up and fight back but this made them hid harder (one or two of the others joined in) and I found that curling up on the ground and protecting my head didn't excite them. Soon they lost their bottle and ran off towards Park St.

They didn't get my phone, or the £60 in my wallet, and only bruised me slightly. Sore cheek and forearm, and back is fine despite taking most of the blows. I was more annoyed by them than scared - it seemed that Dickhead #1 was just showing off, and not really intent on theft or injury.

What a bore. I was going to have to call the police to report the crime, but I wasn't hurt and hadn't lost anything, but it was just enough to utterly disrupt my evening. Chiz.

<b>At this point I should have dialled 999</b> on the phone that the chavs were too crap to steal properly. However I wasn't hurt and I did not see enough of them to be able to identify them (dark clothes, dark night), so I called Rachel to get the Police non-emergency number. I didn't get through to anyone until about 10 minutes after the mugging, at which point I find out that the Police wanted to take it much more seriously, and if I had called 999 they might have been able to see something useful with their CCTV cameras. Silly me.

I walk home to reassure Rachel that I am fine, and soon a couple of coppers come to take a verbal statement and give me an incident number. They classified it as <strike>assaulted by common people</strike> common assault (but not attempted robbery, since they were just being dickheads).

After that I made myself some dinner and Rachel and I settled down to watch yesterday's episode of Heroes. But Charles managed to have a suck on the DVR buttons (which we usually try to prevent) and they didn't work. Faugh, I say! Faugh and thrice faugh!

Time to find another beer and see if half an hour in the airing cupboard has fixed the buttons.
