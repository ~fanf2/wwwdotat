---
dw:
  anum: 43
  eventtime: "2003-03-17T18:20:00Z"
  itemid: 1
  logtime: "2003-03-17T10:45:26Z"
  props:
    commentalter: 1491292303
    import_source: livejournal.com/fanf/422
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/299.html"
format: casual
lj:
  anum: 166
  can_comment: 1
  ditemid: 422
  event_timestamp: 1047925200
  eventtime: "2003-03-17T18:20:00Z"
  itemid: 1
  logtime: "2003-03-17T10:45:26Z"
  props: {}
  reply_count: 6
  url: "https://fanf.livejournal.com/422.html"
title: First post!!!!!
...

The prospect of imminent doom made doing something with my shiny new LJ account seem like a much better idea than work. Thanks to <a href="https://j4.livejournal.com/">👤j4</a> for heading up the pressgang -- though with about 99% of my meatworld friends being on this thing it has been getting increasingly hard to avoid. Like I need more ways to procrastinate :-)
