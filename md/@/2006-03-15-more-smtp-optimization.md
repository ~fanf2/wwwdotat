---
dw:
  anum: 152
  eventtime: "2006-03-15T04:15:00Z"
  itemid: 207
  logtime: "2006-03-15T04:20:51Z"
  props:
    commentalter: 1491292339
    import_source: livejournal.com/fanf/53255
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/53144.html"
format: casual
lj:
  anum: 7
  can_comment: 1
  ditemid: 53255
  event_timestamp: 1142396100
  eventtime: "2006-03-15T04:15:00Z"
  itemid: 208
  logtime: "2006-03-15T04:20:51Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/53255.html"
title: More SMTP optimization
...

A couple of weeks ago I was working on the SMTP QUICKSTART specification, which optimizes the start of the SMTP connection.

https://fanf2.user.srcf.net/hermes/doc/qsmtp/draft-fanf-smtp-quickstart.html

I have now written a specification for SMTP transaction replay, which allows you to use the existing CHUNKING and PIPELINING extensions to acheive the maximum speed possible for bulk data transfer, whilst still behaving correctly if the connection is lost. I imagine this will be most useful clients on slow intermittent links, like mobile wireless users or people on the wrong end of a satellite link.

https://fanf2.user.srcf.net/hermes/doc/qsmtp/draft-fanf-smtp-replay.html

Gosh, over 5000 words...
