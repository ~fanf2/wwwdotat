---
format: html
lj:
  anum: 4
  can_comment: 1
  ditemid: 127748
  event_timestamp: 1376479620
  eventtime: "2013-08-14T11:27:00Z"
  itemid: 499
  logtime: "2013-08-14T10:27:09Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/127748.html"
title: "Subverting BIND's SRTT algorithm: derandomizing NS selection"
...

<p>This morning I saw a link on Twitter to a paper that was presented at <a href="https://www.usenix.org/conference/woot13/tech-schedule/workshop-program">the USENIX Workshop on Offensive Technologies</a> this week. Although it sounds superficially interesting, the vulnerability is (a) not new and (b) not much of a vulnerability.

<p>The starting point of the paper is query randomization to make cache poisoning harder. They cite <a href="http://tools.ietf.org/html/rfc5452">RFC 5452</a>, saying:

<blockquote>The most common values which the resolver randomizes are DNS transaction ID
(TXID), UDP source port and the IP address of the queried name server.</blockquote>

<blockquote>In this work we present a newly discovered vulnerability in BIND which allows an attacker to determine (derandomize) the IP address of the name server a BIND resolver queries. The attack reduces the amount of information a blind attacker must guess to successfully poison BIND's cache.</blockquote>

<p>The problem is that this exact vulnerability is described in RFC 5452 section 4.4:

<blockquote>
   Many zones have two or three authoritative nameservers, which make
   matching the source address of the authentic response very likely
   with even a naive choice having a double digit success rate.

   Most recursing nameservers store relative performance indications of
   authoritative nameservers, which may make it easier to predict which
   nameserver would originally be queried -- the one most likely to
   respond the quickest.
</blockquote>

<p>This vulnerability reduces the amount of randomness in the query by about one bit, and so it is fairly trivial. If you care that much about query randomness you should implement the dns0x20 hack to randomize the case of the query name.

<p>Of course the real defence against cache poisoning attacks is DNSSEC.

<p>The actual technique they use to exploit the vulnerability is new and quite clever. The real impact is that BIND's SRTT algorithm sometimes calculates bogus values, when an NS RRset has a mixture of lame and working nameservers and overlaps with other NS RRsets. Bogus SRTT values can slightly increase average query latency.

<p>It seems to me to be a stretch to claim this is a security bug rather than a performance bug.
