---
dw:
  anum: 222
  eventtime: "2014-11-27T15:52:00Z"
  itemid: 412
  logtime: "2014-11-27T15:52:45Z"
  props:
    commentalter: 1491292431
    import_source: livejournal.com/fanf/132656
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/105694.html"
format: html
lj:
  anum: 48
  can_comment: 1
  ditemid: 132656
  event_timestamp: 1417103520
  eventtime: "2014-11-27T15:52:00Z"
  itemid: 518
  logtime: "2014-11-27T15:52:45Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 15
  url: "https://fanf.livejournal.com/132656.html"
title: Uplift from SCCS to git
...

<p>My current project is to replace Cambridge University's DNS
servers. The first stage of this project is to transfer the code from
SCCS to Git so that it is easier to work with.</p>

<p>Ironically, to do this I have ended up spending lots of time
working with SCCS and RCS, rather than Git. This was mainly developing
analysis and conversion tools to get things into a fit state for Git.<p>

<p>If you find yourself in a similar situation, you might find these
tools helpful.</p>

<h2>Background</h2>

<p>Cambridge was allocated three Class B networks in the 1980s:
first <a href="https://tools.ietf.org/html/rfc1020#page-11">the
Computer Lab got 128.232.0.0/16 in 1987</a>;
then <a href="https://tools.ietf.org/html/rfc1062#page-15">the
Department of Engineering got 129.169.0.0/16 in 1988</a>; and
eventually <a href="https://tools.ietf.org/html/rfc1117#page-23">the
Computing Service got 131.111.0.0/16 in 1989</a> for the University
(and related institutions) as a whole.</p>

<p>The oldest records I have found date from September 1990, which
list about 300 registrations. The next two departments to get
connected were the Statistical Laboratory and Molecular Biology (I
can't say in which order). The Statslab was allocated 131.111.20.0/24,
which it has kept for 24 years!. Things pick up in 1991, when the
JANET IP Service was started and rapidly took over to replace X.25.
(Last month I blogged
about <a href="http://fanf.livejournal.com/131846.html">connectivity
for Astronomy in Cambridge in 1991</a>.)</p>

<p>I have found these historical nuggets in our ip-register directory
tree. This contains the infrastructure and history of IP address and
DNS registration in Cambridge going back a quarter century. But it
isn't just an archive: it is a working system which has been in
production that long. Because of this, converting the directory tree
to Git presents certain challenges.</p>

<h2>Developmestuction</h2>

<p>The ip-register directory tree contains a mixture of:</p>
<ul>
<li>Source code, mostly with SCCS history</li>
<li>Production scripts, mostly with SCCS history</li>
<li>Configuration files, mostly with SCCS history</li>
<li>The occasional executable</li>
<li>A few upstream perl libraries</li>
<li>Output files and other working files used by the production scripts</li>
<li>Secrets, such as private keys and passwords</li>
<li>Mail archives</li>
<li>Historical artifacts, such as old preserved copies of parts of the
 directory tree</li>
<li>Miscellaneous files without SCCS history</li>
<li>Editor backup files with ~ suffixes</li>
</ul>

<p>My aim was to preserve this all as faithfully as I could, while
converting it to Git in a way that represents the history in a useful
manner.</p>

<h2>PLN</h2>

<p>The rough strategy was:</p>

<ol>

<li>Take a copy of the ip-register directory tree, preserving
modification times. (There is no need to preserve owners because any
useful ownership information was lost when the directory tree moved
off the Central Unix Service before that shut down in 2008.)</li>

<li>Convert from SCCS to RCS file-by-file. Converting between these
formats is a simple one-to-one mapping.</li>

<li>Files without SCCS history will have very short artificial RCS
histories created from their modification times and editor backup
files.</li>

<li>Convert the RCS tree to CVS. This is basically just moving files
around, because a CVS repository is little more than a directory tree
of RCS files.</li>

<li>Convert the CVS repository to Git
using <tt>git</tt> <tt>cvsimport</tt>. This is the only phase that
needs to do cross-file history analysis, and other people have already
produced a satisfactory solution.</li>

</ol>

<p>Simples! ... Not.</p>

<h2>sccs2rcs proves inadequate</h2>

<p>I first tried <a href="http://www.catb.org/~esr/sccs2rcs/">ESR's
sccs2rcs</a> Python script. Unfortunately I rapidly ran into a number
of showstoppers.</p>
<ul>
<li>It didn't work with Solaris SCCS, which is what was available on
the ip-register server.</li>
<li>It destructively updates the SCCS tree, losing information about
the relationship between the working files and the SCCS files.</li>
<li>It works on a whole directory tree, so it doesn't give you
file-by-file control.</li>
</ul>
<p>I fixed a bug or two but very soon concluded the program was
entirely the wrong shape.</p>
<p>(In the end, the Solaris incompatibility became moot when I
installed GNU CSSC on my FreeBSD workstation to do the conversion.
But the other problems with sccs2rcs remained.)</p>

<h2>sccs2rcs1</h2>

<p>So I wrote a small script
called <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/sccs2rcs1">sccs2rcs1</a>
which just converts one SCCS file to one RCS file, and gives you
control over where the RCS and temporary files are placed. This meant
that I would not have to shuffle RCS files around: I could just create
them directly in the target CVS repository. Also, sccs2rcs1 uses RCS
options to avoid the need to fiddle with checkout locks, which is a
significant simplification.<p>

<p>The main regression compared to sccs2rcs is that sccs2rcs1 does not
support branches, because I didn't have any files with branches.</p>

<h2>sccscheck</h2>

<p>At this point I needed to work out how I was going to co-ordinate
the invocations of sccs2rcs1 to convert the whole tree. What was in
there?!<p>

<p>I wrote a fairly quick-and-dirty script
called <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/sccscheck">sccscheck</a>
which analyses a directory tree and prints out notes on various
features and anomalies. A significant proportion of the code exists to
work out the relationship between working files, backup files, and
SCCS files.</p>

<p>I could then start work on determining what fix-ups were necessary
before the SCCS-to-CVS conversion.</p>

<h2>sccsprefix</h2>

<p>One notable part of the ip-register directory tree was the archive
subdirectory, which contained lots of gzipped SCCS files with date
stamps. What relationship did they have to each other? My first guess
was that they might be successive snapshots of a growing history, and
that the corresponding SCCS files in the working part of the tree
would contain the whole history.</p>

<p>I
wrote <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/sccsprefix">sccsprefix</a>
to verify if one SCCS file is a prefix of another, i.e. that it
records the same history up to a certain point.</p>

<p>This proved that the files were NOT snapshots! In fact, the working
SCCS files had been periodically moved to the archive, and new working
SCCS files started from scratch. I guess this was to cope with the
files getting uncomfortably large and slow for 1990s hardware.</p>

<h2>rcsappend</h2>

<p>So to represent the history properly in Git, I needed to combine a
series of SCCS files into a linear history. It turns out to be easier
to construct commits with artificial metadata (usernames, dates) with
RCS than with SCCS, so I
wrote <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/rcsappend">rcsappend</a>
to add the commits from a newer RCS file as successors of commits in
an older file.<p>

<p>Converting the archived SCCS files was then a combination of
sccs2rcs1 and rcsappend. Unfortunately this was VERY slow, because RCS
takes a long time to check out old revisions. This is because an RCS
file contains a verbatim copy of the latest revision and a series of
diffs going back one revision at a time. The SCCS format is more
clever and so takes about the same time to check out any revision.</p>

<p>So I changed sccs2rcs1 to incorporate an append mode, and used that
to convert and combine the archived SCCS files, as you can see in
the <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/ipreg-archive-uplift">ipreg-archive-uplift</a>
script. This still takes ages to convert and linearize nearly 20,000
revisions in the history of the hosts.131.111 file - an RCS checkin
rewrites the entire RCS file so they get slower as the number of
revisions grows. Fortunately I don't need to run it many times.</p>

<h2>files2rcs</h2>

<p>There are a lot of files in the ip-register tree without SCCS
histories, which I wanted to preserve. Many of them have old editor
backup ~ files, which could be used to construct a wee bit of history
(in the absence of anything better). So I
wrote <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/files2rcs">files2rcs</a>
to build an RCS file from this kind of miscellanea.</p>

<h2>An aside on file name restrictions</h2>

<p>At this point I need to moan a bit.</p>

<p>Why does RCS object to file names that start with a comma. Why.</p>

<p>I tried running these scripts on my Mac at home. It mostly worked,
except for the directories which contained files like <tt>DB.cam</tt>
(source file) and <tt>db.cam</tt> (generated file). I added a bit of
support in the scripts to cope with case-insensitive filesystems, so I
can use my Macs for testing. But the bulk conversion runs <i>very</i>
slowly, I think because it generates too much churn in the Spotlight
indexes.</p>

<h2>rcsdeadify</h2>

<p>One significant problem is dealing with SCCS files whose working
files have been deleted. In some SCCS workflows this is a normal state
of affairs - see for instance the SCCS support in
the <a href="http://pubs.opengroup.org/onlinepubs/9699919799/utilities/make.html">POSIX
Make XSI extensions</a>. However, in the ip-register directory tree
this corresponds to files that are no longer needed. Unfortunately the
SCCS history generally does not record when the file was deleted. It
might be possible to make a plausible guess from manual analysis, but
perhaps it is more truthful to record an artificial revision saying
the file was not present at the time of conversion.</p>

<p>Like SCCS, RCS does not have a way to represent a deleted file. CVS
uses a convention on top of RCS: when a file is deleted it puts the
RCS file in an "Attic" subdirectory and adds a revision with a "dead"
status.
The <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/rcsdeadify">rcsdeadify</a> applies this convention to an RCS file.</p>

<h2>tar2usermap</h2>

<p>There are situations where it is possible to identify a meaningful
committer and deletion time. Where a .tar.gz archive exists, it
records the original file owners.
The <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/tar2usermap">tar2usermap</a>
script records the file owners from the tar files. The contents can
then be unpacked and converted as if they were part of the main
directory, using the usermap file to provide the correct committer
IDs. After that the files can be marked as deleted at the time the
tarfile was created.</p>

<h2>sccs2cvs</h2>

<p>The main conversion script
is <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/sccs2cvs">sccs2cvs</a>,
which evacuates an SCCS working tree into a CVS repository, leaving
behind a tree of (mostly) empty directories. It is based on a
simplified version of the analysis done by sccscheck, with more
careful error checking of the commands it invokes. It uses
sccs2rcs1, files2rcs, and rcsappend to handle each file.</p>

<p>The rcsappend case occurs when there is an editor backup ~ file
which is older than the oldest SCCS revision, in which case sccs2cvs
uses rcsappend to combine the output of sccs2rcs1 and files2rcs. This
could be done more efficiently with sccs2rcs1's append mode, but for
the ip-register tree it doesn't cause a big slowdown.</p>

<p>To cope with the varying semantics of missing working files,
sccs2rcs leaves behind a tombstone where it expected to find a working
file. This takes the form of a symlink pointing to 'Attic'. Another
script can then deal with these tombstones as appropriate.</p>

<h2>pre-uplift, mid-uplift, post-uplift</h2>

<p>Before sccs2cvs can run, the SCCS working tree should be reasonably
clean. So the overall uplift process goes through several phases:</p>

<ol>
<li>Fetch and unpack copy of SCCS working tree;</li>
<li><a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/ipreg-pre-uplift">pre-uplift fixups</a>;<br>
  (These should be the minimum changes that are required before
  conversion to CVS, such as moving secrets out of the working
  tree.)</li>
<li>sccs2cvs;</li>
<li><a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/ipreg-mid-uplift">mid-uplift fixups</a>;<br>
  (This should include any adjustments to the earlier history
  such as marking when files were deleted in the past.)</li>
<li>git cvsimport or cvs-fast-export | git fast-import;</li>
<li><a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/ipreg-post-uplift">post-uplift fixups</a>;<br>
  (This is when to delete cruft which is now preserved in the git history.)</li>
</ol>

<p>For the ip-register directory tree, the pre-uplift phase also
includes ipreg-archive-uplift which I described earlier. Then in the
mid-uplift phase the combined histories are moved into the proper
place in the CVS repository so that their history is recorded in the
right place.</p>

<p>Similarly, for the tarballs, the pre-uplift phase unpacks them in
place, and moves the tar files aside. Then the mid-uplift phase
rcsdeadifies the tree that was inside the tarball.</p>

<p>I have not stuck to my guidelines very strictly: my scripts delete
quite a lot of cruft in the pre-uplift phase. In particular, they
delete duplicated SCCS history files from the archives, and working
files which are generated by scripts.</p>

<h2>sccscommitters</h2>

<p>SCCS/RCS/CVS all record committers by simple user IDs, whereas git
uses names and email addresses. So git-cvsimport and cvs-fast-export
can be given an authors file containing the translation.
The <a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/sccscommitters">sccscommitters</a>
script produces a list of user IDs as a starting point for an authors
file.</p>

<h2>Uplifting cvs to git</h2>

<p>At first I tried <tt>git</tt> <tt>cvsimport</tt>, since I have
successfully used it before. In this case it turned out not to be the
path to swift enlightenment - it was taking about 3s per commit. This
is mainly because it checks out files from oldest to newest, so it
falls foul of the same performance problem that my rcsappend program
did, as I described above.<p>

<p>So I
compiled <a href="http://www.catb.org/~esr/cvs-fast-export/">cvs-fast-export</a>
and fairly soon I had a populated repository: nearly 30,000 commits at
35 commits per second, so about 100 times faster. The
fast-import/export format allows you to provide file contents in any
order, independent of the order they appear in commits. The fastest
way to get the contents of each revision out of an RCS file is from
newest to oldest, so that is what cvs-fast-export does.</p>

<p>There are a couple of niggles with cvs-fast-export, so I have a
<a href="https://git.csx.cam.ac.uk/x/ucs/ipreg/sccs2rcs2cvs2git.git/blob/HEAD:/cvs-fast-export-1.26.patch">patch</a>
which fixes them in a fairly dumb manner (without adding command-line
switches to control the behaviour):</p>
<ul>
  <li>In RCS and CVS style, cvs-fast-export replaces empty commit
    messages with
    &quot;***&nbsp;empty&nbsp;log&nbsp;message&nbsp;***&quot;, whereas
    I want it to leave them empty.</li>
  <li>cvs-fast-export makes a special effort to translate CVS's
    ignored file behaviour into git by synthesizing a .gitignore file
    into every commit. This is wrong for the ip-register tree.</li>
  <li>Exporting the hosts.131.111 file takes a long time, during which
    cvs-fast-export appears to stall. I added a really bad progress
    meter to indicate that work was being performed.</li>
</ul>

<h2>Wrapping up</h2>

<p>Overall this has taken more programming than I expected, and more
time, very much following the pattern that the last 10% takes the same
time as the first 90%. And I think the initial investigations -
before I got stuck in to the conversion work - probably took the same
time again.</p>

<p>There is one area where the conversion could perhaps be improved:
the archived dumps of various subdirectories have been converted in
the location that the tar files were stored. I have not tried to
incorporate them as part of the history of the directories from which
the tar files were made. On the whole I think combining them, coping
with renames and so on, would take too much time for too little
benefit. The multiple copies of various ancient scripts are a bit
weird, but it is fairly clear from the git history what was going
on.</p>

<p>So, let us declare the job DONE, and move on to building new DNS
servers!</p>
