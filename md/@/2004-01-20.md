---
dw:
  anum: 29
  eventtime: "2004-01-20T18:31:00Z"
  itemid: 52
  logtime: "2004-01-20T18:32:18Z"
  props:
    current_moodid: 31
    import_source: livejournal.com/fanf/13464
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/13341.html"
format: casual
lj:
  anum: 152
  can_comment: 1
  ditemid: 13464
  event_timestamp: 1074623460
  eventtime: "2004-01-20T18:31:00Z"
  itemid: 52
  logtime: "2004-01-20T18:32:18Z"
  props:
    current_moodid: 31
  reply_count: 0
  url: "https://fanf.livejournal.com/13464.html"
title: ""
...

Slides finished at last. Talk tomorrow, then I'll post a link here. Thank god for Google Images, it makes writing talks tolerable.

Next task is to write my paper for the <a href="http://www.ukuug.org/events/winter2004/">UKUUG Winter Conference</a>. I'll do it in <code>groff</code> but I'm not sure whether to use <code>.ms</code> or <code>.me</code>.
