---
dw:
  anum: 173
  eventtime: "2018-10-16T00:37:00Z"
  itemid: 503
  logtime: "2018-10-15T22:43:27Z"
  props:
    interface: flat
    picture_keyword: ""
    revnum: 1
    revtime: 1539643555
  url: "https://fanf.dreamwidth.org/128941.html"
title: Amsterdam day 3
...

Same city, same hotel, same lunch menu, but we have switched from
DNS-OARC ([Fri](https://dotat.at/@/2018-10-12-amsterdam-day-0.html)
[Sat](https://dotat.at/@/2018-10-14-amsterdam-day-1.html)
[Sun](https://dotat.at/@/2018-10-14-amsterdam-day-2.html)) to
[RIPE](https://ripe77.ripe.net/programme/meeting-plan/), which entails
a huge expansion in the bredth of topics and number of people. The
DNS-OARC meeting was the biggest ever with 197 attendees; the RIPE
meeting has 881 registrations and at the time of the opening plenary
there were 514 present. And 286 first-timers, including me!

I have some idea of the technical side of RIPE meetings because I have
looked at slides and other material during previous meetings - lots of
great stuff! But being here in person it is striking how much of an
emphasis there is on social networking as well as IP networking:
getting to know other people doing similar things in other companies
in other countries seems to be a really important part of the meeting.

I have met several people today who I only know from mailing lists and
Twitter, and they keep saying super nice things :-)

I'm not going to deep link to each presentation below - look in [the
RIPE77 meeting
programme](https://ripe77.ripe.net/programme/meeting-plan/) for the
presentation materials.

DoT / DoH / DoQ
---------------

The DNS continues to be an major topic :-)

Sara Dickinson did her "DNS, Jim, but not as we know it" talk to
great acclaim, and widespread consternation.

Ólafur Guðmundsson did a different talk, called "DNS over anything but
UDP". His main point is that DNS implementations have appallingly bad
transport protocol engineering, compared to TCP or QUIC. This affects
things like recovery from packet loss, path MTU discovery,
backpressure, and many other things. He argues that the DNS should
make use of all the stateful protocol performance and scalability
engineering that has been driven by the Web.

Some more or less paraphrased quotes:

  - "I used to be a UDP bigot - DNS would only ever be over UDP - I was wrong"

  - "DoH is for reformed script kiddies who have become application developers"

  - "authenticated connections are the only defence against route
    hijacks"

  - "is the community ready if we start moving 50% - 60%
    of DNS traffic over to TCP?"

I've submitted my lightning talk again, though judging from this
afternoon's talks it is perhaps a bit too brief for the RIPE 10
minutes lightning talk length.

ANAME
-----

Mattijs Mekking read through [the ANAME draft](https://github.com/each/draft-aname)
and came back with lots of really helpful feedback, with plenty of
good questions about things that are unclear or missing.

It might be worth finding some time tomorrow to hammer in some
revisions...

Non-DNS things
--------------

First presentation was by Thomas Weible from Flexoptix on 400Gb/s fibre.

  - lovely explanation of how eye diagrams show signal clarity! I did
    not previously understand them and it was delightful to learn!

  - lots of details about transceiver form factors

  - initial emphasis seems to be based on shorter distance limits,
    because that is cheaper

Steinhor Bjarnason from Arbor talked about defending against DDoS attacks.

  - scary "carpet bombing", spreading DDoS traffic across many targets
    so bandwidth is low enough not to trigger alarms but high enough
    to cause problems and really hard to mitigate

  - networks should rate-limit IP fragments, except for addresses
    running DNS resolvers [because DNS-over-UDP is terrible]

  - recommended [port-based rate-limiting config from Job Snijders and
    Jared Mauch](https://twitter.com/JobSnijders/status/1051846330811932672)

Hisham Ibrahim of RIPE NCC on IPv6 for mobile networks

  - it seems there is a lot of confusion and lack of confidence about
    how to do IPv6 on mobile networks in Europe

  - we are well behind the USA and India

  - how to provide "best current operational practice" advice?

  - what to do about vendors that lie about IPv6 support (no naming
    and shaming happened but it sounds like many of the people
    involved know who the miscreants are)
