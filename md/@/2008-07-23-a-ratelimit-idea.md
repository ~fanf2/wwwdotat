---
dw:
  anum: 22
  eventtime: "2008-07-23T10:28:00Z"
  itemid: 348
  logtime: "2008-07-23T11:28:13Z"
  props:
    commentalter: 1491292358
    import_source: livejournal.com/fanf/89919
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/89110.html"
format: casual
lj:
  anum: 63
  can_comment: 1
  ditemid: 89919
  event_timestamp: 1216808880
  eventtime: "2008-07-23T10:28:00Z"
  itemid: 351
  logtime: "2008-07-23T11:28:13Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/89919.html"
title: A ratelimit idea
...

<a href="https://senji.livejournal.com/">👤senji</a> suggested ratelimiting email based on the MD5 checksum of any attachments, with the goal of slowing down an email virus attack. I think this might be feasible so I'm noting it here as a sort of public to-do list entry...
