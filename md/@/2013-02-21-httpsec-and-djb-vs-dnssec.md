---
format: html
lj:
  anum: 246
  can_comment: 1
  ditemid: 125686
  event_timestamp: 1361451600
  eventtime: "2013-02-21T13:00:00Z"
  itemid: 490
  logtime: "2013-02-21T13:00:55Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 5
  url: "https://fanf.livejournal.com/125686.html"
title: "\"HTTPSEC\" and DJB vs DNSSEC"
...

<p>Dan Bernstein recently published <a href="http://cr.yp.to/talks/2013.02.07/slides.pdf">slides for a talk
criticizing DNSSEC</a>. The talk is framed as a description of the
fictitious protocol HTTPSEC, which is sort of what you would get if
you applied the DNSSEC architecture to HTTP. This seems to be a
rhetorical device to make DNSSEC look stupid, which is rather annoying
because it sometimes makes his points harder to understand, and if his
arguments are strong they shouldn't need help from extra sarcasm.</p>

<p>The analogy serves to exaggerate the apparent foolishness because
there are big differences between HTTP and the DNS. HTTP deals with
objects thousands of times larger than the DNS, and (in simple cases)
a web site occupies a whole directory tree whereas a DNS zone is just
a file. Signing a file isn't a big deal; signing a filesystem is
troublesome.</p>

<p>There are also differences in the way the protocols rely on third
parties. Intermediate caches are ubiquitous in the DNS, and relatively
rare in HTTP. The DNS has always relied a lot on third party secondary
authoritative servers; by analogy HTTP has third party content
delivery networks, but these are a relatively recent innovation and
the https security model was not designed with them in mind.</p>

<p>The DNS's reliance on third parties (users relying on their ISP's
recursive caches; master servers relying on off-site slaves) is a key
part of the DNSSEC threat model. It is designed to preserve the
integrity and authenticity of the data even if these intermediaries
are not reliable. That is, DNSSEC is based on data security rather
than channel security.</p>

<p>I like to use email to explain this distinction. When I connect to
my IMAP server over TLS, I am using a secure channel: I know I am
talking to my server and that no-one can falsify the data I receive.
But the email I download over this channel could have reached the
server from anywhere, and it can contain all sorts of fraudulent
messages. But if I get a message signed with PGP or S/MIME I can be
sure that data is securely authentic.</p>

<p>DJB uses the bad analogy with HTTP to mock DNSSEC, describing the
rational consequences of its threat model as "not a good approach" and
"stupid". I would prefer to see an argument that tackles the reasons
for DNSSEC's apparently distasteful design. For example, DJB prefers an
architecture where authoritative servers have private keys used for online
crypto. So if you want outsourced secondary service you have to make a
rather difficult trust trade-off. It becomes even harder when you
consider the distributed anycast servers used by the root and TLDs: a
lot of the current installations cannot be upgraded to the level of
physical security that would be required for such highly trusted
private keys. And there is the very delicate political relationship
between ICANN and the root server operators.</p>

<p>So the design of DNSSEC is based on an assessment that the current
DNS has a lot of outsourcing to third parties that we would
prefer not to have to trust, but at the same time we do not want to
fix this trust problem by changing the commercial and political
framework around the protocol. You might legitimately argue that this
assessment is wrong, but DJB does not do so.</p>

<p>What follows is a summary of DJB's arguments, translated back to
DNSSEC as best I can, with my commentary. The PDF has 180 pages
because of the way the content is gradually revealed, but there are
less than 40 substantive slides.</p>

<p>Paragraphs starting with a number are my summary of the key points
from that page of the PDF. Paragraphs marked with a circle are me
expanding or clarifying DJB's points. Paragraphs marked with a square
are my corrections and counter-arguments.</p>

<ol>
<li value="4"> HTTPSEC motivation
<li value="9"> Standard security goals
<li value="13"> HTTPSEC "HTTP Security"
         <br> responses signed with PGP

<ul type="circle">
<li><p>DNSSEC uses its own signature format: RRSIG records.
</ul>

<li value="20"> Signature verification
         <br> chain of trust

<ul type="circle">
<li><p>Internet Central Headquarters -> ICANN.
</ul>

<li value="24"> Root key management

<ul>

<li type="circle"><p>The description on this slide is enormously
  simplified, which is fair because DNSSEC root key management
  involves a lot of paranoid bureaucracy. But it gets some of these
  tedious details slightly wrong; fortunately this has no bearing on
  the argument.

<li type="square"><p>Access to the DNSSEC root private key HSM requires
  three out of seven Crypto Officers. There are also seven Recovery
  Key Share officers, five of whom can reconstruct the root private
  key. And there are separate people who control physical access to
  the HSM, and people who are there to watch everything going
  according to plan.

<li type="square"><p>Root zone signatures last a week, but the root zone
  is modified several times a day and each modification requires
  signing, using a private key (ZSK) that is more easily accessed than
  the root key (KSK) which only comes out every three months when the
  public keys are signed.

</ul>

<li value="28"> Verifying the chain of trust

<li value="32"> HTTPSEC performance
         <br> Precomputed signatures and no per-query crypto.

<ul type="square">
<li><p>This design decision is not just about performance. It is also
  driven by the threat model.
</ul>

<li value="36"> Clients don't share the work of verifying signatures
         <br> Crypto primitives chosen for performance
         <br> Many options

<ul type="circle">

<li><p>Another consideration is the size of the signatures: smaller is
  better when it needs to fit into a UDP packet, and when the
  signatures are so much bigger than the data they cover.

<li><p>Elliptic curve signatures are now an excellent choice for their
  small size and good performance, but they are relatively new and
  have been under a patent cloud until fairly recently. So DNSSEC
  mostly uses good old RSA which has been free since the patent
  expired in 2000. If crypto algorithm agility works, DNSSEC will be
  able to move to something better than RSA, though it will probably
  take a long time.

<li><p>Compare TLS crypto algorithm upgrades.

</ul>

<li value="40"> Breakable choices such as 640-bit RSA
         <br> Staggering complexity

<ul type="square">

<li><p>Any fixed choice of crypto primitive is going to be broken at some
    point, so there must be some way to upgrade the crypto over
    time. DNSSEC relies on signatures which in turn rely on hashes,
    and hash algorithms have generally had fairly short lifetimes.

<li><p>Compare SSL/TLS's history of weak crypto. Both protocols date
    back to the mid 1990s.

<li><p>The complexity of DNSSEC is more to do with the awkward parts of
    the DNS, such as wildcards and aliases, and not so much the fact
    of crypto algorithm agility.

</ul>

<li value="46"> HTTPSEC does not provide confidentiality

<ul type="circle">

<li><p>Yes this is a bit of a sore point with DNSSEC. But observe that
  in pretty much all cases, immediately after you make a DNS query you
  use the result to connect to a server, and this reveals the contents
  of the DNS reply to a snooper. On the other hand there are going to
  be uses of the DNS which are not so straight-forward, and this will become more
  common as more people use the security properties of DNSSEC to put
  more interesting data in the DNS which isn't just related to IP
  connectivity.

</ul>

<li value="50"> HTTPSEC data model
         <br> signatures alongside data

<ul type="square">

<li><p>This slide makes DNSSEC signing sound a lot more fiddly than it
  actually is. HTTP deals with sprawling directory trees whereas most
  DNS master files are quite small, e.g. 33Mbytes for a large-ish
  signed zone with 51k records. DNSSEC tools deal with zones as a
  whole and don't make the admin worry about individual signatures.

</ul>

<li value="57"> HTTPSEC purists say "answers should always be static"

<ul type="square">

<li><p>In practice DNSSEC tools fully support dynamic modification of
  zones, e.g. the .com zone is updated several times a minute. In many
  cases it is not particularly hard to add DNSSEC signing as a
  publication stage between an existing DNS management system and the
  public authoritative servers, and it often doesn't require any big
  changes to that system.

<li><p>Static data is supported so that it is possible to have offline
  keys like the root KSK, but that does not prevent dynamic data. (For
  a funny example, see <a href="http://bert.secret-wg.org/Tools/">the
  DNSSEC reverse polish calculator</a>.) In a system that requires
  dynamic signatures static data and offline keys are not possible.

</ul>

<li value="58"> Expiry times stop attackers replaying old signatures
         <br> Frequent resigning is an administrative disaster

<ul type="square">

<li><p>This is true for some of the early unforgiving DNSSEC tools,
  but there has been a lot of improvement in usability and reliability
  in the last couple of years.

</ul>

<li value="68"> HTTPSEC suicide examples

<ul type="circle">

<li><p>These look like they are based on real DNSSEC cockups.

<li><p>Many problems have come from the US government DNSSEC
  deployment requirement of 2008, so many .gov sites set it up using
  the early tools with insufficient expertise. It has not been a very
  good advertisement for the technology.

</ul>

<li value="75"> Nonexistent data - how not to do it

<li value="80"> NSEC records for authenticated denial of existence

<li value="85"> NSEC allows zone enumeration

<li value="91"> DNS data is public
         <br> an extreme notion and a public-relations problem

<ul type="circle">

<li><p>The other problem with NSEC is that it imposes a large overhead
  during the early years of DNSSEC deployment where TLDs mostly
  consist of insecure delegations. Every update requires fiddling with
  NSEC records and signatures even though this provides no security
  benefits. NSEC3 opt-out greatly reduces this problem.

</ul>

<li value="96"> NSEC3 hashed denial of existence

<li value="102"> NSEC3 does not completely prevent attackers from enumerating a zone

<ul type="square">

<li><p>This is true but in practice most sites that want to keep DNS
  data private use hidden zones that are only accessible on their
  internal networks.

<li><p>Alternatively, if your name server does on-demand signing rather
  using pre-generated signatures, you can use dynamic
  <a href="http://tools.ietf.org/html/rfc4470">minimally covering NSEC records</a>
  or <a href="http://dnssec-deployment.org/pipermail/dnssec-deployment/2013-February/006366.html">empty NSEC3 records</a>.

<li><p>So there are ways to deal with the zone privacy
  problem if static NSEC3 isn't strong enough for you.

</ul>

<li value="106"> DNS uses UDP

<li value="110"> DNS can be used for amplification attacks

<ul type="circle">

<li><p>A lot of other protocols have this problem too. Yes, DNSSEC
  makes it particularly bad. DNS software vendors are implementing
  <a href="http://www.redbarn.org/dns/ratelimits">response rate
  limiting</a> which eliminates the amplification effect of most
  attacks. Dealing with spam and criminality is all rather ugly.

<li><p>A better fix would be for network providers to implement
  ingress filtering (RFC 2827, BCP 38), but sadly this seems to be an
  impossible task, so higher-level protocols have to mitigate
  vulnerabilities in the network.

</ul>

<li value="116"> DNSSEC provides no protection against denial of service attacks

<ul type="circle">

<li><p>This quote comes from RFC 4033 section 4. I think (though it
  isn't entirely clear) that what the authors had in mind was the fact that
  attackers can corrupt network traffic or stop it, and DNSSEC can do
  nothing to prevent this. See for example section 4.7 of RFC 4035
  which discusses how resolvers might mitigate this kind of DoS
  attack. So the quote isn't really about reflection attacks.

<li><p>(Other protocols have similar problems; for instance, TLS kills
  the whole connection when it encounters corruption, so it relies on
  the difficulty of breaking TCP connections which are not normally
  hardened with crypto - though see the TCP MD5 signature option in
  RFC 2385.)

</ul>

<li value="123"> The worst part of HTTPSEC
          <br>
          <br> The data signed by HTTPSEC doesn’t actually include the
               web pages that the browser shows to the user.
          <br>
          <br> HTTPSEC signs only routing information: specifically,
               30x HTTP redirects.

<ul type="square">

<li><p>I can't easily understand this slide because the analogy
  between DNS and HTTP breaks down. According to the analogy, HTTP
  redirects are DNS referrals, and web pages are leaf RRsets. But
  DNSSEC does sign leaf RRsets, so the slide can't be talking about
  that.

<li><p>Perhaps it is being more literal and it is talking about actual
  web pages. The DNSSEC answer is that you use records such as SSHFP
  or TLSA to link the DNSSEC authentication chain to your application
  protocol.

<li><p>I asked DJB about this on Twitter, and he confirmed the latter
  interpretation. But he complained that the transition from signed
  DNSSEC data to an encrypted application channel 
  <a href="https://twitter.com/hashbreaker/status/304313501508325377">destroys</a>
  the advantages of signed DNSSEC data, because of the different
  security models behind signed data and encrypted channels.

<li><p>But in this situation we are using DNSSEC as a PKI. The X.509
  PKI is also based on statically signed data (the server certificate)
  which is used to authenticate a secure channel.

</ul>

<li value="127"> redirect example
<li value="131"> redirect example
<li value="135"> redirect example

<li value="141"> If final web page is signed, what is the security benefit of
     signing the redirects? Attacker can’t forge the page.

<ul type="square">

<li><p>The answer to this question is how you trust the signature on the
    web page. At the moment we rely on X.509 which is inadequate in a
    lot of ways. DNSSEC is a new PKI which avoids some of the
    structural problems in the X.509 PKI. This is the reason I think
    it is so important.

<li><p>The X.509 PKI was designed to follow the structure of the X.500
    directory. When it got re-used for SSL and S/MIME it became
    decoupled from its original name space. Because of this, any CA
    can authenticate any name, so every name is only as strong as the
    weakest CA.

<li><p>DNSSEC follows the structure of the Internet's name space. Its
    signing authorities are the same as DNS zone authorities, and they
    can only affect their subdomains. A British .uk name cannot be
    harmed by the actions of the Libyan .ly authorities.

<li><p>What other global PKIs are there? PGP? Bueller?

</ul>

<li value="147"> Deployment is hard

<ul type="circle">

<li><p>If you look at countries like Sweden, Czech, Netherlands,
  Brazil, there is a lot more DNSSEC than elsewhere. They have used
  financial incentives (domain registration discounts for signed
  domains) to make it more popular. Is DNSSEC worth this effort? See
  above.

<li><p>It's amusing to consider the relative popularity of DNSSEC and
  PGP and compare their usage models.

</ul>

<li value="151"> HTTPS is good

<ul type="square">
<li><p>But it relies on the X.509 PKIX which is a disaster.
  Peter Gutmann wrote a series of articles with many examples of why:
  <a href="http://www.metzdowd.com/pipermail/cryptography/2010-July/016188.html">I</a>,
  <a href="http://www.metzdowd.com/pipermail/cryptography/2010-July/016203.html">II</a>,
  <a href="http://www.metzdowd.com/pipermail/cryptography/2010-September/016687.html">III</a>;
  and the subsequent mailing list discussion is also worth reading.
</ul>

<li value="160">The following quotes are straw-man arguments for why
  an https-style security model isn't appropriate for DNSSEC.

<li value="160"> “HTTPS requires keys to be constantly online.”

<ul type="square">

<li><p>So does DNSSEC in most setups. You can fairly easily make DNSSEC
    keys less exposed than a TLS private key, using a hidden master.
    This is a fairly normal non-sec DNS setup so it's nice that DNSSEC
    can continue to use this structure to get better security.
</ul>

<li value="160"> “HTTPS requires servers to use per-query crypto.”

<ul type="square">
<li><p>So does NSEC3.
</ul>

<li value="160"> “HTTPS protects only the channel, not the data. It
               doesn’t provide end-to-end security.”
	  <br> Huh? What does this mean?

<ul type="square">
<li><p>See the discussion in the introduction about the DNSSEC threat
  model and the next few notes.
</ul>

<li value="167"> Why is the site owner putting his data on an untrusted server?

<ul type="square">

<li><p>Redundancy, availability, diversity, scale. The DNS has always
  had third-party secondary authoritative name servers. HTTP also does
  so: content delivery networks. The difference is that with DNSSEC
  your outsourced authoritative servers can only harm you by ceasing
  to provide service: they cannot provide false service; HTTP content
  delivery networks can mess up your data as much as they like, before
  serving it "securely" to your users with a certificate bearing your
  name.

</ul>

<li value="167"> “HTTPS destroys the caching layer. This Matters.”
     <br> Yeah, sure it does. Film at 11: Internet Destroyed By HTTPS.

<ul type="square">

<li><p>Isn't it nicer to get an answer in 3ms instead of 103ms?

<li><p>Many networks do not provide direct access to DNS authoritative
  servers: you have to use their caches, and their caches do not
  provide anything like the web proxy HTTP CONNECT method - or at
  least they are not designed to provide anything like that. A similar
  facility in the DNS would have to be an underhanded
  crypto-blob-over-DNS tunnel hack: a sort of anarchist squatter's
  approach to protocol architecture.

<li><p>To be fair, a lot of DNS middleboxes have crappy
  DNSSEC-oblivious implementations, and DNSSEC does not cope with them
  at all well. Any security upgrade to the DNS probably can't be done
  without upgrading everything.

</ul>

<li value="171"> The DNS security mess
<li value="175"> referral example
<li value="180"> HTTPSEC was all a horrible <strike>dream</strike> analogy

</ol>

<p>So to conclude, what DJB calls the worst part of DNSSEC - that it
secures the DNS and has flexible cryptographic coupling to other
protocols - is actually its best part. It is a new global PKI, and
with sufficient popularity it will be better than the old one.</p>

<p>I think it is sad that someone so prominent and widely respected is
discouraging people from deploying and improving DNSSEC. It would be
more constructive to (say) add his rather nice Curve25519 algorithm
to DNSSEC.</p>

<p>If you enjoyed reading this article, you might also like to read
<a href="http://dankaminsky.com/2011/01/05/djb-ccc/">Dan Kaminsky's
review of DJB's talk at 27C3</a> just over two years ago.</p>
