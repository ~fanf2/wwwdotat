---
dw:
  anum: 61
  eventtime: "2006-05-04T12:01:00Z"
  itemid: 226
  logtime: "2006-05-04T12:20:48Z"
  props:
    commentalter: 1491292333
    import_source: livejournal.com/fanf/58201
    interface: flat
    opt_backdated: 1
    picture_keyword: weather
    picture_mapid: 5
  url: "https://fanf.dreamwidth.org/57917.html"
format: casual
lj:
  anum: 89
  can_comment: 1
  ditemid: 58201
  event_timestamp: 1146744060
  eventtime: "2006-05-04T12:01:00Z"
  itemid: 227
  logtime: "2006-05-04T12:20:48Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/58201.html"
title: AfNOG @ Nairobi
...

Weather: comfortable, high teens to low twenties celsius. Warmer when the sun comes out, which is rarely. Thunder and rain. Nairobi is only a little way south of the equator but it's nearly 1700m up, hence the somewhat temperate climate.

The hotel is quite civilized, with a nice restaurant and good coffee :-)

This morning we had meetings to decide the curricula, which get adjusted from year to year as the Internet and its software evolves. There are now four tracks: F2 is the French routing track, E2 is the English routing track, E1 is the services track (which I am helping with), and E0 is the Unix basics track. (E0 was introduced last year because many students did not have the background knowledge necessary for E1 and E2.)

E1 is roughly as follows. First 1.5 days of DNS then 0.5 days of web stuff. This year we are ditching Squid in favour of more about authentication, specifically RADIUS. Then 1 day of SMTP (Exim, presented by me) and 1 day of POP&IMAP. Finally half a day to consolidate the security stuff that has been done in the course of the previous subjects - SSL certificates and so forth.

I am going to have to adjust Philip Hazel's teaching materials so that we can do SMTP AUTH backed with RADIUS. This should be nice and easy, since FreeBSD (our Unix of choice) comes with the libradius client library, which Exim supports.
