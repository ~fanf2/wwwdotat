---
dw:
  anum: 134
  eventtime: "2009-08-10T13:35:00Z"
  itemid: 390
  logtime: "2009-08-10T13:41:59Z"
  props:
    commentalter: 1491292381
    import_source: livejournal.com/fanf/101605
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/99974.html"
format: html
lj:
  anum: 229
  can_comment: 1
  ditemid: 101605
  event_timestamp: 1249911300
  eventtime: "2009-08-10T13:35:00Z"
  itemid: 396
  logtime: "2009-08-10T13:41:59Z"
  props:
    personifi_tags: "28:5,2:10,18:10,35:5,10:15,11:5,42:5,29:5,3:21,25:5,5:5,9:57,nterms:yes"
    verticals_list: life
  reply_count: 20
  url: "https://fanf.livejournal.com/101605.html"
title: "Sainsbury's self-checkout fail"
...

<p>I hate plastic carrier bags, so I take a canvas bag when I go shopping (unless I forget). The city-centre Sainsbury's in Cambridge recently replaced their express tills with self-checkout tills, which use weighing scales under the bags to check that scanned things have been bagged properly.</p>

<p>If you put an unexpected object in the bagging area it flashes up an annoying message. This screen has a button on it saying "Using your own bag?" which I am pretty sure used to cancel the message and allow you to scan your shopping. It is now non-functional: if you try pressing it a member of staff has to come over and explain the correct procedure. If you want to use your own bag, before you put it on the bagging area you have to go into the "select an item" menu (for bananas and fresh bread etc.) and choose the "bag re-use" icon. <i>Obviously.</i></p>
