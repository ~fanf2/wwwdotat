---
format: html
lj:
  anum: 129
  can_comment: 1
  ditemid: 119169
  event_timestamp: 1332876420
  eventtime: "2012-03-27T19:27:00Z"
  itemid: 465
  logtime: "2012-03-27T18:28:32Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 9
  url: "https://fanf.livejournal.com/119169.html"
title: Pogonotomy
...

<p>I've been shaving with a traditional double-edge safety razor for
about 20 months now. It's good. There are a couple of reasons I
switched.</p>

<p>The main one is <a href="http://www.drmaciver.com/2011/12/and-now-for-something-completely-different/">the
insultingly exploitative business model of the shaving gear
manufacturers</a>. Since the late 1960s <a href="http://en.wikipedia.org/wiki/Gillette_%28brand%29#Newer_products">they
have been ratcheting up the complexity and cost of razors</a> in order
to extract more profit from their customers. It's a classic example of
patent-driven innovation: they have to keep coming up with new
gimmicks that they can monopolize, and use bulshytt to convince
people to buy these "better" razors instead of choosing on the basis
of objective value or quality. This process has been <a href="http://wiki.badgerandblade.com/Parody">obviously ridiculous</a>
practically since the introduction of cartridge razors, and has long
since passed the stage of blatant self-parody.</p>

<p>When Gillette introduced the Mach 3, I stayed with the Sensor; a few
years later I decided to see if a Boots own-brand Contour clone was
any good despite being a lot cheaper. It turned out than any
difference in the razors was dwarfed by variance in my shaving
technique, so I switched to the cheaper one. There are now other
options in the "less eye-watering than Gillette" segment of the
market, such as the <a href="http://shave.com/azor/">King of Shaves
Azor</a> or the <a href="http://pandodaily.com/2012/03/06/dollar-shave-club-punches-gillette-where-it-hurts-in-the-marketing-budget/">Dollar
Shave Club</a>, but they still buy into the Trac LXXVI bulshytt.</p>

<p>The secondary reason was that although Boots own-brand razors do the
job, they are a bit crappy and ugly. I had a vague desire for
something more elegant which involved chucking less plastic in the
bin. Partly based on satisfied reports from <a href="https://twitter.com/furrfu">Tom</a>, I invested £40 in a new
old-fashioned razor and some consumables.</p>

<p>The <a href="http://www.traditionalshaving.co.uk/mall/productpage.cfm/traditionalshaving/_EDW-RAZ-DE89L/268151">Edwin
Jagger DE89L</a> is a lovely object. It is made in Sheffield from
chrome-plated brass, and has a nice heft: it weighs 76g which is more
than four times as much as my old plastic razor. It has a wonderful
economy of design (Occam would approve) with only three parts each of
which serves multiple functions. The handle is threaded to screw the
blade clamp together; the bottom of the clamp includes a safety guard;
and the top of the clamp acts as a guide to the angle of the blade
against the skin. It's just the right shape for safely shaving under
my nose.</p>

<p>ETA: For blades I'm currently using <a href="http://www.traditionalshaving.co.uk/mall/productpage.cfm/traditionalshaving/_FEA-BLA-10/270335/">Feather</a> Japanese blades, mainly on the basis of hearsay and prejudice, er, I mean their reputation for quality and sharpness. I don't think it's possible to make a meaningful comparison without fitting different blades to identical razors and using them both during the same shave, repeatedly. (See above about variability of technique.) And I only have one DE razor at the moment.</p>

<p>I've also switched from using a shaving oil to a shaving cream and
badger brush. <a href="http://www.traditionalshaving.co.uk/mall/productpage.cfm/traditionalshaving/_TAY-CRE-SHA/268154">Taylor
of Old Bond St, Court Hairdressers</a> are awfully posh but a 150ml
bowl of their shaving cream costs less than £7 and you only need one
or two ml for a shave. More fun to use and easier than oil to clean up
afterwards.</p>

<p>So now my morning shaves are still inexpensive but much more
luxurious. Very satisfying :-)</p>
