---
dw:
  anum: 142
  eventtime: "2015-01-24T19:54:00Z"
  itemid: 416
  logtime: "2015-01-24T19:54:32Z"
  props:
    commentalter: 1491292408
    import_source: livejournal.com/fanf/133790
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/106638.html"
format: html
lj:
  anum: 158
  can_comment: 1
  ditemid: 133790
  event_timestamp: 1422129240
  eventtime: "2015-01-24T19:54:00Z"
  itemid: 522
  logtime: "2015-01-24T19:54:32Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 1
  url: "https://fanf.livejournal.com/133790.html"
title: New release of nsdiff and nspatch version 1.55
...

<p>I have released version 1.55 of <a href="http://dotat.at/prog/nsdiff/"><tt>nsdiff</tt></a>, which creates an <tt>nsupdate</tt> script from differences between DNS zone files.</p>

<p>There are not many changes to <tt>nsdiff</tt> itself: the only notable change is support for non-standard port numbers.</p>

<p>The important new thing is <tt>nspatch</tt>, which is an error-checking wrapper around `<tt>nsdiff</tt> | <tt>nsupdate</tt>`. To be friendly when running from <tt>cron</tt>, <tt>nspatch</tt> only produces output when it fails. It can also retry an update if it happens to lose a race against concurrent updates e.g. due to DNSSEC signing activity.</p>

<p>You can read the documentation and download the source from <a href="http://dotat.at/prog/nsdiff/">the <tt>nsdiff</tt> home page</a>.</p>

<p><small>My Mac insists that I should call it nstiff...</small></p>
