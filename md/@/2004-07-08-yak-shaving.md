---
dw:
  anum: 54
  eventtime: "2004-07-08T11:31:00Z"
  itemid: 92
  logtime: "2004-07-08T04:42:24Z"
  props:
    commentalter: 1491292312
    import_source: livejournal.com/fanf/23769
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/23606.html"
format: casual
lj:
  anum: 217
  can_comment: 1
  ditemid: 23769
  event_timestamp: 1089286260
  eventtime: "2004-07-08T11:31:00Z"
  itemid: 92
  logtime: "2004-07-08T04:42:24Z"
  props: {}
  reply_count: 1
  url: "https://fanf.livejournal.com/23769.html"
title: Yak-shaving
...

I've worked out roughly how I'm going to implement per-user forgery protection for hermes.cam.ac.uk, based on the idea of changing the reverse path of a message to contain an unforgeable cookie and using that to validate bounces. David Woodhouse has a similar setup: see http://www.infradead.org/rpr.html

One thing we need to do which David doesn't is work out which user an email address belongs to, so that all of a user's email addresses are protected by this scheme. Fortunately Exim's address_data feature lets us do that -- when an email address is finally routed to a Hermes user's mailbox, we can set the address_data and this value becomes available back in the ACL that can only see the original address.

We can do a table lookup to set the address_data, so that we can determine if the user has opted into the scheme. So when I started fiddling around with a prototype, I needed to add a table to our Exim setup. Which means I should add it to our configuration distribution system. Which I really need to move from the old admin box to the new one, and rewrite it a bit. But actually this would be a good motivator to get cfengine working. So that's what I'll be doing after I've installed the Exim development server...

http://www.ai.mit.edu/lab/gsb/gsb-archive/gsb2000%2D02%2D11.html
