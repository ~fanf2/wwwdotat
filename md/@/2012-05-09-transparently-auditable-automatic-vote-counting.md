---
format: html
lj:
  anum: 125
  can_comment: 1
  ditemid: 120445
  event_timestamp: 1336566960
  eventtime: "2012-05-09T12:36:00Z"
  itemid: 470
  logtime: "2012-05-09T11:36:18Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 19
  url: "https://fanf.livejournal.com/120445.html"
title: Transparently auditable automatic vote counting
...

<p>Electronic voting is impossible to implement in a secure manner, by which I mean there must be a way to audit the result after the fact in a way that is independent of the electronics. If you cannot perform an independent audit then hidden or compromised mechanisms in the electronics can lie without detection.</p>

<p>This audit requirement essentially means you need to make a paper record of each vote that is verified by the voter. That is, you need ballot papers.</p>

<p>Given you need ballot papers, you might as well keep the traditional method of voting (no voting machines) and only use automation for counting. Ideally the process of counting should be transparent, so that observers can see it proceeding correctly. It is not good enough for the ballot papers to disappear into a black box and a result pop out the other end.</p>

<p>It is possible to do this with mechanical collaters and counters - the kind of device that was used for data processing on punched cards. That way you can see the papers being split into a stack for each candidate, just as happens in the manual process. Observers can riffle through the stacks to verify correctness.</p>

<p>To count votes, why not weigh the stacks with precision scales?</p>

<p>So I wonder if you could make a ballot collating machine cheap enough and reliable enough that it is more cost-effective than manual counting. Could you perhaps use off-the-shelf printer/scanner/copier equipment?</p>
