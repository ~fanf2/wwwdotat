---
dw:
  anum: 68
  eventtime: "2008-01-29T15:37:00Z"
  itemid: 319
  logtime: "2008-01-29T15:37:14Z"
  props:
    import_source: livejournal.com/fanf/82277
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/81732.html"
format: casual
lj:
  anum: 101
  can_comment: 1
  ditemid: 82277
  event_timestamp: 1201621020
  eventtime: "2008-01-29T15:37:00Z"
  itemid: 321
  logtime: "2008-01-29T15:37:14Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/82277.html"
title: Still looking for a developer
...

We still have a vacancy in our web development group, and it has just been re-advertized. See http://www.cam.ac.uk/cs/jobs/ for details...
