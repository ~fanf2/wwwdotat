---
dw:
  anum: 98
  eventtime: "2013-10-29T21:11:00Z"
  itemid: 397
  logtime: "2013-10-29T21:11:05Z"
  props:
    commentalter: 1491292405
    import_source: livejournal.com/fanf/128861
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/101730.html"
format: html
lj:
  anum: 93
  can_comment: 1
  ditemid: 128861
  event_timestamp: 1383081060
  eventtime: "2013-10-29T21:11:00Z"
  itemid: 503
  logtime: "2013-10-29T21:11:05Z"
  props:
    personifi_tags: "nterms:no"
  reply_count: 2
  url: "https://fanf.livejournal.com/128861.html"
title: "Temporum: Quorate secure time"
...

<p>There are essentially two ways to find out what the time is: ask an
authoritative source and trust the answer, or ask several more or less
unreliable sources and see what they agree on. NTP is based on the
latter principle, but since the protocol isn't secured, a client also
has to trust the network not to turn NTP responses into lies.

<p>NTP's lack of security causes a bootstrapping problem. Many security
protocols rely on accurate time to avoid replay attacks. So nearly the
first thing a networked device needs to do on startup is get the time,
so that it can then properly verify what it gets from the network -
DNSSEC signatures, TLS certificates, software updates, etc. This is
particularly challenging for cost-constrained devices that do not have
a battery-backed real time clock and so start up in 1970.

<p>When I say NTP isn't secured, I mean that the protocol has security
features but they have not been deployed. I have tried to understand
NTP security, but I have not found a description of how to configure
it for the bootstrap case. What I want is for a minimally configured
client to be able to communicate with some time servers and get
responses with reasonable authenticity and integrity. Extra bonus
points for a clear description of which of <a href="http://www.eecis.udel.edu/~mills/ident.html">NTP's half dozen
identity verification schemes</a> is useful for what, and which ones
are <a href="http://tools.ietf.org/html/rfc5906#page-10">incompatible
with NATs</a> and rely on the client knowing its external IP address.

<p>In the absence of usable security from NTP, <a href="https://twitter.com/ioerror">Jacob Appelbaum</a> of <a href="https://www.torproject.org/">the Tor project</a> has written a
program called <a href="https://github.com/ioerror/tlsdate"><tt>tlsdate</tt></a>. In TLS, the
<tt>ClientHello</tt> and <tt>ServerHello</tt> messages include <a href="http://tools.ietf.org/html/rfc5246#section-7.4.1.2">a random
nonce which includes a Unix <tt>time_t</tt> value as a prefix</a>.  So
you can use any TLS server as a secure replacement for the old port 37
time service.

<p>Unlike NTP, <tt>tlsdate</tt> gets time from a single trusted
source. It would be much better if it were able to consult multiple
servers for their opinions of the time: it would be more robust if a
server is down or has the wrong time, and it would be more secure in
case a server is compromised. There is also the possibility of using
multiple samples spread over a second or two to obtain a more accurate
time than the one second resolution of TLS's <tt>gmt_unix_time</tt>
field.

<p>The essential idea is to find a quorum of servers that agree on the
time. An adversary or a technical failure would have to break at least
that many servers for you to get the wrong time.

<p>In statistical terms, you take a number of samples and find the
mode, the most common time value, and keep taking samples until the
frequency at the mode is greater than the quorum.

<p>But even though time values are discrete, the high school approach
to finding the mode isn't going to work because in many casees we
won't be able to take all the necessary samples close enough together
in time. So it is better to measure the time offset between a server
and the client at each sample, and treat these as a continuous
distribution.

<p>The key technique is kernel density estimation. The mode is the
point of peak density in the distribution estimated from the
samples. The kernel is a function that is used to spread out each
sample; the estimated distribution comes from summing the spread-out
samples.

<p><a href="http://www.eecis.udel.edu/~mills/ntp/html/select.html">NTP's
clock select algorithm</a> is basically kernel density estimation with
a <a href="https://en.wikipedia.org/wiki/Kernel_%28statistics%29">uniform
kernel</a>.

<p>NTP's other algorithms are based on lengthy observations of the
network conditions between the client and its servers, whereas we are
more concerned with getting a quick result from many servers. So
perhaps we can use a simpler, more well-known algorithm to find the
mode. It looks like <a href="https://normaldeviate.wordpress.com/2012/07/20/the-amazing-mean-shift-algorithm/">the
mean shift algorithm</a> is a good candidate.

<p>For the mean shift algorithm to work well, I think it makes sense to
use a smooth kernel such as the Gaussian. (I like exponentials.) The
bandwidth of the kernel should probably be one second (the precision
of the timestamp) plus the round trip time.

<p>Now it's time to break out the editor and write some code... I think
I'll call it "temporum" because that rhymes with "quorum" and it means
"of times" (plural). <a href="https://git.csx.cam.ac.uk/x/ucs/u/fanf2/temporum.git">Get temporum from my git server.</a>
