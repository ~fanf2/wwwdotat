---
format: html
lj:
  anum: 133
  can_comment: 1
  ditemid: 125317
  event_timestamp: 1359539760
  eventtime: "2013-01-30T09:56:00Z"
  itemid: 489
  logtime: "2013-01-30T09:56:25Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 7
  url: "https://fanf.livejournal.com/125317.html"
title: More Oxbridge college name comparisons
...

<p>So I said you'd have to do this comparison yourselves, but I can't resist making another list, to go with <a href="http://theshapeofthings.wordpress.com/2013/01/29/a-name-thats-particular/">Janet's list of Oxford college domain names</a>, <a href="http://fanf.livejournal.com/125010.html">my list of Cambridge college domain names</a>, and <a href="http://fanf.livejournal.com/89717.html">my list of colleges with similar names in the two universities</a>, here is a list of colleges with similar domain names:</p>

<dl>
<dt>Corpus Christi College
<dd>http://www.corpus.cam.ac.uk/ 
<dd>http://www.ccc.ox.ac.uk/
<dt>Jesus College
<dd>http://www.jesus.cam.ac.uk/ 
<dd>http://www.jesus.ox.ac.uk/ 
<dt>Magdalen(e) College
<dd>http://www.magd.cam.ac.uk/
<dd>http://www.magd.ox.ac.uk/
<dt>Pembroke College
<dd>http://www.pem.cam.ac.uk/ 
<dd>http://www.pmb.ox.ac.uk/ 
<dt>(The) Queen's's's College
<dd>http://www.quns.cam.ac.uk/ 
<dd>http://www.queens.cam.ac.uk/ 
<dd>http://www.queens.ox.ac.uk/ 
<dt>St Cath(a/e)rine's College
<dd>http://www.caths.cam.ac.uk/
<dd>http://www.stcatz.ox.ac.uk/
<dt>St John's College
<dd>http://www.joh.cam.ac.uk/ 
<dd>http://www.sjc.ox.ac.uk/ 
<dt>Trinity College
<dd>http://www.trin.cam.ac.uk/
<dd>http://www.trinity.ox.ac.uk/
<dt>Wolfson College
<dd>http://www.wolfson.cam.ac.uk/
<dd>http://www.wolfson.ox.ac.uk/
</dl>

<p>A few other comparisons occur to me.</p>

<p>Cambridge seems to have more colleges whose names are commonly abbreviated relative to the college's usual name (ignoring their sometimes lengthy full formal names): Caius, Catz, Corpus, Emma, Fitz, Lucy Cav, Sidney / Catz, Corpus, the House, LMH, Univ. (Does "New" count when divorced from "College"?)</p>

<p>Cambridge is more godly than Oxford: Christ's, Corpus, Emmanuel, Jesus, Trinity College and Hall / Christ Church, Corpus, Jesus, Trinity. (Does St Cross count?)</p>

<p>Oxford is more saintly: Magdalene, Peterhouse, St Catharine's, St Edmund's, St John's, (plus Corpus, Jesus, King's, Queens', according to their full names) / Magdalen, St Anne’s, St Antony’s, St Benet’s, St Catherine’s, St Cross, St Edmund, St Hilda’s, St Hugh’s, St John’s, St Peter’s, St Stephen’s, (plus Lincoln, New, according to their full names).</p>
