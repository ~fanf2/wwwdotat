---
dw:
  anum: 93
  eventtime: "2007-08-03T20:13:00Z"
  itemid: 294
  logtime: "2007-08-03T20:24:28Z"
  props:
    import_source: livejournal.com/fanf/75756
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/75357.html"
format: html
lj:
  anum: 236
  can_comment: 1
  ditemid: 75756
  event_timestamp: 1186171980
  eventtime: "2007-08-03T20:13:00Z"
  itemid: 295
  logtime: "2007-08-03T20:24:28Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/75756.html"
title: An OpenSSH patch
...

<p>OpenSSH has a neat feature called ControlMaster which allows multiple ssh clients to share the same connection to a target host. This saves time on connection startup by eliminating all the cryptography and authentication for the second and subsequent clients. You can use the feature by explicitly telling ssh when to be a control master (supply -M and -S &lt;socketpath&gt arguments) and when to be a control client (just supply a -S &lt;socketpath&gt argument). However it's much more convenient to tell it to automatically be a master if there isn't already one, or a client if there is, by putting ControlMaster=auto in your .ssh/config file.</p>

<p>However there is a race in the setup of the communications socket in auto mode, as illustrated by the following command line:</p>

<pre>
ssh -oControlMaster=auto -oControlPath=sock localhost 'sleep 1; echo 1' &
ssh -oControlMaster=auto -oControlPath=sock localhost 'sleep 2; echo 2' &
</pre>

<p>Both of the commands will try to start up as a control client, find that <tt>sock</tt> does not exist, and switch into control master mode. One will succeed in creating the control master socket and the other will fail and bomb.</p>

<p>I've written a patch which eliminates this race by trying to create a control master socket first, and falling back to control client mode if master mode fails. See the attachment to <a href="http://article.gmane.org/gmane.network.openssh.devel/13839">the message I posted to the openssh-dev list</a>.
