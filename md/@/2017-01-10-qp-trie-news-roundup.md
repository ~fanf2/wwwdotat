---
dw:
  anum: 82
  eventtime: "2017-01-10T00:39:00Z"
  itemid: 473
  logtime: "2017-01-10T00:40:54Z"
  props:
    import_source: livejournal.com/fanf/148247
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/121170.html"
format: html
lj:
  anum: 23
  can_comment: 1
  ditemid: 148247
  event_timestamp: 1484008740
  eventtime: "2017-01-10T00:39:00Z"
  itemid: 579
  logtime: "2017-01-10T00:40:54Z"
  props:
    give_features: 1
    og_image: "http://l-files.livejournal.net/og_image/936728/579?v=1484036742"
    personifi_tags: "nterms:no"
  reply_count: 0
  url: "https://fanf.livejournal.com/148247.html"
title: QP trie news roundup
...

<p>Firstly, I have to say that it's totally awesome that I am writing
this at all, and it's entirely due to the cool stuff done by people
other than me. Yes! News about other people doing cool stuff with my
half-baked ideas, how cool is that?</p>

<h2>CZ.NIC Knot DNS</h2>

<p>OK, DNS is approximately the ideal application for tries. It needs a
data structure with key/value lookup and lexically ordered traversal.</p>

<p>When qp tries were new, I got some very positive feedback from <a href="https://twitter.com/vavrusam">Marek
Vavrusa</a> who I think was at CZ.NIC at
the time. As well as being the Czech DNS registry, they also develop
their own very competitive DNS server software. Clearly the potential
for a win there, but I didn't have time to push a side project to
production quality, nor any expectation that anyone else would do the
work.</p>

<p>But, in November I got email from Vladim&iacute;r &#x10C;un&aacute;t telling me he had
reimplemented qp tries to fix the portability bugs and missing features
(such as prefix searches) in my qp trie code, and added it to <a href="https://www.knot-dns.cz">Knot
DNS</a>. Knot was previously using a <a href="https://en.wikipedia.org/wiki/HAT-trie">HAT
trie</a>.</p>

<p>Vladim&iacute;r said <a href="https://gitlab.labs.nic.cz/labs/knot/merge_requests/574">qp tries could reduce total server RSS by more than 50%
in a mass hosting test
case</a>. The
disadvantage is that they are slightly slower than HAT tries, e.g. for
the <code>.com</code> zone they do about twice as many memory indirections per
lookup due to checking a nybble per node rather than a byte per node.</p>

<p>On balance, qp tries were a pretty good improvement. Thanks, Vladim&iacute;r,
for making such effective use of my ideas!</p>

<p>(I've written some <a href="http://dotat.at/prog/qp/notes-dns.html">notes on more memory-efficient DNS name lookups in
qp tries</a> in case anyone wants
to help close the speed gap...)</p>

<h2>Rust</h2>

<p>Shortly before Christmas I spotted that <a href="https://twitter.com/jedisct1/">Frank
Denis</a> has <a href="https://github.com/jedisct1/rust-qptrie">a qp trie implementation in
Rust</a>!</p>

<p>Sadly I'm still only appreciating Rust from a distance, but when I
find some time to try it out properly, this will be top of my list of
things to hack around with!</p>

<p>I think qp tries are an interesting test case for Rust, because at the
core of the data structure is a tightly packed two word <code>union</code> with
type tags tucked into the low order bits of a pointer. It is dirty
low-level C, but in principle it ought to work nicely as a Rust
<code>enum</code>, provided Rust can be persuaded to make the same layout
optimizations. In my head a qp trie is a parametric recursive
algebraic data type, and I wish there were a programming language with
which I could express that clearly.</p>

<p>So, thanks, Frank, for giving me an extra incentive to try out Rust!
Also, Frank's Twitter feed is ace, you should totally follow him.</p>

<h2>Time vs space</h2>

<p>Today I had a conversation on Twitter with
<a href="https://twitter.com/tef_ebooks">@tef</a> who has some really interesting
ideas about possible improvements to qp tries.</p>

<p>One of the weaknesses of qp-tries, at least in my proof-of-concept
implementation, is the allocator is called for every insert or delete.
C's allocator is relatively heavyweight (compared to languages with
tightly-coupled GCs) so it's not great to call it so frequently.</p>

<p>(<a href="https://infoscience.epfl.ch/record/64398/files/idealhashtrees.pdf">Bagwell's HAMT
paper</a>
was a major inspiration for qp tries, and he goes into some detail
describing his custom allocator. It makes me feel like I'm slacking!)</p>

<p>There's an important trade-off between small memory size and keeping
some spare space to avoid <code>realloc()</code> calls. I have erred on the side of
optimizing for simple allocator calls and small data structure size at
the cost of greater allocator stress.</p>

<p>@tef suggested adding extra space to each node for use as a write
buffer, in a similar way to <a href="https://www.percona.com/files/presentations/percona-live/PLMCE2012/PLMCE2012-The_Right_Read_Optimization_is_Actually_Write_Optimization.pdf">"fractal tree"
indexes.</a>.
As well as avoiding calls to <code>realloc()</code>, a write buffer could avoid
malloc() calls for inserting new nodes. I was totally <a href="https://xkcd.com/356/">nerd
sniped</a> by his cool ideas!</p>

<p>After some intensive thinking I worked out <a href="http://dotat.at/prog/qp/notes-write-buffer.html">a sketch of how write
buffers might amortize allocation in qp
tries</a>. I don't think
it quite matches what tef had in mind, but it's definitely intriguing.
It's <em>very</em> tempting to steal some time to turn the sketch into code,
but I fear I need to focus more on things that are directly helpful to
my colleagues...</p>

<p>Anyway, thanks, tef, for the inspiring conversation! It also,
tangentially, led me to write this item for my blog.</p>
