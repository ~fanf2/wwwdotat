---
dw:
  anum: 215
  eventtime: "2008-02-07T11:50:00Z"
  itemid: 322
  logtime: "2008-02-07T11:50:14Z"
  props:
    commentalter: 1491292351
    import_source: livejournal.com/fanf/83170
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/82647.html"
format: html
lj:
  anum: 226
  can_comment: 1
  ditemid: 83170
  event_timestamp: 1202385000
  eventtime: "2008-02-07T11:50:00Z"
  itemid: 324
  logtime: "2008-02-07T11:50:14Z"
  props:
    personifi_tags: "nterms:no"
    verticals_list: technology
  reply_count: 3
  url: "https://fanf.livejournal.com/83170.html"
title: Endianness and C
...

<p>This is in response to <a href="http://meta.ath0.com/2008/02/07/c-question/">mathew's recent post</a>. I'm posting it here (and emailing it) because I can't get his frustrating Wordpress installation to log me in.</p>

<p>He asks, "It’s possible to write some C code to work out whether a machine’s architecture is little-endian or big-endian with respect to bytes. Is it possible, using only ANSI C, to work out whether the machine’s architecture is big-endian or little-endian with respect to bits?"</p>

<p>Machines do not (in general) have any bitwise endianness that's visible to programmers. In order to access bits you have to use shift and mask operations in registers, and these operations do not imply an absolute numbering of bits in the same way that the machine's addressing architecture does for bytes. (Instruction architecture documentation will often number the bits in a word, but a program running on the machine can't tell whether this documentation uses big-endian or little-endian conventions, or whether it numbers the bits from zero or one.)</p>

<p>Machines do have bitwise endiannes at a lower level that is invisible to the programmer, when buses or IO serialise and deserialise bytes, e.g. to and from RAM or disk or network. The serialized form is not accessible, so its endianness doesn't matter. There is an exception to this, which is output devices where the serialised form is visible to the user, e.g. displays with sub-byte pixels. But this is the device's endianness, not the whole machine's, and different devices attached to the same machine can legitimately disagree.</p>

<p>The same arguments about machines' instruction architectures apply to most of the C programming model, i.e. bits are mostly only accessible by shifts and masks, so endianness isn't visible. The exception is bit fields in structs, which allow words to be broken down into units of arbitrary size. Bit fields must have an endianness convention for how they are packed into words, but this is chosen by the compiler and need not agree with the hardware's byte-wise endianness. However the compiler usually does agree with the hardware, so that the following two structures have the same
layout, though this is not required by the standard:</p>

<pre>
        struct a {
                unsigned char first;
                unsigned char second;
        };
        struct b {
                unsigned first : 8;
                unsigned second : 8;
        };
</pre>
