---
dw:
  anum: 131
  eventtime: "2005-03-02T21:08:00Z"
  itemid: 130
  logtime: "2005-03-02T21:26:14Z"
  props:
    commentalter: 1491292319
    import_source: livejournal.com/fanf/33728
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/33411.html"
format: casual
lj:
  anum: 192
  can_comment: 1
  ditemid: 33728
  event_timestamp: 1109797680
  eventtime: "2005-03-02T21:08:00Z"
  itemid: 131
  logtime: "2005-03-02T21:26:14Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/33728.html"
title: Push syndication
...

Some of my friends think <a href="http://www.chiark.greenend.org.uk/ucgi/~dans/chiarkwiki?LiveJournalShouldBeNews">Livejournal should be news</a> though I'm not really convinced. Although using NNTP as the transport protocol has significant efficiency advantages, it doesn't help with the interesting problems of distributing LJ's network effects.

I tend to think that a more modern solution to the efficiency problem would be to create a way for (RSS or Atom etc.) syndication feed updates to be pushed rather than pulled (and polled). Fortunately this is a sufficiently obvious approach that other people are working on it so I can just sit back and let the lazy web do its thing. But what protocol should be used for the notifications?

My answer comes via the <a href="http://podcast.resource.org/rf-rfc/">Radio Free RFC</a> episodes about XMPP. The Extensible Messaging and Presence Protocol, aka Jabber, is the IETF's open standard for instant messaging. It has an extension known as PubSub which supports generic publication of update notifcations and subscription to a notification feed, and there's a profile of PubSub for handling updates to an Atom feed: http://www.xmpp.org/drafts/draft-saintandre-atompub-notify-02.html

All that remains to be worked out is how to handle the interesting access control features of "friends" lists in a distributed and easy-to-use manner...
