---
format: html
lj:
  anum: 161
  can_comment: 1
  ditemid: 115873
  event_timestamp: 1314979920
  eventtime: "2011-09-02T16:12:00Z"
  itemid: 452
  logtime: "2011-09-02T15:12:03Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/115873.html"
title: Version 1.13 of nsdiff
...

<p>I have updated <a href="https://fanf2.user.srcf.net/hermes/conf/bind/bin/nsdiff">nsdiff</a> to support wildcards and _underscore tags in owner names. Wildcards were prompted by <a href="http://www.chiark.greenend.org.uk/~ijackson/">Ian Jackson</a>'s desire to test resolver behaviour for his IP-over-DNS implementation. SRV and DKIM records use _underscore tags. Alan Clegg of the ISC says nsdiff is "very cool" :-)</p>

<p>(<a href="http://fanf.livejournal.com/114810.html">Previously</a>, <a href="http://fanf.livejournal.com/112749.html">previously</a>, <a href="http://fanf.livejournal.com/112476.html">previously</a>.)</p>
