---
dw:
  anum: 108
  eventtime: "2006-04-20T16:20:00Z"
  itemid: 221
  logtime: "2006-04-20T16:23:36Z"
  props:
    commentalter: 1491292333
    import_source: livejournal.com/fanf/57076
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/56684.html"
format: casual
lj:
  anum: 244
  can_comment: 1
  ditemid: 57076
  event_timestamp: 1145550000
  eventtime: "2006-04-20T16:20:00Z"
  itemid: 222
  logtime: "2006-04-20T16:23:36Z"
  props: {}
  reply_count: 19
  url: "https://fanf.livejournal.com/57076.html"
title: virtual chat host
...

I need a name for a virtual chat host, to go with wogan and parky, my physical hosts. So far I have:<ul><li>Max Headroom - just a bit too long</li><li>Ananova - virtual newsreader - too confusing</li><li>Wildfire - virtual mobile phone operator</li></ul>Any better ideas?
