---
dw:
  anum: 37
  eventtime: "2014-11-22T17:26:00Z"
  itemid: 411
  logtime: "2014-11-22T17:25:46Z"
  props:
    commentalter: 1491292408
    import_source: livejournal.com/fanf/132570
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/105253.html"
format: html
lj:
  anum: 218
  can_comment: 1
  ditemid: 132570
  event_timestamp: 1416677160
  eventtime: "2014-11-22T17:26:00Z"
  itemid: 517
  logtime: "2014-11-22T17:25:46Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 1
  url: "https://fanf.livejournal.com/132570.html"
title: Nerdy trivia about Unix time_t
...

<p>When I was running <tt>git-cvsimport</tt> yesterday (about which more another time), I wondered what were the nine-digit numbers starting with 7 that it was printing in its progress output. After a moment I realised they were the time_t values corresponding to the commit dates from the early 1990s.</p>

<p><a href="https://twitter.com/PowerDNS_Bert/status/535784454682578946">Bert commented that he started using Unix when time_t values started with an 8</a>, which made me wonder if there was perhaps a 26 year ambiguity - early 1970s or mid 1990s?. (For the sake of pedantry - I don't really think Bert is that old!)</p>

<p>So I checked and time_t = 80,000,000 corresponds to 1972-07-14 22:13:20 and 90,000,000 corresponds to 1972-11-07 16:00:00. But I thought this was before modern time_t started.</p>

<p>Page 183 of this very large PDF of the <a href="https://ia601001.us.archive.org/1/items/bitsavers_attunix3rdersManualThirdEditionFeb73_9537008/UNIX_Programmers_Manual_Third_Edition_Feb73.pdf">3rd Edition Unix manual</a> says:</p>
<pre>
￼TIME (II)                  3/15/72                  TIME (II)

NAME         time -- get time of year

SYNOPSIS     sys time / time = 13
             (time r0-r1)

DESCRIPTION  time returns the time since 00:00:00, Jan. 1,
             1972, measured in sixtieths of a second.  The
             high order word is in the rO register and the
             low order is in the r1.

SEE ALSO     date(I), mdate(II)

DIAGNOSTICS  --

BUGS         The time is stored in 32 bits.  This guarantees
             a crisis every 2.26 years.
</pre>

<p>So back then the 800,000,000 - 900,000,000 period was about three weeks in June 1972.</p>

<p>The <a href="http://www.tuhs.org/Archive/PDP-11/Distributions/research/Dennis_v4/">4th Edition Unix manual</a> (link to tar file of nroff source) says:</p>
<pre>
TIME (II)                   8/5/73                   TIME (II)

NAME         time -- get date and time

SYNOPSIS     (time = 13)
             sys  time
             time(tvec)
             int tvec[2];

DESCRIPTION  Time returns the time since 00:00:00 GMT, Jan. 1,
             1970, measured in seconds. From asm, the high
             order word is in the r0 register and the low
             order is in r1. From C, the user-supplied vector
             is filled in.

SEE ALSO     date(I), stime(II), ctime(III)

DIAGNOSTICS  none
</pre>
<p>I think the date on that page is a reasonably accurate indicator of when the time_t format changed. In the Unix manual, each page has its own date, separate from the date on the published editions of the manual. So, for example, the 3rd Edition is dated February 1973, but its TIME(II) page is dated March 1972. However all the 4th Edition system call man pages have the same date, which suggests that part of the documentation was all revised together, and the actual changes to the code happened some time earlier.</p>

<p>Now, time_t = 100,000,000 corresponds to 1973-03-03 09:46:40, so it is pretty safe to say that the count of seconds since the epoch has always had nine or ten digits.</p>
