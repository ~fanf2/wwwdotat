---
dw:
  anum: 138
  eventtime: "2004-03-06T11:08:00Z"
  itemid: 71
  logtime: "2004-03-06T03:22:08Z"
  props:
    commentalter: 1491292309
    import_source: livejournal.com/fanf/18188
    interface: flat
    opt_backdated: 1
    picture_keyword: passport
    picture_mapid: 4
  url: "https://fanf.dreamwidth.org/18314.html"
format: casual
lj:
  anum: 12
  can_comment: 1
  ditemid: 18188
  event_timestamp: 1078571280
  eventtime: "2004-03-06T11:08:00Z"
  itemid: 71
  logtime: "2004-03-06T03:22:08Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/18188.html"
title: Passport meme
...

Current passport: USA H1B visa (expired); USA entry stamps (SFR x 8; LOS x 2; ORL x 1; red); Australia entry and exit stamps (black); photos of me aged 20 and 26.

Previous passport: USA Entry stamp (MIA) and indefinite nonimmigrant visa (multicoloured); Jamaica entry and exit stamps (3 each; purple) and visa (blue); Malaysia (red); Cameroon visa (3 each; 2 red stamps, 1 large black stamp, 1 round blue stamp on top of 3000 francs payment stickers, plus signature); Hong Kong entry and exit stamps (black); Singapore visa (black stamp, purple stamp, and 2 green stamps); Hungary (brown); photos of me aged 9 and 14.
