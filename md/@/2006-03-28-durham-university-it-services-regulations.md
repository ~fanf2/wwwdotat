---
dw:
  anum: 24
  eventtime: "2006-03-28T03:05:00Z"
  itemid: 211
  logtime: "2006-03-28T02:26:04Z"
  props:
    commentalter: 1491292331
    import_source: livejournal.com/fanf/54469
    interface: flat
    opt_backdated: 1
    picture_keyword: silly
    picture_mapid: 6
  url: "https://fanf.dreamwidth.org/54040.html"
format: casual
lj:
  anum: 197
  can_comment: 1
  ditemid: 54469
  event_timestamp: 1143515100
  eventtime: "2006-03-28T03:05:00Z"
  itemid: 212
  logtime: "2006-03-28T02:26:04Z"
  props: {}
  reply_count: 16
  url: "https://fanf.livejournal.com/54469.html"
title: Durham University IT services regulations
...

Some amusing or odd features:<ul><li>Blasphemy and racism are forbidden, along with the usual porn. This probably covers some <tt>fortune(1)</tt> jokes.</li><li>You can get permission to look at porn for research purposes, but there is no such exemption for reverse engineering software (so no software security research).</li><li>You are not allowed to "access, download, store, or process" comercial spam, or anything that causes "annoyance, inconvenience, or needless anxiety".</li><li>Durham ITS takes a cut of research grants which have provision for computing costs.</li><li>Durham claims all "intellectual property" you create and must be involved in any commercial exploitation.</li><li>For the purpose of these regulations, "any remote IT facilities are deemed to be [Durham] University IT facilities".</li></ul>
