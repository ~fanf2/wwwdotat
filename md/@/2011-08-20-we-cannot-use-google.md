---
format: html
lj:
  anum: 18
  can_comment: 1
  ditemid: 114962
  event_timestamp: 1313884500
  eventtime: "2011-08-20T23:55:00Z"
  itemid: 449
  logtime: "2011-08-20T22:55:59Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 7
  url: "https://fanf.livejournal.com/114962.html"
title: We cannot use Google+
...

<p>If you follow <a href="http://dotat.at/:/">my link log</a> you will know that I have been following the "nymwars" saga with some interest. (The kind of interest one has in watching someone do something bone-headedly self destructive, accompanied by popcorn and preferably beer.) I have in fact signed up for Google+, but I don't use it except for following the occasional link to a post there. I don't have any particular interest in spending the time to work out how to get a decent amount of benefit from it - perhaps that will change once others have beaten the path (as happened with Twitter). That combined with the nymwars led me to delete the circles that I set up in the first few days of the service.</p>

<p>The Google+ name policy means it would be foolish of me to invest any effort in the service. Although I "use the name [my] friends, family or co-workers usually call [me]" this is not the same as the name I use for formal purposes. If anyone were to take exception to me and flag my G+ account, I would not be able to prove to Google that Tony Finch is a valid name for me. In fact I think the name that would be acceptable to their reinstatement process would not be recognisable to most people who know me since no-one refers to me by my first name.</p>

<p><a href="http://rmc28.dreamwidth.org/450708.html">Rachel's name also violates the policy</a> though in a different way. She has chosen to delete her G+ account altogether, because she doesn't want a terms-of-service violation to affect her usage of other more important Google services.</p>
