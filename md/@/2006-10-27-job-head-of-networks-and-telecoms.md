---
dw:
  anum: 125
  eventtime: "2006-10-27T15:30:00Z"
  itemid: 259
  logtime: "2006-10-27T15:35:30Z"
  props:
    commentalter: 1491292336
    import_source: livejournal.com/fanf/66686
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/66429.html"
format: casual
lj:
  anum: 126
  can_comment: 1
  ditemid: 66686
  event_timestamp: 1161963000
  eventtime: "2006-10-27T15:30:00Z"
  itemid: 260
  logtime: "2006-10-27T15:35:30Z"
  props: {}
  reply_count: 7
  url: "https://fanf.livejournal.com/66686.html"
title: "Job: head of networks and telecoms"
...

My employers are currently looking for someone to take over as head of one of the Computing Service's divisions: http://www.cam.ac.uk/cs/jobs/                                                                                                       
                                                                                                                                    
The University has its own ducts and fibre connecting almost all University sites around the city with gigabit or 10gig ethernet. We host the EastNet POP for UKERNA, which provides connectivity for 8 universities and many 6th form colleges. We're currently replacing the old BT switch with VOIP, which will eventually be handling about 5-10,000 phones. The division also runs the CS's managed workstation service, which includes a large central filestore and public access computer rooms all over the place.
