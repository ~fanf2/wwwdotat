---
comments:
  Dreamwidth: https://fanf.dreamwidth.org/144979.html
  Fediverse: https://mendeddrum.org/@fanf/111841893531071322
  Hacker News: https://news.ycombinator.com/item?id=39201332
  Lobsters: https://lobste.rs/s/ty8pa7/constructing_four_point_egg
...
Constructing a four-point egg
=============================

For reasons beyond the scope of this entry, I have been investigating
elliptical and ovoid shapes. The [Wikipedia article for Moss's
egg][moss] has a link to [a tutorial on Euclidean Eggs by Freyja
Hreinsdóttir][freyja] which (amongst other things) describes how to
construct the "four point egg". I think it is a nicer shape than
Moss's egg.

[moss]: https://en.wikipedia.org/wiki/Moss%27s_egg
[freyja]: https://web.archive.org/web/20200618202007/https://www.dynamat.oriw.eu/upload_pdf/20121022_154322__0.pdf

<svg xmlns="http://www.w3.org/2000/svg"
     viewBox="-300 -500 600 1000" height="500">
  <line stroke="grey" x1="-1000" y1="0" x2="+1000" y2="0" />
  <line stroke="grey" x1="0" y1="-1000" x2="0" y2="+1000" />
  <line stroke="grey" x1="-1100" y1="-1000" x2="+900" y2="+1000" />
  <line stroke="grey" x1="+1100" y1="-1000" x2="-900" y2="+1000" />
  <line stroke="grey" x1="+1000" y1="-1241.4213562373"
                      x2="-1000" y2="+758.5786437627" />
  <line stroke="grey" x1="+1000" y1="+758.5786437627"
                      x2="-1000" y2="-1241.4213562373" />
  <circle stroke="grey" fill="#0000" cx="0" cy="0" r="100" />
  <circle stroke="grey" fill="#0000" cx="0" cy="0" r="241.4213562373" />
  <circle stroke="#0c0" fill="#0000" stroke-width="2"
          cx="0" cy="+100" r="200" />
  <circle stroke="#c00" fill="#0000" stroke-width="2"
          cx="-100" cy="0" r="341.4213562373" />
  <circle stroke="#44c" fill="#0000" stroke-width="2"
          cx="+100" cy="0" r="341.4213562373" />
  <circle stroke="#cc0" fill="#0000" stroke-width="2"
          cx="-241.4213562373" cy="0" r="482.8427124746" />
  <circle stroke="#0cc" fill="#0000" stroke-width="2"
          cx="+241.4213562373" cy="0" r="482.8427124746" />
  <circle stroke="#c0c" fill="#0000" stroke-width="2"
          cx="0" cy="-241.4213562373" r="141.4213562373" />
  <path fill="#0000" stroke="#0c0" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 200 200 0 0 0 +141.4213562373 241.4213562373" />
  <path fill="#0000" stroke="#c00" stroke-width="10"
        d="M +141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 0 +241.4213562373 0" />
  <path fill="#0000" stroke="#44c" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 1 -241.4213562373 0" />
  <path fill="#0000" stroke="#cc0" stroke-width="10"
        d="M +241.4213562373 0 A 482.8427124746 482.8427124746 0 0 0 +100 -341.4213562373" />
  <path fill="#0000" stroke="#0cc" stroke-width="10"
        d="M -241.4213562373 0 A 482.8427124746 482.8427124746 0 0 1 -100 -341.4213562373" />
  <path fill="#0000" stroke="#c0c" stroke-width="10"
        d="M +100 -341.4213562373 A 141.4213562373 141.4213562373 0 0 0 -100 -341.4213562373" />
  <circle fill="#0c0" stroke="#0000" cx="0" cy="+100" r="10" />
  <circle fill="#c00" stroke="#0000" cx="-100" cy="0" r="10" />
  <circle fill="#44c" stroke="#0000" cx="+100" cy="0" r="10" />
  <circle fill="#cc0" stroke="#0000" cx="-241.4213562373" cy="0" r="10" />
  <circle fill="#0cc" stroke="#0000" cx="+241.4213562373" cy="0" r="10" />
  <circle fill="#c0c" stroke="#0000" cx="0" cy="-241.4213562373" r="10" />
</svg>

<cut>

Freyja's construction uses a straight edge and compasses in classical
style, so it lacks dimensions. Below is my version, with the numbers
needed to draw it on a computer.

At first this construction seems fairly rigid, but some of its
choices are more arbitrary than they might appear to be. I have made
[an interactive four-point egg][eggsperiment] so you can drag the
points around and observe how its shape changes.

[eggsperiment]: https://dotat.at/@/2024-01-eggsperiment.html

In the following, I will measure angles in fractions of a turn `𝜏`,
clockwise from noon because the egg's pointy end is upwards.

 1. Start with axes and a unit circle.

<svg xmlns="http://www.w3.org/2000/svg"
     viewBox="-250 -425 500 775" height="500">
  <line stroke="grey" x1="-1000" y1="0" x2="+1000" y2="0" />
  <line stroke="grey" x1="0" y1="-1000" x2="0" y2="+1000" />
  <circle stroke="grey" fill="#0000" cx="0" cy="0" r="100" />
</svg>

 2. Imagine two diagonal scaffolding lines: one through the unit
    circle's south and east points; and another through its south and
    west points.

<svg xmlns="http://www.w3.org/2000/svg"
     viewBox="-250 -425 500 775" height="500">
  <line stroke="grey" x1="-1000" y1="0" x2="+1000" y2="0" />
  <line stroke="grey" x1="0" y1="-1000" x2="0" y2="+1000" />
  <line stroke="grey" x1="-1100" y1="-1000" x2="+900" y2="+1000" />
  <line stroke="grey" x1="+1100" y1="-1000" x2="-900" y2="+1000" />
  <circle stroke="grey" fill="#0000" cx="0" cy="0" r="100" />
</svg>

 3. Draw an arc centred on the unit circle's south point and
    equidistant from its north point, from one scaffolding line to the
    other. This forms the big end of the egg.

<pre style='color: #0c0;'>
x: 0 y: -1 radius: 2 from: 𝜏*3/8 to: 𝜏*5/8
</pre>

<svg xmlns="http://www.w3.org/2000/svg"
     viewBox="-250 -425 500 775" height="500">
  <line stroke="grey" x1="-1000" y1="0" x2="+1000" y2="0" />
  <line stroke="grey" x1="0" y1="-1000" x2="0" y2="+1000" />
  <line stroke="grey" x1="-1100" y1="-1000" x2="+900" y2="+1000" />
  <line stroke="grey" x1="+1100" y1="-1000" x2="-900" y2="+1000" />
  <circle stroke="grey" fill="#0000" cx="0" cy="0" r="100" />
  <circle stroke="#0c0" fill="#0000" stroke-width="2"
          cx="0" cy="+100" r="200" />
  <path fill="#0000" stroke="#0c0" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 200 200 0 0 0 +141.4213562373 241.4213562373" />
  <circle fill="#0c0" stroke="#0000" cx="0" cy="+100" r="10" />
</svg>

 4. Draw the egg's lower right arc, centred on the unit circle's west
    point, from the right end of the bottom arc up to the positive X
    axis.

<pre style='color: #c00;'>
x: -1 y: 0 radius: 2+√2 from: +𝜏*2/8 to: +𝜏*3/8
</pre>

<svg xmlns="http://www.w3.org/2000/svg"
     viewBox="-250 -425 500 775" height="500">
  <line stroke="grey" x1="-1000" y1="0" x2="+1000" y2="0" />
  <line stroke="grey" x1="0" y1="-1000" x2="0" y2="+1000" />
  <line stroke="grey" x1="-1100" y1="-1000" x2="+900" y2="+1000" />
  <line stroke="grey" x1="+1100" y1="-1000" x2="-900" y2="+1000" />
  <circle stroke="#c00" fill="#0000" stroke-width="2"
          cx="-100" cy="0" r="341.4213562373" />
  <path fill="#0000" stroke="#0c0" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 200 200 0 0 0 +141.4213562373 241.4213562373" />
  <path fill="#0000" stroke="#c00" stroke-width="10"
        d="M +141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 0 +241.4213562373 0" />
  <circle fill="#0c0" stroke="#0000" cx="0" cy="+100" r="10" />
  <circle fill="#c00" stroke="#0000" cx="-100" cy="0" r="10" />
</svg>

 5. Draw the egg's lower left arc mirroring the lower right arc.

<pre style='color: #44c;'>
x: +1 y: 0 radius: 2+√2 from: -𝜏*3/8 to: -𝜏*2/8
</pre>

<svg xmlns="http://www.w3.org/2000/svg"
     viewBox="-250 -425 500 775" height="500">
  <line stroke="grey" x1="-1000" y1="0" x2="+1000" y2="0" />
  <line stroke="grey" x1="0" y1="-1000" x2="0" y2="+1000" />
  <line stroke="grey" x1="-1100" y1="-1000" x2="+900" y2="+1000" />
  <line stroke="grey" x1="+1100" y1="-1000" x2="-900" y2="+1000" />
  <circle stroke="#c00" fill="#0000" stroke-width="2"
          cx="-100" cy="0" r="341.4213562373" />
  <circle stroke="#44c" fill="#0000" stroke-width="2"
          cx="+100" cy="0" r="341.4213562373" />
  <path fill="#0000" stroke="#0c0" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 200 200 0 0 0 +141.4213562373 241.4213562373" />
  <path fill="#0000" stroke="#c00" stroke-width="10"
        d="M +141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 0 +241.4213562373 0" />
  <path fill="#0000" stroke="#44c" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 1 -241.4213562373 0" />
  <circle fill="#0c0" stroke="#0000" cx="0" cy="+100" r="10" />
  <circle fill="#c00" stroke="#0000" cx="-100" cy="0" r="10" />
  <circle fill="#44c" stroke="#0000" cx="+100" cy="0" r="10" />
</svg>

 6. The lower right and lower left arcs meet the X axis at the egg's
    east and west points. Imagine a scaffolding circle centred at the
    origin joining these points.

<svg xmlns="http://www.w3.org/2000/svg"
     viewBox="-250 -425 500 775" height="500">
  <line stroke="grey" x1="-1000" y1="0" x2="+1000" y2="0" />
  <line stroke="grey" x1="0" y1="-1000" x2="0" y2="+1000" />
  <circle stroke="grey" fill="#0000" cx="0" cy="0" r="241.4213562373" />
  <path fill="#0000" stroke="#0c0" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 200 200 0 0 0 +141.4213562373 241.4213562373" />
  <path fill="#0000" stroke="#c00" stroke-width="10"
        d="M +141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 0 +241.4213562373 0" />
  <path fill="#0000" stroke="#44c" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 1 -241.4213562373 0" />
  <circle fill="#0c0" stroke="#0000" cx="0" cy="+100" r="10" />
  <circle fill="#c00" stroke="#0000" cx="-100" cy="0" r="10" />
  <circle fill="#44c" stroke="#0000" cx="+100" cy="0" r="10" />
</svg>

 7. The egg's north centre is where this scaffolding circle meets the
    Y axis at `+1+√2`.

    Imagine two new diagonal scaffolding lines: one through the egg's
    west point and north centre; and another through its east point
    and north centre.

<svg xmlns="http://www.w3.org/2000/svg"
     viewBox="-250 -425 500 775" height="500">
  <line stroke="grey" x1="-1000" y1="0" x2="+1000" y2="0" />
  <line stroke="grey" x1="0" y1="-1000" x2="0" y2="+1000" />
  <line stroke="grey" x1="+1000" y1="-1241.4213562373"
                      x2="-1000" y2="+758.5786437627" />
  <line stroke="grey" x1="+1000" y1="+758.5786437627"
                      x2="-1000" y2="-1241.4213562373" />
  <circle stroke="grey" fill="#0000" cx="0" cy="0" r="241.4213562373" />
  <path fill="#0000" stroke="#0c0" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 200 200 0 0 0 +141.4213562373 241.4213562373" />
  <path fill="#0000" stroke="#c00" stroke-width="10"
        d="M +141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 0 +241.4213562373 0" />
  <path fill="#0000" stroke="#44c" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 1 -241.4213562373 0" />
  <circle fill="#0c0" stroke="#0000" cx="0" cy="+100" r="10" />
  <circle fill="#c00" stroke="#0000" cx="-100" cy="0" r="10" />
  <circle fill="#44c" stroke="#0000" cx="+100" cy="0" r="10" />
  <circle fill="#c0c" stroke="#0000" cx="0" cy="-241.4213562373" r="10" />
</svg>

 8. Draw the egg's upper right arc, centred on the egg's west point,
    from the egg's east point up to a scaffolding line.

<pre style='color: #cc0;'>
x: -1-√2 y: 0 radius: 2+2√2 from: +𝜏*1/8 to: +𝜏*2/8
</pre>

<svg xmlns="http://www.w3.org/2000/svg"
     viewBox="-250 -425 500 775" height="500">
  <line stroke="grey" x1="-1000" y1="0" x2="+1000" y2="0" />
  <line stroke="grey" x1="0" y1="-1000" x2="0" y2="+1000" />
  <line stroke="grey" x1="+1000" y1="-1241.4213562373"
                      x2="-1000" y2="+758.5786437627" />
  <line stroke="grey" x1="+1000" y1="+758.5786437627"
                      x2="-1000" y2="-1241.4213562373" />
  <circle stroke="#cc0" fill="#0000" stroke-width="2"
          cx="-241.4213562373" cy="0" r="482.8427124746" />
  <path fill="#0000" stroke="#0c0" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 200 200 0 0 0 +141.4213562373 241.4213562373" />
  <path fill="#0000" stroke="#c00" stroke-width="10"
        d="M +141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 0 +241.4213562373 0" />
  <path fill="#0000" stroke="#44c" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 1 -241.4213562373 0" />
  <path fill="#0000" stroke="#cc0" stroke-width="10"
        d="M +241.4213562373 0 A 482.8427124746 482.8427124746 0 0 0 +100 -341.4213562373" />
  <circle fill="#0c0" stroke="#0000" cx="0" cy="+100" r="10" />
  <circle fill="#c00" stroke="#0000" cx="-100" cy="0" r="10" />
  <circle fill="#44c" stroke="#0000" cx="+100" cy="0" r="10" />
  <circle fill="#cc0" stroke="#0000" cx="-241.4213562373" cy="0" r="10" />
  <circle fill="#c0c" stroke="#0000" cx="0" cy="-241.4213562373" r="10" />
</svg>

 9. Draw the egg's upper left arc mirroring the upper right arc.

<pre style='color: #0cc;'>
x: +1+√2 y: 0 radius: 2+2√2 from: -𝜏*2/8 to: -𝜏*1/8
</pre>

<svg xmlns="http://www.w3.org/2000/svg"
     viewBox="-250 -425 500 775" height="500">
  <line stroke="grey" x1="-1000" y1="0" x2="+1000" y2="0" />
  <line stroke="grey" x1="0" y1="-1000" x2="0" y2="+1000" />
  <line stroke="grey" x1="+1000" y1="-1241.4213562373"
                      x2="-1000" y2="+758.5786437627" />
  <line stroke="grey" x1="+1000" y1="+758.5786437627"
                      x2="-1000" y2="-1241.4213562373" />
  <circle stroke="#cc0" fill="#0000" stroke-width="2"
          cx="-241.4213562373" cy="0" r="482.8427124746" />
  <circle stroke="#0cc" fill="#0000" stroke-width="2"
          cx="+241.4213562373" cy="0" r="482.8427124746" />
  <path fill="#0000" stroke="#0c0" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 200 200 0 0 0 +141.4213562373 241.4213562373" />
  <path fill="#0000" stroke="#c00" stroke-width="10"
        d="M +141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 0 +241.4213562373 0" />
  <path fill="#0000" stroke="#44c" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 1 -241.4213562373 0" />
  <path fill="#0000" stroke="#cc0" stroke-width="10"
        d="M +241.4213562373 0 A 482.8427124746 482.8427124746 0 0 0 +100 -341.4213562373" />
  <path fill="#0000" stroke="#0cc" stroke-width="10"
        d="M -241.4213562373 0 A 482.8427124746 482.8427124746 0 0 1 -100 -341.4213562373" />
  <circle fill="#0c0" stroke="#0000" cx="0" cy="+100" r="10" />
  <circle fill="#c00" stroke="#0000" cx="-100" cy="0" r="10" />
  <circle fill="#44c" stroke="#0000" cx="+100" cy="0" r="10" />
  <circle fill="#cc0" stroke="#0000" cx="-241.4213562373" cy="0" r="10" />
  <circle fill="#0cc" stroke="#0000" cx="+241.4213562373" cy="0" r="10" />
  <circle fill="#c0c" stroke="#0000" cx="0" cy="-241.4213562373" r="10" />
</svg>

10. Draw an arc on the egg's north centre joining the ends of the
    upper right and upper left arcs. This arc forms the little end of
    the egg.

<pre style='color: #c0c;'>
x: 0 y: 1+√2 radius: √2 from: -𝜏*1/8 to: +𝜏*1/8
</pre>

<svg xmlns="http://www.w3.org/2000/svg"
     viewBox="-250 -425 500 775" height="500">
  <line stroke="grey" x1="-1000" y1="0" x2="+1000" y2="0" />
  <line stroke="grey" x1="0" y1="-1000" x2="0" y2="+1000" />
  <line stroke="grey" x1="+1000" y1="-1241.4213562373"
                      x2="-1000" y2="+758.5786437627" />
  <line stroke="grey" x1="+1000" y1="+758.5786437627"
                      x2="-1000" y2="-1241.4213562373" />
  <circle stroke="#c0c" fill="#0000" stroke-width="2"
          cx="0" cy="-241.4213562373" r="141.4213562373" />
  <path fill="#0000" stroke="#0c0" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 200 200 0 0 0 +141.4213562373 241.4213562373" />
  <path fill="#0000" stroke="#c00" stroke-width="10"
        d="M +141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 0 +241.4213562373 0" />
  <path fill="#0000" stroke="#44c" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 1 -241.4213562373 0" />
  <path fill="#0000" stroke="#cc0" stroke-width="10"
        d="M +241.4213562373 0 A 482.8427124746 482.8427124746 0 0 0 +100 -341.4213562373" />
  <path fill="#0000" stroke="#0cc" stroke-width="10"
        d="M -241.4213562373 0 A 482.8427124746 482.8427124746 0 0 1 -100 -341.4213562373" />
  <path fill="#0000" stroke="#c0c" stroke-width="10"
        d="M +100 -341.4213562373 A 141.4213562373 141.4213562373 0 0 0 -100 -341.4213562373" />
  <circle fill="#0c0" stroke="#0000" cx="0" cy="+100" r="10" />
  <circle fill="#c00" stroke="#0000" cx="-100" cy="0" r="10" />
  <circle fill="#44c" stroke="#0000" cx="+100" cy="0" r="10" />
  <circle fill="#cc0" stroke="#0000" cx="-241.4213562373" cy="0" r="10" />
  <circle fill="#0cc" stroke="#0000" cx="+241.4213562373" cy="0" r="10" />
  <circle fill="#c0c" stroke="#0000" cx="0" cy="-241.4213562373" r="10" />
</svg>

The six coloured points marked in the diagram above are the centres of
the six arcs that make the egg. These comprise the four points the egg
is named after, plus their mirror images.

<svg xmlns="http://www.w3.org/2000/svg"
     viewBox="-250 -425 500 775" height="500">
  <path fill="#0000" stroke="#0c0" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 200 200 0 0 0 +141.4213562373 241.4213562373" />
  <path fill="#0000" stroke="#c00" stroke-width="10"
        d="M +141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 0 +241.4213562373 0" />
  <path fill="#0000" stroke="#44c" stroke-width="10"
        d="M -141.4213562373 241.4213562373 A 341.4213562373 341.4213562373 0 0 1 -241.4213562373 0" />
  <path fill="#0000" stroke="#cc0" stroke-width="10"
        d="M +241.4213562373 0 A 482.8427124746 482.8427124746 0 0 0 +100 -341.4213562373" />
  <path fill="#0000" stroke="#0cc" stroke-width="10"
        d="M -241.4213562373 0 A 482.8427124746 482.8427124746 0 0 1 -100 -341.4213562373" />
  <path fill="#0000" stroke="#c0c" stroke-width="10"
        d="M +100 -341.4213562373 A 141.4213562373 141.4213562373 0 0 0 -100 -341.4213562373" />
</svg>

Moss's egg is a three-point egg. It has the same top half as this
four-point egg, but its bottom half is a simple semi-circle.
