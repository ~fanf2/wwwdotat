---
dw:
  anum: 63
  eventtime: "2014-07-14T13:05:00Z"
  itemid: 407
  logtime: "2014-07-14T12:05:52Z"
  props:
    commentalter: 1491292407
    import_source: livejournal.com/fanf/131421
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/104255.html"
format: casual
lj:
  anum: 93
  can_comment: 1
  ditemid: 131421
  event_timestamp: 1405343100
  eventtime: "2014-07-14T13:05:00Z"
  itemid: 513
  logtime: "2014-07-14T12:05:52Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 7
  url: "https://fanf.livejournal.com/131421.html"
title: Data structures and algorithms
...

"Bad programmers worry about the code. Good programmers worry about data structures and their relationships." - <a href="https://lwn.net/Articles/193245/">Linus Torvalds</a>

"If you've chosen the right data structures and organized things well, the algorithms will almost always be self-evident. Data structures, not algorithms, are central to programming." - <a href="http://users.ece.utexas.edu/~adnan/pike.html">Rob Pike</a>

"Show me your flowcharts and conceal your tables, and I shall continue to be mystified. Show me your tables, and I won’t  usually need your flowcharts; they’ll be obvious." - <a href="https://en.wikiquote.org/wiki/Fred_Brooks">Fred Brooks</a>
