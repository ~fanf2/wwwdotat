My spaghetti carbonara recipe
=============================

In recent weeks I have been obsessed with carbonara: I have probably
been eating it far too frequently. Here's my recipe. It works well for
1 - 3 people but gets unweildy at larger quantities.


ingredients
-----------

Rough quantities per person:

  * 100g pasta

    Spaghetti is traditional but I'll use any shape.

  * 50g streaky bacon

    The traditional ingredient is guanciale; maybe I'll try that one
    day for a special occasion. I use 4 rashers of the thin-sliced
    bacon that we get, which is 60g.

  * one large egg

    Typically about 60g

  * 40g grated parmesan

    Again, for a special occasion I might try the traditional pecorino
    romano. My rule of thumb is there should be as much cheese as half
    the weight of the egg, but I usually round it up so there's 100g
    of mixture.

  * lots and lots of ground black pepper


method
------

  * Get the kettle on the boil and measure out the pasta.

  * While waiting for the kettle, shred the bacon into a pan.

    I use kitchen scissors. (I ought to get our knives sharpened.)

    The pan needs to be big enough to stir everything together at the end.

  * Get the pasta cooking in another pan.

    _Don't_ salt the water, there's plenty in the bacon and cheese.

    Use relatively little water so that it becomes starchy while
    cooking. The pasta water will loosen and stabilize the sauce.

  * Fry the bacon until it has taken on some nice colour.

    I bash it about with a wooden spoon to make sure the bits have
    separated. It will probably be done before the pasta, which is
    fine. Turn off the heat and let it rest.

  * When the cooking is under control, break the egg(s) into a bowl,
    and grate the cheese into the eggs.

    I do this on top of the weighing scales.

  * Grind lots of pepper onto the cheese and egg and mix them all
    together. It will make a thick sludge.

  * When the pasta is done to your liking, use a slotted spoon to
    transfer it to the pan with the bacon.

    I find a slotted spoon carries a nice quantity of water with the
    pasta. Many of the recipes I have seen say that the pasta should
    be slightly under-done at this point, because it will finish
    cooking in the sauce, but that doesn't work for me.

  * Mix the bacon and pasta and deglaze the pan.

    It should be cool enough after this point that the egg will not
    curdle immediately when you add it.

  * Add the egg and cheese and mix over a gentle heat.

    As you stir, the cheese will melt and the sauce will become smooth
    and creamy. If it's too thick, add a tablespoon of pasta water.
    If it's too runny, boost the heat to help the egg thicken up.

  * Dish up and serve.

    Best eaten immediately: it's nicest hot but it cools relatively fast.
