---
dw:
  anum: 197
  eventtime: "2008-11-06T19:30:00Z"
  itemid: 367
  logtime: "2008-11-06T19:30:48Z"
  props:
    commentalter: 1491292361
    import_source: livejournal.com/fanf/95404
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/94149.html"
format: html
lj:
  anum: 172
  can_comment: 1
  ditemid: 95404
  event_timestamp: 1225999800
  eventtime: "2008-11-06T19:30:00Z"
  itemid: 372
  logtime: "2008-11-06T19:30:48Z"
  props:
    verticals_list: "technology,computers_and_software"
  reply_count: 5
  url: "https://fanf.livejournal.com/95404.html"
title: Licence to spam
...

<p>Over a year ago we had some discussions inside the Computing Service about better provision for large mailing lists within the University. At the moment we rely too much on paper spam, and electronic spam is distributed to staff by departmental administrators who manually forward irrelevant utterances from the Old Schools. My hope was that officially sanctioned mailing lists for internal communications would improve this situation because, as well as being more efficient, there would be better moderation and recipients would be able to unsubscribe. We approved a document which I thought was quite good, and which I expected would live alongside our existing, very general, bulk email guidelines. This then went to the IT Syndicate for approval.</p>

<p>Unfortunately the IT Syndicate was disbanded at about this time to be replaced by an Information Services and Strategy Syndicate which has a broader remit. The large list policy got dropped for several months as the committee rebooted. When the work item was picked up again it somehow got corrupted. It was rewritten with absolutely no input from us, and it replaced the bulk email policy instead of being adopted alongside it. As a result there is now almost no policy against spam intentionally sent by University members to recipients inside <i>and outside</i> the University. I am extremely annoyed.</p>

<p><a href="http://www.admin.cam.ac.uk/committee/isss/otherguidelines/bulkemail.html">The new policy is on the ISSS web site</a> and I'll put a copy of the old policy here to preserve it after Google's cache expires.</p>

<h2>Guidelines on Use of Bulk Email in Cambridge</h2>

<p>Although the use of bulk email can on occasion be in the interests of the University, it can nevertheless present real problems and dangers. Guidance on the use of bulk email is often sought but it is very hard to be precise. Nevertheless, it is felt that an attempt should be made and the IT Syndicate has approved the following guidelines.</p>

<h3>Terminology</h3>

<p>Bulk email is identical email sent out to groups of individuals, irrespective of whether this is done by repeated sendings or single sendings, via mailing list addresses or any other means.</p>

<p>Unsolicited email is email of a category that the individual did not request, irrespective of whether it is welcome.</p>

<h3>Under what circumstances is bulk email a bad idea?</h3>

<p>It is unacceptable to do anything likely to invite external retaliation (such as black-listing) against the University or any part thereof. It is therefore never acceptable to send bulk unsolicited email out of the CUDN.</p>

<p>Even within the University it is normally unacceptable to send bulk unsolicited email unless there is reason to suppose that a substantial proportion of the recipients will be interested or need to know.</p>

<p>Although an individual may have assented to certain bulk email implicitly by being on some particular list, or indeed by virtue of his or her position, say as an employee or student, caution is needed in any such presumption. Assent to mail inappropriate to a list is never implied, and mere appearance in some email directory does not imply assent either.</p>

<p>It is unacceptable to send email in such bulk as would overwhelm the underlying mechanisms, be this on account of the number of ultimate targets, volume of data transmitted, volume of data consequentially stored or anything else.</p>

<p>Electronic bulletin boards are frequently more appropriate than email and should be preferred wherever possible and reasonable.</p>

<p>Bulk unsolicited email is frequently counter-productive and can generate considerable annoyance. The utmost caution and reluctance should precede any emission of bulk email.</p>

<p>Rules and guidelines that apply to ordinary individual or personal email apply at least as strongly to bulk email, strictures being the more significant on account of the size and generality of the audience. See the section on mail etiquette at <a href="http://www.cam.ac.uk/cs/email/etiquette.html">http://www.cam.ac.uk/cs/email/etiquette.html</a>, and the IT Syndicate guidelines on the use and misuse of computing facilities at <a href="http://www.cam.ac.uk/cs/itsyndicate/rules/guidelines.html">http://www.cam.ac.uk/cs/itsyndicate/rules/guidelines.html</a>.</p>

<h3>Points to consider in bulk email</h3>

<p>If it is accepted that some particular instance of bulk unsolicited email is appropriate, surviving the guidelines above, these further guidelines should be applied to the composition and transmission, for essentially the same reasons.</p>

<p>The email should be legible on the most basic of equipment, not requiring anything that recipients might reasonably not have. In particular, it should be wholly in plain text, not encoded in any way, and certainly not include any part in proprietary format.</p>

<p>The message should be short, perhaps a page at most.</p>

<p>If the object is to draw attention to bulkier material or material inherently in other formats, then references to this (e.g. URLs) can be included, which interested recipients can pursue. There is still no warrant to contravene the preceding two paragraphs.</p>

<p>The message as a whole should make it plain that it is indeed a circular, and should make plain the general constituency of its recipients.</p>

<p>The headers should be such as to prevent any replies accidentally going to the whole constituency as well. Expert advice should be sought as to specific methods.</p>

<p>If the constituency is large it may well be appropriate to take special steps to minimize the logistical impact. It is always the responsibility of the sender to verify that the underlying systems can cope with what he or she wants to do. Expert advice should be sought.</p>

<p>The address list used should be appropriate for the message. In particular , see the posting policy in <a href="http://www.cam.ac.uk/cs/docs/leaflets/G90/">the Computing Service leaflet on mailing lists, G90</a>.</p>
