---
dw:
  anum: 56
  eventtime: "2004-01-23T15:30:00Z"
  itemid: 55
  logtime: "2004-01-23T15:31:16Z"
  props:
    import_source: livejournal.com/fanf/14200
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/14136.html"
format: casual
lj:
  anum: 120
  can_comment: 1
  ditemid: 14200
  event_timestamp: 1074871800
  eventtime: "2004-01-23T15:30:00Z"
  itemid: 55
  logtime: "2004-01-23T15:31:16Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/14200.html"
title: Insta-BSD-meet
...

Seigo Tanimura will be in Cambridge tomorrow, so we'll be meeting for lunch in the Eagle. Any people feeling BSDish are welcome to join us.
