---
dw:
  anum: 7
  eventtime: "2017-02-02T16:07:00Z"
  itemid: 476
  logtime: "2017-02-02T16:07:58Z"
  props:
    commentalter: 1491292431
    import_source: livejournal.com/fanf/149116
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/121863.html"
format: html
lj:
  anum: 124
  can_comment: 1
  ditemid: 149116
  event_timestamp: 1486051620
  eventtime: "2017-02-02T16:07:00Z"
  itemid: 582
  logtime: "2017-02-02T16:07:58Z"
  props:
    give_features: 1
    og_image: "http://l-files.livejournal.net/og_image/936728/582?v=1486051679"
    personifi_tags: "nterms:yes"
  reply_count: 4
  url: "https://fanf.livejournal.com/149116.html"
title: keepalived DNS health checker revamp
...

<p>Today I rolled out a significant improvement to the automatic recovery
system on Cambridge University's recursive DNS servers. This change
was because of three bugs.</p>

<h2>BIND RPZ catatonia</h2>

<p>The first bug is that sometimes BIND will lock up for a few seconds
doing <a href="http://dnsrpz.info/">RPZ</a> maintenance work. This can happen
with very large and frequently updated response policy zones such as
<a href="https://www.spamhaus.org/news/article/669">the Spamhaus Domain Block List</a>.</p>

<p>When this happens on my servers, <code>keepalived</code> starts a failover
process after a couple of seconds - it is deliberately configured to
respond quickly. However, BIND soon recovers, so a few seconds later
<code>keepalived</code> fails back.</p>

<h2>BIND lost listening socket</h2>

<p>This brief <code>keepalived</code> flap has an unfortunate effect on BIND. It
sees the service addresses disappear, so it closes its listening
sockets, then the service addresses reappear, so it tries to reopen
its listening sockets.</p>

<p>Now, because the server is fairly busy, it doesn't have time to clean
up all the state from the old listening socket before BIND tries to
open the new one, so BIND gets an "address already in use" error.</p>

<p>Sadly, BIND gives up at this point - it does not keep trying
periodically to reopen the socket, as you might hope.</p>

<h2>Holy health check script, Bat Man!</h2>

<p>At this point BIND is still listening on most of the interface
addresses, except for a TCP socket on the public service IP address.
Ideally this should have been spotted by my health check script, which
should have told <code>keepalived</code> to fail over again.</p>

<p>But there's a gaping hole in the health checker's coverage: it only
tests the loopback interfaces!</p>

<h2>In a fix</h2>

<p>Ideally all three of these bugs should be fixed. I'm not expert enough
to fix the BIND bugs myself, since they are in some of the gnarliest
bits of the code, so I'll leave them to the good folks at ISC.org.
Even if they are fixed, I still need to fix my health check script so
that it actually checks the user-facing service addresses, and there's
no-one else I can leave that to.</p>

<h2>Previously...</h2>

<p>I wrote about my setup for <a href="http://fanf.livejournal.com/133294.html">recursive DNS server failover with
<code>keepalived</code></a> when I set it
up a couple of years ago. My recent work leaves the <code>keepalived</code>
configuration bascially unchanged, and concentrates on the health
check script.</p>

<p>For the purpose of this article, the key feature of my <code>keepalived</code>
configuration is that it runs the health checker script many times per
second, in order to fake up dynamically reconfigurable server
priorities. The old script did DNS queries inline, which was OK when
it was only checking loopback addresses, but the new script needs to
make typically 16 queries which is getting a bit much.</p>

<h2>Daemonic decoupling</h2>

<p>The new health checker is split in two.</p>

<p>The script called by <code>keepalived</code> now just examines the contents of a
status file, so it runs predictably fast regardless of the speed of
DNS responses.</p>

<p>There is a separate daemon which performs the actual health checks,
and writes the results to the status file.</p>

<p>The speed thing is nice, but what is really important is that the
daemon is naturally stateful in a way the old health checker could not
be. When I started I knew statefulness was necessary because I clearly
needed some kind of hysteresis or flap damping or hold-down or something.</p>

<h2>This is much more complex</h2>

<p><a href="https://www.youtube.com/watch?v=DNb4VKln1uw">https://www.youtube.com/watch?v=DNb4VKln1uw</a></p>

<blockquote>
  <p><em>There is this theory of the M&ouml;bius:
 a twist in the fabric of space where time becomes a loop</em></p>
</blockquote>

<ul>
<li><p>BIND observes the list of network interfaces, and opens and closes
listening sockets as addresses come and go.</p></li>
<li><p>The health check daemon verifies that BIND is responding properly on
all the network interface addresses.</p></li>
<li><p><code>keepalived</code> polls the health checker and brings interfaces up and
down depending on the results.</p></li>
</ul>

<p>Without care it is inevitable that unexpected interactions between
these components will destroy the Enterprise!</p>

<h2>Winning the race</h2>

<p>The health checker gets into races with the other daemons when
interfaces are deleted or added.</p>

<p>The deletion case is simpler. The health checker gets the list of
addresses, then checks them all in turn. If <code>keepalived</code> deletes an
address during this process then the checker can detect a failure -
but actually, it's OK if we don't get a respose from a missing
address! Fortunately there is a distinctive error message in this case
which the health checker can treat as an alternative successful
response.</p>

<p>New interfaces are more tricky, because the health checker needs to
give BIND a little time to open its sockets. It would be really bad if
the server appears to be healthy, so keepalived brings up the
addresses, which the health checker tests before BIND is ready,
causing it to immediately fail - a huge flap.</p>

<h2>Back off</h2>

<p>The main technique that the new health checker uses to suppress
flapping is exponential backoff.</p>

<p>Normally, when everything is working, the health checker queries every
network interface address, writes an OK to the status file, then
sleeps for 1 second before looping.</p>

<p>When a query fails, it immediately writes BAD to the status file, and
sleeps for a while before looping. The sleep time increases
exponentially as more failures occur, so repeated failures cause
longer and longer intervals before the server tries to recover.</p>

<p>Exponential backoff handles my original problem somewhat indirectly:
if there's a flap that causes BIND to lose a listening socket, there
will then be a (hopefully short) series of slower and slower flaps
until eventually a flap is slow enough that BIND is able to re-open
the socket and the server recovers. I will probably have to tune the
backoff parameters to minimize the disruption in this kind of event.</p>

<h2>Hold down</h2>

<p>Another way to suppress flapping is to avoid false recoveries.</p>

<p>When all the test queries succeed, the new health checker decreases
the failure sleep time, rather than zeroing it, so if more failures
occur the exponential backoff can continue. It still reports the
success immediately to <code>keepalived</code>, because I want true recoveries to
be fast, for instance if the server accidentally crashes and is
restarted.</p>

<p>The hold-down mechanism is linked to the way the health checker keeps
track of network interface addresses.</p>

<p>After an interface goes away the checker does not decrease the sleep
time for several seconds even if the queries are now working OK. This
hold-down is supposed to cover a flap where the interface immediately
returns, in which case we want exponential backoff to continue.</p>

<p>Similarly, to avoid those tricky races, we also record the time when
each interface is brought up, so we can ignore failures that occur in
the first few seconds.</p>

<h2>Result</h2>

<p>It took quite a lot of headscratching and trial and error, but in the
end I think I came up with something resonably simple. Rather than
targeting it specifically at failures I have observed in production, I
have tried to use general purpose robustness techniques, and I hope
this means it will behave OK if some new weird problem crops up.</p>

<p>Actually, I hope NO new weird problems crop up!</p>

<blockquote>
  <p><em>PS. the ST:TNG quote above is because I have recently been
listening to my old Orbital albums again</em> -
<a href="https://www.youtube.com/watch?v=RlB-PN3M1vQ">https://www.youtube.com/watch?v=RlB-PN3M1vQ</a></p>
</blockquote>
