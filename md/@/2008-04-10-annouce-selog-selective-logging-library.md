---
dw:
  anum: 194
  eventtime: "2008-04-10T14:25:00Z"
  itemid: 332
  logtime: "2008-04-10T14:25:40Z"
  props:
    commentalter: 1491292352
    import_source: livejournal.com/fanf/85925
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/85186.html"
format: html
lj:
  anum: 165
  can_comment: 1
  ditemid: 85925
  event_timestamp: 1207837500
  eventtime: "2008-04-10T14:25:00Z"
  itemid: 335
  logtime: "2008-04-10T14:25:40Z"
  props:
    verticals_list: computers_and_software
  reply_count: 11
  url: "https://fanf.livejournal.com/85925.html"
title: "ANNOUCE: selog - selective logging library"
...

<p>I'm pleased to announce the first release of selog, which you can download from &lt;<a href="http://dotat.at/prog/selog/">http://dotat.at/prog/selog/</a>&gt;.</p>

<p>Selog is a library of routines that unifies error reporting,
 activity logging, and debug tracing. It allows programmers to give
 their users flexible control over which messages are written and
 where they are written to.</p>

<p>Selog is designed to be as simple as possible while still being
 extremely efficient, powerful, and flexible. The essence of selog
 is:</p>

<ul>
 <li>Obtain a configuration string from the user, which looks like:
  <blockquote><p>
   <tt>+log_pid+example@syslog;-example@stderr</tt>
  </p></blockquote>
 </li>
 <li>When the program starts, initialize selog with the user's configuration.
  <blockquote><p>
   <tt>selog_open(<i>config</i>, <i>spelling</i>)</tt>
  </p></blockquote>
 </li>
 <li>Define a selector for each different kind of message.
  <blockquote><p>
   <tt>static selog_selector <i>sel</i> = SELINIT("example", SELOG_INFO);</tt>
  </p></blockquote>
 </li>
 <li>Use the selog() function to emit messages instead of printf() or syslog().
  <blockquote><p>
   <tt>selog(<i>sel</i>, "message number %d", <i>n</i>);</tt>
  </p></blockquote>
 </li>
</ul>

<p>Selectors determine which messages are written and where they are
 written to, under the control of the user's configuration. You can
 direct messages to any combination of stderr, syslog, files, pipes,
 etc. You can omit or include optional parts of messages under the
 control of selectors. You don't have to signal the program when you
 rotate its log files.</p>

<p>The C interface consists of just 13 functions, 5 macros, 2 types,
 and an enum. There are a few variations of the basic <tt>selog()</tt>
 one-shot logging function, or you can quickly and easily compose
 messages in stages. The check to skip disabled messages is extremely
 small and fast.</p>

<p>Selog comes with shell command and Lua interfaces, plus
 interposition libraries which you can use to fool old code that calls
 <tt>err()</tt> or <tt>syslog()</tt> into using selog instead.</p>

<p><b>Selog's origins</b></p>

<p>I started work on selog when I found myself writing yet another half-arsed
 logging/debugging module that could only be used by the program it was written
 for. The problem needed to be solved properly, but I couldn't find a decent
 existing solution.</p>

<p>Exim's logging and debugging facilities provided the basis for selog's
 configuration syntax and the idea for the fast check to disable
 debugging code. Like many other programs, Exim's logging and debugging
 code is non-orthogonal and non-reusable; selog exists to avoid
 problems like Exim's fixed set of log files and the lack of debugging
 redirection.</p>

<p>The architecture of log4j and its many imitators provided many nice
 ideas to copy, in particular selog's selectors and channels are
 similar to log4j's loggers and appenders. However log4j is
 unnecessarily complicated, so selog discards plenty of its ideas, such
 as hierarchial category names and arbitrarily configurable message
 layouts. Bloaty log4c provided a salutory anti-pattern.</p>

<p>I've tried to make this first release fairly polished - e.g. it has
 comprehensive documentation - however it has not had its portability
 catches polished off, and in particular I'm going to be interested to
 see how much trouble my use of C99 features causes. The next step is
 to go back to the program that I wrote selog for, and put the code to
 use...</p>
