---
dw:
  anum: 14
  eventtime: "2007-09-30T21:28:00Z"
  itemid: 308
  logtime: "2007-09-30T22:15:47Z"
  props:
    commentalter: 1491292391
    hasscreened: 1
    import_source: livejournal.com/fanf/79433
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/78862.html"
format: casual
lj:
  anum: 73
  can_comment: 1
  ditemid: 79433
  event_timestamp: 1191187680
  eventtime: "2007-09-30T21:28:00Z"
  itemid: 310
  logtime: "2007-09-30T22:15:47Z"
  props: {}
  reply_count: 43
  url: "https://fanf.livejournal.com/79433.html"
title: Computers for children
...

I've been thinking about teaching children to program (or helping them to learn), especially since it came up in conversation at a recent party. (This is slightly inspired by my son, but he's only a year old so he won't be getting to grips with computers for some years yet.) The discussion at the party was based on our experience of starting with 1980s micros, which we think was a good way to learn programming, so how could we provide something similar to the children of today? I think this kind of micro must be:

* safe for playing with minimal supervision
* very simple (or apparently so)
* good for fun as well as for programming

More detailed requirements are a consequence of the above:

* The hardware has to be robust, so no moving parts. The drivers have to be robust too, so you can yank the power or plug/unplug peripherals without bad consequences.
* As cheap as possible, so it doesn't matter if it dies from abuse. This implies it should be off-the-shelf, and able to use standard peripherals, e.g. hand-me-down display etc.
* No high-level networking. Low level features are useful for older children to learn network programming, but for most usage it'll have an air gap for safety.
* No complicated user interface. It'll run one full-screen task at a time, such as a game or a programmer's read/eval/print loop.
* Easy to do fun things with graphics, sounds, and robotics.

I think the right solution is to use a "thin client": one of those computers that's designed to provide a keyboard and display to a remote server. You can get plausible ones for about £100. However, instead of booting off the net, it must be able to boot off flash (built-in or USB). At the moment a couple of good options are the <a href="http://www.tinytuxbox.com/ttbox4">TinyTuxBox 4</a> or the <a href="http://www.linutop.com/">Linutop</a>, both based on the same AMD Geode CPU, though the latter has the advantage of using LinuxBIOS.

It might not be easy or sensible to reflash the BIOS so that these computers can boot straight into BASIC for that authentic 1980s experience, so it's probably best to use USB flash drives as if they were Atari or Nintendo cartridges. Then it should be easy to ship software as disk images - with perhaps collections of games for fun as well as a programming language.

I'm not actually keen on BASIC as a language or environment. Its only surviving descendent is Visual BASIC which is too complicated, volatile, and proprietary for my purposes, and I don't see any advantage in resurrecting an older variant. Logo has plenty of teaching materials, which is handy, and it's a reasonably nice language with a fair amount of power. Or maybe Lua - though perhaps that only seems simple to people who already know how to program. I think it's also worth providing a text editor for programming, not just a command prompt, since we aren't constrained by memory in the way the eight-bits were, and the children will already have some familiarity with word processors.

Under the simple language and simple editor, it is unfortunately difficult to remain simple without expending a great deal of effort. It's a challenge just to get a modern CPU to run without toasting itself to a cinder. On the x86 platform you have 25 years of accumulated expedient hacks to work around in software. Doing general-purpose IO over USB is just a bit more complicated than a latch wired to your data bus. Given all that it seems that the most sensible plan is to start with a Linux or BSD kernel, and put a simplified userland on top.

It's difficult to choose how to support the key features - graphics for example. X11 is <em>not</em> simple, but it is very well supported and it's easy to make it look simple (if you throw away the display manager and window manager etc.) On the other hand it ought to be simple to build on top of the framebuffer device, but then (since I don't want to re-invent many wheels) I'd still end up pulling in layers of complexity (DirectFB and associated libraries) some of which are explicitly experimental and incomplete. But since it's all sitting on a Unix kernel I suppose I've already given up the idea of making its innards easily comprehensible.

Another example of the appearance of simplicity despite enormous amounts of hidden complexity has to be robotics. <a href="http://www.phidgets.com/">Phidgets</a> look like fun hardware interfaces, but of course they depend on USB so they only appear to be simple...

Before signing off I should note that the OLPC XO is not what I'm after. It has more user interface and more networking than I want. I don't want a general-purpose computer, I want a programmable toy.
