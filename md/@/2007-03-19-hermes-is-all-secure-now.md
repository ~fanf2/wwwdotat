---
dw:
  anum: 202
  eventtime: "2007-03-19T17:00:00Z"
  itemid: 279
  logtime: "2007-03-19T17:05:54Z"
  props:
    commentalter: 1491292340
    import_source: livejournal.com/fanf/71694
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/71626.html"
format: casual
lj:
  anum: 14
  can_comment: 1
  ditemid: 71694
  event_timestamp: 1174323600
  eventtime: "2007-03-19T17:00:00Z"
  itemid: 280
  logtime: "2007-03-19T17:05:54Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/71694.html"
title: Hermes is all secure now
...

This afternoon I have been clearing up the remnants of the infrastructure that supported the withdrawal of insecure access to Hermes. For a final tombstone, I've written the following to replace the text on the Computing Service's summary page about the campaign...

Between March 2005 and March 2007, all insecure modes of access to Hermes were withdrawn. The following is a brief history of the process.

Following a decision made by the Computing Service, an initial announcement was sent out in early March 2005. The original timetable was to disable insecure access via protocols that do not require user configuration in July 2005, and to disable insecure access via other protocols between July 2005 and July 2006.

In early May 2005, warning banners were added to the insecure telnet, rlogin, FTP, and webmail login screens. At the start of July 2005, telnet, rlogin and FTP access were disabled, leaving the secure alternatives ssh, scp, and sfp as the only way of accessing the Hermes menu system. At the same time the webmail service was changed to automatically redirect insecure http logins to https.

Over the summer of 2005 we developed documentation to help people reconfigure their MUA - http://www.cam.ac.uk/cs/email/muasettings.html - and more detailed guidelines for sending email from the University network - http://www.cam.ac.uk/cs/email/sending.html

Between October and December 2005, the Computing Service developed software to support the withdrawal of insecure access via IMAP, POP, and SMTP. This included a system for emailing users to tell them that they needed to reconfigure their MUA settings, and a web site that provided information to IT support staff about the configuration status of their users. The software was piloted within the Computing Service before being announced to the University's IT support staff: https://fanf2.user.srcf.net/hermes/doc/talks/2006-01-techlinks/

Over 10,000 users had to change their MUA settings. The software spread out the process of notifying users to minimise the resulting load on IT support staff. We used a ratchet system to disable insecure access for users who had corrected their settings, without affecting other users who were not yet ready. The notification process for each user became gradually more insistent and eventually irritating over a pertiod of about two months, in order to encourage users to change their settings before their insecure access was disabled. This minimised the problem of users coming to work in the morning and ambushing their computer officer because their email did not work; instead they could be dealt with later in the day.

This process was mostly complete by Christmas 2006 - five months later than planned, because of the delayed start, but not because it took longer than intended. During that time, the Computing Service help desk dealt with at least 15% more calls than in the preceding year.

There remained about 250 users sending email through smtp.hermes using email addresses that could not be automatically linked to a Hermes account. These users were notified and reconfigured between January and March 2007. The whole process was complete just over two years after it started.

The Computing Service is grateful to IT staff in the departments and colleges who did a lot of work to help with this campaign, and without whom it would not have been possible.
