---
dw:
  anum: 136
  eventtime: "2006-03-06T12:23:00Z"
  itemid: 206
  logtime: "2006-03-06T12:31:17Z"
  props:
    commentalter: 1491292330
    import_source: livejournal.com/fanf/52992
    interface: flat
    opt_backdated: 1
    picture_keyword: photo
    picture_mapid: 3
  url: "https://fanf.dreamwidth.org/52872.html"
format: casual
lj:
  anum: 0
  can_comment: 1
  ditemid: 52992
  event_timestamp: 1141647780
  eventtime: "2006-03-06T12:23:00Z"
  itemid: 207
  logtime: "2006-03-06T12:31:17Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/52992.html"
title: In the news
...

I've just been talking to the features editor of Varsity, one of the University's student newspapers. She sent us an email on Saturday asking if we would talk to her about Hermes, because "it is such an essential part of student life yet most people know nothing about it. It just is." I said yes, since I try to project a friendly face to the Computing Service.

So if you get a copy of this Friday's Varsity, expect to read about how we deal with dead users, how Hermes has only caught fire once, growing pains, and speculation about life before email.
