---
dw:
  anum: 63
  eventtime: "2008-07-24T11:09:00Z"
  itemid: 350
  logtime: "2008-07-24T12:09:16Z"
  props:
    commentalter: 1491292358
    import_source: livejournal.com/fanf/90413
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/89663.html"
format: html
lj:
  anum: 45
  can_comment: 1
  ditemid: 90413
  event_timestamp: 1216897740
  eventtime: "2008-07-24T11:09:00Z"
  itemid: 353
  logtime: "2008-07-24T12:09:16Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/90413.html"
title: More Kaminsky
...

<p>So there's another form of attack which is closer to the Matasano
description but still different in significant ways.</p>
<pre>
$ md5 <~/doc/kaminsky2
d4b70e6abfa3e7d49e159d75b5fc277b
</pre>
