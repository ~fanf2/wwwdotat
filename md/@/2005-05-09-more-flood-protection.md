---
dw:
  anum: 13
  eventtime: "2005-05-09T18:30:00Z"
  itemid: 141
  logtime: "2005-05-09T18:49:13Z"
  props:
    commentalter: 1491292326
    import_source: livejournal.com/fanf/36373
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/36109.html"
format: casual
lj:
  anum: 21
  can_comment: 1
  ditemid: 36373
  event_timestamp: 1115663400
  eventtime: "2005-05-09T18:30:00Z"
  itemid: 142
  logtime: "2005-05-09T18:49:13Z"
  props: {}
  reply_count: 4
  url: "https://fanf.livejournal.com/36373.html"
title: More flood protection
...

Following on from http://www.livejournal.com/users/fanf/36178.html
I have updated the document at http://www.cus.cam.ac.uk/~fanf2/hermes/doc/antiforgery/ratelimit.html with a new subsection that does a little analysis of the behaviour of the mathematical model.

On Saturday night at <a href="https://j4.livejournal.com/">👤j4</a> I gave <a href="https://hoiho.livejournal.com/">👤hoiho</a> a quick run-down on what it was all about. (We really know how to party in Cambridge!) He suggested looking at the leaky bucket model which is often used in networking circles, because it fairly closely models the way that packet buffers operate. From the mathematical point of view it's probably similar to adjusting the <a href="http://mathworld.wolfram.com/MovingAverage.html">moving average</a> for variable event frequencies as I have done for the exponentially-weighted average.

The reason I didn't look at it initially was because the amount of state it requires increases with the permitted sending rate and burstiness. This is OK if you are a router and are already keeping that state along with the packets, but I have to re-load and re-save the state for each message. However it turns out that the model I picked has pretty much the same intuitive model, where one of the configuration parameters can be thought of as the bucket size, i.e. the maximum size of a burst of messages.

It's nice when a result shows you that your first choice of maths turned out to be a better fit than you expected, and pleasingly neat to boot. It has been fun resurrecting some more of my secondary school knowledge :-)
