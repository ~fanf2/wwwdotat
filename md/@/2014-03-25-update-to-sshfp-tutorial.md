---
dw:
  anum: 91
  eventtime: "2014-03-25T14:52:00Z"
  itemid: 405
  logtime: "2014-03-25T14:52:05Z"
  props:
    import_source: livejournal.com/fanf/131045
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/103771.html"
format: html
lj:
  anum: 229
  can_comment: 1
  ditemid: 131045
  event_timestamp: 1395759120
  eventtime: "2014-03-25T14:52:00Z"
  itemid: 511
  logtime: "2014-03-25T14:52:05Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/131045.html"
title: Update to SSHFP tutorial
...

<p>I have updated yesterday's article on <a href="http://fanf.livejournal.com/130577.html">how to get SSHFP records, DNSSEC, and VerifyHostKeyDNS=yes to work</a>.

<p>I have re-ordered the sections to avoid interrupting the flow of the instructions with chunks of background discussion.

<p>I have also added a section discussing the improved usability vs weakened security of the RRSET_FORCE_EDNS0 patch in Debian and Ubuntu.
