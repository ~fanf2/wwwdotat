---
format: html
lj:
  anum: 64
  can_comment: 1
  ditemid: 115520
  event_timestamp: 1314819420
  eventtime: "2011-08-31T19:37:00Z"
  itemid: 451
  logtime: "2011-08-31T18:37:09Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 3
  url: "https://fanf.livejournal.com/115520.html"
title: Web browser stats
...

<p>Here are some numbers from our webmail service. These cover the dates 2011-08-03 - 2011-08-29 inclusive, during which time there were 1320906 logins by 23087 users. <b>Edit:</b> I have corrected & clarified the Mobile Safari breakdown.</p>

<pre>
users : platform

20130   Windows
 7193   Mac OS
 2671   iOS
 1714   Linux (desktop)
 1054   Android
  393   BlackBerry
  313   Symbian
   92   Samsung
   89   Kindle
   69   SonyEricsson
   57   Nokia
   54   J2ME
   36   SunOS
   11   LG

users : browser

12965   MS IE
12063   Firefox
 7190   Chrome
 5165   Safari (desktop)
 4014   Safari (mobile)
  446   other mobile
  406   Opera
  109   Opera Mini

 9052   MS IE 8
 5277   MS IE 7
 2960   MS IE 9
 1477   MS IE 6
   12   MS IE 5 for Mac

 6566   Firefox 5
 6392   Firefox 3.6
 4226   Firefox 6
 1457   Firefox 4
  823   Firefox 3.5
  775   Firefox 3.0
  322   Firefox 2
   66   Firefox 7
   48   Firefox 1
   31   Firefox non-standard

 6390   Chrome 13
 4132   Chrome 12
  176   Chrome 14
  124   Chrome 11
  112   Chrome 10
   49   Chrome 8
   42   Chrome 6
   41   Chrome 9
   32   Chrome 7
   32   Chrome 5
   28   Chrome 4
   25   Chrome 15

 4974   Safari 53x
  194   Safari 52x
   61   Safari 3xx
   61   other desktop WebKit
   14   Safari 4xx

 3445   Mobile Safari 53x
  554   Mobile Safari 52x
  136   Mobile Safari 41x
   40   Mobile Safari 42x

 2466   iOS     WebKit 53x
  960   Android WebKit 53x
  258   iOS     WebKit 52x
  257   other   WebKit 52x
  170   other   WebKit 53x
   55   Android WebKit 52x
</pre>
