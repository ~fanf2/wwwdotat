---
dw:
  anum: 91
  eventtime: "2006-04-11T19:29:00Z"
  itemid: 219
  logtime: "2006-04-11T19:37:53Z"
  props:
    commentalter: 1491292332
    import_source: livejournal.com/fanf/56547
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/56155.html"
format: casual
lj:
  anum: 227
  can_comment: 1
  ditemid: 56547
  event_timestamp: 1144783740
  eventtime: "2006-04-11T19:29:00Z"
  itemid: 220
  logtime: "2006-04-11T19:37:53Z"
  props: {}
  reply_count: 2
  url: "https://fanf.livejournal.com/56547.html"
title: Why you want single sign-on
...

<a href="http://pch.mit.edu/pipermail/saag/2006q2/001565.html">This message</a> contains a good rant about single sign-on:

<em>The fact that "users don't necessarily want to have to manually authenticate each time some service wants authentication" is not the reason we want to promote single sign-on. We don't want the user to manually authenticate every time because doing so trains the user to supply their credentials so frequently that they will not think it is strange when they are asked to provide them to an attacker.  The only way to prevent phishing attacks are by training users that they only authenticate in very small number of circumstances that rarely occur.</em>
