---
dw:
  anum: 233
  eventtime: "2016-05-13T01:06:00Z"
  itemid: 457
  logtime: "2016-05-13T00:06:46Z"
  props:
    commentalter: 1491292420
    import_source: livejournal.com/fanf/144151
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/117225.html"
format: html
lj:
  anum: 23
  can_comment: 1
  ditemid: 144151
  event_timestamp: 1463101560
  eventtime: "2016-05-13T01:06:00Z"
  itemid: 563
  logtime: "2016-05-13T00:06:46Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 3
  url: "https://fanf.livejournal.com/144151.html"
title: 6 reasons I like Wintergatan
...

<ol>
<li><p>If you have not yet seen the <a href="https://www.youtube.com/watch?v=IvUU8joBb1Q">Wintergatan Marble
Machine</a> video, you
should. It is totally amazing: a musical instrument and a Heath
Robinson machine and a marble run and a great tune. It has been viewed
nearly 19 million times since it was published on the 29th February.</p>

<p><a href="https://www.youtube.com/user/wintergatan2000/videos">Wintergatan have a lot of videos on YouTube</a>
most of which tell the story of the construction of the Marble
Machine. I loved watching them all: an impressive combination of
experimentation and skill.</p></li>
<li><p>The <a href="https://www.youtube.com/watch?v=rEeiRXOlWUE">Starmachine 2000</a>
video is also great. It interleaves a stop-motion animation
(featuring Lego) filmed using a slide machine (which apparently
provides the percussion riff for the tune) with the band showing
some of their multi-instrument virtuosity.</p>

<p>The second half of the video tells the story of another home-made
musical instrument: a music box driven by punched tape, which you
can see playing the melody at the start of the video. Starmachine
2000 was published in January 2013, so it's a tiny hint of their
later greatness.</p>

<p>I think the synth played on the shoulder is also home-made.</p></li>
<li><p><a href="https://www.youtube.com/watch?v=SBK2AF-NdVA">Sommarfågel</a> is the
band's introductory video, also featuring stop-motion animation and
the punched-tape music box.</p>

<p>Its intro ends with a change of time signature from 4/4 to 6/8 time!</p>

<p>The second half of the video is the making of the first half - the
set, the costumes, the animation. The soundtrack sounds like it
was played on the music box.</p></li>
<li><p>They have an album which you can
<a href="https://www.youtube.com/playlist?list=PLLLYkE3G1HEBjOwevVnv-x3OxNqjhVp5f">listen to on YouTube</a>
or <a href="https://itunes.apple.com/gb/artist/wintergatan/id590981684">buy on iTunes</a>.</p>

<p>By my reckoning, 4 of the tracks are in 3/4 time, 3 in 4/4 time,
and 2 in 6/8 time. I seem to like music in unusual time signatures.</p></li>
<li><p>One of the tracks is called "Biking is better".</p></li>
<li><p>No lyrics.</p></li>
</ol>
