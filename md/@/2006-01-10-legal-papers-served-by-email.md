---
dw:
  anum: 0
  eventtime: "2006-01-10T10:54:00Z"
  itemid: 176
  logtime: "2006-01-10T10:58:10Z"
  props:
    commentalter: 1491292327
    import_source: livejournal.com/fanf/45534
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/45056.html"
format: casual
lj:
  anum: 222
  can_comment: 1
  ditemid: 45534
  event_timestamp: 1136890440
  eventtime: "2006-01-10T10:54:00Z"
  itemid: 177
  logtime: "2006-01-10T10:58:10Z"
  props: {}
  reply_count: 7
  url: "https://fanf.livejournal.com/45534.html"
title: Legal papers served by email
...

http://www.theregister.co.uk/2006/01/10/lawsuit_started-by_email_is_valid/

This is a significant case, even if it is only directly relevant to maritime arbitration. The precedent is likely to be applied in other cases where the status of email as written communication is in doubt.

Note that the last paragraph of the article says:

<em>Scottish court actions cannot be served by email. In England, email service is possible but only when there is written consent to this from the other party in advance, according to the Civil Procedure Rules. Accordingly, if a British business receives a court action "out of the blue" by email, it could generally argue that service has not been affected.</em>

Even so, this underlines the responsibility of employees to treat email as seriously as they do the dead tree post.

I wonder when legal papers will first be served by IM :-)
