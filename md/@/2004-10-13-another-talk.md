---
dw:
  anum: 57
  eventtime: "2004-10-13T14:28:00Z"
  itemid: 106
  logtime: "2004-10-13T07:33:17Z"
  props:
    commentalter: 1491292315
    import_source: livejournal.com/fanf/27337
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/27193.html"
format: casual
lj:
  anum: 201
  can_comment: 1
  ditemid: 27337
  event_timestamp: 1097677680
  eventtime: "2004-10-13T14:28:00Z"
  itemid: 106
  logtime: "2004-10-13T07:33:17Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 4
  url: "https://fanf.livejournal.com/27337.html"
title: Another talk
...

Even a quick half-hour talk is very tiring to prepare, and is not good for the wrists. I think this one was bad because other things continued to happen while I was working on it (why is our email load twice what it was last week?), and the subject matter really is too basic to be interesting. So I was <i>even sillier</i> than usual in my choice of illustrations.

http://www.cus.cam.ac.uk/~fanf2/
http://www.cus.cam.ac.uk/~fanf2/hermes/doc/talks/2004-10-techlinks/
http://www.cus.cam.ac.uk/~fanf2/hermes/doc/talks/2004-10-techlinks/source.html
