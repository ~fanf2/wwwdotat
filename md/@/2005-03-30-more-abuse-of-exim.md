---
dw:
  anum: 126
  eventtime: "2005-03-30T20:15:00Z"
  itemid: 137
  logtime: "2005-03-30T20:15:46Z"
  props:
    commentalter: 1491292319
    import_source: livejournal.com/fanf/35567
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/35198.html"
format: casual
lj:
  anum: 239
  can_comment: 1
  ditemid: 35567
  event_timestamp: 1112213700
  eventtime: "2005-03-30T20:15:00Z"
  itemid: 138
  logtime: "2005-03-30T20:15:46Z"
  props: {}
  reply_count: 3
  url: "https://fanf.livejournal.com/35567.html"
title: More abuse of Exim
...

http://www.exim.org/mail-archives/exim-users/Week-of-Mon-20050328/msg00087.html

This arose from my thoughts about domain-specific languages and the programmability thereof. I felt the need to examine further the computational abilities of Exim's ACLs.

[more]
http://www.exim.org/mail-archives/exim-users/Week-of-Mon-20050328/msg00108.html
