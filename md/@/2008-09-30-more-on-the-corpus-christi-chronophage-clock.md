---
dw:
  anum: 162
  eventtime: "2008-09-30T10:34:00Z"
  itemid: 364
  logtime: "2008-09-30T10:34:10Z"
  props:
    commentalter: 1491292370
    import_source: livejournal.com/fanf/94411
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/93346.html"
format: html
lj:
  anum: 203
  can_comment: 1
  ditemid: 94411
  event_timestamp: 1222770840
  eventtime: "2008-09-30T10:34:00Z"
  itemid: 368
  logtime: "2008-09-30T10:34:10Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/94411.html"
title: More on the Corpus Christi Chronophage clock
...

<ul>
<li>I said the clock has three circles of 60 slits. In fact it has two circles of 60 and one of 48 - the hour circle has a slit for every quarter hour.</li>
<li><a href="http://www.chronophage.co.uk/">A website about the clock</a> is going live soon.</li>
<li>A book about the clock is being published, and it will be available from the Corpus Christi porters' lodge amongst other places.</li>
</ul>
