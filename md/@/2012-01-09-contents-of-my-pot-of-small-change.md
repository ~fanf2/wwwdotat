---
format: html
lj:
  anum: 150
  can_comment: 1
  ditemid: 117910
  event_timestamp: 1326118440
  eventtime: "2012-01-09T14:14:00Z"
  itemid: 460
  logtime: "2012-01-09T14:14:33Z"
  props:
    personifi_tags: "nterms:yes"
  reply_count: 9
  url: "https://fanf.livejournal.com/117910.html"
title: Contents of my pot of small change
...

<img src="http://dotat.at/graphics/cash.jpg" style="margin-left:1em; float:right; clear:right;">
<p>I have an earthenware pot into which I dump my small change: any coins worth less than 50p. (It's like a pot of gold that has suffered hyperinflation.) It was nearly full, so I took the coins to my bank in a <i>very sturdy bag</i> to convert them back to bits. Happily HSBC have a coin counting machine which is free for customers. Here are the results:</p>
<tt>
<table>
<tr> <th>coin</th>       <th>count</th>  <th>value</th>   <th>weight</th>   </tr>
<tr> <td>1p (3.56g)</td> <td> 309</td>   <td>£  3.09</td> <td> 1100.04g</td> </tr>
<tr> <td>2p (7.12g)</td> <td> 163</td>   <td>£  3.26</td> <td> 1160.56g</td> </tr>
<tr> <td>5p (3.25g)</td> <td> 283</td>   <td>£ 14.15</td> <td>  919.75g</td> </tr>
<tr> <td>10p (6.5g)</td> <td> 294</td>   <td>£ 29.40</td> <td> 1911.00g</td> </tr>
<tr> <td>20p (5.0g)</td> <td> 426</td>   <td>£ 85.20</td> <td> 2130.00g</td> </tr>
<tr> <th>total</th>      <td>1475</td>   <td>£135.10</td> <td> 7221.35g</td> </tr>
</table>
</tt>
<p>The machine also found US$1.91, NZ$0.50, HK$0.50, and €0.02.</p>
