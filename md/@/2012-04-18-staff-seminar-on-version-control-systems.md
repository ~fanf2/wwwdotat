---
format: html
lj:
  anum: 70
  can_comment: 1
  ditemid: 119878
  event_timestamp: 1334748660
  eventtime: "2012-04-18T11:31:00Z"
  itemid: 468
  logtime: "2012-04-18T10:31:44Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 2
  url: "https://fanf.livejournal.com/119878.html"
title: Staff seminar on version control systems
...

<p>This morning <a href="http://jw35.blogspot.co.uk/">Jon Warbrick</a> and I gave a pair of talks to our colleagues about version control systems. Jon did an introduction and overview of the topic aimed at people who aren't particularly technical. I followed with descriptions of the particular version control systems we are using in the Computing Service, organized historically. You can see <a href="https://fanf2.user.srcf.net/hermes/doc/talks/2012-04-staffsem/slides.pdf">my slides</a> and <a href="https://fanf2.user.srcf.net/hermes/doc/talks/2012-04-staffsem/notes.pdf">my notes</a>.</p>
