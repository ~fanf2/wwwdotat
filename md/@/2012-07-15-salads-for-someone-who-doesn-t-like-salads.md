---
format: html
lj:
  anum: 143
  can_comment: 1
  ditemid: 122767
  event_timestamp: 1342365600
  eventtime: "2012-07-15T15:20:00Z"
  itemid: 479
  logtime: "2012-07-15T14:20:54Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 3
  url: "https://fanf.livejournal.com/122767.html"
title: "Salads for someone who doesn't like salads"
...

<p><a href="http://rmc28.dreamwidth.org">Rachel</a> (<a href="http://twitter.com/rmc28">@rmc28</a>) doesn't like most salads: she doesn't like vinegary dressings, and she doesn't like peppery salad leaves. But recently we have been finding some salads that she <i>does</i> like.</p>

<p>Based on a suggestion from her mother Ruth, she has become quite fond of a combination of baby spinach, feta, walnuts, and cherry tomatoes - sometimes with romaine hearts and/or avocado.</p>

<p>On a summery day a few weeks ago I made a seasonal lunch with steamed new potatoes, tuna and sweetcorn mayo, and spinach leaves. I made a dressing from three teaspoons each of mustard and honey, juice of a lemon and a lime, about two or three times that amount of olive oil, and a bit of salt and pepper. (I might not have remembered the quantities correctly - adjust to your taste! - and I made far too much for two.) This turned out to be a great success.</p>

<p>Earlier this week I made a chunky salad of diced cucumber, celery, tomatoes, feta, and grated carrot, with the honey/mustard/lemon dressing. This went quite well with pork pies.</p>

<p>If you follow <a href="http://dotat.at/:/">my link log</a> you might have seen <a href="http://www.sciencedaily.com/releases/2012/06/120619230234.htm">an article about oil in salad dressings</a> which says it helps the body absorb fat-soluble nutrients. So it isn't just tasty, it's nutritious too!</p>
