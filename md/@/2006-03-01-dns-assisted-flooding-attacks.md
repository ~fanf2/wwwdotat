---
dw:
  anum: 63
  eventtime: "2006-03-01T17:38:00Z"
  itemid: 204
  logtime: "2006-03-01T17:44:26Z"
  props:
    import_source: livejournal.com/fanf/52521
    interface: flat
    opt_backdated: 1
    picture_keyword: ""
  url: "https://fanf.dreamwidth.org/52287.html"
format: casual
lj:
  anum: 41
  can_comment: 1
  ditemid: 52521
  event_timestamp: 1141234680
  eventtime: "2006-03-01T17:38:00Z"
  itemid: 205
  logtime: "2006-03-01T17:44:26Z"
  props: {}
  reply_count: 0
  url: "https://fanf.livejournal.com/52521.html"
title: DNS-assisted flooding attacks
...

http://lists.oarci.net/pipermail/dns-operations/2006-February/000122.html

At the moment there are a lot of DNS-based attacks going on. They generally rely on spoofed queries, where an attacker sends a forged DNS query to an open resolver (the reflector) which sends a large response (amplification) to the victim. A lot of people are saying that wider implementation of BCP38 would significantly reduce the problem, because that requires ISPs to filter spoofed packets at their borders. However the DNS relies on referrals from one name server to another, which can be used for reflecting and amplifying attacks even when UDP forgery is prevented.
