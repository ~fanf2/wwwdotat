---
dw:
  anum: 199
  eventtime: "2016-05-08T13:53:00Z"
  itemid: 455
  logtime: "2016-05-08T12:53:45Z"
  props:
    import_source: livejournal.com/fanf/143787
    interface: flat
    opt_backdated: 1
    picture_keyword: dotat
    picture_mapid: 1
  url: "https://fanf.dreamwidth.org/116679.html"
format: html
lj:
  anum: 171
  can_comment: 1
  ditemid: 143787
  event_timestamp: 1462715580
  eventtime: "2016-05-08T13:53:00Z"
  itemid: 561
  logtime: "2016-05-08T12:53:45Z"
  props:
    give_features: 1
    personifi_tags: "nterms:yes"
  reply_count: 0
  url: "https://fanf.livejournal.com/143787.html"
title: Belated job update
...

<p><a href="https://twitter.com/fanf/status/728659337862844416">I mentioned obliquely on Twitter that I am no longer responsible for email at Cambridge</a>. This surprised a number of people so I realised I should do a proper update for my non-Cambridge friends.

<p>Since Chris Thompson retired at the end of September 2014, I have been the University's hostmaster. I had been part of the hostmaster team for a few years before then, but now I am the person chiefly responsible. I am assisted by my colleagues in the Network Systems team.

<p>Because of my increased DNS responsibilities, I was spending a lot less time on email. This was fine from my point of view - I had been doing it for about 13 years, which frankly is more than enough.

<p>Also, during the second half of 2015, our department reorganization at long last managed to reach down from rarefied levels of management and started to affect the technical staff.

<p>The result is that at the start of 2016 I moved into the Network Systems team. We are mainly responsible for the backbone and data centre network switches and routers, plus a few managed institution networks. (Most departments and colleges manage their own switches.) There are various support functions like DHCP, RADIUS, and now DNS, and web interfaces to some of these functions.

<p>My long-time fellow postmaster David Carter continues to be responsible for Hermes and has moved into a new team that includes the Exchange admins. There will be more Exchange in our future; it remains to be decided what will happen to Hermes.

<p>So my personal mail is no longer a dodgy test domain and reconfiguration canary on Hermes. Instead I am using <a href="https://www.fastmail.com">Fastmail</a> which has been hosting our family domain for a few years.
