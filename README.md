Tony Finch's web site
=====================

A static-site generator written in Rust using `pulldown-cmark` to
render markdown, and `handlebars` templates.


filenames / URLs
----------------

  * blog entries: /@/YYYY-MM-DD-slug.html

    (maybe without .html ??)

  * archive contents: /@/ aka /@/index.html

  * recent entries: /@/blog.html /@/blog.atom

  * redirects: /blog.* -> /@/blog.*


template variables
------------------

  * all pages:

      * `head_title`
      * `body_title`
      * `blog_links`? or maybe a partial called `extra_links`?
      * `template` (except for blog pages)
      * (todo) `revision`

  * recent entries and atom feed

      * `recent` list of entries:

          * `title`
          * `url`
          * `date`
          * `long`
          * `short`
          * `updated`
          * `published`

  * blog contents

      * `annual` list of:

          * `year`
          * `entry` list
