use crate::prelude::*;

pub fn html(md: &str) -> String {
    use pulldown_cmark::Options;

    let mut options = Options::empty();
    options.insert(Options::ENABLE_FOOTNOTES);
    options.insert(Options::ENABLE_SMART_PUNCTUATION);
    options.insert(Options::ENABLE_STRIKETHROUGH);
    options.insert(Options::ENABLE_TABLES);

    let parser = pulldown_cmark::Parser::new_ext(md, options);
    let mut html = String::with_capacity(md.len() * 3 / 2);
    pulldown_cmark::html::push_html(&mut html, parser);

    html
}

pub fn split_yaml<'a, T>(contents: &'a str) -> Result<(T, String, String)>
where
    T: Deserialize<'a>,
{
    // I would prefer to use multiple YAML documents in a file, but
    // (like pyyaml but unlike perl's various YAML implementations)
    // yaml-rust does not support a top-level un-indented preformatted
    // scalar, like "--- |\nmarkdown\n". So alternatively let's use
    // the YAML "..." end marker to find the start of the markdown.
    let parts = regex!(
        r#"(?sx)^
            (?P<metadata>
              ---\n
              .*?\n
              \.\.\.\n+
            )?
            (?:
              (?P<title>[^\n{}]+)\n
              ===+\n+
            )?
            (?P<body>
              .*?
            )
            $"#
    );
    if let Some(found) = parts.captures(contents) {
        let part =
            move |name, or| found.name(name).map_or(or, |cap| cap.as_str());
        let metadata = yaml::from_str(part("metadata", "---\n{}\n"))?;
        let title = part("title", "").to_string();
        let body = part("body", "").to_string();
        Ok((metadata, title, body))
    } else {
        bail!("could not find yaml cut");
    }
}

pub fn cut(body: String, url: &str) -> (String, String) {
    let cut = regex!(
        r#"(?sx)^
                (.*?)
                \n<(cut|toc)>\n
                (.*)
                $"#
    );
    if let Some(split) = cut.captures(&body) {
        let short = format!(
            "{}\n\n<p><i><a href='{}'>read more ...</a></i></p>\n",
            &split[1], url
        );
        let (anchored, toc) = anchor_toc(&split[3]);
        if &split[2] == "toc" {
            (short, split[1].to_string() + &toc + &anchored)
        } else {
            (short, split[1].to_string() + &anchored)
        }
    } else {
        let (anchored, _) = anchor_toc(&body);
        (body, anchored)
    }
}

fn anchor_toc(original: &str) -> (String, String) {
    let heading = regex!(
        r#"(?sx)^
            (?P<before>.*?\n)
            (?:
              (?P<atx>\#{2,6})\s+(?P<heading>[^\n{}]+)\n
            |
              (?P<setext>[^\n{}]+)\n--+\n
            |
              $
            )
            "#
    );

    let mut md = String::new();
    let mut toc = String::new();
    let mut start = 0;

    while let Some(found) = heading.captures(&original[start..]) {
        md.push_str(&found["before"]);

        let mut level = 0;
        let mut heading = "";
        if let Some(atx) = found.name("atx") {
            level = atx.as_str().len();
            heading = &found["heading"];
        }
        if let Some(setext) = found.name("setext") {
            level = 2;
            heading = setext.as_str();
        }

        if level > 0 {
            let indent = " ".repeat(4 * (level - 2) + 2);
            let mut anchor =
                regex!(r"[^0-9A-Za-z]+").replace_all(heading, "-").into_owned();
            anchor.make_ascii_lowercase();
            writeln!(md, "<h{}><a name='{}' href='#{}'>{}</a></h{}>",
                     level, anchor, anchor, heading, level).unwrap();
            writeln!(toc, "{}* [{}](#{})", indent, heading, anchor).unwrap();
        }
        start += found[0].len();
    }

    (md, toc)
}
