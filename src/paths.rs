use crate::prelude::*;

pub fn site_source_directory(subdir: &str) -> Result<PathBuf, std::io::Error> {
    let mut path = std::env::current_exe()?;
    // on chiark we want
    // ~/public-cgi/cgi-links -> ~/wwwdotat/
    path.pop();
    path.pop();
    path.push("wwwdotat");
    if !path.is_dir() {
        // otherwise assume we are in the build environment so
        // wwwdotat/target/debug/cgi-links
        // -> wwwdotat/target/wwwdotat/
        // -> wwwdotat/
        path.pop();
        path.pop();
    }
    path.push(subdir);
    Ok(path)
}

pub fn data_file_path(filename: &str) -> Result<PathBuf> {
    let mut path = site_source_directory("data")?;
    path.push(filename);
    Ok(path)
}

pub fn read_stylesheet() -> Result<String> {
    let mut path = site_source_directory("other")?;
    path.push("fanf.css");
    let ctx = || format!("could not read {}", path.display());
    std::fs::read_to_string(&path).with_context(ctx)
}

pub fn read_yaml<T>(filename: &str) -> Result<T>
where
    T: DeserializeOwned,
{
    let path = data_file_path(filename)?;
    let ctx = |what| format!("could not {} {}", what, path.display());
    let f = File::open(&path).with_context(|| ctx("read"))?;
    yaml::from_reader(f).with_context(|| ctx("parse YAML in"))
}

pub fn write_yaml<T>(filename: &str, data: &T) -> Result<()>
where
    T: Serialize,
{
    let tempname = now3339() + "." + filename;
    let temppath = data_file_path(&tempname)?;
    let filepath = data_file_path(filename)?;
    let ctx = || {
        let _ = std::fs::remove_file(&temppath);
        format!("could not write {}", filepath.display())
    };

    let mut f = std::fs::OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(&temppath)
        .with_context(ctx)?;
    yaml::to_writer(&f, data).with_context(ctx)?;
    f.flush().with_context(ctx)?;
    std::fs::rename(&temppath, &filepath).with_context(ctx)
}

pub fn find_all_markdown() -> Result<Vec<PathBuf>> {
    use walkdir::{DirEntry, Error, WalkDir};

    fn keep(entry: Result<DirEntry, Error>) -> Option<DirEntry> {
        match entry {
            Err(_) => None,
            Ok(entry) => {
                let name = entry.file_name().to_string_lossy();
                if entry.file_type().is_file()
                    && name.ends_with(".md")
                    && !name.starts_with('.')
                    && !name.starts_with('#')
                {
                    Some(entry)
                } else {
                    None
                }
            }
        }
    }

    let md = site_source_directory("md")?;
    let parent = md.parent().unwrap();
    Ok(WalkDir::new(&md)
        .sort_by_file_name()
        .into_iter()
        .filter_map(keep)
        .map(|entry| {
            entry.into_path().strip_prefix(parent).unwrap().to_path_buf()
        })
        .collect())
}
