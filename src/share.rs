use crate::prelude::*;
use futures::prelude::*;
use irc::client::prelude::*;

pub enum Via {
    Irc,
    Drum,
    Newsy,
    ArchiveOrg,
    ArchiveToday,
}

pub async fn share_all(link: &RawLink) {
    tokio::join! {
        share_one(link, share::Via::Irc),
        share_one(link, share::Via::Drum),
        share_one(link, share::Via::Newsy),
        share_one(link, share::Via::ArchiveOrg),
        share_one(link, share::Via::ArchiveToday),
    };
}

pub async fn share_one(link: &RawLink, via: Via) {
    if let Err(err) = match via {
        Via::Irc => irc(link).await,
        Via::Drum => drum(link).await,
        Via::Newsy => newsy(link).await,
        Via::ArchiveOrg => archive_org(link).await,
        Via::ArchiveToday => archive_today(link).await,
    } {
        eprintln!("{err:#}");
    }
}

async fn irc(link: &RawLink) -> Result<()> {
    fn some(s: &str) -> Option<String> {
        Some(s.to_owned())
    }
    fn vec(s: &str) -> Vec<String> {
        vec![s.to_owned()]
    }

    let config = Config {
        owners: vec("fanf"),
        nickname: some("fanf-url"),
        username: some("fanf"),
        realname: some("Tony Finch"),
        server: some("irc.chiark.greenend.org.uk"),
        port: Some(6667),
        use_tls: Some(false),
        ..Config::default()
    };

    let mut client = Client::from_config(config).await?;
    client.identify()?;

    let mut stream = client.stream()?;
    let sender = client.sender();

    while let Some(message) = stream.next().await.transpose()? {
        match message.command {
            Command::PING(peer, _) => {
                sender.send_pong(peer)?;
            }
            Command::NOTICE(to, msg)
                if to == "fanf-url" && msg.starts_with("on ") =>
            {
                sender.send_privmsg("#chiark", link.format())?;
                sender.send_quit("quit")?;
            }
            _ => (),
        }
    }

    Ok(())
}

async fn send_request(request: reqwest::RequestBuilder) -> Result<String> {
    // why do i have to unwrap the request builder like this, sigh
    let rclone = request.try_clone().unwrap().build().unwrap();
    let request_brief = format!("{} {}", rclone.method(), rclone.url());
    let request_ctx = || format!("failed to {}", request_brief);
    let request = request.header("User-Agent", "fanf-url/2.0");
    let response = request.send().await.with_context(request_ctx)?;
    let status = response.status();
    let bytes = response.bytes().await.with_context(request_ctx)?;
    let body = std::str::from_utf8(&bytes)
        .with_context(|| format!("malformed utf8 from {request_brief}"))?;
    if status != reqwest::StatusCode::OK {
        bail!("{} failed\n{}\n{}", request_brief, status, body)
    } else {
        Ok(body.to_owned())
    }
}

async fn drum(link: &RawLink) -> Result<()> {
    let cookies: HashMap<String, HashMap<String, String>> =
        read_yaml("cookies.yml")?;
    let request = reqwest::Client::new()
        .post("https://mendeddrum.org/api/v1/statuses")
        .bearer_auth(&cookies["mendeddrum.org"]["bearer"])
        .form(&[("status", link.format())]);
    let _ = send_request(request).await?;
    Ok(())
}

async fn newsy(link: &RawLink) -> Result<()> {
    let cookies: HashMap<String, HashMap<String, String>> =
        read_yaml("cookies.yml")?;
    let cookie = format!("user={}", &cookies["news.ycombinator.com"]["user"]);
    let client = reqwest::Client::new();
    // first get csrf token
    let xsrf_request = client
        .get("https://news.ycombinator.com/submit")
        .header("Cookie", &cookie);
    let xsrf_body = send_request(xsrf_request).await?;
    let match_fnid = regex!(
        r#"(?sx)<\s*input
                 \s+type=["']hidden["']
                 \s+name=["']fnid["']
                 \s+value=["']([^"']+)["']
                 \s*>"#
    );
    let caps = match_fnid
        .captures(&xsrf_body)
        .ok_or_else(|| anyhow!("could not find newsy fnid"))
        .with_context(|| xsrf_body.clone())?;
    let form = [
        ("fnop", "submit-page"),
        ("fnid", &caps[1]),
        ("url", &link.link),
        ("title", &link.format_newsy()),
    ];
    let submit_request = client
        .post("https://news.ycombinator.com/r")
        .header("Cookie", &cookie)
        .form(&form);
    let _ = send_request(submit_request).await?;
    Ok(())
}

async fn archive_org(link: &RawLink) -> Result<()> {
    let request = reqwest::Client::new()
        .post("https://web.archive.org/save")
        .form(&[("url_preload", &link.link)]);
    let _ = send_request(request).await?;
    Ok(())
}

async fn archive_today(link: &RawLink) -> Result<()> {
    let request = reqwest::Client::new()
        .post("https://archive.ph/submit/")
        .form(&[("url", &link.link)]);
    let _ = send_request(request).await?;
    Ok(())
}
