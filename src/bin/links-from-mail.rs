use mailparse::*;
use wwwdotat::prelude::*;

const VERBOSE_DUPS: bool = false;
const VERBOSE_NEW: bool = false;
const VERBOSE_PARSE: bool = false;

fn random_tag() -> String {
    std::iter::repeat_with(|| fastrand::digit(36).to_ascii_uppercase())
        .take(5)
        .collect()
}

fn find_text_plain(msg: &ParsedMail) -> Result<String> {
    if msg.ctype.mimetype == "text/plain" {
        return Ok(msg.get_body()?);
    }
    for part in &msg.subparts {
        if let Ok(body) = find_text_plain(part) {
            return Ok(body);
        }
    }
    bail!("missing text/plain part");
}

fn find_link(msg: &ParsedMail) -> Result<String> {
    let body = find_text_plain(msg)?;
    for line in body.lines() {
        let line = line.trim();
        if line.starts_with("http://") || line.starts_with("https://") {
            return Ok(line.to_string());
        }
    }
    bail!("missing link");
}

fn process_message(raw_msg: &str) -> Result<RawLink> {
    let msg = parse_mail(raw_msg.as_bytes())?;
    let mut from = false;
    let mut date = String::new();
    let mut text = String::new();
    for hdr in msg.get_headers() {
        match hdr.get_key_ref().as_ref() {
            "Date" => {
                date = unix3339(dateparse(&hdr.get_value())?)?;
            }
            "From" => {
                if hdr.get_value().contains("Tony Finch") {
                    from = true;
                } else {
                    bail!("from a stranger");
                }
            }
            "Subject" => {
                text = hdr.get_value();
            }
            _ => continue,
        }
    }
    if !from {
        bail!("missing from");
    }
    if date.is_empty() {
        bail!("missing date");
    }
    if text.is_empty() {
        bail!("missing subject");
    }
    let link = find_link(&msg)?;
    let updated = None;
    Ok(RawLink { text, link, date, updated })
}

fn process_mbox(mbox: &Path) -> Result<Vec<RawLink>> {
    let mut links = vec![];
    let mut msg = String::new();
    let fh = File::open(mbox)
        .with_context(|| format!("opening {}", mbox.display()))?;
    for line in BufReader::new(fh).lines() {
        let mut line = line?;
        line.push('\n');
        if line.starts_with("From ") {
            if !msg.is_empty() {
                match process_message(&msg) {
                    Ok(link) => links.push(link),
                    Err(err) if VERBOSE_PARSE => {
                        dbg!(err);
                        print!("{msg}");
                    }
                    _ => (),
                }
                msg = String::new();
            }
        } else if line.starts_with(">From ") {
            msg.push_str(line.trim_start_matches('>'));
        } else {
            msg.push_str(&line);
        }
    }
    fastrand::shuffle(&mut links);
    Ok(links)
}

fn git(args: &[&str]) -> Result<()> {
    let failed = || format!("could not git {}", args[0]);
    ensure!(
        std::process::Command::new("git")
            .args(args)
            .current_dir(site_source_directory("data")?)
            .status()
            .with_context(failed)?
            .success(),
        failed()
    );
    Ok(())
}

fn commit_links(links: &TagsLinks, message: &str) -> Result<()> {
    write_yaml("links.yml", &links)?;
    git(&["commit", "-m", message, "links.yml"])?;
    git(&["push", "--quiet"])?;
    Ok(())
}

fn swizzle(links: &TagsLinks) -> BTreeMap<String, String> {
    let mut index = BTreeMap::new();
    for (tag, raw) in links.iter() {
        index.insert(raw.link.clone(), tag.clone());
    }
    index
}

#[tokio::main]
async fn main() -> Result<()> {
    what_ident_options(&["--version"]);
    git(&["pull", "--quiet", "--ff-only"])?;
    let mut links = read_yaml("links.yml")?;
    let mbox_name = data_file_path("to-link-log")?;
    let new_links = process_mbox(&mbox_name)?;
    let index = swizzle(&links);
    let mut duplicates = 0;
    let mut shared = false;
    for link in new_links {
        if let Some(tag) = index.get(&link.link) {
            duplicates += 1;
            if VERBOSE_DUPS {
                dbg!(("match", link, tag, links.get(tag)));
            }
        } else {
            let tag = random_tag();
            let link = RawLink { updated: Some(now3339()), ..link };
            if VERBOSE_NEW {
                dbg!(("new", &tag, &link));
            } else {
                eprintln!("{} {}", &tag, &link.link);
            }
            links.insert(tag.clone(), link.clone());
            commit_links(&links, &format!("{}: {}", link.date, link.text))?;
            share::share_all(&link).await;
            shared = true;
            break;
        }
    }
    if !shared {
        while let Some((_, link)) = fastrand::choice(links.iter()) {
            let age = link.age();
            if 24 < age && age < 84 {
                share::share_all(link).await;
                break;
            }
        }
    }
    dbg!(duplicates);
    Ok(())
}
