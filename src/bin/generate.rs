use wwwdotat::prelude::*;

const DST: &str = "target/site";

const BLOG_RECENT_COUNT: usize = 20;
const BLOG_TEASER_COUNT: usize = 10;

// we're treating casual as markdown, which seems to work OK
#[derive(
    Clone, Copy, Debug, Default, Deserialize, Eq, PartialEq, Serialize,
)]
#[serde(rename_all = "lowercase")]
enum Format {
    Html,
    #[serde(alias = "casual")]
    Casual,
    #[default]
    #[serde(alias = "md")]
    Markdown,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
struct BlogLink {
    url: String,
    year: String,
    date: String,
    title: String,
}

impl BlogLink {
    fn new_from_file(file: &SiteFile) -> Option<BlogLink> {
        let caps = regex!(r"^https://dotat\.at/@/((\d{4})-\d\d-\d\d)[.-]")
            .captures(&file.url)?;
        // typographical improvement
        let date = caps[1].to_string();
        let year = caps[2].to_string();
        let title = match file.metadata.title.as_str() {
            "" => "&star;".to_string(),
            t => t.to_string(),
        };
        let url = file.url.clone();
        Some(BlogLink { url, year, date, title })
    }
}

impl Default for BlogLink {
    fn default() -> BlogLink {
        BlogLink {
            url: "https://dotat.at/@/".to_string(),
            year: "&star;".to_string(),
            date: "&star;".to_string(),
            title: "&star;".to_string(),
        }
    }
}

fn write_blog_toc(
    hbs: &Handlebars,
    global: &SiteGlobal,
    blog: &[BlogEntry],
) -> Result<()> {
    #[derive(Debug, Serialize)]
    struct BlogToc<'a, 'g> {
        global: &'g SiteGlobal,
        metadata: Titles,
        by_year: Vec<BlogYear<'a>>,
    }

    #[derive(Debug, Serialize)]
    struct BlogYear<'a> {
        year: String,
        entry: Vec<&'a BlogLink>,
    }

    impl BlogYear<'_> {
        fn new(year: &str) -> BlogYear {
            BlogYear { year: year.to_string(), entry: vec![] }
        }
    }

    let mut by_year = vec![];
    let mut this = BlogYear::new("");
    for entry in blog.iter().rev() {
        if entry.me.year != this.year {
            let next = BlogYear::new(&entry.me.year);
            let prev = std::mem::replace(&mut this, next);
            by_year.push(prev);
        }
        this.entry.push(&entry.me);
    }
    by_year.push(this);
    by_year.remove(0);

    let metadata = Titles::new("Tony Finch &ndash; blog archive contents");
    let data = BlogToc { global, metadata, by_year };
    let dst = format!("{}/@/index.html", DST);
    hbs.render_to_file("blog-toc", &data, &dst)?;

    Ok(())
}

fn write_blog_recent(
    hbs: &Handlebars,
    global: &SiteGlobal,
    blog: &[BlogEntry],
) -> Result<()> {
    #[derive(Debug, Serialize)]
    struct BlogRecent<'a, 'b, 'g> {
        global: &'g SiteGlobal,
        metadata: Titles,
        recent: Vec<&'a BlogEntry<'b>>,
    }

    let first = blog.len().saturating_sub(BLOG_RECENT_COUNT);
    let recent = blog[first..].iter().rev().collect();

    let metadata = Titles::new("Tony Finch &ndash; blog");
    let data = BlogRecent { global, metadata, recent };

    let dst = format!("{}/@/blog.html", DST);
    hbs.render_to_file("blog-recent", &data, &dst)?;

    let dst = format!("{}/@/blog.atom", DST);
    hbs.render_to_file("blog-atom", &data, &dst)?;

    Ok(())
}

#[derive(Debug, Serialize)]
struct BlogEntry<'g> {
    toc_year: String,
    me: BlogLink,
    prev: BlogLink,
    next: BlogLink,
    time: FileTimes,
    html: bool,
    short: String,
    long: String,
    metadata: Metadata,
    migrated: bool,
    global: &'g SiteGlobal,
}

fn write_blog(
    hbs: &Handlebars,
    files: &mut BTreeMap<String, SiteFile>,
    global: &SiteGlobal,
) -> Result<Vec<BlogLink>> {
    let mut links = vec![];
    for file in files.values() {
        if let Some(link) = BlogLink::new_from_file(file) {
            links.push(link);
        }
    }

    let render = |(i, link): (usize, &BlogLink)| {
        let mut file = files[&link.url].clone();
        if !file.metadata.template.is_empty() {
            bail!("cannot specify a template in {}", file.src);
        }
        if file.metadata.head_title != file.metadata.title {
            bail!("mismated head_title in {}", file.src);
        }
        if file.metadata.body_title != file.metadata.title {
            bail!("mismated body_title in {}", file.src);
        }
        file.metadata.head_title = format!("{} &ndash; Tony Finch", link.title);
        file.metadata.body_title = "Tony Finch &ndash; blog".to_string();
        let data = BlogEntry {
            toc_year: format!("#{}", link.year),
            me: link.clone(),
            prev: links.get(i.wrapping_sub(1)).cloned().unwrap_or_default(),
            next: links.get(i.wrapping_add(1)).cloned().unwrap_or_default(),
            time: file.time,
            html: file.html,
            short: file.short,
            long: file.long,
            migrated: file.metadata.migrated(),
            metadata: file.metadata,
            global,
        };
        hbs.render_to_file("blog-entry", &data, &file.dst)?;
        Ok(data)
    };

    let results: Vec<Result<BlogEntry>> =
        links.par_iter().enumerate().map(render).collect();
    let mut blog = vec![];
    for result in results {
        let entry = result?;
        files.remove(&entry.me.url).unwrap();
        blog.push(entry);
    }

    write_blog_toc(hbs, global, &blog)?;
    write_blog_recent(hbs, global, &blog)?;

    let first = blog.len().saturating_sub(BLOG_TEASER_COUNT);
    let blog_teaser =
        blog[first..].iter().rev().map(|entry| entry.me.clone()).collect();

    Ok(blog_teaser)
}

// a page's yaml frontmatter

#[derive(Clone, Debug, Default, Deserialize, Eq, PartialEq, Serialize)]
#[serde(default, deny_unknown_fields)]
struct Metadata {
    title: String,
    head_title: String,
    body_title: String,
    format: Format,
    template: String,
    comments: HashMap<String, String>,
    wip: bool,
    dw: yaml::Value,
    lj: yaml::Value,
}

impl Metadata {
    fn migrated(&self) -> bool {
        self.lj != yaml::Value::Null || self.dw != yaml::Value::Null
    }
}

#[derive(Clone, Debug, Default, Eq, PartialEq, Serialize)]
struct SiteGlobal {
    stylesheet: String,
    what_ident: &'static str,
    blog_teaser: Vec<BlogLink>,
}

#[derive(Clone, Debug, Default, Eq, PartialEq, Serialize)]
struct SiteFile<'g> {
    src: String,
    dst: String,
    url: String,
    time: FileTimes,
    metadata: Metadata,
    html: bool,
    short: String,
    long: String,
    global: Option<&'g SiteGlobal>,
}

fn write_pages(
    hbs: &Handlebars,
    files: BTreeMap<String, SiteFile>,
    global: &SiteGlobal,
) -> Result<()> {
    let render = |page: SiteFile| write_one_page(page, global, hbs);
    let mut pages: Vec<SiteFile> = files.into_values().collect();
    let results: Vec<Result<()>> = pages.par_drain(..).map(render).collect();
    for result in results {
        result?;
    }
    Ok(())
}

fn write_one_page(
    page: SiteFile,
    global: &SiteGlobal,
    hbs: &Handlebars,
) -> Result<()> {
    let page = SiteFile { global: Some(global), ..page };
    match page.metadata.template.as_ref() {
        "" => hbs.render_to_file("page", &page, &page.dst),
        "self" => hbs.render_template_to_file(&page.long, &page, &page.dst),
        template => hbs.render_to_file(template, &page, &page.dst),
    }
}

fn read_one_page<'f>(
    path: PathBuf,
    times: &filetimes::Cache,
) -> Result<SiteFile<'f>> {
    let contents = std::fs::read_to_string(&path)?;
    let (mut metadata, title, body): (Metadata, String, String) =
        md::split_yaml(&contents)
            .with_context(|| format!("in {}", path.display()))?;
    if metadata.title.is_empty() {
        metadata.title = title;
    } else if !title.is_empty() {
        bail!("conflicting titles in {}", path.display());
    }
    if metadata.head_title.is_empty() {
        metadata.head_title = metadata.title.clone();
    }
    if metadata.body_title.is_empty() {
        metadata.body_title = metadata.title.clone();
    }
    // TODO: use Path API properly
    let disp = path.display();
    let src = path
        .to_str()
        .ok_or_else(|| anyhow!("invalid UTF-8: {}", &disp))?
        .to_string();
    let caps = regex!(r"md/(.*)\.md$")
        .captures(&src)
        .ok_or_else(|| anyhow!("failed to match md/**.md: {}", &disp))?;
    let url = format!("https://dotat.at/{}.html", &caps[1]);
    let dst = format!("{}/{}.html", DST, &caps[1]);

    let mut time = FileTimes::default();
    if metadata.migrated() {
        if let yaml::Value::String(date) = &metadata.lj["eventtime"] {
            time.update(date);
        }
        if let yaml::Value::String(date) = &metadata.lj["logtime"] {
            time.update(date);
        }
        if let yaml::Value::String(date) = &metadata.dw["eventtime"] {
            time.update(date);
        }
        if let yaml::Value::String(date) = &metadata.dw["logtime"] {
            time.update(date);
        }
    } else {
        time = times.get(&src)?;
    }

    let global = None; // we have not collected global data yet
    let html = metadata.format == Format::Html;
    let (short, long) =
        if html { html_cut(body, &url) } else { md::cut(body, &url) };

    Ok(SiteFile { src, dst, url, metadata, time, html, short, long, global })
}

fn main() -> Result<()> {
    let hbs = read_all_handlebars()?;
    let paths = find_all_markdown()?;
    let mut times = filetimes::Cache::new()?;
    let results: Vec<Result<SiteFile>> =
        paths.into_par_iter().map(|file| read_one_page(file, &times)).collect();
    let mut files = BTreeMap::new();
    for result in results {
        let file = result?;
        if !file.metadata.migrated() {
            times.set(&file.src, &file.time);
        }
        if !file.metadata.wip {
            files.insert(file.url.clone(), file);
        }
    }
    let mut global = SiteGlobal {
        stylesheet: read_stylesheet()?,
        what_ident: WHAT_IDENT,
        blog_teaser: vec![],
    };
    global.blog_teaser = write_blog(&hbs, &mut files, &global)?;
    write_pages(&hbs, files, &global)?;
    times.write()?;
    Ok(())
}
