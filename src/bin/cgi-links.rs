#![allow(clippy::comparison_to_empty)]
use wwwdotat::prelude::*;

// so that we can get the URL as a literal for concat!()
macro_rules! URL {
    () => {
        "https://dotat.at/:"
    };
}

const URL: &str = URL!();
const HTML_FEED_URL: &str = concat!(URL!(), "/feed.html?");

fn getenv(name: &str) -> String {
    std::env::var(name).unwrap_or_default()
}

fn join<W, V>(f: &mut W, vec: &[V], sep: &str) -> std::fmt::Result
where
    W: std::fmt::Write,
    V: Display,
{
    let mut s = "";
    for v in vec.iter() {
        write!(f, "{}{}", s, v)?;
        s = sep;
    }
    Ok(())
}

////////////////////////////////////////////////////////////////////////
//
//  errors
//

type HttpResult = Result<(), HttpStatus>;

#[derive(Clone, Debug, Eq, PartialEq)]
enum HttpStatus {
    Ok,
    Permanent(String),
    Temporary(String),
    BadRequest,
    NotFound,
    BadMethod,
    Failed,
}

impl HttpStatus {
    fn status(&self) -> u16 {
        match self {
            HttpStatus::Ok => 200,
            HttpStatus::Permanent(_) => 301,
            HttpStatus::Temporary(_) => 302,
            HttpStatus::BadRequest => 400,
            HttpStatus::NotFound => 404,
            HttpStatus::BadMethod => 405,
            HttpStatus::Failed => 500,
        }
    }

    fn message(&self) -> &'static str {
        match self {
            HttpStatus::Ok => "ok",
            HttpStatus::Permanent(_) => "found",
            HttpStatus::Temporary(_) => "found",
            HttpStatus::BadRequest => "bad request",
            HttpStatus::NotFound => "not found",
            HttpStatus::BadMethod => "method not allowed",
            HttpStatus::Failed => "internal server error",
        }
    }

    fn location(&self) -> String {
        match self {
            HttpStatus::Permanent(loc) => format!("Location: {loc}\n"),
            HttpStatus::Temporary(loc) => format!("Location: {loc}\n"),
            _ => String::new(),
        }
    }

    fn headers(&self, mime: MimeType) {
        let location = self.location();
        print!(
            "Status: {self}\n\
             {location}\
             Content-Type: text/{mime}\n\
             X-Clacks-Overhead: GNU Terry Pratchett\n\
             \n"
        );
    }
}

impl Display for HttpStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {}", self.status(), self.message())
    }
}

impl From<anyhow::Error> for HttpStatus {
    fn from(err: anyhow::Error) -> HttpStatus {
        for cause in err.chain() {
            eprintln!("cgi-links: {}", cause);
        }
        HttpStatus::Failed
    }
}

impl std::error::Error for HttpStatus {}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum HttpMethod {
    Get,
    Head,
}

impl HttpMethod {
    fn response(self, mime: MimeType, body: String) {
        HttpStatus::Ok.headers(mime);
        if self != HttpMethod::Head {
            print!("{body}");
        }
    }
}

////////////////////////////////////////////////////////////////////////
//
//  link targets
//

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum MimeType {
    Html,
    Atom,
    Plain,
}

impl MimeType {
    fn as_str(&self) -> &'static str {
        match self {
            MimeType::Atom => "atom",
            MimeType::Html => "html",
            MimeType::Plain => "plain",
        }
    }

    fn from_str(text: &str) -> Result<MimeType, HttpStatus> {
        match text {
            ".atom" | "atom" => Ok(MimeType::Atom),
            ".html" | "html" => Ok(MimeType::Html),
            _ => Err(HttpStatus::NotFound),
        }
    }
}

impl Display for MimeType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum LinkTo {
    Short,
    Long,
    Landing(MimeType),
}

impl Display for LinkTo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let text = match self {
            LinkTo::Short => "short",
            LinkTo::Long => "long",
            LinkTo::Landing(mime) => mime.as_str(),
        };
        write!(f, "link={}", text)
    }
}

impl LinkTo {
    fn url(&self, short: &str, long: &str) -> String {
        match self {
            LinkTo::Short => short.to_owned(),
            LinkTo::Long => long.to_owned(),
            LinkTo::Landing(mime) => format!("{short}.{mime}"),
        }
    }
}

////////////////////////////////////////////////////////////////////////
//
//  read links
//

#[derive(Clone, Serialize)]
struct Link {
    text: String,
    long: String,
    short: String,
    tag: String,
    url: String,
    date: String,
    date_time: String,
    updated: String,
}

impl Link {
    fn new(tag: &str, raw: &RawLink, linkto: LinkTo) -> Link {
        let tag = tag.to_owned();
        let text = raw.text.clone();
        let long = raw.link.clone();
        let short = format!("{}/{}", URL, tag);
        let url = linkto.url(&short, &long);
        // non-breaking hyphen U+2011
        let date = raw.date[..10].replace('-', "‑");
        let date_time = raw.date.clone();
        let updated = raw.updated.clone().unwrap_or_default();
        Link { text, long, short, url, tag, date, date_time, updated }
    }

    fn from_map((tag, raw): (&String, &RawLink), linkto: LinkTo) -> Link {
        Self::new(tag, raw, linkto)
    }
}

fn cook_links(linkto: LinkTo, raw: TagsLinks) -> Vec<Link> {
    let mut links: Vec<Link> =
        raw.iter().map(|raw| Link::from_map(raw, linkto)).collect();
    links.sort_by(|a, b| b.date_time.cmp(&a.date_time));
    links
}

////////////////////////////////////////////////////////////////////////
//
//  full-text search
//

#[derive(Clone, Debug)]
struct Search {
    res: Vec<regex::Regex>,
    text: String,
    query: String,
}

#[derive(Clone, Serialize)]
struct LinkMatch {
    url: String,
    text: String,
    long: String,
    short: String,
    date: String,
}

impl Search {
    fn from_query(input: &str) -> Result<Option<Search>> {
        let mut res = Vec::new();
        let mut text = String::new();
        let mut query = String::new();
        let words: Vec<&str> =
            regex!(r"\W+").split(input).filter(|s| !s.is_empty()).collect();
        if words.is_empty() {
            return Ok(None);
        }
        for word in &words {
            res.push(regex::Regex::new(&format!("(?i)({word})"))?);
        }
        join(&mut text, &words, " ")?;
        join(&mut query, &words, "+")?;
        Ok(Some(Search { res, text, query }))
    }

    fn match_links(&self, links: &[Link]) -> Vec<LinkMatch> {
        let mut found = vec![];
        'next: for link in links {
            let mut text = link.text.clone();
            let mut url = link.long.clone();
            for re in &self.res {
                let btext = re.replace_all(&text, "<b>$1</b>").into_owned();
                let burl = re.replace_all(&url, "<b>$1</b>").into_owned();
                if (&text, &url) != (&btext, &burl) {
                    (text, url) = (btext, burl);
                } else {
                    continue 'next;
                }
            }
            let long = link.long.clone();
            let short = format!("{}.html", link.short);
            let date = link.date.clone();
            found.push(LinkMatch { text, url, long, short, date })
        }
        found
    }
}

////////////////////////////////////////////////////////////////////////
//
//  parse and print path info
//

#[derive(Clone, Debug, Eq, PartialEq)]
enum PathInfo {
    EBooks,
    Feed(MimeType),
    Landing(String, MimeType),
    Redirect(String),
}

impl PathInfo {
    fn new(path_info: String) -> Result<PathInfo, HttpStatus> {
        use PathInfo::*;
        if path_info.is_empty() || path_info == "/" {
            return Ok(Feed(MimeType::Html));
        }
        let caps = regex!(r"^/([A-Za-z0-9]+)(|\.atom|\.html)$")
            .captures(&path_info)
            .ok_or(HttpStatus::NotFound)?;
        let tag = match &caps[1] {
            // mistakes
            "LZ7HR" => "PMV75",
            "GWZK6" => "WD6ZL",
            "PUKED" => "P5K7D",
            "ZVNFZVJHTZC268B" => "C268B",
            tag => tag,
        };
        match (tag, &caps[2]) {
            ("ebooks", "") => Ok(EBooks),
            ("feed", ext) => Ok(Feed(MimeType::from_str(ext)?)),
            (tag, "") => Ok(Redirect(tag.to_string())),
            (tag, ext) => {
                Ok(Landing(tag.to_string(), MimeType::from_str(ext)?))
            }
        }
    }
}

impl Display for PathInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PathInfo::EBooks => write!(f, "/ebooks"),
            PathInfo::Feed(mime) => write!(f, "/feed.{mime}"),
            PathInfo::Landing(tag, mime) => write!(f, "/{tag}.{mime}"),
            PathInfo::Redirect(tag) => write!(f, "/{tag}"),
        }
    }
}

////////////////////////////////////////////////////////////////////////
//
//  parse and print query
//

const LIMIT: usize = 1234;
const UNLIMITED: usize = 123456;

#[derive(Clone, Debug)]
struct Query {
    method: HttpMethod,
    path: PathInfo,
    year: String,
    linkto: LinkTo,
    limit: usize,
    search: Option<Search>,
}

impl Query {
    fn new_from_cgi() -> Result<Query, HttpStatus> {
        let path = PathInfo::new(getenv("PATH_INFO"))?;
        let query = getenv("QUERY_STRING");

        let method = match getenv("REQUEST_METHOD").as_str() {
            "HEAD" => HttpMethod::Head,
            "GET" | "" => HttpMethod::Get,
            _ => return Err(HttpStatus::BadMethod),
        };

        match getenv("SERVER_PORT").as_ref() {
            "443" | "" => (),
            _ => {
                let sep = if query == "" { "" } else { "?" };
                let loc = format!("{URL}{path}{sep}{query}");
                return Err(HttpStatus::Permanent(loc));
            }
        }

        use LinkTo::*;
        use MimeType::*;
        let mut linkto = Long;
        let mut limit = LIMIT;
        let mut year = String::new();
        let mut search = None;

        for kv in regex!(r"[;&]+").split(&query) {
            if let Some(caps) = regex!(r"^([a-z]+)=(.*)$").captures(kv) {
                match (&caps[1], &caps[2]) {
                    ("link", "atom") => linkto = Landing(Atom),
                    ("link", "html") => linkto = Landing(Html),
                    ("link", "land") => linkto = Landing(Html), // back compat
                    ("link", "long") => linkto = Long,
                    ("link", "short") => linkto = Short,
                    ("link", _) => return Err(HttpStatus::BadRequest),
                    ("limit", n) => match usize::from_str(n) {
                        Ok(n) => limit = n,
                        _ => return Err(HttpStatus::BadRequest),
                    },
                    ("q", words) => {
                        search = Search::from_query(words)?;
                    }
                    _ => return Err(HttpStatus::BadRequest),
                }
                continue;
            }
            if let Some(caps) =
                regex!(r"^(20\d\d)(-\d\d)?(-\d\d)?$").captures(kv)
            {
                year.clear();
                year.push_str(&caps[1]);
                limit = UNLIMITED;
                continue;
            }
            // kv is empty when query string is empty
            if !kv.is_empty() {
                return Err(HttpStatus::BadRequest);
            }
        }

        match path {
            PathInfo::Feed(Html) => {
                Ok(Query { method, path, year, linkto, limit, search })
            }
            PathInfo::Feed(Atom) => {
                if search.is_none() {
                    Ok(Query { method, path, year, linkto, limit, search })
                } else {
                    Err(HttpStatus::BadRequest)
                }
            }
            _ => {
                if search.is_none() && year.is_empty() && linkto == Long {
                    Ok(Query { method, path, year, linkto, limit, search })
                } else {
                    Err(HttpStatus::BadRequest)
                }
            }
        }
    }

    fn match_year(&self, links: &[Link]) -> Vec<Link> {
        let mut found = vec![];
        for link in links {
            if link.date.starts_with(&self.year) {
                found.push(link.clone())
            }
        }
        found
    }

    fn with_year(&self, year: &str) -> Query {
        let method = HttpMethod::Get;
        let path = PathInfo::Feed(MimeType::Html);
        let year = year.to_string();
        let linkto = self.linkto;
        let limit = self.limit;
        let search = None;
        Query { method, path, year, linkto, limit, search }
    }

    fn year_list(&self, links: &[Link]) -> String {
        let mut years = BTreeMap::new();
        for link in links {
            years.insert(link.date[..4].to_string(), ());
        }
        let mut html = "<p>".to_string();
        for year in years.keys() {
            if *year == self.year {
                write!(html, " {year}").unwrap();
            } else {
                let query = self.with_year(year);
                write!(html, " <a href='{query}'>{year}</a>").unwrap();
            }
        }
        html += " </p>";
        html
    }

    fn search_box(&self) -> String {
        if let Some(search) = &self.search {
            search.text.clone()
        } else {
            String::new()
        }
    }
}

impl Display for Query {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut sep = "?";
        let default = self.path == PathInfo::Feed(MimeType::Html);
        if default {
            sep = HTML_FEED_URL;
        } else {
            write!(f, "{}{}", URL, self.path)?;
        }
        if !self.year.is_empty() {
            write!(f, "{}{}", sep, self.year)?;
            sep = "&";
        }
        if self.limit != LIMIT {
            write!(f, "{}limit={}", sep, self.limit)?;
            sep = "&";
        }
        if self.linkto != LinkTo::Long {
            write!(f, "{}{}", sep, self.linkto)?;
            sep = "&";
        }
        if let Some(search) = &self.search {
            write!(f, "{}q={}", sep, search.query)?;
            sep = "&";
        }
        if default && sep != "&" {
            write!(f, "{URL}/")?;
        }
        Ok(())
    }
}

////////////////////////////////////////////////////////////////////////
//
//  main
//

#[derive(Clone, Serialize)]
struct SiteGlobal {
    stylesheet: String,
    what_ident: &'static str,
}

#[derive(Clone, Serialize)]
struct PageCommon {
    global: SiteGlobal,
    metadata: Titles,
    search_box: String,
    year_list: String,
}

#[derive(Clone, Serialize)]
struct ListPage {
    #[serde(flatten)]
    common: PageCommon,
    updated: String,
    entry: Vec<Link>,
    debug: String,
}

#[derive(Clone, Serialize)]
struct SearchPage {
    #[serde(flatten)]
    common: PageCommon,
    found: Vec<LinkMatch>,
    debug: String,
}

#[derive(Clone, Serialize)]
struct LandingPage {
    #[serde(flatten)]
    common: PageCommon,
    link: Link,
}

fn render_feed(
    stylesheet: String,
    hbs: Handlebars,
    links: TagsLinks,
    query: &Query,
    mime: MimeType,
) -> HttpResult {
    let links = cook_links(query.linkto, links);
    let global = SiteGlobal { stylesheet, what_ident: WHAT_IDENT };
    let metadata = Titles::new("Tony Finch &ndash; link log");
    let year_list = query.year_list(&links);
    let search_box = query.search_box();
    let common = PageCommon { global, metadata, search_box, year_list };
    let debug = format!("{query}\n\n{query:#?}");
    let mut entry = query.match_year(&links);
    if let Some(search) = &query.search {
        let found = search.match_links(&entry);
        let data = SearchPage { common, found, debug };
        let body = hbs.render_to_string("links-searched", &data)?;
        query.method.response(mime, body);
    } else {
        entry.truncate(query.limit);
        let mut updated = "";
        for e in &entry {
            if updated < e.updated.as_str() {
                updated = &e.updated;
            }
            if updated < e.date_time.as_str() {
                updated = &e.date_time;
            }
        }
        let updated = updated.to_string();
        let data = ListPage { common, updated, entry, debug };
        let template = format!("links-{mime}-feed");
        let body = hbs.render_to_string(&template, &data)?;
        query.method.response(mime, body);
    }
    Ok(())
}

fn render_one(
    stylesheet: String,
    hbs: Handlebars,
    links: TagsLinks,
    query: &Query,
    tag: &str,
    mime: MimeType,
) -> HttpResult {
    let link = links.get(tag).ok_or(HttpStatus::NotFound)?;
    let link = Link::new(tag, link, query.linkto);
    let links = cook_links(query.linkto, links);
    let global = SiteGlobal { stylesheet, what_ident: WHAT_IDENT };
    let metadata = Titles::new("Tony Finch &ndash; link log");
    let year_list = query.year_list(&links);
    let search_box = String::new();
    let common = PageCommon { global, metadata, search_box, year_list };
    let data = LandingPage { common, link };
    let template = format!("links-{mime}-one");
    let body = hbs.render_to_string(&template, &data)?;
    query.method.response(mime, body);
    Ok(())
}

fn do_it() -> HttpResult {
    let stylesheet = read_stylesheet()?;
    let hbs = read_all_handlebars()?;
    let links = read_yaml("links.yml")?;
    let query = Query::new_from_cgi()?;

    use PathInfo::*;
    match &query.path {
        Feed(mime) => render_feed(stylesheet, hbs, links, &query, *mime),
        Landing(tag, mime) => {
            render_one(stylesheet, hbs, links, &query, tag, *mime)
        }
        Redirect(tag) => {
            let link = links.get(tag).ok_or(HttpStatus::NotFound)?;
            Err(HttpStatus::Temporary(link.link.clone()))
        }
        EBooks => Err(HttpStatus::NotFound),
    }
}

fn main() -> Result<()> {
    if let Err(status) = do_it() {
        status.headers(MimeType::Plain);
        if getenv("REQUEST_METHOD") != "HEAD" {
            let location = status.location();
            print!("{status}\n{location}");
        }
    }
    Ok(())
}

////////////////////////////////////////////////////////////////////////
